﻿using System;
using LendFoundry.Foundation.Services.Settings;

namespace LendFoundry.Syndication.Equifax.Api
{
    /// <summary>
    /// Settings
    /// </summary>
    public class Settings
    {
      /// <summary>
      /// ServiceName
      /// </summary>
      /// <returns></returns>
      public static string ServiceName => Environment.GetEnvironmentVariable($"CONFIGURATION_NAME") ?? "equifax";
    }
}