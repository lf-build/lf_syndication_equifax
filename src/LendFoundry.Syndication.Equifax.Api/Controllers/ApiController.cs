﻿using LendFoundry.Foundation.Services;
#if DOTNET2
using Microsoft.AspNetCore.Mvc;
#else
using Microsoft.AspNet.Mvc;
#endif
namespace LendFoundry.Syndication.Equifax.Api.Controllers
{
    /// <summary>
    /// ApiController
    /// </summary>
    [Route("/")]
    public class ApiController : ExtendedController
    {
        /// <summary>
        /// ApiController constructor
        /// </summary>
        /// <param name="service"></param>
        public ApiController(IEquifaxCreditReportService service)
        {
            Service = service;
        }

        private IEquifaxCreditReportService Service { get; }

        /// <summary>
        /// Creates the report.
        /// </summary>
        /// <param name="entityType"></param>
        /// <param name="entityId"></param>
        /// <param name="creditReportRequest"></param>
        /// <returns></returns>
        [HttpPost("/{entityType}/{entityId}/creditreport")]
        [Consumes("application/json")]
        [ProducesResponseType(typeof(ICreditReport), 200)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
        public IActionResult CreateReport(string entityType,string entityId,[FromBody]CreditReportRequest creditReportRequest)
        {
            return Execute(() =>
            {
                var creditReport = Service.CreateReport(entityType, entityId, creditReportRequest);
                return Ok(creditReport);
            });
        }

        /// <summary>
        /// Gets the report.
        /// </summary>
        /// <param name="entityType">Type of the entity</param>
        /// <param name="entityId">The entity identifier</param>
        /// <param name="ssn">The SSN</param>
        /// <param name="reportId">The report identifier</param>
        /// <returns></returns>
        [HttpGet("/{entityType}/{entityId}/{ssn}/{reportId}")]
        [Consumes("application/json")]
        [ProducesResponseType(typeof(ICreditReport), 200)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
        public IActionResult GetReport(string entityType, string entityId, string ssn, string reportId)
        {
            return Execute(() =>
            {
                var entries = Service.GetReport(entityType, entityId, new CreditReportRequestKey { Ssn = ssn }, reportId);
                return Ok(entries);
            });
        }

        /// <summary>
        /// Gets the reports.
        /// </summary>
        /// <param name="entityType">Type of the entity</param>
        /// <param name="entityId">The entity identifier</param>
        /// <param name="ssn">The SSN</param>
        /// <param name="skip">The skip</param>
        /// <param name="take">The take</param>
        /// <returns></returns>
        [HttpGet("/{entityType}/{entityId}/{ssn}/{skip?}/{take?}")]
        [Consumes("application/json")]
  
        [ProducesResponseType(typeof(ICreditReport), 200)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
        public IActionResult GetReports(string entityType, string entityId, string ssn, int? skip = default(int?), int? take = default(int?))
        {
            return Execute(() =>
            {
                var entries = Service.GetReports(entityType, entityId, new CreditReportRequestKey { Ssn = ssn }, skip, take);
                return Ok(entries);
            });
        }

        /// <summary>
        /// Gets the reports.
        /// </summary>
        /// <param name="entityType">Type of the entity</param>
        /// <param name="entityId">The entity identifier</param>
        /// <param name="key">Credit Report Request parameter</param>
        /// <param name="skip">The skip</param>
        /// <param name="take">The take</param>
        /// <returns></returns>
        [HttpPost("/{entityType}/{entityId}/{skip?}/{take?}")]
        [Consumes("application/json")]
        [ProducesResponseType(typeof(ICreditReport), 200)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
        public IActionResult GetReports(string entityType, string entityId, [FromBody]CreditReportRequestKey key, int? skip = default(int?), int? take = default(int?))
        {
            return Execute(() =>
            {
                var entries = Service.GetReports(entityType, entityId, key, skip, take);
                return Ok(entries);
            });
        }

        /// <summary>
        /// Gets the report.
        /// </summary>
        /// <param name="entityType">Type of the entity.</param>
        /// <param name="entityId">The entity identifier.</param>
        /// <param name="reportId">The report identifier.</param>
        /// <param name="key">Credit Report Request parameter</param>
        /// <returns></returns>
        [HttpPost("/{entityType}/{entityId}/{reportId}")]
        [Consumes("application/json")]
        [ProducesResponseType(typeof(ICreditReport), 200)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
        public IActionResult GetReport(string entityType, string entityId, string reportId, [FromBody]CreditReportRequestKey key)
        {
            return Execute(() =>
            {
                var entries = Service.GetReport(entityType, entityId, key, reportId);
                return Ok(entries);
            });
        }

    }
}