﻿using LendFoundry.Syndication.Equifax.CreditReportResponse;
using System.Collections.Generic;

namespace LendFoundry.Syndication.Equifax.Response
{
    public interface IGetCreditReportResponse
    {
        // Todo : Refactor this
        List<IConsumerCreditReport> ConsumerCreditReport { get; set; }
    }
}