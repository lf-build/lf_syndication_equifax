﻿namespace LendFoundry.Syndication.Equifax.Response
{
    public interface IIncome
    {
        double? RateOfPay { get; set; }
        string PayFrequency { get; set; }
        int? AverageHoursWorked { get; set; }
        double? CalculatedAnnualIncome { get; set; }
    }
}