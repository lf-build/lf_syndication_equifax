﻿using System.Collections.Generic;

namespace LendFoundry.Syndication.Equifax.Response
{
    public interface IEmploymentDetailResponse
    {
        List<IAnnualCompensation> AnnualCompensations { get; set; }
        IEmployee Employee { get; set; }
        IEmployer Employer { get; set; }
        IIncome Income { get; set; }
    }
}