﻿using System;
using System.Linq;
using LendFoundry.Syndication.Equifax.Proxy.WorkNumberRequest;

namespace LendFoundry.Syndication.Equifax.Response
{
    public class Employee : IEmployee
    {
        public Employee()
        {

        }

        public Employee(TSVEMPLOYEE_V100 tsvemployeeV100)
        {
            if (tsvemployeeV100 == null)
                return;

            if (tsvemployeeV100.SSN?.Text != null) Ssn = tsvemployeeV100.SSN.Text.FirstOrDefault();

            if (tsvemployeeV100.FIRSTNAME?.Text != null)
                FirstName = tsvemployeeV100.FIRSTNAME.Text.FirstOrDefault();

            if (tsvemployeeV100.LASTNAME?.Text != null)
                LastName = tsvemployeeV100.LASTNAME.Text.FirstOrDefault();

            var employmentLengthOfService = tsvemployeeV100.TOTALLENGTHOFSVC?.Text?.FirstOrDefault();

            if (!string.IsNullOrWhiteSpace(employmentLengthOfService))
                EmploymentLengthOfService = Convert.ToInt32(employmentLengthOfService);

            if (tsvemployeeV100.POSITIONTITLE?.Text != null)
                PositionTitle = tsvemployeeV100.POSITIONTITLE.Text.FirstOrDefault();

            if (tsvemployeeV100.EMPLOYEESTATUS?.MESSAGE?.Text != null)
                EmploymentStatus = tsvemployeeV100.EMPLOYEESTATUS.MESSAGE.Text.FirstOrDefault();

            var employmentStartDate = tsvemployeeV100.DTMOSTRECENTHIRE?.Text?.FirstOrDefault();
            if (!string.IsNullOrWhiteSpace(employmentStartDate))
            {
                try
                {
                    var year = Convert.ToInt32(employmentStartDate.Substring(0, 4));
                    var month = Convert.ToInt32(employmentStartDate.Substring(4, 2));
                    var day = Convert.ToInt32(employmentStartDate.Substring(6, 2));

                    EmploymentStartDate = new DateTimeOffset(new DateTime(year, month, day));
                }
                catch (Exception)
                {
                    //TODO:Ignore if invalid date found?
                }
            }

            var employmentTerminationDate = tsvemployeeV100.DTENDEMPLOYMENT?.Text?.FirstOrDefault();
            if (!string.IsNullOrWhiteSpace(employmentTerminationDate))
            {
                try
                {
                    var year = Convert.ToInt32(employmentTerminationDate.Substring(0, 4));
                    var month = Convert.ToInt32(employmentTerminationDate.Substring(4, 2));
                    var day = Convert.ToInt32(employmentTerminationDate.Substring(6, 2));

                    EmploymentTerminationDate = new DateTimeOffset(new DateTime(year, month, day));
                }
                catch (Exception)
                {
                    //TODO:Ignore if invalid date found?
                }
            }

            var asOnDate = tsvemployeeV100.DTINFO?.Text?.FirstOrDefault();
            if (!string.IsNullOrWhiteSpace(asOnDate))
            {
                try
                {
                    var year = Convert.ToInt32(asOnDate.Substring(0, 4));
                    var month = Convert.ToInt32(asOnDate.Substring(4, 2));
                    var day = Convert.ToInt32(asOnDate.Substring(6, 2));

                    AsOnDate = new DateTimeOffset(new DateTime(year, month, day));
                }
                catch (Exception)
                {
                    //TODO:Ignore if invalid date found?
                }
            }
            
            
        }

        public string Ssn { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public int? EmploymentLengthOfService { get; set; }
        public string PositionTitle { get; set; }
        public string EmploymentStatus { get; set; }
        public DateTimeOffset? EmploymentStartDate { get; set; }
        public DateTimeOffset? EmploymentTerminationDate { get; set; }
        public DateTimeOffset? AsOnDate { get; set; }
    }
}