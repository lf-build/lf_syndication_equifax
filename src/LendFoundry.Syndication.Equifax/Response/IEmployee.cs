﻿using System;

namespace LendFoundry.Syndication.Equifax.Response
{
    public interface IEmployee
    {
        string FirstName { get; set; }
        string LastName { get; set; }
        string Ssn { get; set; }

        string EmploymentStatus { get; set; }
        DateTimeOffset? EmploymentStartDate { get; set; }
        DateTimeOffset? EmploymentTerminationDate { get; set; }
        int? EmploymentLengthOfService { get; set; }
        string PositionTitle { get; set; }
        DateTimeOffset? AsOnDate { get; set; }
    }
}