﻿namespace LendFoundry.Syndication.Equifax.Response
{
    public interface IAnnualCompensation
    {
        double Total { get; set; }
        int Year { get; set; }
    }
}