﻿using System;
using LendFoundry.Syndication.Equifax.Proxy.WorkNumberRequest;
using System.Linq;

namespace LendFoundry.Syndication.Equifax.Response
{
    public class Income : IIncome
    {

        public Income()
        {
            
        }

        public Income(TSVBASECOMP tsvBaseComp, TSVRESPONSE_V100 tsvresponse)
        {
            if (tsvBaseComp != null)
            {
                var rateOfPay = tsvBaseComp.TSVRATEOFPAY?.Text?.FirstOrDefault();
                if (rateOfPay != null)
                    RateOfPay = Convert.ToDouble(rateOfPay);

                if (tsvBaseComp.TSVPAYFREQUENCY?.MESSAGE?.Text != null)
                    PayFrequency = tsvBaseComp.TSVPAYFREQUENCY.MESSAGE.Text.FirstOrDefault();

                var averageHoursWorked = tsvBaseComp.TSVAVGHRSWORKED?.Text?.FirstOrDefault();
                if (averageHoursWorked != null)
                    AverageHoursWorked = Convert.ToInt32(averageHoursWorked);

            }

            var calculatedAnnualIncome = tsvresponse?.TSVPROJINCOME?.Text?.FirstOrDefault();
            if (calculatedAnnualIncome != null)
                CalculatedAnnualIncome = Convert.ToDouble(calculatedAnnualIncome);
        }

        public double? RateOfPay { get; set; }
        public string PayFrequency { get; set; }
        public int? AverageHoursWorked { get; set; }
        public double? CalculatedAnnualIncome { get; set; }
    }
}