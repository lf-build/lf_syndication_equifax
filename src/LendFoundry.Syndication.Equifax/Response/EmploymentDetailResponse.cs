﻿using System;
using System.Collections.Generic;
using System.Linq;
using LendFoundry.Syndication.Equifax.Proxy.WorkNumberRequest;

namespace LendFoundry.Syndication.Equifax.Response
{
    public class EmploymentDetailResponse : IEmploymentDetailResponse
    {

        public EmploymentDetailResponse()
        {
            
        }

        public EmploymentDetailResponse(TSVRESPONSE_V100 tsvresponseV100)
        {
            if (tsvresponseV100.TSVEMPLOYEE_V100 != null)
                Employee = new Employee(tsvresponseV100.TSVEMPLOYEE_V100);
            if (tsvresponseV100.TSVEMPLOYER_V100 != null)
                Employer = new Employer(tsvresponseV100.TSVEMPLOYER_V100);

            if (tsvresponseV100.TSVBASECOMP != null)
                Income = new Income(tsvresponseV100.TSVBASECOMP, tsvresponseV100);

            AnnualCompensations = new List<IAnnualCompensation>();

            foreach (var tsvannualcomp in tsvresponseV100.TSVANNUALCOMP)
            {
                if (tsvannualcomp.TSVYEAR?.Text != null && tsvannualcomp.TSVTOTAL?.Text!=null)
                {
                    AnnualCompensations.Add(new AnnualCompensation()
                    {
                        Total = Convert.ToDouble(tsvannualcomp.TSVTOTAL.Text.FirstOrDefault()),
                        Year = Convert.ToInt32(tsvannualcomp.TSVYEAR.Text.FirstOrDefault())
                    });
                }
            }
        }

        public IEmployer Employer
        {
            get;
            set;
        }

        public IIncome Income { get; set; }

        public IEmployee Employee
        {
            get;
            set;
        }

        public List<IAnnualCompensation> AnnualCompensations { get; set; }
    }
}