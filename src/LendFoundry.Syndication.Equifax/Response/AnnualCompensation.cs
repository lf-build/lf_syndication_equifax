namespace LendFoundry.Syndication.Equifax.Response
{
    public class AnnualCompensation :IAnnualCompensation
    {
        public int Year { get; set; }
        public double Total { get; set; }
    }
}