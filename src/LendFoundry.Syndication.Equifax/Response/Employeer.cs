﻿using System.Linq;
using LendFoundry.Syndication.Equifax.Proxy.WorkNumberRequest;

namespace LendFoundry.Syndication.Equifax.Response
{
    public class Employer : IEmployer
    {
        public Employer()
        {
        }

        public Employer(TSVEMPLOYER_V100 tsvemployerV100)
        {
            if (tsvemployerV100 == null) 
                return;

            if (tsvemployerV100.EMPLOYERCODE?.Text != null)
                Code = tsvemployerV100.EMPLOYERCODE.Text.FirstOrDefault();
            if (tsvemployerV100.NAME1?.Text != null )
                Name = tsvemployerV100.NAME1.Text.FirstOrDefault();

            Disclaimer = tsvemployerV100.DISCLAIMER?.FirstOrDefault()?.Text?.FirstOrDefault();
        }

        public string Code { get; set; }
        public string Name { get; set; }
        public string Disclaimer { get; set; }
    }
}