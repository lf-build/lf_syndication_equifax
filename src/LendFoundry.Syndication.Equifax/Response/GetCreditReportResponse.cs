﻿using LendFoundry.Syndication.Equifax.CreditReportResponse;
using System.Collections.Generic;
using System.Linq;

namespace LendFoundry.Syndication.Equifax.Response
{
    public class GetCreditReportResponse : IGetCreditReportResponse
    {
        public GetCreditReportResponse(Proxy.CreditReportResponse.EfxTransmit efxTransmit)
        {
            if (efxTransmit?.EfxReport != null)
                ConsumerCreditReport =
                    efxTransmit.EfxReport.Select(p => new ConsumerCreditReport(p)).ToList<IConsumerCreditReport>();
        }

        public List<IConsumerCreditReport> ConsumerCreditReport { get; set; }
    }
}