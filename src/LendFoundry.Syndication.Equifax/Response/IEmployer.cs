﻿namespace LendFoundry.Syndication.Equifax.Response
{
    public interface IEmployer
    {
        string Code { get; set; }
        string Name { get; set; }
        string Disclaimer { get; set; }
    }
}