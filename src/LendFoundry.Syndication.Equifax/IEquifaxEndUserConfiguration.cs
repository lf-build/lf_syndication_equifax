﻿namespace LendFoundry.Syndication.Equifax
{
    public interface IEquifaxEndUserConfiguration
    {
        string UserName { get; set; }
        string PermissiblePurposeCode { get; set; }
    }
}
