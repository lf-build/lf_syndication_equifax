namespace LendFoundry.Syndication.Equifax.CreditReportResponse
{
    public class MoneyType : IMoneyType
    {
        public MoneyType()
        {
            Currency = CurrencyType.USD;
        }

        public MoneyType(Proxy.CreditReportResponse.MoneyType moneyType)
        {
            if (moneyType != null)
            {
                Currency = (CurrencyType)moneyType.currency;
                Value = moneyType.Value;
            }
        }

        public CurrencyType Currency { get; set; }

        public long Value { get; set; }
    }
}