using System.Collections.Generic;
using System.Linq;

namespace LendFoundry.Syndication.Equifax.CreditReportResponse
{
    public class UsficoType : IUsficoType
    {
        public UsficoType(Proxy.CreditReportResponse.USFICOType usficoType)
        {
            if (usficoType != null)
            {
                FicoScore = usficoType.FICOScore;
                if (usficoType.ScoreReasons != null)
                    ScoreReasons = usficoType.ScoreReasons.Select(p => new ScoreReasonType(
                        new Proxy.CreditReportResponse.CodeType
                        {
                            code = p.code,
                            description = p.description
                        },
                        p)).ToList<IScoreReasonType>();

                if (usficoType.RejectCode != null)
                    RejectCode = new CodeType(usficoType.RejectCode);

                if (usficoType.RiskBasedPricing != null)
                    RiskBasedPricing = usficoType.RiskBasedPricing;

                if (usficoType.DoddFrank != null)
                    DoddFrank = usficoType.DoddFrank;

                //TODO: Changed FICOIndicator to FICOScore
                if (usficoType.FICOScore != null)
                    FicoIndicator = usficoType.FICOScore;

                FactActInquiriesAreKeyFactor = usficoType.FACTActInquiriesAreKeyFactor;
            }
        }

        public string FicoScore { get; set; }

        public List<IScoreReasonType> ScoreReasons { get; set; }

        public ICodeType RejectCode { get; set; }

        public object RiskBasedPricing { get; set; }

        public object DoddFrank { get; set; }

        //TODO: Changed to strng
        public string FicoIndicator { get; set; }

        public string FactActInquiriesAreKeyFactor { get; set; }
    }
}