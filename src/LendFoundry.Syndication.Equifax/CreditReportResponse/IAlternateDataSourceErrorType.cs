﻿using System.Collections.Generic;

namespace LendFoundry.Syndication.Equifax.CreditReportResponse
{
    public interface IAlternateDataSourceErrorType
    {
        string CustomerReferenceNumber{get;set;}
        string CustomerNumber{get;set;}
        ICodeType DataSourceCode{get;set;}
        List<IDerrType> DerrErrors{get;set;}
    }
}
