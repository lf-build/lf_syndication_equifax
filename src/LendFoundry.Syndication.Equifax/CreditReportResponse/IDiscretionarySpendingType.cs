﻿namespace LendFoundry.Syndication.Equifax.CreditReportResponse
{
    public interface IDiscretionarySpendingType
    {
        string ProductId { get; set; }
        string PostalCode { get; set; }
        string PostalCodePlus4 { get; set; }
        short Age { get; set; }
        bool AgeSpecified { get; set; }
        string AgeBand { get; set; }
        IMoneyType IncomeofConsumer { get; set; }
        IMoneyType Income { get; set; }
        IMoneyType SpendingIndex { get; set; }
        IMoneyType SpendingMassMarket { get; set; }
        string DispositionCode { get; set; }
    }
}
