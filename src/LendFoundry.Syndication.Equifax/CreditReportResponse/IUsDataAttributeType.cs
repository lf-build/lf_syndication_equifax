﻿using System.Collections.Generic;

namespace LendFoundry.Syndication.Equifax.CreditReportResponse
{
    public interface IUsDataAttributeType
    {
        List<IIdAndValueType> DataAttribute { get; set; }
        string ModelNumber { get; set; }
    }
}