namespace LendFoundry.Syndication.Equifax.CreditReportResponse
{
    public interface IUsFileIdType
    {
        string FileIdNumber { get; set; }
    }
}