﻿using System.Collections.Generic;

namespace LendFoundry.Syndication.Equifax.CreditReportResponse
{
    public interface IUsTradeType
    {
        ICodeType AccountDesignator { get; set; }
        string AccountNumber { get; set; }
        ICodeType AccountType { get; set; }
        ICodeType ActivityDesignator { get; set; }
        IMoneyType ActualPaymentAmount { get; set; }
        IMoneyType BalanceAmount { get; set; }
        IMoneyType BalloonPaymentAmount { get; set; }
        IDateType BalloonPaymentDueDate { get; set; }
        IMoneyType CreditLimit { get; set; }
        ICodeType CreditorClassification { get; set; }
        ICustomerIdType CreditorId { get; set; }
        IDateType DateClosed { get; set; }
        IDateType DateMajorDelinquencyReported { get; set; }
        IDateType DateOfFirstDelinquency { get; set; }
        IDateType DateOfLastActivity { get; set; }
        IDateType DateOfLastPayment { get; set; }
        IDateType DateOpened { get; set; }
        IDateType DateReported { get; set; }
        IDateType DeferredPaymentStartDate { get; set; }
        IMoneyType HighCreditAmount { get; set; }
        IUsHistoryDerogatoryCounters HistoryDerogatoryCounters { get; set; }
        short MonthsReviewed { get; set; }
        string MortgageIdNumber { get; set; }
        List<ICodeType> Narratives { get; set; }
        IMoneyType OriginalChargeOffAmount { get; set; }
        IMoneyType PastDueAmount { get; set; }
        string PaymentHistory { get; set; }
        ICodeType PortfolioType { get; set; }
        IDateType PreviousHighDateOutsideHistory { get; set; }
        List<ICodeDateType> PreviousHighPaymentRates { get; set; }
        ICodeType PreviousHighRateOutsideHistory { get; set; }
        ICodeType PurchasedPortfolioIndicator { get; set; }
        string PurchasedPortfolioName { get; set; }
        IMoneyType ScheduledPaymentAmount { get; set; }
        ICodeType Status { get; set; }
        string TermsDuration { get; set; }
        ICodeType TermsFrequency { get; set; }
        ICodeType UpdateIndicator { get; set; }
    }
}