﻿namespace LendFoundry.Syndication.Equifax.CreditReportResponse
{
    public interface IUsSubjectDeathType
    {
        IDateType DateOfDeath { get; set; }
    }
}