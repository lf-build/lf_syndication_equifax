﻿namespace LendFoundry.Syndication.Equifax.CreditReportResponse
{
    public interface IWealthCompleteType
    {
        string ProductId { get; set; }
        string PostalCode { get; set; }
        string PostalCodePlus4 { get; set; }
        short Age { get; set; }
        bool AgeSpecified { get; set; }
        string AgeBand { get; set; }
        IMoneyType IncomeofConsumer { get; set; }
        IMoneyType Annuities { get; set; }
        IMoneyType Stocks { get; set; }
        IMoneyType Bonds { get; set; }
        IMoneyType Deposits { get; set; }
        IMoneyType MutualFunds { get; set; }
        IMoneyType OtherAssets { get; set; }
        IMoneyType TotalAssets { get; set; }
        IMoneyType CertificateofDeposit { get; set; }
        IMoneyType InterestChecking { get; set; }
        IMoneyType MoneyMarketDepositAcct { get; set; }
        IMoneyType NonInterestChecking { get; set; }
        IMoneyType OtherChecking { get; set; }
        IMoneyType Savings { get; set; }
        string ModelVersion { get; set; }
        string DataVintage { get; set; }
        string DispositionCode { get; set; }
    }
}
