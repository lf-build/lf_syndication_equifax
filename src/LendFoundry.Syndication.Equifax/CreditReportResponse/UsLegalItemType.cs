using System.Collections.Generic;
using System.Linq;

namespace LendFoundry.Syndication.Equifax.CreditReportResponse
{
    public class UsLegalItemType : IUsLegalItemType
    {
        public UsLegalItemType(Proxy.CreditReportResponse.USLegalItemType usLegalItemType)
        {
            if (usLegalItemType != null)
            {
                if (usLegalItemType.DateFiled != null)
                    DateFiled = new DateType(usLegalItemType.DateFiled);
                if (usLegalItemType.CourtId != null)
                    CourtId = new CustomerIdType(usLegalItemType.CourtId);

                CaseNumber = usLegalItemType.CaseNumber;
                if (usLegalItemType.Amount != null)
                    Amount = new MoneyType(usLegalItemType.Amount);

                if (usLegalItemType.DateSatisfied != null)
                    DateSatisfied = new DateType(usLegalItemType.DateSatisfied);

                if (usLegalItemType.Status != null)
                    Status = new CodeType(usLegalItemType.Status);

                if (usLegalItemType.VerificationDate != null)
                    VerificationDate = new DateType(usLegalItemType.VerificationDate);

                Defendant = usLegalItemType.Defendant;
                Plaintiff = usLegalItemType.Plaintiff;

                //TODO: DateReported not found
                //if (usLegalItemType.DateReported != null)
                //    DateReported = new DateType(usLegalItemType.DateReported);

                if (usLegalItemType.Narratives != null)
                    Narratives = usLegalItemType.Narratives.Select(p => new CodeType(p)).ToList<ICodeType>();

                Code = usLegalItemType.code;
                Description = usLegalItemType.description;
            }
        }

        public IDateType DateFiled { get; set; }

        public ICustomerIdType CourtId { get; set; }

        public string CaseNumber { get; set; }

        public IMoneyType Amount { get; set; }

        public IDateType DateSatisfied { get; set; }

        public ICodeType Status { get; set; }

        public IDateType VerificationDate { get; set; }

        public string Defendant { get; set; }

        public string Plaintiff { get; set; }

        public IDateType DateReported { get; set; }

        public List<ICodeType> Narratives { get; set; }

        public string Code { get; set; }

        public string Description { get; set; }
    }
}