﻿namespace LendFoundry.Syndication.Equifax.CreditReportResponse
{
    public class ReasonCodeType : IReasonCodeType
    {
        public ReasonCodeType(Proxy.CreditReportResponse.ReasonCodeType reasonCodeType)
        {
            if (reasonCodeType != null)
            {
                Number = reasonCodeType.number;
                Code = reasonCodeType.code;
                Description = reasonCodeType.description;
            }
        }
        public short Number { get; set; }
        public string Code { get; set; }
        public string Description { get; set; }
    }
}
