﻿namespace LendFoundry.Syndication.Equifax.CreditReportResponse
{
    public interface IUsErrorMessageType
    {
        CodeType Error { get; set; }
    }
}