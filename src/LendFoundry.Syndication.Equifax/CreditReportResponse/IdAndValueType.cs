namespace LendFoundry.Syndication.Equifax.CreditReportResponse
{
    public class IdAndValueType : IIdAndValueType
    {
        public IdAndValueType(Proxy.CreditReportResponse.IdAndValueType idAndValueType)
        {
            if (idAndValueType != null)
            {
                Identifier = idAndValueType.identifier;

                Value = idAndValueType.value;
            }
        }

        public string Identifier { get; set; }

        public string Value { get; set; }
    }
}