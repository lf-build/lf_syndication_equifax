using LendFoundry.Syndication.Equifax.Proxy.CreditReportResponse;

namespace LendFoundry.Syndication.Equifax.CreditReportResponse
{
    public class UsRequest : IUsRequest
    {
        public UsRequest(USHeaderTypeRequest usRequest)
        {
            if (usRequest != null)
            {
                CustomerReferenceNumber = usRequest.CustomerReferenceNumber;
                //TODO: Not found
                //DpmvTransactionNumber = usRequest.DPMVTransactionNumber;
                CustomerNumber = usRequest.CustomerNumber;
                ConsumerReferralCode = usRequest.ConsumerReferralCode;
                EcoaInquiryType = (EcoaInquiryType)usRequest.ECOAInquiryType;
                NumberOfMonthsToCountInquiries = usRequest.NumberOfMonthsToCountInquiries;
            }
        }

        public string CustomerReferenceNumber { get; set; }

        public string DpmvTransactionNumber { get; set; }

        public string CustomerNumber { get; set; }

        public string ConsumerReferralCode { get; set; }

        public EcoaInquiryType EcoaInquiryType { get; set; }

        public string NumberOfMonthsToCountInquiries { get; set; }
    }
}