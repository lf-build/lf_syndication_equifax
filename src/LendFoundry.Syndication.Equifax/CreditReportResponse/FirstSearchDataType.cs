﻿namespace LendFoundry.Syndication.Equifax.CreditReportResponse
{
    public class FirstSearchDataType : IFirstSearchDataType
    {
        public FirstSearchDataType(Proxy.CreditReportResponse.FirstSearchDataType firstSearchDataType)
        {
            if (firstSearchDataType != null)
            {
                if (firstSearchDataType.RegulatedIdentifier != null)
                    RegulatedIdentifier = new CodeType(firstSearchDataType.RegulatedIdentifier);
                if (firstSearchDataType.HitNoHitIndicator != null)
                    HitNoHitIndicator = new CodeType(firstSearchDataType.HitNoHitIndicator);
                CustomerTrackingNumber = firstSearchDataType.CustomerTrackingNumber;
                SocialSecurityNumber = firstSearchDataType.SocialSecurityNumber;
                CustomerName = firstSearchDataType.CustomerName;
                AddrPhoneNumber = firstSearchDataType.AddrPhoneNumber;
                if (firstSearchDataType.UnparsedStreetAddress != null)
                    UnparsedStreetAddress = new UnparsedStreetAddressType(firstSearchDataType.UnparsedStreetAddress);
                CityStateZip = firstSearchDataType.CityStateZip;
                if (firstSearchDataType.Telephone != null)
                    Telephone = new TelephoneType(firstSearchDataType.Telephone);
                if (firstSearchDataType.DateReported != null)
                    DateReported = new DateType(firstSearchDataType.DateReported);
                if (firstSearchDataType.DateOfBirth != null)
                    DateOfBirth = new DateType(firstSearchDataType.DateOfBirth);
            }
        }
        public ICodeType RegulatedIdentifier { get; set; }
        public ICodeType HitNoHitIndicator { get; set; }
        public string CustomerTrackingNumber { get; set; }
        public string SocialSecurityNumber { get; set; }
        public string CustomerName { get; set; }
        public string AddrPhoneNumber { get; set; }
        public IUnparsedStreetAddressType UnparsedStreetAddress { get; set; }
        public string CityStateZip { get; set; }
        public ITelephoneType Telephone { get; set; }
        public IDateType DateReported { get; set; }
        public IDateType DateOfBirth { get; set; }
    }
}
