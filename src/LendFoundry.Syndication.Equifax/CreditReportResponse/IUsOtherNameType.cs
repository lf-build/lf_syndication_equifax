﻿namespace LendFoundry.Syndication.Equifax.CreditReportResponse
{
    public interface IUsOtherNameType
    {
        string Code { get; set; }
        string Description { get; set; }
    }
}