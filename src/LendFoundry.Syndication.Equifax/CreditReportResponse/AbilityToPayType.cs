﻿namespace LendFoundry.Syndication.Equifax.CreditReportResponse
{
    public class AbilityToPayType : IAbilityToPayType
    {
        public AbilityToPayType(Proxy.CreditReportResponse.AbilityToPayType abilityToPayType)
        {
            if (abilityToPayType != null)
            {
                ProductId = abilityToPayType.ProductID;
                PostalCode = abilityToPayType.PostalCode;
                PostalCodePlus4 = abilityToPayType.PostalCodePlus4;
                Age = abilityToPayType.Age;
                AgeSpecified = abilityToPayType.AgeSpecified;
                AgeBand = abilityToPayType.AgeBand;
                if (abilityToPayType.IncomeofConsumer != null)
                    IncomeofConsumer = new MoneyType(abilityToPayType.IncomeofConsumer);
                ATPScore = abilityToPayType.ATPScore;
                DispositionCode = abilityToPayType.DispositionCode;
            }
        }
        public string ProductId { get; set; }
        public string PostalCode { get; set; }
        public string PostalCodePlus4 { get; set; }
        public short Age { get; set; }
        public bool AgeSpecified { get; set; }
        public string AgeBand { get; set; }
        public IMoneyType IncomeofConsumer { get; set; }
        public string ATPScore { get; set; }
        public string DispositionCode { get; set; }
    }
}
