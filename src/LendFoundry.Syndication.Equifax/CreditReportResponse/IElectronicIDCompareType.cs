﻿using System.Collections.Generic;

namespace LendFoundry.Syndication.Equifax.CreditReportResponse
{
    public interface IElectronicIdCompareType
    {
        ICodeType RegulatedIdentifier { get; set; }
        ICodeType HitNoHitIndicator { get; set; }
        List<IReasonCodeType> ReasonCodes { get; set; }
        ISimpleAddressType CurrentAddress { get; set; }
        ISimpleAddressType FormerAddress { get; set; }
        ISimpleAddressType OtherAddress { get; set; }
        ICodeType FraudIndicator { get; set; }
        string Fraudlevel { get; set; }
        short FraudIndexScore { get; set; }
        bool FraudIndexScoreSpecified { get; set; }
        ICodeType MatchAssessment { get; set; }
        short OverallScore { get; set; }
        bool OverallScoreSpecified { get; set; }
        string AssessmentPassed { get; set; }
        string AddressStandardization { get; set; }
        string ConsumerID { get; set; }
        string AggregatorClientID { get; set; }
        ICodeType TransactionStatus { get; set; }
        string TransactionKey { get; set; }
    }
}
