﻿namespace LendFoundry.Syndication.Equifax.CreditReportResponse
{
    public class ReasonCodeStringType : IReasonCodeStringType
    {
        public ReasonCodeStringType(Proxy.CreditReportResponse.ReasonCodeStringType reasonCodeStringType)
        {
            if (reasonCodeStringType != null)
            {
                Number = reasonCodeStringType.number;
                Value = reasonCodeStringType.Value;
            }
        }
        public short Number { get; set; }
        public string Value { get; set; }
    }
}
