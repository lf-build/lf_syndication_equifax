﻿using System.Collections.Generic;

namespace LendFoundry.Syndication.Equifax.CreditReportResponse
{
    public interface IPropertyDataAndAnalyticLIENType
    {
        ICodeType HitNoHitIndicator { get; set; }
        IMoneyType EstimatedLoantoValue { get; set; }
        IMoneyType CombinedLoantoValue { get; set; }
        IMoneyType EstimatedBalanceofFirstLien { get; set; }
        IMoneyType EstimatedBalanceofSecondLien { get; set; }
        List<IPropertyDataAndAnalyticLIENDetailType> LienDetail { get; set; }
    }
}
