namespace LendFoundry.Syndication.Equifax.CreditReportResponse
{
    public interface IUsHeaderType
    {
        IUsRequest UsRequest { get; set; }

        IUsCreditFile UsCreditFile { get; set; }

        IUsConsumerSubject UsConsumerSubject{ get; set; }
    }
}