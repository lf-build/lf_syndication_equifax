﻿namespace LendFoundry.Syndication.Equifax.CreditReportResponse
{
    public interface IUsConsumerReferralType
    {
        string AddressLine1 { get; set; }
        string AddressLine2 { get; set; }
        string BureauId { get; set; }
        ICityType City { get; set; }
        string Name { get; set; }
        string PostalCode { get; set; }
        string State { get; set; }
        ITelephoneType Telephone { get; set; }
    }
}