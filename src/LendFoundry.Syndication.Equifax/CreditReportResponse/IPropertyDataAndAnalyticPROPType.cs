﻿using System.Collections.Generic;

namespace LendFoundry.Syndication.Equifax.CreditReportResponse
{
    public interface IPropertyDataAndAnalyticPROPType
    {
        ICodeType HitNoHitIndicator { get; set; }
        string AssessedLotSizeofPropertyinAcres { get; set; }
        IMoneyType AppraisedValueofPropertybyCtyAssessor { get; set; }
        IMoneyType AssessedValueofPropertyValueAssigned { get; set; }
        string YearthatPropertywasLastAssessed { get; set; }
        IMoneyType ValueAssignedbyCountyTaxAssesImprovements { get; set; }
        IMoneyType AssessedLandValueoftheProperty { get; set; }
        string FinishedSquareFootageofPropertyBasement { get; set; }
        string TotalSquareFootageofpropertiesBasement { get; set; }
        string CountyWherePropertyisLocated { get; set; }
        string ElementarySchoolDistrictforProperty { get; set; }
        string FederalInformationProcessingStandard { get; set; }
        string TotalSquareFootageofPropertiesGarage { get; set; }
        string TypeofGarage { get; set; }
        string SpecifiesNorthSouthPositionoftheProperty { get; set; }
        string SpecifiesEastWestPositionoftheProperty { get; set; }
        string MainSquareFootageofProperty { get; set; }
        IMoneyType LikelyPriceaPropertyisWorthinaFairSale { get; set; }
        string MunicipalCode { get; set; }
        short NumberofBathrooms { get; set; }
        bool NumberofBathroomsSpecified { get; set; }
        short NumberofBedrooms { get; set; }
        bool NumberofBedroomsSpecified { get; set; }
        short NumberofFireplacesontheProperty { get; set; }
        bool NumberofFireplacesonthePropertySpecified { get; set; }
        short NumberofGarageSpacesontheProperty { get; set; }
        bool NumberofGarageSpacesonthePropertySpecified { get; set; }
        short NumberofLevelsonProperty { get; set; }
        bool NumberofLevelsonPropertySpecified { get; set; }
        List<IPropertyOwnersNamesType> PropertyOwnersNames { get; set; }
        ICodeType FlagtoIndicateifOwnerOccupiestheProperty { get; set; }
        string PropertyStyle { get; set; }
        string TypeofProperty { get; set; }
        string TypeofRoofonProperty { get; set; }
        string SecondarySchoolDistrictforProperty { get; set; }
        string SideofStreetWherePropertyisLocated { get; set; }
        string StateFIPSinWhichthePropertyResides { get; set; }
        string NameofSubdivisionWherePropertyisLocated { get; set; }
        string WhetherorNotthePropertyisOwnedByanREOentity { get; set; }
        IDateType TaxSaleDateforthePropertypertheCounty { get; set; }
        IMoneyType TaxSalePriceforthePropertypertheCounty { get; set; }
        short TotalNumberofRoomsonProperty { get; set; }
        bool TotalNumberofRoomsonPropertySpecified { get; set; }
        string UseCodeforthePropertyPertheCounty { get; set; }
        string UnifiedSchoolDistrictforthePropertypertheCounty { get; set; }
        IDateType YearthatthePropertywasBuilt { get; set; }
        string LocalMunicipalLawSpecifiesWhatPurposeMaybeUsed { get; set; }
        ISimpleAddressType TaxMailingAddressforthePropertysOwner { get; set; }
        IDateType LastSaleDateofthePropertyPertheCounty { get; set; }
        IMoneyType LastSalePriceofthePropertyPertheCounty { get; set; }
    }
}
