namespace LendFoundry.Syndication.Equifax.CreditReportResponse
{
    public class PersonNameType : IPersonNameType
    {
        public PersonNameType(Proxy.CreditReportResponse.SubjectNameType personNameType)
        {
            if (personNameType != null)
            {
                //TODO: Not Exist in new schema
                //NamePrefix = (NamePrefix)personNameType.NamePrefix;
                LastName = personNameType.LastName;
                FirstName = personNameType.FirstName;
                MiddleName = personNameType.MiddleName;
                NameSuffix = personNameType.NameSuffix;
            }
        }

        public NamePrefix NamePrefix { get; set; }

        public string LastName { get; set; }

        public string FirstName { get; set; }

        public string MiddleName { get; set; }

        public string NameSuffix { get; set; }
    }
}