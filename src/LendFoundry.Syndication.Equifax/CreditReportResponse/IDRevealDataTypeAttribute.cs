﻿namespace LendFoundry.Syndication.Equifax.CreditReportResponse
{
    public class IDRevealDataTypeAttribute : IIDRevealDataTypeAttribute
    {
        public IDRevealDataTypeAttribute(Proxy.CreditReportResponse.IDRevealDataTypeAttribute idRevealDataTypeAttribute)
        {
            if (idRevealDataTypeAttribute != null)
            {
                Number = idRevealDataTypeAttribute.number;
                if (idRevealDataTypeAttribute.Name != null)
                    Name = new CodeType(idRevealDataTypeAttribute.Name);
                if (idRevealDataTypeAttribute.Value != null)
                    Value = new CodeType(idRevealDataTypeAttribute.Value);
            }
        }
        public short Number { get; set; }
        public ICodeType Name { get; set; }
        public ICodeType Value { get; set; }
    }
}
