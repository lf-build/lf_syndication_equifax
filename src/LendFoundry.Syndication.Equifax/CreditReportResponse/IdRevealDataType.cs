﻿using System.Collections.Generic;
using System.Linq;

namespace LendFoundry.Syndication.Equifax.CreditReportResponse
{
    public class IdRevealDataType : IIdRevealDataType
    {
        public IdRevealDataType(Proxy.CreditReportResponse.IDRevealDataType idRevealDataType)
        {
            if (idRevealDataType != null)
            {
                if (idRevealDataType.RegulatedIdentifier != null)
                    RegulatedIdentifier = new CodeType(idRevealDataType.RegulatedIdentifier);
                if (idRevealDataType.HitNoHitIndicator != null)
                    HitNoHitIndicator = new CodeType(idRevealDataType.HitNoHitIndicator);
                ModelScore = idRevealDataType.ModelScore;
                if (idRevealDataType.ReasonCodes != null)
                    ReasonCodes = idRevealDataType.ReasonCodes.Select(p => new ReasonCodeType(p)).ToList<IReasonCodeType>();
                if (idRevealDataType.RejectCode != null)
                    RejectCode = new CodeType(idRevealDataType.RejectCode);
                if (idRevealDataType.Attributes != null)
                    Attributes = idRevealDataType.Attributes.Select(p => new IDRevealDataTypeAttribute(p)).ToList<IIDRevealDataTypeAttribute>();
            }
        }
        public ICodeType RegulatedIdentifier { get; set; }
        public ICodeType HitNoHitIndicator { get; set; }
        public string ModelScore { get; set; }
        public List<IReasonCodeType> ReasonCodes { get; set; }
        public ICodeType RejectCode { get; set; }
        public List<IIDRevealDataTypeAttribute> Attributes { get; set; }
    }
}
