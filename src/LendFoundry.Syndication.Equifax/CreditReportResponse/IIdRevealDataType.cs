﻿using System.Collections.Generic;

namespace LendFoundry.Syndication.Equifax.CreditReportResponse
{
    public interface IIdRevealDataType
    {
        ICodeType RegulatedIdentifier { get; set; }
        ICodeType HitNoHitIndicator { get; set; }
        string ModelScore { get; set; }
        List<IReasonCodeType> ReasonCodes { get; set; }
        ICodeType RejectCode { get; set; }
        List<IIDRevealDataTypeAttribute> Attributes { get; set; }
    }
}
