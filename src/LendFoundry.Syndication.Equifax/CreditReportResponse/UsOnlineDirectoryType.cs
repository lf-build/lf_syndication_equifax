namespace LendFoundry.Syndication.Equifax.CreditReportResponse
{
    public class UsOnlineDirectoryType : IUsOnlineDirectoryType
    {
        public UsOnlineDirectoryType(Proxy.CreditReportResponse.USOnlineDirectoryType usOnlineDirectoryType)
        {
            if (usOnlineDirectoryType != null)
            {
                if (usOnlineDirectoryType.CustomerId != null)
                    CustomerId = new CustomerIdType(usOnlineDirectoryType.CustomerId);

                //TODO: Changed type
                if (usOnlineDirectoryType.Telephone != null)
                {
                    var parsedTelephoneType =usOnlineDirectoryType.Telephone.Item as Proxy.CreditReportResponse.ParsedTelephoneType;
                    if (parsedTelephoneType != null)
                    {
                        Telephone = new ParsedTelephoneType(parsedTelephoneType);
                    }
                    else
                    {
                        var unparsedTelephone = usOnlineDirectoryType.Telephone.Item as string;
                        if (!string.IsNullOrEmpty(unparsedTelephone))
                            UnparsedTelephone = unparsedTelephone;
                    }
                }

                AddressLine1 = usOnlineDirectoryType.AddressLine1;
                AddressLine2 = usOnlineDirectoryType.AddressLine2;

                if (usOnlineDirectoryType.City != null)
                    City = new CityType(usOnlineDirectoryType.City);
                State = usOnlineDirectoryType.State;
                PostalCode = usOnlineDirectoryType.PostalCode;
            }
        }

        public ICustomerIdType CustomerId { get; set; }

        public IParsedTelephoneType Telephone { get; set; }

        public string UnparsedTelephone { get; set; }
        public string AddressLine1 { get; set; }

        public string AddressLine2 { get; set; }

        public ICityType City { get; set; }

        public string State { get; set; }

        public string PostalCode { get; set; }
    }
}