﻿namespace LendFoundry.Syndication.Equifax.CreditReportResponse
{
    public interface IAttribute3Type
    {
        string AttributeCode { get; set; }
        string AttributeValue { get; set; }
        short Number { get; set; }
    }
}
