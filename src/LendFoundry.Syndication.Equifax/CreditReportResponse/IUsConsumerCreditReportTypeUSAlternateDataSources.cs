﻿using System.Collections.Generic;

namespace LendFoundry.Syndication.Equifax.CreditReportResponse
{
    public interface IUsConsumerCreditReportTypeUSAlternateDataSources
    {
        List<IAlternateDataSourceErrorType> AlternateDataSourceErrors { get; set; }
        List<ITheWorkNumberSelectType> TheWorkNumberSelects { get; set; }
        List<IElectronicIdCompareType> ElectronicIdCompares { get; set; }
        List<IPropertyDataAndAnalyticType> PropertyDataAndAnalytics { get; set; }
        List<IMilitaryLendingType> MilitaryLendingActDataSegments { get; set; }
        List<IInterConnectDecisioningType> InterConnectDecisionings { get; set; }
        List<IIxiServicesType> IxiServicesDatas { get; set; }
        List<IIdRevealDataType> IdRevealDatas { get; set; }
        List<IFirstSearchDataType> FirstSearchDatas { get; set; }
    }
}
