﻿using System.Collections.Generic;
using System.Linq;

namespace LendFoundry.Syndication.Equifax.CreditReportResponse
{
    public class AlternateDataSourceErrorType : IAlternateDataSourceErrorType
    {
        public AlternateDataSourceErrorType(Proxy.CreditReportResponse.AlternateDataSourceErrorType alternateDataSourceErrorType)
        {
            if (alternateDataSourceErrorType != null)
            {
                CustomerReferenceNumber = alternateDataSourceErrorType.CustomerReferenceNumber;
                CustomerNumber = alternateDataSourceErrorType.CustomerNumber;
                if (alternateDataSourceErrorType.DataSourceCode != null)
                    DataSourceCode = new CodeType(alternateDataSourceErrorType.DataSourceCode);
                if (alternateDataSourceErrorType.DerrErrors != null)
                    DerrErrors = alternateDataSourceErrorType.DerrErrors.Select(p => new DerrType(p)).ToList<IDerrType>();
            }
        }
        public string CustomerReferenceNumber { get; set; }
        public string CustomerNumber { get; set; }
        public ICodeType DataSourceCode { get; set; }
        public List<IDerrType> DerrErrors { get; set; }
    }
}
