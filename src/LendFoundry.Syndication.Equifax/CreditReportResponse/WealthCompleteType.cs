﻿namespace LendFoundry.Syndication.Equifax.CreditReportResponse
{
    public class WealthCompleteType : IWealthCompleteType
    {
        public WealthCompleteType(Proxy.CreditReportResponse.WealthCompleteType wealthCompleteType)
        {
            if (wealthCompleteType != null)
            {
                ProductId = wealthCompleteType.ProductID;
                PostalCode = wealthCompleteType.PostalCode;
                PostalCodePlus4 = wealthCompleteType.PostalCodePlus4;
                Age = wealthCompleteType.Age;
                AgeSpecified = wealthCompleteType.AgeSpecified;
                AgeBand = wealthCompleteType.AgeBand;
                if (wealthCompleteType.IncomeofConsumer != null)
                    IncomeofConsumer = new MoneyType(wealthCompleteType.IncomeofConsumer);
                if (wealthCompleteType.Annuities != null)
                    Annuities = new MoneyType(wealthCompleteType.Annuities);
                if (wealthCompleteType.Stocks != null)
                    Stocks = new MoneyType(wealthCompleteType.Stocks);
                if (wealthCompleteType.Bonds != null)
                    Bonds = new MoneyType(wealthCompleteType.Bonds);
                if (wealthCompleteType.Deposits != null)
                    Deposits = new MoneyType(wealthCompleteType.Deposits);
                if (wealthCompleteType.MutualFunds != null)
                    MutualFunds = new MoneyType(wealthCompleteType.MutualFunds);
                if (wealthCompleteType.OtherAssets != null)
                    OtherAssets = new MoneyType(wealthCompleteType.OtherAssets);
                if (wealthCompleteType.TotalAssets != null)
                    TotalAssets = new MoneyType(wealthCompleteType.TotalAssets);
                if (wealthCompleteType.CertificateofDeposit != null)
                    CertificateofDeposit = new MoneyType(wealthCompleteType.CertificateofDeposit);
                if (wealthCompleteType.InterestChecking != null)
                    InterestChecking = new MoneyType(wealthCompleteType.InterestChecking);
                if (wealthCompleteType.MoneyMarketDepositAcct != null)
                    MoneyMarketDepositAcct = new MoneyType(wealthCompleteType.MoneyMarketDepositAcct);
                if (wealthCompleteType.NonInterestChecking != null)
                    NonInterestChecking = new MoneyType(wealthCompleteType.NonInterestChecking);
                if (wealthCompleteType.OtherChecking != null)
                    OtherChecking = new MoneyType(wealthCompleteType.OtherChecking);
                if (wealthCompleteType.Savings != null)
                    Savings = new MoneyType(wealthCompleteType.Savings);
                ModelVersion = wealthCompleteType.ModelVersion;
                DataVintage = wealthCompleteType.DataVintage;
                DispositionCode = wealthCompleteType.DispositionCode;
            }
        }
        public string ProductId { get; set; }
        public string PostalCode { get; set; }
        public string PostalCodePlus4 { get; set; }
        public short Age { get; set; }
        public bool AgeSpecified { get; set; }
        public string AgeBand { get; set; }
        public IMoneyType IncomeofConsumer { get; set; }
        public IMoneyType Annuities { get; set; }
        public IMoneyType Stocks { get; set; }
        public IMoneyType Bonds { get; set; }
        public IMoneyType Deposits { get; set; }
        public IMoneyType MutualFunds { get; set; }
        public IMoneyType OtherAssets { get; set; }
        public IMoneyType TotalAssets { get; set; }
        public IMoneyType CertificateofDeposit { get; set; }
        public IMoneyType InterestChecking { get; set; }
        public IMoneyType MoneyMarketDepositAcct { get; set; }
        public IMoneyType NonInterestChecking { get; set; }
        public IMoneyType OtherChecking { get; set; }
        public IMoneyType Savings { get; set; }
        public string ModelVersion { get; set; }
        public string DataVintage { get; set; }
        public string DispositionCode { get; set; }
    }
}
