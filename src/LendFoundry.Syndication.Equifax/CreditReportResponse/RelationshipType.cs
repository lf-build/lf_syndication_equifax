namespace LendFoundry.Syndication.Equifax.CreditReportResponse
{
    public enum RelationshipType
    {
        /// <remarks/>
        Primary,

        /// <remarks/>
        Secondary,
    }
}