﻿namespace LendFoundry.Syndication.Equifax.CreditReportResponse
{
    public interface IAttribute2Type
    {
        ICodeType Name { get; set; }
        ICodeType Value { get; set; }
    }
}
