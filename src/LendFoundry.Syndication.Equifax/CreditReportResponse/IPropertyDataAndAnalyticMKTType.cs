﻿namespace LendFoundry.Syndication.Equifax.CreditReportResponse
{
    public interface IPropertyDataAndAnalyticMKTType
    {
        ICodeType HitNoHitIndicator { get; set; }
        string PercentofNonOwnerOccupiedSegment { get; set; }
        string PercentofNonOwnerOccupiedZip { get; set; }
        string PercentSpreadMedianListSaleSeg { get; set; }
        string PercentSpreadMedianListSaleZip { get; set; }
        string AvgDaysonMarketSegment { get; set; }
        string AvgDaysonMarketZip { get; set; }
        short AvgLotSize { get; set; }
        bool AvgLotSizeSpecified { get; set; }
        short AvgMainSquareFeet { get; set; }
        bool AvgMainSquareFeetSpecified { get; set; }
        short AvgNumberofBathrooms { get; set; }
        bool AvgNumberofBathroomsSpecified { get; set; }
        short AvgNumberofBedrooms { get; set; }
        bool AvgNumberofBedroomsSpecified { get; set; }
        short AvgNumberofMonthlySales { get; set; }
        bool AvgNumberofMonthlySalesSpecified { get; set; }
        short AvgNumberofMonthlySalesZip { get; set; }
        bool AvgNumberofMonthlySalesZipSpecified { get; set; }
        short AvgPricePerSquareFoot { get; set; }
        bool AvgPricePerSquareFootSpecified { get; set; }
        short AvgYearsofConstruction { get; set; }
        bool AvgYearsofConstructionSpecified { get; set; }
        string EstMonthsofCurrentInvestment { get; set; }
        string EstMonthsofCurrentInvestmentZip { get; set; }
        string HighListRangeSeg { get; set; }
        string HighListRangeZip { get; set; }
        string HighSaleRangeSeg { get; set; }
        string HighSaleRangeZip { get; set; }
        string LowListRangeSeg { get; set; }
        string LowListRangeZip { get; set; }
        string LowSaleRangeSeg { get; set; }
        string LowSaleRangeZip { get; set; }
        string MedianSale { get; set; }
        string MedianSaleZip { get; set; }
        string NoticeofDefaultRateSeg { get; set; }
        string NoticeofDefaultRateZip { get; set; }
        string NoticeofTrusteeSaleRateSeg { get; set; }
        string NoticeofTrusteeSaleRateZip { get; set; }
        string ReoRate1stQtr { get; set; }
        string ReoRate1stQtrZip { get; set; }
        string ReoRate1YearSegment { get; set; }
        string ReoRate1yearZip { get; set; }
        string ReoRate2YearSegment { get; set; }
        string ReoRate2yearZip { get; set; }
        string ReoSaleRate1stQtr { get; set; }
        string ReoSaleRate1stQtrZip { get; set; }
        string ReoSaleRate1YearSegment { get; set; }
        string ReoSaleRate1YearZip { get; set; }
        string ReoSaleRate2YearSegment { get; set; }
        string ReoSaleRate2YearZip { get; set; }
        string TotalLoanValueSeg { get; set; }
        string TotalLoanValueZip { get; set; }
    }
}
