﻿using System.Collections.Generic;

namespace LendFoundry.Syndication.Equifax.CreditReportResponse
{
    public interface IProductType
    {
        string ProductCode { get; set; }
        List<IReasonCodeStringType> ReasonCode { get; set; }
        List<IAttribute3Type> ProductAttribute { get; set; }
        short Number { get; set; }
    }
}
