﻿using System.Collections.Generic;

namespace LendFoundry.Syndication.Equifax.CreditReportResponse
{
    public interface IUsAlertContactType
    {
        string AddressLine1 { get; set; }
        string AddressLine2 { get; set; }
        ICodeType AlertType { get; set; }
        ICityType City { get; set; }
        string CountryCode { get; set; }
        IDateType DateEffective { get; set; }
        IDateType DateReported { get; set; }
        string FreeformInformation { get; set; }
        List<IUsAlertContactTypePhoneNumber> PhoneNumbers { get; set; }
        string PostalCode { get; set; }
        string State { get; set; }
    }
}