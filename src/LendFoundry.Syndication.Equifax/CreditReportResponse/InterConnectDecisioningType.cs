﻿using System.Collections.Generic;
using System.Linq;

namespace LendFoundry.Syndication.Equifax.CreditReportResponse
{
    public class InterConnectDecisioningType : IInterConnectDecisioningType
    {
        public InterConnectDecisioningType(Proxy.CreditReportResponse.InterConnectDecisioningType interConnectDecisioningType)
        {
            if (interConnectDecisioningType != null)
            {
                if (interConnectDecisioningType.RegulatedIdentifier != null)
                    RegulatedIdentifier = new CodeType(interConnectDecisioningType.RegulatedIdentifier);
                if (interConnectDecisioningType.HitNoHitIndicator != null)
                    HitNoHitIndicator = new CodeType(interConnectDecisioningType.HitNoHitIndicator);
                DecisionType = interConnectDecisioningType.DecisionType;
                if (interConnectDecisioningType.DecisionDesignator != null)
                    DecisionDesignator = new CodeType(interConnectDecisioningType.DecisionDesignator);
                Description = interConnectDecisioningType.Description;
                DecisionCode = interConnectDecisioningType.DecisionCode;
                if (interConnectDecisioningType.Products != null)
                    Products = interConnectDecisioningType.Products.Select(p => new ProductType(p)).ToList<IProductType>();
            }
        }
        public ICodeType RegulatedIdentifier { get; set; }
        public ICodeType HitNoHitIndicator { get; set; }
        public string DecisionType { get; set; }
        public ICodeType DecisionDesignator { get; set; }
        public string Description { get; set; }
        public string DecisionCode { get; set; }
        public List<IProductType> Products { get; set; }
    }
}
