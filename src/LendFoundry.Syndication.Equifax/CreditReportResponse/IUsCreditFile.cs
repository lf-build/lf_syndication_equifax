﻿using System.Collections.Generic;

namespace LendFoundry.Syndication.Equifax.CreditReportResponse
{
    public interface IUsCreditFile
    {
        string AddressDiscrepancyIndicator { get; set; }
        string BureauCode { get; set; }
        string CreateCode { get; set; }
        IDateType DateOfLastActivity { get; set; }
        IDateType DateOfRequest { get; set; }
        ICodeType DoNotCombineIndicator { get; set; }
        IDateType FileSinceDate { get; set; }
        string FileStatus1 { get; set; }
        string FileStatus2 { get; set; }
        string FileStatus3 { get; set; }
        ICodeType FraudVictimIndicator { get; set; }
        ICodeType HitCode { get; set; }
        List<ICodeType> Identityscans { get; set; }
        ICodeType LinkIndicator { get; set; }
        string NameMatch { get; set; }
        List<ICodeType> Safescans { get; set; }
        string SsnAffirmIndicator { get; set; }
    }
}