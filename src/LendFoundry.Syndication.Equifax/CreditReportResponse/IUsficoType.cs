﻿using System.Collections.Generic;

namespace LendFoundry.Syndication.Equifax.CreditReportResponse
{
    public interface IUsficoType
    {
        object DoddFrank { get; set; }
        string FactActInquiriesAreKeyFactor { get; set; }
        string FicoIndicator { get; set; }
        string FicoScore { get; set; }
        ICodeType RejectCode { get; set; }
        object RiskBasedPricing { get; set; }
        List<IScoreReasonType> ScoreReasons { get; set; }
    }
}