﻿namespace LendFoundry.Syndication.Equifax.CreditReportResponse
{
    public interface IUsErrorMessages
    {
        IUsErrorMessageType UsErrorMessage { get; set; }
    }
}