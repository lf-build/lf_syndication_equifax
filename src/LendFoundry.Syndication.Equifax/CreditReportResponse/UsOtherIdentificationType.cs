namespace LendFoundry.Syndication.Equifax.CreditReportResponse
{
    public class UsOtherIdentificationType : IUsOtherIdentificationType
    {
        public UsOtherIdentificationType(Proxy.CreditReportResponse.USOtherIdentificationType usOtherIdentificationType)
        {
            if (usOtherIdentificationType != null)
            {
                if (usOtherIdentificationType.DateReported != null)
                    DateReported = new DateType(usOtherIdentificationType.DateReported);
                if (!string.IsNullOrWhiteSpace(usOtherIdentificationType.OtherIdentificationNumber))
                    OtherIdentificationNumber = usOtherIdentificationType.OtherIdentificationNumber;
                if (usOtherIdentificationType.Reason != null)
                    Reason = new CodeType(usOtherIdentificationType.Reason);
                if (usOtherIdentificationType.Type != null)
                    Type = new CodeType(usOtherIdentificationType.Type);
            }
        }

        public IDateType DateReported { get; set; }

        public string OtherIdentificationNumber { get; set; }

        public ICodeType Reason { get; set; }

        public ICodeType Type { get; set; }
    }
}