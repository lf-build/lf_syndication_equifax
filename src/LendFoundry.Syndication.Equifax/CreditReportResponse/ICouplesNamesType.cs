﻿namespace LendFoundry.Syndication.Equifax.CreditReportResponse
{
    public interface ICouplesNamesType
    {
        string FullName { get; set; }
        string FirstName { get; set; }
        string LastName { get; set; }
        string MiddleName { get; set; }
        string SpouseFirstName { get; set; }
        string SpouseMiddleName { get; set; }
    }
}
