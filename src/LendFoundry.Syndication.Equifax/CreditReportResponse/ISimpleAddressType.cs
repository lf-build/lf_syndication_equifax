﻿namespace LendFoundry.Syndication.Equifax.CreditReportResponse
{
    public interface ISimpleAddressType
    {
        IUnparsedStreetAddressType UnparsedStreetAddress { get; set; }
        ICityType City { get; set; }
        string State { get; set; }
        string PostalCode { get; set; }
        ITelephoneType Telephone { get; set; }
        string Code { get; set; }
        string Description { get; set; }
    }
}
