using System.Collections.Generic;
using System.Linq;

namespace LendFoundry.Syndication.Equifax.CreditReportResponse
{
    public class UsofacAlertType : IUsofacAlertType
    {
        public UsofacAlertType(Proxy.CreditReportResponse.USCDCOFACType usOfacAlertType)
        {
            RevisedLegalVerbiageIndicator = usOfacAlertType.RevisedLegalVerbiageIndicator;
            CdcMemberCode = usOfacAlertType.CDCMemberCode;
            CdcBranchAccountNumber = usOfacAlertType.CDCBranchAccountNumber;
            if (usOfacAlertType.TransactionDate != null)
                TransactionDate = new DateType(usOfacAlertType.TransactionDate);
            TransactionTime = usOfacAlertType.TransactionTime;
            if (usOfacAlertType.ResponseType != null)
                ResponseType = new CodeType(usOfacAlertType.ResponseType);

            if (usOfacAlertType.ResponseCode != null)
                ResponseCode = new CodeType(usOfacAlertType.ResponseCode);

            if (usOfacAlertType.ProblemCode != null)
                ProblemCode = new CodeType(usOfacAlertType.ProblemCode);

            if (usOfacAlertType.MatchCodes != null)
                MatchCodes = usOfacAlertType.MatchCodes.Select(p => new CodeType(p)).ToList<ICodeType>();

            if (usOfacAlertType.ProblemReportDate != null)
                ProblemReportDate = new DateType(usOfacAlertType.ProblemReportDate);

            IssueSource = usOfacAlertType.IssueSource;
            IssueId = usOfacAlertType.IssueId;
            Comment = usOfacAlertType.Comment;

            if (usOfacAlertType.SubjectName != null)
                PersonName = new PersonNameType(usOfacAlertType.SubjectName);

            AddressLine1 = usOfacAlertType.AddressLine1;

            if (usOfacAlertType.City != null)
                City = new CityType(usOfacAlertType.City);

            State = usOfacAlertType.State;
            PostalCode = usOfacAlertType.PostalCode;
            CountryCode = usOfacAlertType.CountryCode;
            LegalVerbiage = usOfacAlertType.LegalVerbiage;
        }

        public string RevisedLegalVerbiageIndicator { get; set; }

        public string CdcMemberCode { get; set; }

        public string CdcBranchAccountNumber { get; set; }

        public IDateType TransactionDate { get; set; }

        public string TransactionTime { get; set; }

        public ICodeType ResponseType { get; set; }

        public ICodeType ResponseCode { get; set; }

        public ICodeType ProblemCode { get; set; }

        public List<ICodeType> MatchCodes { get; set; }

        public IDateType ProblemReportDate { get; set; }

        public string IssueSource { get; set; }

        public string IssueId { get; set; }

        public string Comment { get; set; }

        public IPersonNameType PersonName { get; set; }

        public string AddressLine1 { get; set; }

        public ICityType City { get; set; }

        public string State { get; set; }

        public string PostalCode { get; set; }

        public string CountryCode { get; set; }

        public string LegalVerbiage { get; set; }
    }
}