using System.Collections.Generic;
using System.Linq;

namespace LendFoundry.Syndication.Equifax.CreditReportResponse
{
    public class UsTaxLienType : IUsTaxLienType
    {
        public UsTaxLienType(Proxy.CreditReportResponse.USTaxLienType usTaxLienType)
        {
            if (usTaxLienType != null)
            {
                if (usTaxLienType.DateFiled != null)
                    DateFiled = new DateType(usTaxLienType.DateFiled);

                if (usTaxLienType.CourtId != null)
                    CourtId = new CustomerIdType(usTaxLienType.CourtId);

                CaseNumber = usTaxLienType.CaseNumber;

                if (usTaxLienType.Amount != null)
                    Amount = new MoneyType(usTaxLienType.Amount);

                //TODO: Not Found
                //if (usTaxLienType.LienClass != null)
                //    LienClass = new CodeType(usTaxLienType.LienClass);

                if (usTaxLienType.ReleaseDate != null)
                    ReleaseDate = new DateType(usTaxLienType.ReleaseDate);

                if (usTaxLienType.VerificationDate != null)
                    VerificationDate = new DateType(usTaxLienType.VerificationDate);

                //TODO: Not Found
                //if (usTaxLienType.DateReported != null)
                //    DateReported = new DateType(usTaxLienType.DateReported);

                //TODO: Not Found
                //if (usTaxLienType.LienStatus != null)
                //    LienStatus = new CodeType(usTaxLienType.LienStatus);

                if (usTaxLienType.Narratives != null)
                    Narratives = usTaxLienType.Narratives.Select(p => new CodeType(p)).ToList<ICodeType>();
            }
        }

        public IDateType DateFiled { get; set; }

        public ICustomerIdType CourtId { get; set; }

        public string CaseNumber { get; set; }

        public IMoneyType Amount { get; set; }

        public ICodeType LienClass { get; set; }

        public IDateType ReleaseDate { get; set; }

        public IDateType VerificationDate { get; set; }

        public IDateType DateReported { get; set; }

        public ICodeType LienStatus { get; set; }

        public List<ICodeType> Narratives { get; set; }
    }
}