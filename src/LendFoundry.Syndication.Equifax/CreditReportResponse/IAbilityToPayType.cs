﻿namespace LendFoundry.Syndication.Equifax.CreditReportResponse
{
    public interface IAbilityToPayType
    {
        string ProductId { get; set; }
        string PostalCode { get; set; }
        string PostalCodePlus4 { get; set; }
        short Age { get; set; }
        bool AgeSpecified { get; set; }
        string AgeBand { get; set; }
        IMoneyType IncomeofConsumer { get; set; }
        string ATPScore { get; set; }
        string DispositionCode { get; set; }
    }
}
