namespace LendFoundry.Syndication.Equifax.CreditReportResponse
{
    public class ParsedTelephoneType : IParsedTelephoneType
    {

        public ParsedTelephoneType()
        {
        }

        public ParsedTelephoneType(Proxy.CreditReportResponse.ParsedTelephoneType parsedTelephoneType)
        {
            if (parsedTelephoneType != null)
            {
                AreaCode = parsedTelephoneType.AreaCode;
                Number = parsedTelephoneType.Number;
                Extension = parsedTelephoneType.Extension;
            }
        }

        public short AreaCode { get; set; }

        public string Number { get; set; }

        public short Extension { get; set; }
    }
}