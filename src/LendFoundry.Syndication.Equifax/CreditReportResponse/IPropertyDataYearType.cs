﻿namespace LendFoundry.Syndication.Equifax.CreditReportResponse
{
    public interface IPropertyDataYearType
    {
        DateFormat Format { get; set; }
        string Value { get; set; }
    }
}
