﻿namespace LendFoundry.Syndication.Equifax.CreditReportResponse
{
    public interface IIxiServicesType
    {
        ICodeType RegulatedIdentifier { get; set; }
        ICodeType HitNoHitIndicator { get; set; }
        string Disclaimer { get; set; }
        IWealthCompleteType WealthComplete { get; set; }
        IIncome360Type Income360 { get; set; }
        IDiscretionarySpendingType DiscretionarySpending { get; set; }
        IAbilityToPayType AbilityToPay { get; set; }
    }
}
