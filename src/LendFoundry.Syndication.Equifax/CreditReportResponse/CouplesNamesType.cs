﻿namespace LendFoundry.Syndication.Equifax.CreditReportResponse
{
    public class CouplesNamesType : ICouplesNamesType
    {
        public CouplesNamesType(Proxy.CreditReportResponse.CouplesNamesType couplesNamesType)
        {
            if (couplesNamesType != null)
            {
                FullName = couplesNamesType.FullName;
                FirstName = couplesNamesType.FirstName;
                LastName = couplesNamesType.LastName;
                MiddleName = couplesNamesType.MiddleName;
                SpouseFirstName = couplesNamesType.SpouseFirstName;
                SpouseMiddleName = couplesNamesType.SpouseMiddleName;
            }
        }
        public string FullName { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string MiddleName { get; set; }
        public string SpouseFirstName { get; set; }
        public string SpouseMiddleName { get; set; }
    }
}
