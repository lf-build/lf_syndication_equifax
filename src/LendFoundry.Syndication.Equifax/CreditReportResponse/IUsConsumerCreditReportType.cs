﻿using System.Collections.Generic;

namespace LendFoundry.Syndication.Equifax.CreditReportResponse
{
    public interface IUsConsumerCreditReportType
    {
        IIdentityScanType IdentityScan { get; set; }
        short MultipleNumber { get; set; }
        string SubjectType { get; set; }
        RelationshipType RelationshipIdentifier { get; set; }
        List<IUsAddressType> UsAddresses { get; set; }
        List<IUsAlertContactType> UsAlertContacts { get; set; }
        List<IUsBankruptcyType> UsBankruptcies { get; set; }
        IUsBeaconType UsBeacon { get; set; }
        List<IUsCollectionType> UsCollections { get; set; }
        IUsConsumerReferralType UsConsumerReferral { get; set; }
        List<IUsConsumerStatementType> UsConsumerStatements { get; set; }
        List<IUsDataAttributeType> UsDataAttributes { get; set; }
        List<IUsDecisionPowerDataType> UsDecisionPowerSegments { get; set; }
        IUsedasType Usedas { get; set; }
        List<IUseMailAddressType> UseMailAddresses { get; set; }
        List<IUsEmploymentType> UsEmployments { get; set; }
        List<IUsErrorMessageType> UsErrorMessages { get; set; }
        IUsficoType Usfico { get; set; }
        IUsFileIdType UsFileId { get; set; }
        IUsFraudAdvisorType UsFraudAdvisor { get; set; }
        IUsGeoCodeType UsGeoCode { get; set; }
        IUsHeaderType UsHeader { get; set; }
        IUsIdentificationSsnType UsIdentificationSsn { get; set; }
        List<IUsInquiryType> UsInquiries { get; set; }
        List<IUsLegalItemType> UsLegalItems { get; set; }
        List<IUsScoreType> UsModelScores { get; set; }
        List<IUsofacAlertType> UsofacAlerts { get; set; }
        List<IUsOnlineDirectoryType> UsOnlineDirectories { get; set; }
        List<IUsOtherIdentificationType> UsOtherIdentifications { get; set; }
        List<IUsOtherNameType> UsOtherNames { get; set; }
        List<IUsOtherTelephoneNumberType> UsOtherTelephoneNumbers { get; set; }
        IUsSubjectDeathType UsSubjectDeath { get; set; }
        List<IUsTaxLienType> UsTaxLiens { get; set; }
        List<IUsTradeType> UsTrades { get; set; }
        List<IUsCdcOfacType> UsCdcOfacAlerts { get; set; }
        IUsConsumerCreditReportTypeUSAlternateDataSources UsAlternateDataSources { get; set; }
    }
}