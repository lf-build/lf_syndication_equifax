﻿namespace LendFoundry.Syndication.Equifax.CreditReportResponse
{
    public interface ISubjectNameFMLType
    {
        string FirstName { get; set; }
        string MiddleName { get; set; }
        string LastName { get; set; }
        string NameSuffix { get; set; }
    }
}
