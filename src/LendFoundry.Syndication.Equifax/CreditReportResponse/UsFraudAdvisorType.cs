using System.Collections.Generic;
using System.Linq;

namespace LendFoundry.Syndication.Equifax.CreditReportResponse
{
    public class UsFraudAdvisorType : IUsFraudAdvisorType
    {
        public UsFraudAdvisorType(Proxy.CreditReportResponse.USFraudAdvisorType usFraudAdvisorType)
        {
            if (usFraudAdvisorType != null)
            {
                FraudAdvisorVersion = usFraudAdvisorType.FraudAdvisorVersion;

                FraudAdvisorIndex = usFraudAdvisorType.FraudAdvisorIndex;

                if (usFraudAdvisorType.Warnings != null)
                    Warnings =
                        usFraudAdvisorType.Warnings.Select(
                            p => new ScoreReasonType(new Proxy.CreditReportResponse.CodeType
                            {
                                code = p.code,
                                description = p.description
                            }, p)).ToList<IScoreReasonType>();

                if (usFraudAdvisorType.RejectCode != null)
                    RejectCode = new CodeType(usFraudAdvisorType.RejectCode);

                FirstNameCounter = usFraudAdvisorType.FirstNameCounter;
                LastNameCounter = usFraudAdvisorType.LastNameCounter;
                BusinessNameCounter = usFraudAdvisorType.BusinessNameCounter;
                StreetAddressCounter = usFraudAdvisorType.StreetAddressCounter;
                SsnCounter = usFraudAdvisorType.SSNCounter;
                HomePhoneCounter = usFraudAdvisorType.HomePhoneCounter;

                WorkPhoneCounter = usFraudAdvisorType.WorkPhoneCounter;

                DateOfBirthConfirmed = usFraudAdvisorType.DateOfBirthConfirmed;

                DriversLicenseConfirmed = usFraudAdvisorType.DriversLicenseConfirmed;

                EMailAddressConfirmed = usFraudAdvisorType.EMailAddressConfirmed;

                SsnVerifiedLevel = usFraudAdvisorType.SSNVerifiedLevel;

                NumberVerifiedFieldsGtZero = usFraudAdvisorType.NumberVerifiedFieldsGTZero;

                SumOfVerifiedCounters = usFraudAdvisorType.SumOfVerifiedCounters;

                IdAdvisorIndex = usFraudAdvisorType.IDAdvisorIndex;

                if (usFraudAdvisorType.IDAdvisorWarnings != null)
                    IdAdvisorWarnings =
                        usFraudAdvisorType.IDAdvisorWarnings.Select(
                            p => new ScoreReasonType(new Proxy.CreditReportResponse.CodeType
                            {
                                code = p.code,
                                description = p.description
                            }, p)).ToList<IScoreReasonType>();

                FirstName = usFraudAdvisorType.FirstName;

                LastName = usFraudAdvisorType.LastName;

                AddressLine1 = usFraudAdvisorType.AddressLine1;

                AddressLine2 = usFraudAdvisorType.AddressLine2;

                if (usFraudAdvisorType.City != null)
                    City = new CityType(usFraudAdvisorType.City);
                State = usFraudAdvisorType.State;
                PostalCode = usFraudAdvisorType.PostalCode;
                //TODO: Changed SocialSecurityNumber to SubjectSSN 
                SocialSecurityNumber = usFraudAdvisorType.SubjectSSN;

                if (usFraudAdvisorType.DateOfBirth != null)
                    DateOfBirth = new DateType(usFraudAdvisorType.DateOfBirth);
                if (usFraudAdvisorType.Telephone != null)
                    Telephone = new TelephoneType(usFraudAdvisorType.Telephone);
            }
        }

        public string FraudAdvisorVersion { get; set; }

        public string FraudAdvisorIndex { get; set; }

        public List<IScoreReasonType> Warnings { get; set; }

        public ICodeType RejectCode { get; set; }

        public string FirstNameCounter { get; set; }

        public string LastNameCounter { get; set; }

        public string BusinessNameCounter { get; set; }

        public string StreetAddressCounter { get; set; }

        public string SsnCounter { get; set; }

        public string HomePhoneCounter { get; set; }

        public string WorkPhoneCounter { get; set; }

        public string DateOfBirthConfirmed { get; set; }

        public string DriversLicenseConfirmed { get; set; }

        public string EMailAddressConfirmed { get; set; }

        public string SsnVerifiedLevel { get; set; }

        public string NumberVerifiedFieldsGtZero { get; set; }

        public string SumOfVerifiedCounters { get; set; }

        public string IdAdvisorIndex { get; set; }

        public List<IScoreReasonType> IdAdvisorWarnings { get; set; }

        public string FirstName { get; set; }

        public string LastName { get; set; }

        public string AddressLine1 { get; set; }

        public string AddressLine2 { get; set; }

        public ICityType City { get; set; }

        public string State { get; set; }

        public string PostalCode { get; set; }

        public string SocialSecurityNumber { get; set; }

        public IDateType DateOfBirth { get; set; }

        public ITelephoneType Telephone { get; set; }
    }
}