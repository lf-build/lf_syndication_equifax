﻿namespace LendFoundry.Syndication.Equifax.CreditReportResponse
{
    public class IDerrType
    {
        string ErrorVerbiage { get; set; }
        string ErrorText { get; set; }
        short Number { get; set; }
    }
}
