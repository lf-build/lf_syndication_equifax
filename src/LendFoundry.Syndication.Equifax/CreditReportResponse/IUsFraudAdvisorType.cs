﻿using System.Collections.Generic;

namespace LendFoundry.Syndication.Equifax.CreditReportResponse
{
    public interface IUsFraudAdvisorType
    {
        string AddressLine1 { get; set; }
        string AddressLine2 { get; set; }
        string BusinessNameCounter { get; set; }
        ICityType City { get; set; }
        IDateType DateOfBirth { get; set; }
        string DateOfBirthConfirmed { get; set; }
        string DriversLicenseConfirmed { get; set; }
        string EMailAddressConfirmed { get; set; }
        string FirstName { get; set; }
        string FirstNameCounter { get; set; }
        string FraudAdvisorIndex { get; set; }
        string FraudAdvisorVersion { get; set; }
        string HomePhoneCounter { get; set; }
        string IdAdvisorIndex { get; set; }
        List<IScoreReasonType> IdAdvisorWarnings { get; set; }
        string LastName { get; set; }
        string LastNameCounter { get; set; }
        string NumberVerifiedFieldsGtZero { get; set; }
        string PostalCode { get; set; }
        ICodeType RejectCode { get; set; }
        string SocialSecurityNumber { get; set; }
        string SsnCounter { get; set; }
        string SsnVerifiedLevel { get; set; }
        string State { get; set; }
        string StreetAddressCounter { get; set; }
        string SumOfVerifiedCounters { get; set; }
        ITelephoneType Telephone { get; set; }
        List<IScoreReasonType> Warnings { get; set; }
        string WorkPhoneCounter { get; set; }
    }
}