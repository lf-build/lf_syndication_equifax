﻿namespace LendFoundry.Syndication.Equifax.CreditReportResponse
{
    public class Attribute3Type : IAttribute3Type
    {
        public Attribute3Type(Proxy.CreditReportResponse.Attribute3Type attribute3Type)
        {
            if (attribute3Type != null)
            {
                AttributeCode = attribute3Type.AttributeCode;
                AttributeValue = attribute3Type.AttributeValue;
                Number = attribute3Type.number;
            }
        }
        public string AttributeCode { get; set; }
        public string AttributeValue { get; set; }
        public short Number { get; set; }
    }
}
