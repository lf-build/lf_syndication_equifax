﻿namespace LendFoundry.Syndication.Equifax.CreditReportResponse
{
    public interface IYearlyIncomeType
    {
        IDateType Year { get; set; }
        IMoneyType Income { get; set; }
        short Number { get; set; }
    }
}
