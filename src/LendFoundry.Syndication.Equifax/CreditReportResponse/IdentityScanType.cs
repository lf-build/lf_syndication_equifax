using System.Collections.Generic;
using System.Linq;

namespace LendFoundry.Syndication.Equifax.CreditReportResponse
{
    public class IdentityScanType : IIdentityScanType
    {
        public IdentityScanType(Proxy.CreditReportResponse.IdentityScanType identityScanType)
        {
            if (identityScanType?.AlertCodes != null)
                AlertCodes = identityScanType.AlertCodes.Select(p => new CodeType(p)).ToList<ICodeType>();
        }

        public List<ICodeType> AlertCodes { get; set; }
    }
}