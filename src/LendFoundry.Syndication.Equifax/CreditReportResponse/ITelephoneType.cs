﻿namespace LendFoundry.Syndication.Equifax.CreditReportResponse
{
    public interface ITelephoneType
    {
        IParsedTelephoneType ParsedTelephoneType { get; set; }
        string UnparsedTelephone { get; set; }
    }
}