﻿namespace LendFoundry.Syndication.Equifax.CreditReportResponse
{
    public interface IPropertyDataAndAnalyticHPIType
    {
        ICodeType HitNoHitIndicator { get; set; }
        IPropertyDataCommonType BlendedMedianPriceChangeZipCode { get; set; }
        IPropertyDataCommonType BlendedMedianPriceChangeForSegorNeighborhood { get; set; }
        IPropertyDataCommonType BlendedMedianPriceChangeState { get; set; }
        IPropertyDataCommonType BlendedMedianPriceChangeCounty { get; set; }
        IPropertyDataCommonType RetailMedianPriceChangeZipCode { get; set; }
        IPropertyDataCommonType RetailMedianPriceChangeSegment { get; set; }
        IPropertyDataCommonType RetailMedianPriceChangeState { get; set; }
        IPropertyDataCommonType RetailMedianPriceChangeCounty { get; set; }
        IPropertyDataCommonType REOMedianPriceChangeZipCode { get; set; }
        IPropertyDataCommonType REOBlendedMedianPriceChangeSegment { get; set; }
        IPropertyDataCommonType REOMedianPriceChangeState { get; set; }
        IPropertyDataCommonType REOMedianPriceChangeCounty { get; set; }
    }
}
