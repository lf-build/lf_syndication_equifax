namespace LendFoundry.Syndication.Equifax.CreditReportResponse
{
    public interface ICityType
    {
        string Code { get; set; }
        string Value { get; set; }
    }
}