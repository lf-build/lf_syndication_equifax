﻿namespace LendFoundry.Syndication.Equifax.CreditReportResponse
{
    public class DerrType : IDerrType
    {
        public DerrType(Proxy.CreditReportResponse.DerrType derrType)
        {
            if (derrType != null)
            {
                ErrorVerbiage = derrType.ErrorVerbiage;
                ErrorText = derrType.ErrorText;
                Number = derrType.number;
            }
        }
        public string ErrorVerbiage { get; set; }
        public string ErrorText { get; set; }
        public short Number { get; set; }
    }
}
