namespace LendFoundry.Syndication.Equifax.CreditReportResponse
{
    public interface IUsOtherTelephoneNumberType
    {
        string TelephoneNumber { get; set; }
        ICodeType TelephoneType { get; set; }
        string InternationalCallingCode { get; set; }
        IDateType DateTelephoneReported { get; set; }
        ICodeType TelephoneSource { get; set; }
        IDateType VerificationDate { get; set; }
    }
}