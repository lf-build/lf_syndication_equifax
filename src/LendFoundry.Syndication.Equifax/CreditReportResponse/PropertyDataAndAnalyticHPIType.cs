﻿namespace LendFoundry.Syndication.Equifax.CreditReportResponse
{
    public class PropertyDataAndAnalyticHPIType : IPropertyDataAndAnalyticHPIType
    {
        public PropertyDataAndAnalyticHPIType(Proxy.CreditReportResponse.PropertyDataAndAnalyticHPIType propertyDataAndAnalyticHPIType)
        {
            if (propertyDataAndAnalyticHPIType != null)
            {
                if (propertyDataAndAnalyticHPIType.HitNoHitIndicator != null)
                    HitNoHitIndicator = new CodeType(propertyDataAndAnalyticHPIType.HitNoHitIndicator);
                if (propertyDataAndAnalyticHPIType.BlendedMedianPriceChangeZipCode != null)
                    BlendedMedianPriceChangeZipCode = new PropertyDataCommonType(propertyDataAndAnalyticHPIType.BlendedMedianPriceChangeZipCode);
                if (propertyDataAndAnalyticHPIType.BlendedMedianPriceChangeForSegorNeighborhood != null)
                    BlendedMedianPriceChangeForSegorNeighborhood = new PropertyDataCommonType(propertyDataAndAnalyticHPIType.BlendedMedianPriceChangeForSegorNeighborhood);
                if (propertyDataAndAnalyticHPIType.BlendedMedianPriceChangeState != null)
                    BlendedMedianPriceChangeState = new PropertyDataCommonType(propertyDataAndAnalyticHPIType.BlendedMedianPriceChangeState);
                if (propertyDataAndAnalyticHPIType.BlendedMedianPriceChangeCounty != null)
                    BlendedMedianPriceChangeCounty = new PropertyDataCommonType(propertyDataAndAnalyticHPIType.BlendedMedianPriceChangeCounty);
                if (propertyDataAndAnalyticHPIType.RetailMedianPriceChangeZipCode != null)
                    RetailMedianPriceChangeZipCode = new PropertyDataCommonType(propertyDataAndAnalyticHPIType.RetailMedianPriceChangeZipCode);
                if (propertyDataAndAnalyticHPIType.RetailMedianPriceChangeSegment != null)
                    RetailMedianPriceChangeSegment = new PropertyDataCommonType(propertyDataAndAnalyticHPIType.RetailMedianPriceChangeSegment);
                if (propertyDataAndAnalyticHPIType.RetailMedianPriceChangeState != null)
                    RetailMedianPriceChangeState = new PropertyDataCommonType(propertyDataAndAnalyticHPIType.RetailMedianPriceChangeState);
                if (propertyDataAndAnalyticHPIType.RetailMedianPriceChangeCounty != null)
                    RetailMedianPriceChangeCounty = new PropertyDataCommonType(propertyDataAndAnalyticHPIType.RetailMedianPriceChangeCounty);
                if (propertyDataAndAnalyticHPIType.REOMedianPriceChangeZipCode != null)
                    REOMedianPriceChangeZipCode = new PropertyDataCommonType(propertyDataAndAnalyticHPIType.REOMedianPriceChangeZipCode);
                if (propertyDataAndAnalyticHPIType.REOBlendedMedianPriceChangeSegment != null)
                    REOBlendedMedianPriceChangeSegment = new PropertyDataCommonType(propertyDataAndAnalyticHPIType.REOBlendedMedianPriceChangeSegment);
                if (propertyDataAndAnalyticHPIType.REOMedianPriceChangeState != null)
                    REOMedianPriceChangeState = new PropertyDataCommonType(propertyDataAndAnalyticHPIType.REOMedianPriceChangeState);
                if (propertyDataAndAnalyticHPIType.REOMedianPriceChangeCounty != null)
                    REOMedianPriceChangeCounty = new PropertyDataCommonType(propertyDataAndAnalyticHPIType.REOMedianPriceChangeCounty);
            }
        }
        public ICodeType HitNoHitIndicator { get; set; }
        public IPropertyDataCommonType BlendedMedianPriceChangeZipCode { get; set; }
        public IPropertyDataCommonType BlendedMedianPriceChangeForSegorNeighborhood { get; set; }
        public IPropertyDataCommonType BlendedMedianPriceChangeState { get; set; }
        public IPropertyDataCommonType BlendedMedianPriceChangeCounty { get; set; }
        public IPropertyDataCommonType RetailMedianPriceChangeZipCode { get; set; }
        public IPropertyDataCommonType RetailMedianPriceChangeSegment { get; set; }
        public IPropertyDataCommonType RetailMedianPriceChangeState { get; set; }
        public IPropertyDataCommonType RetailMedianPriceChangeCounty { get; set; }
        public IPropertyDataCommonType REOMedianPriceChangeZipCode { get; set; }
        public IPropertyDataCommonType REOBlendedMedianPriceChangeSegment { get; set; }
        public IPropertyDataCommonType REOMedianPriceChangeState { get; set; }
        public IPropertyDataCommonType REOMedianPriceChangeCounty { get; set; }
    }
}
