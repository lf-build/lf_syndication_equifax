namespace LendFoundry.Syndication.Equifax.CreditReportResponse
{
    public interface IUsAlertContactTypePhoneNumber
    {
        CodeType TelephoneType { get; set; }
        string InternationalCallingCode { get; set; }
        string TelephoneNumber { get; set; }
        string TelephoneExtension { get; set; }
    }
}