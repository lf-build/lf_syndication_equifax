﻿namespace LendFoundry.Syndication.Equifax.CreditReportResponse
{
    public interface IUsInquiryType
    {
        ICustomerIdType CustomerId { get; set; }
        IDateType DateOfInquiry { get; set; }
        string EndUserText { get; set; }
        ICodeType InquiryAbbreviation { get; set; }
        ICodeType InquiryIntent { get; set; }
        ICodeType PermissiblePurposeCode { get; set; }
    }
}