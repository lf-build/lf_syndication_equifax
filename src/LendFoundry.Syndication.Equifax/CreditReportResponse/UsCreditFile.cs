using System.Collections.Generic;
using System.Linq;
using LendFoundry.Syndication.Equifax.Proxy.CreditReportResponse;

namespace LendFoundry.Syndication.Equifax.CreditReportResponse
{
    public class UsCreditFile : IUsCreditFile
    {
        public UsCreditFile(USHeaderTypeCreditFile usCreditFile)
        {
            if (usCreditFile != null)
            {
                if (usCreditFile.HitCode != null)
                    HitCode = new CodeType(usCreditFile.HitCode);
                if (usCreditFile.FileSinceDate != null)
                    FileSinceDate = new DateType(usCreditFile.FileSinceDate);
                if (usCreditFile.DateOfLastActivity != null)
                    DateOfLastActivity = new DateType(usCreditFile.DateOfLastActivity);
                if (usCreditFile.DateOfRequest != null)
                    DateOfRequest = new DateType(usCreditFile.DateOfRequest);
                if (usCreditFile.LinkIndicator != null)
                    LinkIndicator = new CodeType(usCreditFile.LinkIndicator);
                if (usCreditFile.DoNotCombineIndicator != null)
                    DoNotCombineIndicator = new CodeType(usCreditFile.DoNotCombineIndicator);
                if (usCreditFile.FraudVictimIndicator != null)
                    FraudVictimIndicator = new CodeType(usCreditFile.FraudVictimIndicator);

                if (usCreditFile.Safescan != null)
                    Safescans = new List<ICodeType>() { new CodeType(usCreditFile.Safescan)};
                 
                //usCreditFile.Safescan.Select(p => new CodeType(p)).ToList<ICodeType>();

                if (usCreditFile.Identityscans != null)
                    Identityscans = usCreditFile.Identityscans.Select(p => new CodeType(p)).ToList<ICodeType>();
                NameMatch = usCreditFile.NameMatch;
                AddressDiscrepancyIndicator = usCreditFile.AddressDiscrepancyIndicator;
                //if (usCreditFile.SSNAffirmIndicator != null)
                SsnAffirmIndicator = usCreditFile.SSNAffirmIndicator;//new CodeType();
                CreateCode = usCreditFile.CreateCode;
                FileStatus1 = usCreditFile.FileStatus1;
                FileStatus2 = usCreditFile.FileStatus2;
                FileStatus3 = usCreditFile.FileStatus3;
                BureauCode = usCreditFile.BureauCode;
            }
        }

        public ICodeType HitCode { get; set; }

        public IDateType FileSinceDate { get; set; }

        public IDateType DateOfLastActivity { get; set; }

        public IDateType DateOfRequest { get; set; }

        public ICodeType LinkIndicator { get; set; }

        public ICodeType DoNotCombineIndicator { get; set; }

        public ICodeType FraudVictimIndicator { get; set; }

        public List<ICodeType> Safescans { get; set; }

        public List<ICodeType> Identityscans { get; set; }

        public string NameMatch { get; set; }

        public string AddressDiscrepancyIndicator { get; set; }

        //TODO: Changed CodeType to string
        public string SsnAffirmIndicator { get; set; }

        public string CreateCode { get; set; }

        public string FileStatus1 { get; set; }

        public string FileStatus2 { get; set; }

        public string FileStatus3 { get; set; }

        public string BureauCode { get; set; }
    }
}