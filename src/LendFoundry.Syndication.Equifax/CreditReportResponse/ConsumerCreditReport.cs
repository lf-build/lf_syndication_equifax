using System.Collections.Generic;

namespace LendFoundry.Syndication.Equifax.CreditReportResponse
{
    public class ConsumerCreditReport : IConsumerCreditReport
    {
        public ConsumerCreditReport(Proxy.CreditReportResponse.EfxTransmitEfxReport efxReport)
        {
            if (efxReport?.USConsumerCreditReports?.Items != null)
            {
                foreach (var item in efxReport.USConsumerCreditReports.Items)
                {
                    if (item != null)
                    {
                        if (item.GetType() == typeof(Proxy.CreditReportResponse.USConsumerCreditReportType))
                        {
                            if (UsConsumerCreditReportType == null)
                                UsConsumerCreditReportType = new List<IUsConsumerCreditReportType>();

                            UsConsumerCreditReportType.Add(
                                new UsConsumerCreditReportType(
                                    (Proxy.CreditReportResponse.USConsumerCreditReportType)item));
                        }
                        else if (item.GetType() ==
                                 typeof(
                                     Proxy.CreditReportResponse.
                                         EfxTransmitEfxReportUSConsumerCreditReportsUSErrorMessages))
                        {
                            UsErrorMessages =
                                new UsErrorMessages(
                                    (
                                        Proxy.CreditReportResponse.
                                            EfxTransmitEfxReportUSConsumerCreditReportsUSErrorMessages)item);
                        }
                    }
                }
            }

            if (efxReport != null)
            {
                UsPrintImage = efxReport.USPrintImage;
                RequestNumber = efxReport.requestNumber;
                ReportId = efxReport.reportId;
            }
        }

        public List<IUsConsumerCreditReportType> UsConsumerCreditReportType { get; set; }
        public IUsErrorMessages UsErrorMessages { get; set; }
        public string UsPrintImage { get; set; }
        public short RequestNumber { get; set; }
        public string ReportId { get; set; }
    }
}