﻿namespace LendFoundry.Syndication.Equifax.CreditReportResponse
{
    public class PropertyOwnersNamesType : IPropertyOwnersNamesType
    {
        public PropertyOwnersNamesType(Proxy.CreditReportResponse.PropertyOwnersNamesType propertyOwnersNamesType)
        {
            if (propertyOwnersNamesType != null)
            {
                if (propertyOwnersNamesType.PropertyOwners != null)
                    PropertyOwners = new CouplesNamesType(propertyOwnersNamesType.PropertyOwners);
                Number = propertyOwnersNamesType.number;
            }
        }
        public ICouplesNamesType PropertyOwners { get; set; }
        public short Number { get; set; }
    }
}
