﻿namespace LendFoundry.Syndication.Equifax.CreditReportResponse
{
    public interface IPropertyDataAndAnalyticLIENDetailType
    {
        string TypeofLien { get; set; }
        IMoneyType AmountofLien { get; set; }
        IDateType OriginationDateofLien { get; set; }
        string InterestRateofLien { get; set; }
        string InterestRateTypeofLien { get; set; }
        string NameofLender { get; set; }
        string ModeledMortgageType { get; set; }
        short Number { get; set; }
    }
}
