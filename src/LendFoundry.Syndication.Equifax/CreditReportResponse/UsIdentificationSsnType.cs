namespace LendFoundry.Syndication.Equifax.CreditReportResponse
{
    public class UsIdentificationSsnType : IUsIdentificationSsnType
    {
        public UsIdentificationSsnType(Proxy.CreditReportResponse.USIdentificationSSNType usIdentificationSsnType)
        {
            if (usIdentificationSsnType != null)
            {
                MdbSubjectAge = usIdentificationSsnType.MDBSubjectAge;
                MdbSubjectSsn = usIdentificationSsnType.MDBSubjectSSN;
                MdbSubjectSsnConfirmed = usIdentificationSsnType.MDBSubjectSSNConfirmed;
                SsnMatch = usIdentificationSsnType.SSNMatch;
                InquirySubjectSsn = usIdentificationSsnType.InquirySubjectSSN;
                InquirySsnDateIssued = usIdentificationSsnType.InquirySSNDateIssued;
                if (usIdentificationSsnType.InquirySSNDateIssuedRange != null)
                    InquirySsnDateIssuedRange = new DateType(usIdentificationSsnType.InquirySSNDateIssuedRange);
                InquirySsnStateIssued = usIdentificationSsnType.InquirySSNStateIssued;
                InquirySsnDateOfDeath = usIdentificationSsnType.InquirySSNDateOfDeath;
                InquirySsnStateOfDeath = usIdentificationSsnType.InquirySSNStateOfDeath;
                InquirySsnByteToByteMatch = usIdentificationSsnType.InquirySSNByteToByteMatch;
            }
        }

        public string MdbSubjectAge { get; set; }

        public string MdbSubjectSsn { get; set; }

        public string MdbSubjectSsnConfirmed { get; set; }

        public string SsnMatch { get; set; }

        public string InquirySubjectSsn { get; set; }

        public string InquirySsnDateIssued { get; set; }

        public IDateType InquirySsnDateIssuedRange { get; set; }

        public string InquirySsnStateIssued { get; set; }

        public string InquirySsnDateOfDeath { get; set; }

        public string InquirySsnStateOfDeath { get; set; }

        public string InquirySsnByteToByteMatch { get; set; }
    }
}