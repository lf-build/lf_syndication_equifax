﻿namespace LendFoundry.Syndication.Equifax.CreditReportResponse
{
    public interface IUsRequest
    {
        string ConsumerReferralCode { get; set; }
        string CustomerNumber { get; set; }
        string CustomerReferenceNumber { get; set; }
        string DpmvTransactionNumber { get; set; }
        EcoaInquiryType EcoaInquiryType { get; set; }
        string NumberOfMonthsToCountInquiries { get; set; }
    }
}