using LendFoundry.Syndication.Equifax.Proxy.CreditReportResponse;

namespace LendFoundry.Syndication.Equifax.CreditReportResponse
{
    public class UsAddressType : IUsAddressType
    {
        public UsAddressType(USAddressType usAdressType)
        {
            var parsedStreetAddressType = usAdressType.Item as ParsedStreetAddressType;
            if (parsedStreetAddressType != null)
            {
                StreetPreDirection = parsedStreetAddressType.StreetPreDirection;
                StreetNumber = parsedStreetAddressType.StreetNumber;
                StreetName = parsedStreetAddressType.StreetName;
                StreetType = parsedStreetAddressType.StreetType;
                StreetPostDirection = parsedStreetAddressType.StreetPostDirection;
                UnitDesignator = parsedStreetAddressType.UnitDesignator;
                UnitNumber = parsedStreetAddressType.UnitNumber;
                UrbanizationCondo = parsedStreetAddressType.UrbanizationCondo;
            }

            //TODO: What if un parsed street address type
            //var unParsedStreetAddressType = usAdressType.Item as UnparsedStreetAddressType;
            //if (unParsedStreetAddressType != null)
            //{
            //    StreetPreDirection = unParsedStreetAddressType.StreetPreDirection;
            //    StreetNumber = unParsedStreetAddressType.StreetNumber;
            //    StreetName = unParsedStreetAddressType.StreetName;
            //    StreetType = unParsedStreetAddressType.StreetType;
            //    StreetPostDirection = unParsedStreetAddressType.StreetPostDirection;
            //    UnitDesignator = unParsedStreetAddressType.UnitDesignator;
            //    UnitNumber = unParsedStreetAddressType.UnitNumber;
            //    UrbanizationCondo = unParsedStreetAddressType.UrbanizationCondo;
            //}

            if (usAdressType.City != null)
                City = new CityType(usAdressType.City);
            State = usAdressType.State;
            PostalCode = usAdressType.PostalCode;
            if (usAdressType.DateAddressFirstReported != null)
                DateAddressFirstReported = new DateType(usAdressType.DateAddressFirstReported);
            if (usAdressType.RentOwnBuy != null)
                RentOwnBuy = new CodeType(usAdressType.RentOwnBuy);

            if (usAdressType.AddressSource != null)
                AddressSource = new CodeType(usAdressType.AddressSource);

            if (usAdressType.DateAddressLastReported != null)
                DateAddressLastReported = new DateType(usAdressType.DateAddressLastReported);
            if (usAdressType.Telephone != null)
                Telephone = new TelephoneType(usAdressType.Telephone);
            if (usAdressType.TelephoneSource != null)
                TelephoneSource = new CodeType(usAdressType.TelephoneSource);

            if (usAdressType.DateTelephoneReported != null)
                DateTelephoneReported = new DateType(usAdressType.DateTelephoneReported);

            //TODO: Not Found
            //if (usAdressType.StandardizationIndicator != null)
            //    StandardizationIndicator = new CodeType(usAdressType.StandardizationIndicator);

            //TODO: Not Found
            //if (usAdressType.Verification != null)
            //    Verification = new CodeDateType(new Proxy.CreditReportResponse.CodeType { code = usAdressType.Verification.code, description = usAdressType.Verification.description }, usAdressType.Verification);

            //TODO: Not Found
            //CountryCode = usAdressType.CountryCode;

            //TODO: Not Found
            //if (usAdressType.AddressIndicator != null)
            //    AddressIndicator = new CodeType(usAdressType.AddressIndicator);

            if (usAdressType.AddressVariance != null)
                AddressVariance = new CodeType(usAdressType.AddressVariance);

            Code = usAdressType.code;
            Description = usAdressType.description;
        }

        public string StreetPreDirection { get; set; }

        public string StreetNumber { get; set; }

        public string StreetName { get; set; }

        public string StreetType { get; set; }

        public string StreetPostDirection { get; set; }

        public string UnitDesignator { get; set; }

        public string UnitNumber { get; set; }

        public string UrbanizationCondo { get; set; }

        public ICityType City { get; set; }

        public string State { get; set; }

        public string PostalCode { get; set; }

        public IDateType DateAddressFirstReported { get; set; }

        public ICodeType RentOwnBuy { get; set; }

        public ICodeType AddressSource { get; set; }

        public IDateType DateAddressLastReported { get; set; }

        public ITelephoneType Telephone { get; set; }

        public ICodeType TelephoneSource { get; set; }

        public IDateType DateTelephoneReported { get; set; }

        public ICodeType StandardizationIndicator { get; set; }

        public ICodeDateType Verification { get; set; }

        public string CountryCode { get; set; }

        public ICodeType AddressIndicator { get; set; }

        public ICodeType AddressVariance { get; set; }

        public string Code { get; set; }

        public string Description { get; set; }
    }
}