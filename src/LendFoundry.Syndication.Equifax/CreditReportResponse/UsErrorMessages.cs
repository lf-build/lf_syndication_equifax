namespace LendFoundry.Syndication.Equifax.CreditReportResponse
{
    public class UsErrorMessages : IUsErrorMessages
    {
        public UsErrorMessages(
            Proxy.CreditReportResponse.EfxTransmitEfxReportUSConsumerCreditReportsUSErrorMessages usErrorMessages)
        {
            if (usErrorMessages?.USErrorMessage != null)
                UsErrorMessage = new UsErrorMessageType(usErrorMessages.USErrorMessage);
        }

        public IUsErrorMessageType UsErrorMessage { get; set; }
    }
}