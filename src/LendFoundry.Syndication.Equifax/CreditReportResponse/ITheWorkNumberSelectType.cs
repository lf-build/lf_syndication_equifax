﻿using System.Collections.Generic;

namespace LendFoundry.Syndication.Equifax.CreditReportResponse
{
    public interface ITheWorkNumberSelectType
    {
        ICodeType RegulatedIdentifier { get; set; }
        ICodeType HitNoHitIndicator { get; set; }
        string TwnDisclaimer { get; set; }
        ISubjectNameFMLType EmployeeName { get; set; }
        string EmployeeSSN { get; set; }
        string EmployerName { get; set; }
        IDateType DateEmployeeInformationisEffective { get; set; }
        string EmployeePositionNameorTitle { get; set; }
        IMoneyType RateofpayperPayPeriod { get; set; }
        string FrequencyDescription { get; set; }
        IMoneyType CalculatedAnnualIncome { get; set; }
        string HoursWorkedPerPayPeriod { get; set; }
        IDateType MostRecentStartDate { get; set; }
        string TotalLengthofServiceinMonths { get; set; }
        IDateType DateEmploymentTerminated { get; set; }
        string StatusMessage { get; set; }
        List<IYearlyIncomeType> YearlyIncomes { get; set; }
        string EmployerCode { get; set; }
        string EmployerDisclaimer { get; set; }
        string ReferenceNumberServerId { get; set; }
        string ReferralURL { get; set; }
        string ReferralBureauName { get; set; }
        ISimpleAddressType ReferralBureauAddress { get; set; }
    }
}
