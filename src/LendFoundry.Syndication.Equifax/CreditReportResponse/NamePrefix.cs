namespace LendFoundry.Syndication.Equifax.CreditReportResponse
{
    public enum NamePrefix
    {
        Mr,

        Mrs,

        Ms,

        Dr,

        Miss,

        Master,
    }
}