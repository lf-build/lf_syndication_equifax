﻿using System.Collections.Generic;

namespace LendFoundry.Syndication.Equifax.CreditReportResponse
{
    public interface IUsBeaconType
    {
        ICodeType BeaconIndicator { get; set; }
        string BeaconScore { get; set; }
        object DoddFrank { get; set; }
        string FactActInquiriesAreKeyFactor { get; set; }
        ICodeType RejectCode { get; set; }
        object RiskBasedPricing { get; set; }
        List<IScoreReasonType> ScoreReasons { get; set; }
    }
}