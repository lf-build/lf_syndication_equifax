﻿namespace LendFoundry.Syndication.Equifax.CreditReportResponse
{
    public class PropertyDataAndAnalyticMKTType : IPropertyDataAndAnalyticMKTType
    {
        public PropertyDataAndAnalyticMKTType(Proxy.CreditReportResponse.PropertyDataAndAnalyticMKTType propertyDataAndAnalyticMKTType)
        {
            if (propertyDataAndAnalyticMKTType != null)
            {
                if (propertyDataAndAnalyticMKTType.HitNoHitIndicator != null)
                    HitNoHitIndicator = new CodeType(propertyDataAndAnalyticMKTType.HitNoHitIndicator);
                PercentofNonOwnerOccupiedSegment = propertyDataAndAnalyticMKTType.PercentofNonOwnerOccupiedSegment;
                PercentofNonOwnerOccupiedZip = propertyDataAndAnalyticMKTType.PercentofNonOwnerOccupiedZip;
                PercentSpreadMedianListSaleSeg = propertyDataAndAnalyticMKTType.PercentSpreadMedianListSaleSeg;
                PercentSpreadMedianListSaleZip = propertyDataAndAnalyticMKTType.PercentSpreadMedianListSaleZip;
                AvgDaysonMarketSegment = propertyDataAndAnalyticMKTType.AvgDaysonMarketSegment;
                AvgDaysonMarketZip = propertyDataAndAnalyticMKTType.AvgDaysonMarketZip;
                AvgLotSize = propertyDataAndAnalyticMKTType.AvgLotSize;
                AvgLotSizeSpecified = propertyDataAndAnalyticMKTType.AvgLotSizeSpecified;
                AvgMainSquareFeet = propertyDataAndAnalyticMKTType.AvgMainSquareFeet;
                AvgMainSquareFeetSpecified = propertyDataAndAnalyticMKTType.AvgMainSquareFeetSpecified;
                AvgNumberofBathrooms = propertyDataAndAnalyticMKTType.AvgNumberofBathrooms;
                AvgNumberofBathroomsSpecified = propertyDataAndAnalyticMKTType.AvgNumberofBathroomsSpecified;
                AvgNumberofBedrooms = propertyDataAndAnalyticMKTType.AvgNumberofBedrooms;
                AvgNumberofBedroomsSpecified = propertyDataAndAnalyticMKTType.AvgNumberofBedroomsSpecified;
                AvgNumberofMonthlySales = propertyDataAndAnalyticMKTType.AvgNumberofMonthlySales;
                AvgNumberofMonthlySalesSpecified = propertyDataAndAnalyticMKTType.AvgNumberofMonthlySalesSpecified;
                AvgNumberofMonthlySalesZip = propertyDataAndAnalyticMKTType.AvgNumberofMonthlySalesZip;
                AvgNumberofMonthlySalesZipSpecified = propertyDataAndAnalyticMKTType.AvgNumberofMonthlySalesZipSpecified;
                AvgPricePerSquareFoot = propertyDataAndAnalyticMKTType.AvgPricePerSquareFoot;
                AvgPricePerSquareFootSpecified = propertyDataAndAnalyticMKTType.AvgPricePerSquareFootSpecified;
                AvgYearsofConstruction = propertyDataAndAnalyticMKTType.AvgYearsofConstruction;
                AvgYearsofConstructionSpecified = propertyDataAndAnalyticMKTType.AvgYearsofConstructionSpecified;
                EstMonthsofCurrentInvestment = propertyDataAndAnalyticMKTType.EstMonthsofCurrentInvestment;
                EstMonthsofCurrentInvestmentZip = propertyDataAndAnalyticMKTType.EstMonthsofCurrentInvestmentZip;
                HighListRangeSeg = propertyDataAndAnalyticMKTType.HighListRangeSeg;
                HighListRangeZip = propertyDataAndAnalyticMKTType.HighListRangeZip;
                HighSaleRangeSeg = propertyDataAndAnalyticMKTType.HighSaleRangeSeg;
                HighSaleRangeZip = propertyDataAndAnalyticMKTType.HighSaleRangeZip;
                LowListRangeSeg = propertyDataAndAnalyticMKTType.LowListRangeSeg;
                LowListRangeZip = propertyDataAndAnalyticMKTType.LowListRangeZip;
                LowSaleRangeSeg = propertyDataAndAnalyticMKTType.LowSaleRangeSeg;
                LowSaleRangeZip = propertyDataAndAnalyticMKTType.LowSaleRangeZip;
                MedianSale = propertyDataAndAnalyticMKTType.MedianSale;
                MedianSaleZip = propertyDataAndAnalyticMKTType.MedianSaleZip;
                NoticeofDefaultRateSeg = propertyDataAndAnalyticMKTType.NoticeofDefaultRateSeg;
                NoticeofDefaultRateZip = propertyDataAndAnalyticMKTType.NoticeofDefaultRateZip;
                NoticeofTrusteeSaleRateSeg = propertyDataAndAnalyticMKTType.NoticeofTrusteeSaleRateSeg;
                NoticeofTrusteeSaleRateZip = propertyDataAndAnalyticMKTType.NoticeofTrusteeSaleRateZip;
                ReoRate1stQtr = propertyDataAndAnalyticMKTType.ReoRate1stQtr;
                ReoRate1stQtrZip = propertyDataAndAnalyticMKTType.ReoRate1stQtrZip;
                ReoRate1YearSegment = propertyDataAndAnalyticMKTType.ReoRate1YearSegment;
                ReoRate1yearZip = propertyDataAndAnalyticMKTType.ReoRate1yearZip;
                ReoRate2YearSegment = propertyDataAndAnalyticMKTType.ReoRate2YearSegment;
                ReoRate2yearZip = propertyDataAndAnalyticMKTType.ReoRate2yearZip;
                ReoSaleRate1stQtr = propertyDataAndAnalyticMKTType.ReoSaleRate1stQtr;
                ReoSaleRate1stQtrZip = propertyDataAndAnalyticMKTType.ReoSaleRate1stQtrZip;
                ReoSaleRate1YearSegment = propertyDataAndAnalyticMKTType.ReoSaleRate1YearSegment;
                ReoSaleRate1YearZip = propertyDataAndAnalyticMKTType.ReoSaleRate1YearZip;
                ReoSaleRate2YearSegment = propertyDataAndAnalyticMKTType.ReoSaleRate2YearSegment;
                ReoSaleRate2YearZip = propertyDataAndAnalyticMKTType.ReoSaleRate2YearZip;
                TotalLoanValueSeg = propertyDataAndAnalyticMKTType.TotalLoanValueSeg;
                TotalLoanValueZip = propertyDataAndAnalyticMKTType.TotalLoanValueZip;
            }
        }
        public ICodeType HitNoHitIndicator { get; set; }
        public string PercentofNonOwnerOccupiedSegment { get; set; }
        public string PercentofNonOwnerOccupiedZip { get; set; }
        public string PercentSpreadMedianListSaleSeg { get; set; }
        public string PercentSpreadMedianListSaleZip { get; set; }
        public string AvgDaysonMarketSegment { get; set; }
        public string AvgDaysonMarketZip { get; set; }
        public short AvgLotSize { get; set; }
        public bool AvgLotSizeSpecified { get; set; }
        public short AvgMainSquareFeet { get; set; }
        public bool AvgMainSquareFeetSpecified { get; set; }
        public short AvgNumberofBathrooms { get; set; }
        public bool AvgNumberofBathroomsSpecified { get; set; }
        public short AvgNumberofBedrooms { get; set; }
        public bool AvgNumberofBedroomsSpecified { get; set; }
        public short AvgNumberofMonthlySales { get; set; }
        public bool AvgNumberofMonthlySalesSpecified { get; set; }
        public short AvgNumberofMonthlySalesZip { get; set; }
        public bool AvgNumberofMonthlySalesZipSpecified { get; set; }
        public short AvgPricePerSquareFoot { get; set; }
        public bool AvgPricePerSquareFootSpecified { get; set; }
        public short AvgYearsofConstruction { get; set; }
        public bool AvgYearsofConstructionSpecified { get; set; }
        public string EstMonthsofCurrentInvestment { get; set; }
        public string EstMonthsofCurrentInvestmentZip { get; set; }
        public string HighListRangeSeg { get; set; }
        public string HighListRangeZip { get; set; }
        public string HighSaleRangeSeg { get; set; }
        public string HighSaleRangeZip { get; set; }
        public string LowListRangeSeg { get; set; }
        public string LowListRangeZip { get; set; }
        public string LowSaleRangeSeg { get; set; }
        public string LowSaleRangeZip { get; set; }
        public string MedianSale { get; set; }
        public string MedianSaleZip { get; set; }
        public string NoticeofDefaultRateSeg { get; set; }
        public string NoticeofDefaultRateZip { get; set; }
        public string NoticeofTrusteeSaleRateSeg { get; set; }
        public string NoticeofTrusteeSaleRateZip { get; set; }
        public string ReoRate1stQtr { get; set; }
        public string ReoRate1stQtrZip { get; set; }
        public string ReoRate1YearSegment { get; set; }
        public string ReoRate1yearZip { get; set; }
        public string ReoRate2YearSegment { get; set; }
        public string ReoRate2yearZip { get; set; }
        public string ReoSaleRate1stQtr { get; set; }
        public string ReoSaleRate1stQtrZip { get; set; }
        public string ReoSaleRate1YearSegment { get; set; }
        public string ReoSaleRate1YearZip { get; set; }
        public string ReoSaleRate2YearSegment { get; set; }
        public string ReoSaleRate2YearZip { get; set; }
        public string TotalLoanValueSeg { get; set; }
        public string TotalLoanValueZip { get; set; }
    }
}
