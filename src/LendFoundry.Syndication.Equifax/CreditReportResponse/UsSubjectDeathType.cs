namespace LendFoundry.Syndication.Equifax.CreditReportResponse
{
    public class UsSubjectDeathType : IUsSubjectDeathType
    {
        public UsSubjectDeathType(Proxy.CreditReportResponse.USSubjectDeathType usSubjectDeathType)
        {
            if (usSubjectDeathType?.DateOfDeath != null)
                DateOfDeath = new DateType(usSubjectDeathType.DateOfDeath);
        }

        public IDateType DateOfDeath { get; set; }
    }
}