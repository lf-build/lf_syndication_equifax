namespace LendFoundry.Syndication.Equifax.CreditReportResponse
{
    public class UsOtherNameType : PersonNameType, IUsOtherNameType
    {
        public UsOtherNameType(Proxy.CreditReportResponse.SubjectNameType personNameType, Proxy.CreditReportResponse.USOtherNameType usOtherNameType) : base(personNameType)
        {
            Code = usOtherNameType.code;
            Description = usOtherNameType.description;
        }

        public string Code { get; set; }

        public string Description { get; set; }
    }
}