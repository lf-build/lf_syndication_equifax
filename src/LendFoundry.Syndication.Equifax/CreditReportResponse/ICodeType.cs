namespace LendFoundry.Syndication.Equifax.CreditReportResponse
{
    public interface ICodeType
    {
        string Code { get; set; }

        string Description { get; set; }
    }
}