﻿namespace LendFoundry.Syndication.Equifax.CreditReportResponse
{
    public class UnparsedStreetAddressType : IUnparsedStreetAddressType
    {
        public UnparsedStreetAddressType(Proxy.CreditReportResponse.UnparsedStreetAddressType unparsedStreetAddressType)
        {
            if(unparsedStreetAddressType != null)
            {
                AddressLine1 = unparsedStreetAddressType.AddressLine1;
                AddressLine2 = unparsedStreetAddressType.AddressLine2;
            }
        }
        public string AddressLine1 { get; set; }
        public string AddressLine2 { get; set; }
    }
}
