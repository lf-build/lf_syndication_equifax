﻿using System.Collections.Generic;

namespace LendFoundry.Syndication.Equifax.CreditReportResponse
{
    public interface IUsLegalItemType
    {
        IMoneyType Amount { get; set; }
        string CaseNumber { get; set; }
        string Code { get; set; }
        ICustomerIdType CourtId { get; set; }
        IDateType DateFiled { get; set; }
        IDateType DateReported { get; set; }
        IDateType DateSatisfied { get; set; }
        string Defendant { get; set; }
        string Description { get; set; }
        List<ICodeType> Narratives { get; set; }
        string Plaintiff { get; set; }
        ICodeType Status { get; set; }
        IDateType VerificationDate { get; set; }
    }
}