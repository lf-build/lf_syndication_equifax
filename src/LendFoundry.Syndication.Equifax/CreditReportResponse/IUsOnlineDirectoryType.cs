﻿namespace LendFoundry.Syndication.Equifax.CreditReportResponse
{
    public interface IUsOnlineDirectoryType
    {
        string AddressLine1 { get; set; }
        string AddressLine2 { get; set; }
        ICityType City { get; set; }
        ICustomerIdType CustomerId { get; set; }
        string PostalCode { get; set; }
        string State { get; set; }
        IParsedTelephoneType Telephone { get; set; }

        string UnparsedTelephone { get; set;}
    }
}