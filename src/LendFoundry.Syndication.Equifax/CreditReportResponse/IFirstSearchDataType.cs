﻿namespace LendFoundry.Syndication.Equifax.CreditReportResponse
{
    public interface IFirstSearchDataType
    {
        ICodeType RegulatedIdentifier { get; set; }
        ICodeType HitNoHitIndicator { get; set; }
        string CustomerTrackingNumber { get; set; }
        string SocialSecurityNumber { get; set; }
        string CustomerName { get; set; }
        string AddrPhoneNumber { get; set; }
        IUnparsedStreetAddressType UnparsedStreetAddress { get; set; }
        string CityStateZip { get; set; }
        ITelephoneType Telephone { get; set; }
        IDateType DateReported { get; set; }
        IDateType DateOfBirth { get; set; }
    }
}
