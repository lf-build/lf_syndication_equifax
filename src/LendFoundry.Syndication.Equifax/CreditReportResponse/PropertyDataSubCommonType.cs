﻿namespace LendFoundry.Syndication.Equifax.CreditReportResponse
{
    public class PropertyDataSubCommonType : IPropertyDataSubCommonType
    {
        public PropertyDataSubCommonType(Proxy.CreditReportResponse.PropertyDataSubCommonType propertyDataSubCommonType)
        {
            if (propertyDataSubCommonType != null)
            {
                Quarter = propertyDataSubCommonType.Quarter;
                if (propertyDataSubCommonType.Year != null)
                    Year = new PropertyDataYearType(propertyDataSubCommonType.Year);
                if (propertyDataSubCommonType.SmoothedMedianPrice != null)
                    SmoothedMedianPrice = new MoneyType(propertyDataSubCommonType.SmoothedMedianPrice);
                SmoothedIndex = propertyDataSubCommonType.SmoothedIndex;
                SmoothedPercentChange = propertyDataSubCommonType.SmoothedPercentChange;
                Number = propertyDataSubCommonType.number;
            }
        }
        public string Quarter { get; set; }
        public IPropertyDataYearType Year { get; set; }
        public IMoneyType SmoothedMedianPrice { get; set; }
        public string SmoothedIndex { get; set; }
        public string SmoothedPercentChange { get; set; }
        public short Number { get; set; }
    }
}
