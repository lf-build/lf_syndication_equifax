namespace LendFoundry.Syndication.Equifax.CreditReportResponse
{
    public class CodeType : ICodeType
    {
        public CodeType(Proxy.CreditReportResponse.CodeType codeType)
        {
            if (codeType != null)
            {
                Code = codeType.code;
                Description = codeType.description;
            }
        }

        public string Code { get; set; }

        public string Description { get; set; }
    }
}