namespace LendFoundry.Syndication.Equifax.CreditReportResponse
{
    public enum CurrencyType
    {
        /// <remarks/>
        USD,

        /// <remarks/>
        CAD,

        /// <remarks/>
        EUR,

        /// <remarks/>
        GBP,
    }
}