﻿using System.Collections.Generic;

namespace LendFoundry.Syndication.Equifax.CreditReportResponse
{
    public interface IUsGeoCodeType
    {
        ICityType City { get; set; }
        string GeoBlockGroup { get; set; }
        string GeoCensusTract { get; set; }
        string GeoCountyCode { get; set; }
        List<string> GeoReturnCodes { get; set; }
        string GeoSmsa { get; set; }
        string GeoStateCode { get; set; }
        string GeoSuffix { get; set; }
        string MicroVisionCode { get; set; }
        string MicroVisionReturnCode { get; set; }
        string PostalCode { get; set; }
        string State { get; set; }
        string StreetName { get; set; }
        string StreetNumber { get; set; }
        string StreetType { get; set; }
        ICodeType Type { get; set; }
    }
}