namespace LendFoundry.Syndication.Equifax.CreditReportResponse
{
    public class UsAlertContactTypePhoneNumber : IUsAlertContactTypePhoneNumber
    {
        public UsAlertContactTypePhoneNumber(
            Proxy.CreditReportResponse.USAlertContactTypePhoneNumber usAlertContactTypePhoneNumber)
        {
            if (usAlertContactTypePhoneNumber != null)
            {
                if (usAlertContactTypePhoneNumber.TelephoneType != null)
                    TelephoneType = new CodeType(usAlertContactTypePhoneNumber.TelephoneType);

                InternationalCallingCode = usAlertContactTypePhoneNumber.InternationalCallingCode;
                TelephoneNumber = usAlertContactTypePhoneNumber.TelephoneNumber;
                TelephoneExtension = usAlertContactTypePhoneNumber.TelephoneExtension;
            }
        }

        public CodeType TelephoneType { get; set; }

        public string InternationalCallingCode { get; set; }

        public string TelephoneNumber { get; set; }

        public string TelephoneExtension { get; set; }
    }
}