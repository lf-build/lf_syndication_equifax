﻿namespace LendFoundry.Syndication.Equifax.CreditReportResponse
{
    public interface IPropertyOwnersNamesType
    {
        ICouplesNamesType PropertyOwners { get; set; }
        short Number { get; set; }
    }
}
