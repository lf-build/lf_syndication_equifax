using System.Collections.Generic;
using System.Linq;

namespace LendFoundry.Syndication.Equifax.CreditReportResponse
{
    public class UsBankruptcyType : IUsBankruptcyType
    {
        public UsBankruptcyType(Proxy.CreditReportResponse.USBankruptcyType usBankruptcyType)
        {
            if (usBankruptcyType != null)
            {
                if (usBankruptcyType.DateFiled != null)
                    DateFiled = new DateType(usBankruptcyType.DateFiled);

                if (usBankruptcyType.CourtId != null)
                    CourtId = new CustomerIdType(usBankruptcyType.CourtId);

                CaseNumber = usBankruptcyType.CaseNumber;

                if (usBankruptcyType.Type != null)
                    Type = new CodeType(usBankruptcyType.Type);

                if (usBankruptcyType.Filer != null)
                    Filer = new CodeType(usBankruptcyType.Filer);

                if (usBankruptcyType.Disposition != null)
                    Disposition = new CodeDateType(new Proxy.CreditReportResponse.CodeType
                    {
                        code = usBankruptcyType.Disposition.code,
                        description = usBankruptcyType.Disposition.description
                    }, usBankruptcyType.Disposition);

                //TODO: Not Found
                //if (usBankruptcyType.LiabilityAmount != null)
                //    LiabilityAmount = new MoneyType(usBankruptcyType.LiabilityAmount);

                //TODO: Not Found
                //if (usBankruptcyType.AssetAmount != null)
                //    AssetAmount = new MoneyType(usBankruptcyType.AssetAmount);

                //TODO: Not Found
                //if (usBankruptcyType.ExemptAmount != null)
                //    ExemptAmount = new MoneyType(usBankruptcyType.ExemptAmount);

                //TODO: Changed from VerificationDate =>ExpandedVerificationDate
                if (usBankruptcyType.ExpandedVerificationDate != null)
                    VerificationDate = new DateType(usBankruptcyType.ExpandedVerificationDate);

                //TODO: Not Found
                //if (usBankruptcyType.PriorIntentCode != null)
                //    PriorIntentCode = new CodeType(usBankruptcyType.PriorIntentCode);

                //TODO: Changed from DateReported => ExpandedDateReported
                if (usBankruptcyType.ExpandedDateReported != null)
                    DateReported = new DateType(usBankruptcyType.ExpandedDateReported);

                if (usBankruptcyType.Narratives != null)
                    Narratives = usBankruptcyType.Narratives.Select(p => new CodeType(p)).ToList<ICodeType>();
            }
        }

        public IDateType DateFiled { get; set; }

        public ICustomerIdType CourtId { get; set; }

        public string CaseNumber { get; set; }

        public ICodeType Type { get; set; }

        public ICodeType Filer { get; set; }

        public ICodeDateType Disposition { get; set; }

        public IMoneyType LiabilityAmount { get; set; }

        public IMoneyType AssetAmount { get; set; }

        public IMoneyType ExemptAmount { get; set; }

        public IDateType VerificationDate { get; set; }

        public ICodeType PriorIntentCode { get; set; }

        public IDateType DateReported { get; set; }

        public List<ICodeType> Narratives { get; set; }
    }
}