﻿namespace LendFoundry.Syndication.Equifax.CreditReportResponse
{
    public interface IUsConsumerStatementType
    {
        IDateType DateReported { get; set; }
        IDateType DateToBePurged { get; set; }
        string Statement { get; set; }
    }
}