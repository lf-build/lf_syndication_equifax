﻿namespace LendFoundry.Syndication.Equifax.CreditReportResponse
{
    public interface IUsHistoryDerogatoryCounters
    {
        short Count30DayPastDue { get; set; }
        short Count60DayPastDue { get; set; }
        short Count90DayPastDue { get; set; }
    }
}