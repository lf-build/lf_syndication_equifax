﻿using System.Collections.Generic;

namespace LendFoundry.Syndication.Equifax.CreditReportResponse
{
    public interface IPropertyDataCommonType
    {
        string GeoAreaType { get; set; }
        string GeoValue { get; set; }
        List<IPropertyDataSubCommonType> PricingData { get; set; }
        short Number { get; set; }
    }
}
