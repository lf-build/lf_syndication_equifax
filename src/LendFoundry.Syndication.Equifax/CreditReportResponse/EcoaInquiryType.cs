namespace LendFoundry.Syndication.Equifax.CreditReportResponse
{
    public enum EcoaInquiryType
    {
        /// <remarks/>
        I,

        /// <remarks/>
        J,

        /// <remarks/>
        P,

        /// <remarks/>
        S,
    }
}