﻿namespace LendFoundry.Syndication.Equifax.CreditReportResponse
{
    public class PropertyDataAndAnalyticLIENDetailType : IPropertyDataAndAnalyticLIENDetailType
    {
        public PropertyDataAndAnalyticLIENDetailType(Proxy.CreditReportResponse.PropertyDataAndAnalyticLIENDetailType propertyDataAndAnalyticLIENDetailType)
        {
            if (propertyDataAndAnalyticLIENDetailType != null)
            {
                TypeofLien = propertyDataAndAnalyticLIENDetailType.TypeofLien;
                if (propertyDataAndAnalyticLIENDetailType.AmountofLien != null)
                    AmountofLien = new MoneyType(propertyDataAndAnalyticLIENDetailType.AmountofLien);
                if (propertyDataAndAnalyticLIENDetailType.OriginationDateofLien != null)
                    OriginationDateofLien = new DateType(propertyDataAndAnalyticLIENDetailType.OriginationDateofLien);
                InterestRateofLien = propertyDataAndAnalyticLIENDetailType.InterestRateofLien;
                InterestRateTypeofLien = propertyDataAndAnalyticLIENDetailType.InterestRateTypeofLien;
                NameofLender = propertyDataAndAnalyticLIENDetailType.NameofLender;
                ModeledMortgageType = propertyDataAndAnalyticLIENDetailType.ModeledMortgageType;
                Number = propertyDataAndAnalyticLIENDetailType.number;
            }
        }
        public string TypeofLien { get; set; }
        public IMoneyType AmountofLien { get; set; }
        public IDateType OriginationDateofLien { get; set; }
        public string InterestRateofLien { get; set; }
        public string InterestRateTypeofLien { get; set; }
        public string NameofLender { get; set; }
        public string ModeledMortgageType { get; set; }
        public short Number { get; set; }
    }
}
