﻿namespace LendFoundry.Syndication.Equifax.CreditReportResponse
{
    public class IxiServicesType : IIxiServicesType
    {
        public IxiServicesType(Proxy.CreditReportResponse.IXIServicesType ixiServicesType)
        {
            if (ixiServicesType != null)
            {
                if (ixiServicesType.RegulatedIdentifier != null)
                    RegulatedIdentifier = new CodeType(ixiServicesType.RegulatedIdentifier);
                if (ixiServicesType.HitNoHitIndicator != null)
                    HitNoHitIndicator = new CodeType(ixiServicesType.HitNoHitIndicator);
                Disclaimer = ixiServicesType.Disclaimer;
                if (ixiServicesType.WealthComplete != null)
                    WealthComplete = new WealthCompleteType(ixiServicesType.WealthComplete);
                if (ixiServicesType.Income360 != null)
                    Income360 = new Income360Type(ixiServicesType.Income360);
                if (ixiServicesType.DiscretionarySpending != null)
                    DiscretionarySpending = new DiscretionarySpendingType(ixiServicesType.DiscretionarySpending);
                if (ixiServicesType.AbilityToPay != null)
                    AbilityToPay = new AbilityToPayType(ixiServicesType.AbilityToPay);
            }
        }
        public ICodeType RegulatedIdentifier { get; set; }
        public ICodeType HitNoHitIndicator { get; set; }
        public string Disclaimer { get; set; }
        public IWealthCompleteType WealthComplete { get; set; }
        public IIncome360Type Income360 { get; set; }
        public IDiscretionarySpendingType DiscretionarySpending { get; set; }
        public IAbilityToPayType AbilityToPay { get; set; }
    }
}
