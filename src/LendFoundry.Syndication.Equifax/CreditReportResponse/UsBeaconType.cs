using System.Collections.Generic;
using System.Linq;

namespace LendFoundry.Syndication.Equifax.CreditReportResponse
{
    public class UsBeaconType : IUsBeaconType
    {
        public UsBeaconType(Proxy.CreditReportResponse.USBeaconType usBeaconType)
        {
            if (usBeaconType != null)
            {
                BeaconScore = usBeaconType.BeaconScore;
                if (usBeaconType.ScoreReasons != null)
                    ScoreReasons =
                        usBeaconType.ScoreReasons.Select(
                            p =>
                                new ScoreReasonType(
                                    new Proxy.CreditReportResponse.CodeType { code = p.code, description = p.description },
                                    p)).ToList<IScoreReasonType>();

                if (usBeaconType.RejectCode != null)
                    RejectCode = new CodeType(usBeaconType.RejectCode);

                RiskBasedPricing = usBeaconType.RiskBasedPricing;
                DoddFrank = usBeaconType.DoddFrank;
                //TODO: Changed from BeaconIndicator to ScoreIndicator
                if (usBeaconType.ScoreIndicator != null)
                    BeaconIndicator = new CodeType(usBeaconType.ScoreIndicator);
                FactActInquiriesAreKeyFactor = usBeaconType.FACTActInquiriesAreKeyFactor;
            }
        }

        public string BeaconScore { get; set; }

        public List<IScoreReasonType> ScoreReasons { get; set; }

        public ICodeType RejectCode { get; set; }

        public object RiskBasedPricing { get; set; }

        public object DoddFrank { get; set; }

        public ICodeType BeaconIndicator { get; set; }

        public string FactActInquiriesAreKeyFactor { get; set; }
    }
}