namespace LendFoundry.Syndication.Equifax.CreditReportResponse
{
    public class UsInquiryType : IUsInquiryType
    {
        public UsInquiryType(Proxy.CreditReportResponse.USInquiryType usInquiryType)
        {
            if (usInquiryType.DateOfInquiry != null)
                DateOfInquiry = new DateType(usInquiryType.DateOfInquiry);

            if (usInquiryType.InquiryAbbreviation != null)
                InquiryAbbreviation = new CodeType(usInquiryType.InquiryAbbreviation);

            EndUserText = usInquiryType.EndUserText;
            if (usInquiryType.CustomerId != null)
                CustomerId = new CustomerIdType(usInquiryType.CustomerId);

            if (usInquiryType.InquiryIntent != null)
                InquiryIntent = new CodeType(usInquiryType.InquiryIntent);

            //TODO: Not found PermissiblePurposeCode
            //if (usInquiryType.PermissiblePurposeCode != null)
            //    PermissiblePurposeCode = new CodeType(usInquiryType.PermissiblePurposeCode);
        }

        public IDateType DateOfInquiry { get; set; }

        public ICodeType InquiryAbbreviation { get; set; }

        public string EndUserText { get; set; }

        public ICustomerIdType CustomerId { get; set; }

        public ICodeType InquiryIntent { get; set; }

        public ICodeType PermissiblePurposeCode { get; set; }
    }
}