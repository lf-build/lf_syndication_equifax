﻿namespace LendFoundry.Syndication.Equifax.CreditReportResponse
{
    public class PropertyDataYearType : IPropertyDataYearType
    {
        public PropertyDataYearType(Proxy.CreditReportResponse.PropertyDataYearType propertyDataYearType)
        {
            if(propertyDataYearType != null)
            {
                Format = (DateFormat)propertyDataYearType.format;
                Value = propertyDataYearType.Value;
            }
        }
        public DateFormat Format { get; set; }
        public string Value { get; set; }
    }
}
