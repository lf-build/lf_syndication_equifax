﻿namespace LendFoundry.Syndication.Equifax.CreditReportResponse
{
    public interface IReasonCodeType : ICodeType
    {
        short Number { get; set; }
    }
}
