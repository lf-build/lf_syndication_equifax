using System.Collections.Generic;
using System.Linq;

namespace LendFoundry.Syndication.Equifax.CreditReportResponse
{
    public class UsedasType : IUsedasType
    {
        public UsedasType(Proxy.CreditReportResponse.USEDASType usedasType)
        {
            if (usedasType != null)
            {
                EdasScore = usedasType.EDASScore;
                if (usedasType.ScoreReasons != null)
                    ScoreReasons = usedasType.ScoreReasons.Select(p => new ScoreReasonType(
                        new Proxy.CreditReportResponse.CodeType
                        {
                            code = p.code,
                            description = p.description
                        },
                        p)).ToList<IScoreReasonType>();

                if (usedasType.RejectCode != null)
                    RejectCode = new CodeType(usedasType.RejectCode);

                if (usedasType.RegionalIndicator != null)
                    RegionalIndicator = new CodeType(usedasType.RegionalIndicator);

                if (usedasType.EnhancedDASIndicator != null)
                    EnhancedDasIndicator = new CodeType(usedasType.EnhancedDASIndicator);

                FactActInquiriesAreKeyFactor = usedasType.FACTActInquiriesAreKeyFactor;
            }
        }

        public string EdasScore { get; set; }

        public List<IScoreReasonType> ScoreReasons { get; set; }

        public ICodeType RejectCode { get; set; }

        public ICodeType RegionalIndicator { get; set; }

        public ICodeType EnhancedDasIndicator { get; set; }

        public string FactActInquiriesAreKeyFactor { get; set; }
    }
}