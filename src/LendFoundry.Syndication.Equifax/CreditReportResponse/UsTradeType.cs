using System.Collections.Generic;
using System.Linq;

namespace LendFoundry.Syndication.Equifax.CreditReportResponse
{
    public class UsTradeType : IUsTradeType
    {
        public UsTradeType(Proxy.CreditReportResponse.USTradeType usTradeType)
        {
            if (usTradeType != null)
            {
                if (usTradeType.CreditorId != null)
                    CreditorId = new CustomerIdType(usTradeType.CreditorId);

                if (usTradeType.DateReported != null)
                    DateReported = new DateType(usTradeType.DateReported);

                if (usTradeType.DateOpened != null)
                    DateOpened = new DateType(usTradeType.DateOpened);

                AccountNumber = usTradeType.AccountNumber;

                if (usTradeType.HighCreditAmount != null)
                    HighCreditAmount = new MoneyType(usTradeType.HighCreditAmount);

                //TODO: Changed from CreditLimit to ExpandedCreditLimit  
                if (usTradeType.ExpandedCreditLimit != null)
                    CreditLimit = new MoneyType(usTradeType.ExpandedCreditLimit);

                if (usTradeType.BalanceAmount != null)
                    BalanceAmount = new MoneyType(usTradeType.BalanceAmount);

                if (usTradeType.PastDueAmount != null)
                    PastDueAmount = new MoneyType(usTradeType.PastDueAmount);

                if (usTradeType.PortfolioType != null)
                    PortfolioType = new CodeType(usTradeType.PortfolioType);
                if (usTradeType.Status != null)
                    Status = new CodeType(usTradeType.Status);
                MonthsReviewed = usTradeType.MonthsReviewed;

                if (usTradeType.AccountDesignator != null)
                    AccountDesignator = new CodeType(usTradeType.AccountDesignator);

                if (usTradeType.DateOfLastActivity != null)
                    DateOfLastActivity = new DateType(usTradeType.DateOfLastActivity);
                //TODO: Changed from DateOfFirstDelinquency to ExpandedDateMajorDelinquencyReported
                if (usTradeType.ExpandedDateMajorDelinquencyReported != null)
                    DateOfFirstDelinquency = new DateType(usTradeType.ExpandedDateMajorDelinquencyReported);
                if (usTradeType.PreviousHighPaymentRates != null)

                    PreviousHighPaymentRates =
                        usTradeType.PreviousHighPaymentRates.Select(
                            p =>
                                new CodeDateType(
                                    new Proxy.CreditReportResponse.CodeType { code = p.code, description = p.description },
                                    p))
                            .ToList<ICodeDateType>();

                HistoryDerogatoryCounters = new UsHistoryDerogatoryCounters(usTradeType.HistoryDerogatoryCounters);

                //TODO: Changed from AccountType to ExpandedAccountType
                if (usTradeType.ExpandedAccountType != null)
                    AccountType = new CodeType(usTradeType.ExpandedAccountType);

                //TODO: Changed from DateOfLastPayment to ExpandedDateOfLastPayment
                if (usTradeType.ExpandedDateOfLastPayment != null)
                    DateOfLastPayment = new DateType(usTradeType.ExpandedDateOfLastPayment);

                //TODO: Changed from DateClosed to ExpandedDateClosed
                if (usTradeType.ExpandedDateClosed != null)
                    DateClosed = new DateType(usTradeType.ExpandedDateClosed);

                //TODO: Changed DateMajorDelinquencyReported to ExpandedDateMajorDelinquencyReported
                if (usTradeType.ExpandedDateMajorDelinquencyReported != null)
                    DateMajorDelinquencyReported = new DateType(usTradeType.ExpandedDateMajorDelinquencyReported);

                //TODO: Changed ActualPaymentAmount to ExpandedActualPaymentAmount
                if (usTradeType.ExpandedActualPaymentAmount != null)
                    ActualPaymentAmount = new MoneyType(usTradeType.ExpandedActualPaymentAmount);

                if (usTradeType.ScheduledPaymentAmount != null)
                    ScheduledPaymentAmount = new MoneyType(usTradeType.ScheduledPaymentAmount);

                //TODO: Changed TermsFrequency to ExpandedTermsFrequency
                if (usTradeType.ExpandedTermsFrequency != null)
                    TermsFrequency = new CodeType(usTradeType.ExpandedTermsFrequency);

                TermsDuration = usTradeType.TermsDuration;

                //TODO: Changed PurchasedPortfolioIndicator to ExpandedPurchasedPortfolioIndicator
                if (usTradeType.ExpandedPurchasedPortfolioIndicator != null)
                    PurchasedPortfolioIndicator = new CodeType(usTradeType.ExpandedPurchasedPortfolioIndicator);

                //TODO: Changed PurchasedPortfolioName to ExpandedPurchasedPortfolioName
                PurchasedPortfolioName = usTradeType.ExpandedPurchasedPortfolioName;

                //TODO: Changed CreditorClassification to ExpandedCreditorClassification
                if (usTradeType.ExpandedCreditorClassification != null)
                    CreditorClassification = new CodeType(usTradeType.ExpandedCreditorClassification);

                //TODO: Changed ActivityDesignator to ExpandedActivityDesignator
                if (usTradeType.ExpandedActivityDesignator != null)
                    ActivityDesignator = new CodeType(usTradeType.ExpandedActivityDesignator);

                //TODO: Changed OriginalChargeOffAmount to ExpandedOriginalChargeOffAmount
                if (usTradeType.ExpandedOriginalChargeOffAmount != null)
                    OriginalChargeOffAmount = new MoneyType(usTradeType.ExpandedOriginalChargeOffAmount);

                //TODO: Changed DeferredPaymentStartDate to ExpandedDeferredPaymentStartDate
                if (usTradeType.ExpandedDeferredPaymentStartDate != null)
                    DeferredPaymentStartDate = new DateType(usTradeType.ExpandedDeferredPaymentStartDate);

                //TODO: Changed BalloonPaymentAmount to ExpandedDeferredPaymentStartDate
                if (usTradeType.ExpandedBalloonPaymentAmount != null)
                    BalloonPaymentAmount = new MoneyType(usTradeType.ExpandedBalloonPaymentAmount);

                //TODO: Changed BalloonPaymentDueDate to ExpandedBalloonPaymentDueDate
                if (usTradeType.ExpandedBalloonPaymentDueDate != null)
                    BalloonPaymentDueDate = new DateType(usTradeType.ExpandedBalloonPaymentDueDate);

                //TODO: Changed MortgageIDNumber to ExpandedMortgageIDNumber
                MortgageIdNumber = usTradeType.ExpandedMortgageIDNumber;

                //TODO: Changed PreviousHighRateOutsideHistory to ExpandedPreviousHighRateOutsideHistory
                if (usTradeType.ExpandedPreviousHighRateOutsideHistory != null)
                    PreviousHighRateOutsideHistory = new CodeType(usTradeType.ExpandedPreviousHighRateOutsideHistory);

                //TODO: Changed PreviousHighDateOutsideHistory to ExpandedPreviousHighDateOutsideHistory
                if (usTradeType.ExpandedPreviousHighDateOutsideHistory != null)
                    PreviousHighDateOutsideHistory = new DateType(usTradeType.ExpandedPreviousHighDateOutsideHistory);

                PaymentHistory = usTradeType.PaymentHistory;
                if (usTradeType.UpdateIndicator != null)
                    UpdateIndicator = new CodeType(usTradeType.UpdateIndicator);

                if (usTradeType.Narratives != null)

                    Narratives = usTradeType.Narratives.Select(p => new CodeType(p))
                        .ToList<ICodeType>();
            }
        }

        public ICustomerIdType CreditorId { get; set; }

        public IDateType DateReported { get; set; }

        public IDateType DateOpened { get; set; }

        public string AccountNumber { get; set; }

        public IMoneyType HighCreditAmount { get; set; }

        public IMoneyType CreditLimit { get; set; }

        public IMoneyType BalanceAmount { get; set; }

        public IMoneyType PastDueAmount { get; set; }

        public ICodeType PortfolioType { get; set; }

        public ICodeType Status { get; set; }

        public short MonthsReviewed { get; set; }

        public ICodeType AccountDesignator { get; set; }

        public IDateType DateOfLastActivity { get; set; }

        public IDateType DateOfFirstDelinquency { get; set; }

        public IUsHistoryDerogatoryCounters HistoryDerogatoryCounters { get; set; }

        public List<ICodeDateType> PreviousHighPaymentRates { get; set; }

        public ICodeType AccountType { get; set; }

        public IDateType DateOfLastPayment { get; set; }

        public IDateType DateClosed { get; set; }

        public IDateType DateMajorDelinquencyReported { get; set; }

        public IMoneyType ActualPaymentAmount { get; set; }

        public IMoneyType ScheduledPaymentAmount { get; set; }

        public ICodeType TermsFrequency { get; set; }

        public string TermsDuration { get; set; }

        public ICodeType PurchasedPortfolioIndicator { get; set; }

        public string PurchasedPortfolioName { get; set; }

        public ICodeType CreditorClassification { get; set; }

        public ICodeType ActivityDesignator { get; set; }

        public IMoneyType OriginalChargeOffAmount { get; set; }

        public IDateType DeferredPaymentStartDate { get; set; }

        public IMoneyType BalloonPaymentAmount { get; set; }

        public IDateType BalloonPaymentDueDate { get; set; }

        public string MortgageIdNumber { get; set; }

        public ICodeType PreviousHighRateOutsideHistory { get; set; }

        public IDateType PreviousHighDateOutsideHistory { get; set; }

        public string PaymentHistory { get; set; }

        public ICodeType UpdateIndicator { get; set; }

        public List<ICodeType> Narratives { get; set; }
    }
}