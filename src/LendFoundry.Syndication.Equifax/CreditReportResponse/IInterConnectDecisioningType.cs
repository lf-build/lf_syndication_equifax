﻿using System.Collections.Generic;

namespace LendFoundry.Syndication.Equifax.CreditReportResponse
{
    public interface IInterConnectDecisioningType
    {
        ICodeType RegulatedIdentifier { get; set; }
        ICodeType HitNoHitIndicator { get; set; }
        string DecisionType { get; set; }
        ICodeType DecisionDesignator { get; set; }
        string Description { get; set; }
        string DecisionCode { get; set; }
        List<IProductType> Products { get; set; }
    }
}
