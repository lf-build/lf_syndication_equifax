﻿using LendFoundry.Syndication.Equifax.Proxy.CreditReportResponse;
using System.Collections.Generic;
using System.Linq;

namespace LendFoundry.Syndication.Equifax.CreditReportResponse
{
    public class UsConsumerCreditReportTypeUSAlternateDataSources : IUsConsumerCreditReportTypeUSAlternateDataSources
    {
        public UsConsumerCreditReportTypeUSAlternateDataSources(USConsumerCreditReportTypeUSAlternateDataSources usConsumerCreditReportTypeUSAlternateDataSources)
        {
            if (usConsumerCreditReportTypeUSAlternateDataSources.AlternateDataSourceErrors != null)
                AlternateDataSourceErrors =
                    usConsumerCreditReportTypeUSAlternateDataSources.AlternateDataSourceErrors.Select(p => new AlternateDataSourceErrorType(p))
                        .ToList<IAlternateDataSourceErrorType>();
            if (usConsumerCreditReportTypeUSAlternateDataSources.TheWorkNumberSelects != null)
                TheWorkNumberSelects =
                    usConsumerCreditReportTypeUSAlternateDataSources.TheWorkNumberSelects.Select(p => new TheWorkNumberSelectType(p))
                        .ToList<ITheWorkNumberSelectType>();
            if (usConsumerCreditReportTypeUSAlternateDataSources.ElectronicIDCompares != null)
                ElectronicIdCompares =
                    usConsumerCreditReportTypeUSAlternateDataSources.ElectronicIDCompares.Select(p => new ElectronicIdCompareType(p))
                        .ToList<IElectronicIdCompareType>();
            if (usConsumerCreditReportTypeUSAlternateDataSources.PropertyDataAndAnalytics != null)
                PropertyDataAndAnalytics =
                    usConsumerCreditReportTypeUSAlternateDataSources.PropertyDataAndAnalytics.Select(p => new PropertyDataAndAnalyticType(p))
                        .ToList<IPropertyDataAndAnalyticType>();
            if (usConsumerCreditReportTypeUSAlternateDataSources.MilitaryLendingActDataSegments != null)
                MilitaryLendingActDataSegments =
                    usConsumerCreditReportTypeUSAlternateDataSources.MilitaryLendingActDataSegments.Select(p => new MilitaryLendingType(p))
                        .ToList<IMilitaryLendingType>();
            if (usConsumerCreditReportTypeUSAlternateDataSources.InterConnectDecisionings != null)
                InterConnectDecisionings =
                    usConsumerCreditReportTypeUSAlternateDataSources.InterConnectDecisionings.Select(p => new InterConnectDecisioningType(p))
                        .ToList<IInterConnectDecisioningType>();
            if (usConsumerCreditReportTypeUSAlternateDataSources.IXIServicesDatas != null)
                IxiServicesDatas =
                    usConsumerCreditReportTypeUSAlternateDataSources.IXIServicesDatas.Select(p => new IxiServicesType(p))
                        .ToList<IIxiServicesType>();
            if (usConsumerCreditReportTypeUSAlternateDataSources.IDRevealDatas != null)
                IdRevealDatas =
                    usConsumerCreditReportTypeUSAlternateDataSources.IDRevealDatas.Select(p => new IdRevealDataType(p))
                        .ToList<IIdRevealDataType>();
            if (usConsumerCreditReportTypeUSAlternateDataSources.FirstSearchDatas != null)
                FirstSearchDatas =
                    usConsumerCreditReportTypeUSAlternateDataSources.FirstSearchDatas.Select(p => new FirstSearchDataType(p))
                        .ToList<IFirstSearchDataType>();
        }
        public List<IAlternateDataSourceErrorType> AlternateDataSourceErrors { get; set; }
        public List<ITheWorkNumberSelectType> TheWorkNumberSelects { get; set; }
        public List<IElectronicIdCompareType> ElectronicIdCompares { get; set; }
        public List<IPropertyDataAndAnalyticType> PropertyDataAndAnalytics { get; set; }
        public List<IMilitaryLendingType> MilitaryLendingActDataSegments { get; set; }
        public List<IInterConnectDecisioningType> InterConnectDecisionings { get; set; }
        public List<IIxiServicesType> IxiServicesDatas { get; set; }
        public List<IIdRevealDataType> IdRevealDatas { get; set; }
        public List<IFirstSearchDataType> FirstSearchDatas { get; set; }
    }
}
