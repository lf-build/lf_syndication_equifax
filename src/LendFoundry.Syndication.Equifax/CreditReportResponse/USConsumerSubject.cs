using LendFoundry.Syndication.Equifax.Proxy.CreditReportResponse;

namespace LendFoundry.Syndication.Equifax.CreditReportResponse
{
    public class UsConsumerSubject : IUsConsumerSubject
    {
        public UsConsumerSubject(USHeaderTypeSubject usConsumerSubject)
        {
            if (usConsumerSubject != null)
            {
                //TODO: Changed PersonName to SubjectName
                PersonName = new PersonNameType(usConsumerSubject.SubjectName);

                if (usConsumerSubject.SubjectId != null)
                {
                    if (usConsumerSubject.SubjectId.AgeSpecified)
                        Age = usConsumerSubject.SubjectId.Age;
                    

                    if (usConsumerSubject.SubjectId.DateOfBirth != null)
                        DateOfBirth = new DateType(usConsumerSubject.SubjectId.DateOfBirth);

                    SocialSecurityNumber = usConsumerSubject.SubjectId.SubjectSSN;
                }
            }
        }

        public IPersonNameType PersonName { get; set; }

        public int Age { get; set; }

        public IDateType DateOfBirth { get; set; }

        public string SocialSecurityNumber { get; set; }
    }
}