﻿namespace LendFoundry.Syndication.Equifax.CreditReportResponse
{
    public interface IParsedTelephoneType
    {
        short AreaCode { get; set; }
        short Extension { get; set; }
        string Number { get; set; }
    }
}