﻿namespace LendFoundry.Syndication.Equifax.CreditReportResponse
{
    public interface IMoneyType
    {
        CurrencyType Currency { get; set; }
        long Value { get; set; }
    }
}