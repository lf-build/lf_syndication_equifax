namespace LendFoundry.Syndication.Equifax.CreditReportResponse
{
    public class UsConsumerReferralType : IUsConsumerReferralType
    {
        public UsConsumerReferralType(Proxy.CreditReportResponse.USConsumerReferralType usConsumerReferralType)
        {
            if (usConsumerReferralType != null)
            {
                BureauId = usConsumerReferralType.BureauId;
                Name = usConsumerReferralType.Name;
                AddressLine1 = usConsumerReferralType.AddressLine1;
                AddressLine2 = usConsumerReferralType.AddressLine2;
                if (usConsumerReferralType.City != null)
                    City = new CityType(usConsumerReferralType.City);
                State = usConsumerReferralType.State;
                PostalCode = usConsumerReferralType.PostalCode;
                if (usConsumerReferralType.Telephone != null)
                    Telephone = new TelephoneType(usConsumerReferralType.Telephone);
            }
        }

        public string BureauId { get; set; }

        public string Name { get; set; }

        public string AddressLine1 { get; set; }

        public string AddressLine2 { get; set; }

        public ICityType City { get; set; }

        public string State { get; set; }

        public string PostalCode { get; set; }

        public ITelephoneType Telephone { get; set; }
    }
}