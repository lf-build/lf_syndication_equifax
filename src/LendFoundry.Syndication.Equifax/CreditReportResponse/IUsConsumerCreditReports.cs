namespace LendFoundry.Syndication.Equifax.CreditReportResponse
{
    public interface IUsConsumerCreditReports
    {
        IUsConsumerCreditReportType UsConsumerCreditReportType { get; set; }
        IUsErrorMessages UsErrorMessages { get; set; }
    }
}