﻿namespace LendFoundry.Syndication.Equifax.CreditReportResponse
{
    public interface IUsAddressType
    {
        ICodeType AddressIndicator { get; set; }
        ICodeType AddressSource { get; set; }
        ICodeType AddressVariance { get; set; }
        ICityType City { get; set; }
        string Code { get; set; }
        string CountryCode { get; set; }
        IDateType DateAddressFirstReported { get; set; }
        IDateType DateAddressLastReported { get; set; }
        IDateType DateTelephoneReported { get; set; }
        string Description { get; set; }
        string PostalCode { get; set; }
        ICodeType RentOwnBuy { get; set; }
        ICodeType StandardizationIndicator { get; set; }
        string State { get; set; }
        string StreetName { get; set; }
        string StreetNumber { get; set; }
        string StreetPostDirection { get; set; }
        string StreetPreDirection { get; set; }
        string StreetType { get; set; }
        ITelephoneType Telephone { get; set; }
        ICodeType TelephoneSource { get; set; }
        string UnitDesignator { get; set; }
        string UnitNumber { get; set; }
        string UrbanizationCondo { get; set; }
        ICodeDateType Verification { get; set; }
    }
}