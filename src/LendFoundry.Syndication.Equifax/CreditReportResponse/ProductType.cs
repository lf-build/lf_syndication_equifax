﻿using System.Collections.Generic;
using System.Linq;

namespace LendFoundry.Syndication.Equifax.CreditReportResponse
{
    public class ProductType : IProductType
    {
        public ProductType(Proxy.CreditReportResponse.ProductType productType)
        {
            if (productType != null)
            {
                ProductCode = productType.ProductCode;
                if (productType.ReasonCode != null)
                    ReasonCode = productType.ReasonCode.Select(p => new ReasonCodeStringType(p)).ToList<IReasonCodeStringType>();
                if (productType.ProductAttribute != null)
                    ProductAttribute = productType.ProductAttribute.Select(p => new Attribute3Type(p)).ToList<IAttribute3Type>();
                Number = productType.number;
            }
        }
        public string ProductCode { get; set; }
        public List<IReasonCodeStringType> ReasonCode { get; set; }
        public List<IAttribute3Type> ProductAttribute { get; set; }
        public short Number { get; set; }
    }
}
