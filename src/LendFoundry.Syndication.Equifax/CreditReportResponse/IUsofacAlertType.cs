﻿using System.Collections.Generic;

namespace LendFoundry.Syndication.Equifax.CreditReportResponse
{
    public interface IUsofacAlertType
    {
        string AddressLine1 { get; set; }
        string CdcBranchAccountNumber { get; set; }
        string CdcMemberCode { get; set; }
        ICityType City { get; set; }
        string Comment { get; set; }
        string CountryCode { get; set; }
        string IssueId { get; set; }
        string IssueSource { get; set; }
        string LegalVerbiage { get; set; }
        List<ICodeType> MatchCodes { get; set; }
        IPersonNameType PersonName { get; set; }
        string PostalCode { get; set; }
        ICodeType ProblemCode { get; set; }
        IDateType ProblemReportDate { get; set; }
        ICodeType ResponseCode { get; set; }
        ICodeType ResponseType { get; set; }
        string RevisedLegalVerbiageIndicator { get; set; }
        string State { get; set; }
        IDateType TransactionDate { get; set; }
        string TransactionTime { get; set; }
    }
}