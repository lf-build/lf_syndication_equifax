namespace LendFoundry.Syndication.Equifax.CreditReportResponse
{
    public class UsHeaderType : IUsHeaderType
    {
        public UsHeaderType(Proxy.CreditReportResponse.USHeaderType usHeaderType)
        {
            if (usHeaderType != null)
            {
                //TODO: Changed from USRequest to Request
                if (usHeaderType.Request != null)
                    UsRequest = new UsRequest(usHeaderType.Request);

                //TODO: Changed from USCreditFile to CreditFile
                if (usHeaderType.CreditFile != null)
                    UsCreditFile = new UsCreditFile(usHeaderType.CreditFile);

                //TODO: Changed from USConsumerSubject to Subject
                if (usHeaderType.Subject != null)
                {
                    UsConsumerSubject = new UsConsumerSubject(usHeaderType.Subject);
                }
            }
        }

        public IUsRequest UsRequest { get; set; }

        public IUsCreditFile UsCreditFile { get; set; }

        public IUsConsumerSubject UsConsumerSubject{ get; set; }
    }
}