﻿namespace LendFoundry.Syndication.Equifax.CreditReportResponse
{
    public interface IReasonCodeStringType
    {
        short Number { get; set; }
        string Value { get; set; }
    }
}
