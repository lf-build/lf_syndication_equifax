namespace LendFoundry.Syndication.Equifax.CreditReportResponse
{
    public class CityType : ICityType
    {
        public CityType(Proxy.CreditReportResponse.CityType cityType)
        {
            if (cityType != null)
            {
                Code = cityType.code;
                Value = cityType.Value;
            }
        }

        public string Code { get; set; }

        public string Value { get; set; }
    }
}