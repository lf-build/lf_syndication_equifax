﻿namespace LendFoundry.Syndication.Equifax.CreditReportResponse
{
    public interface IUsOtherIdentificationType
    {
        IDateType DateReported { get; set; }
        string OtherIdentificationNumber { get; set; }
        ICodeType Reason { get; set; }
        ICodeType Type { get; set; }
    }
}