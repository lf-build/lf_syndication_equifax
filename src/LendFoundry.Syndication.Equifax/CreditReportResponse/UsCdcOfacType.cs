﻿using LendFoundry.Syndication.Equifax.Proxy.CreditReportResponse;
using System.Collections.Generic;
using System.Linq;

namespace LendFoundry.Syndication.Equifax.CreditReportResponse
{
    public class UsCdcOfacType : IUsCdcOfacType
    {
        public UsCdcOfacType(USCDCOFACType usCdcOfacType)
        {
            if (usCdcOfacType != null)
            {
                RevisedLegalVerbiageIndicator = usCdcOfacType.RevisedLegalVerbiageIndicator;
                CdcMemberCode = usCdcOfacType.CDCMemberCode;
                CdcBranchAccountNumber = usCdcOfacType.CDCBranchAccountNumber;
                if (usCdcOfacType.TransactionDate != null)
                    TransactionDate = new DateType(usCdcOfacType.TransactionDate);
                TransactionTime = usCdcOfacType.TransactionTime;
                if (usCdcOfacType.ResponseType != null)
                    ResponseType = new CodeType(usCdcOfacType.ResponseType);
                if (usCdcOfacType.ResponseCode != null)
                    ResponseCode = new CodeType(usCdcOfacType.ResponseCode);
                if (usCdcOfacType.ProblemCode != null)
                    ProblemCode = new CodeType(usCdcOfacType.ProblemCode);
                if (usCdcOfacType.MatchCodes != null)
                    usCdcOfacType.MatchCodes.Select(p => new CodeType(p))
                                    .ToList<CodeType>();
                if (usCdcOfacType.ProblemReportDate != null)
                    ProblemReportDate = new DateType(usCdcOfacType.ProblemReportDate);
                IssueSource = usCdcOfacType.IssueSource;
                IssueId = usCdcOfacType.IssueId;
                Comment = usCdcOfacType.Comment;
                if (usCdcOfacType.SubjectName != null)
                    SubjectName = new PersonNameType(usCdcOfacType.SubjectName);
                AddressLine1 = usCdcOfacType.AddressLine1;
                if (usCdcOfacType.City != null)
                    City = new CityType(usCdcOfacType.City);
                State = usCdcOfacType.State;
                PostalCode = usCdcOfacType.PostalCode;
                CountryCode = usCdcOfacType.CountryCode;
                LegalVerbiage = usCdcOfacType.LegalVerbiage;
                RegulatedNonRegulatedFlag = (BlankorNFlagType)usCdcOfacType.RegulatedNonRegulatedFlag;
                RegulatedNonRegulatedFlagSpecified = usCdcOfacType.RegulatedNonRegulatedFlagSpecified;
            }
        }
        public string RevisedLegalVerbiageIndicator { get; set; }

        public string CdcMemberCode { get; set; }

        public string CdcBranchAccountNumber { get; set; }

        public IDateType TransactionDate { get; set; }

        public string TransactionTime { get; set; }

        public ICodeType ResponseType { get; set; }

        public ICodeType ResponseCode { get; set; }

        public ICodeType ProblemCode { get; set; }

        public List<ICodeType> MatchCodes { get; set; }

        public IDateType ProblemReportDate { get; set; }

        public string IssueSource { get; set; }

        public string IssueId { get; set; }

        public string Comment { get; set; }

        public IPersonNameType SubjectName { get; set; }

        public string AddressLine1 { get; set; }

        public ICityType City { get; set; }

        public string State { get; set; }

        public string PostalCode { get; set; }

        public string CountryCode { get; set; }

        public string LegalVerbiage { get; set; }

        public BlankorNFlagType RegulatedNonRegulatedFlag { get; set; }

        public bool RegulatedNonRegulatedFlagSpecified { get; set; }
    }
}
