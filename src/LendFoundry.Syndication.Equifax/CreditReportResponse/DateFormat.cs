namespace LendFoundry.Syndication.Equifax.CreditReportResponse
{
    public enum DateFormat
    {
        MMDDCCYY,

        MMCCYY,

        MMDDCCYY1,

        MMCCYY1,

        CCYYMMDD,

        CCYYMM,

        CCYYMMDD1,

        CCYY,

        MMDDCCYY2,
    }
}