namespace LendFoundry.Syndication.Equifax.CreditReportResponse
{
    public class ScoreReasonType : CodeType, IScoreReasonType
    {
        public ScoreReasonType(Proxy.CreditReportResponse.CodeType codeType, Proxy.CreditReportResponse.ScoreReasonType scoreReasonType) : base(codeType)
        {
            Number = scoreReasonType.number;
        }

        public short Number { get; set; }
    }
}