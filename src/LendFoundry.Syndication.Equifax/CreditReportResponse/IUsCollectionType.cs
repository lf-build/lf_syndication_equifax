using System.Collections.Generic;

namespace LendFoundry.Syndication.Equifax.CreditReportResponse
{
    public interface IUsCollectionType
    {
        IDateType DateReported { get; set; }
        IDateType AssignedDate { get; set; }
        ICustomerIdType CustomerId { get; set; }
        string ClientNameNumber { get; set; }
        string AccountNumber { get; set; }
        IMoneyType OriginalAmount { get; set; }
        IMoneyType BalanceAmount { get; set; }
        IDateType BalanceDate { get; set; }
        IDateType DateOfLastPayment { get; set; }
        IDateType StatusDate { get; set; }
        ICodeType Status { get; set; }
        IDateType DateOfFirstDelinquency { get; set; }
        ICodeType AccountDesignator { get; set; }
        ICodeType CreditorClassification { get; set; }
        ICodeType UpdateIndicator { get; set; }
        List<ICodeType> Narratives { get; set; }
    }
}