﻿namespace LendFoundry.Syndication.Equifax.CreditReportResponse
{
    public class Income360Type : IIncome360Type
    {
        public Income360Type(Proxy.CreditReportResponse.Income360Type income360Type)
        {
            if (income360Type != null)
            {
                ProductId = income360Type.ProductID;
                PostalCode = income360Type.PostalCode;
                PostalCodePlus4 = income360Type.PostalCodePlus4;
                Age = income360Type.Age;
                AgeSpecified = income360Type.AgeSpecified;
                AgeBand = income360Type.AgeBand;
                if (income360Type.IncomeofConsumer != null)
                    IncomeofConsumer = new MoneyType(income360Type.IncomeofConsumer);
                DispositionCode = income360Type.DispositionCode;
            }
        }
        public string ProductId { get; set; }
        public string PostalCode { get; set; }
        public string PostalCodePlus4 { get; set; }
        public short Age { get; set; }
        public bool AgeSpecified { get; set; }
        public string AgeBand { get; set; }
        public IMoneyType IncomeofConsumer { get; set; }
        public string DispositionCode { get; set; }
    }
}
