﻿namespace LendFoundry.Syndication.Equifax.CreditReportResponse
{
    public interface IMilitaryLendingType
    {
        ICodeType RegulatedIdentifier { get; set; }
        string Disclaimer { get; set; }
        string CoveredBorrowerStatus { get; set; }
        string InsufficientDataForMatch { get; set; }
        string ReferralContactNumber { get; set; }
    }
}
