﻿namespace LendFoundry.Syndication.Equifax.CreditReportResponse
{
    public interface IUsIdentificationSsnType
    {
        string InquirySsnByteToByteMatch { get; set; }
        string InquirySsnDateIssued { get; set; }
        IDateType InquirySsnDateIssuedRange { get; set; }
        string InquirySsnDateOfDeath { get; set; }
        string InquirySsnStateIssued { get; set; }
        string InquirySsnStateOfDeath { get; set; }
        string InquirySubjectSsn { get; set; }
        string MdbSubjectAge { get; set; }
        string MdbSubjectSsn { get; set; }
        string MdbSubjectSsnConfirmed { get; set; }
        string SsnMatch { get; set; }
    }
}