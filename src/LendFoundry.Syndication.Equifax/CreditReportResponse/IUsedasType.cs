﻿using System.Collections.Generic;

namespace LendFoundry.Syndication.Equifax.CreditReportResponse
{
    public interface IUsedasType
    {
        string EdasScore { get; set; }
        ICodeType EnhancedDasIndicator { get; set; }
        string FactActInquiriesAreKeyFactor { get; set; }
        ICodeType RegionalIndicator { get; set; }
        ICodeType RejectCode { get; set; }
        List<IScoreReasonType> ScoreReasons { get; set; }
    }
}