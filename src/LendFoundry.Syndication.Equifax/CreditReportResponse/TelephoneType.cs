namespace LendFoundry.Syndication.Equifax.CreditReportResponse
{
    public class TelephoneType : ITelephoneType
    {
        public TelephoneType(Proxy.CreditReportResponse.TelephoneType telephoneType)
        {
            if (telephoneType?.Item != null)
            {
                var s = telephoneType.Item as string;
                if (s != null)
                    UnparsedTelephone = s;
                else if (telephoneType.Item.GetType() == typeof(Proxy.CreditReportResponse.ParsedTelephoneType))
                    ParsedTelephoneType =
                        new ParsedTelephoneType(telephoneType.Item as Proxy.CreditReportResponse.ParsedTelephoneType);
            }
        }

        public string UnparsedTelephone { get; set; }

        public IParsedTelephoneType ParsedTelephoneType { get; set; }
    }
}