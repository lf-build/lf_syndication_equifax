namespace LendFoundry.Syndication.Equifax.CreditReportResponse
{
    public interface ICustomerIdType
    {
        string CustomerNumber { get; set; }
        string Name { get; set; }
        ICodeType Industry { get; set; }
    }
}