using System.Collections.Generic;
using System.Linq;

namespace LendFoundry.Syndication.Equifax.CreditReportResponse
{
    public class UsScoreType : IUsScoreType
    {
        public UsScoreType(Proxy.CreditReportResponse.USScoreType usScoreType)
        {
            if (usScoreType != null)
            {
                ScoreFormatType = usScoreType.ScoreFormatType;
                ModelNumber = usScoreType.ModelNumber;
                ModelId = usScoreType.ModelId;
                ScoreNumber = usScoreType.ScoreNumber;
                ScoreResult = usScoreType.ScoreResult;

                if (usScoreType.ScoreReasons != null)
                    ScoreReasons =
                        usScoreType.ScoreReasons.Select(
                            p =>
                                new ScoreReasonType(
                                    new Proxy.CreditReportResponse.CodeType { code = p.code, description = p.description },
                                    p)).ToList<IScoreReasonType>();
                if (usScoreType.RejectCode != null)
                {
                    RejectCode = new CodeType(usScoreType.RejectCode);
                }

                RiskBasedPricing = usScoreType.RiskBasedPricing;
                DoddFrank = usScoreType.DoddFrank;
                FactActInquiriesAreKeyFactor = usScoreType.FACTActInquiriesAreKeyFactor;
            }
        }

        public string ScoreFormatType { get; set; }

        public string ModelNumber { get; set; }

        public string ModelId { get; set; }

        public string ScoreNumber { get; set; }

        public string ScoreResult { get; set; }

        public List<IScoreReasonType> ScoreReasons { get; set; }

        public ICodeType RejectCode { get; set; }

        public object RiskBasedPricing { get; set; }

        public object DoddFrank { get; set; }

        public string FactActInquiriesAreKeyFactor { get; set; }
    }
}