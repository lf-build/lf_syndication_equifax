﻿namespace LendFoundry.Syndication.Equifax.CreditReportResponse
{
    public interface IPropertyDataSubCommonType
    {
        string Quarter{get;set;}
        IPropertyDataYearType Year{get;set;}
        IMoneyType SmoothedMedianPrice{get;set;}
        string SmoothedIndex{get;set;}
        string SmoothedPercentChange{get;set;}
        short Number{get;set;}
    }
}
