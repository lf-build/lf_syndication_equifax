﻿namespace LendFoundry.Syndication.Equifax.CreditReportResponse
{
    public interface ICodeDateType
    {
        DateType Date { get; set; }
    }
}