using System.Collections.Generic;
using System.Linq;

namespace LendFoundry.Syndication.Equifax.CreditReportResponse
{
    public class UsDataAttributeType : IUsDataAttributeType
    {
        public UsDataAttributeType(Proxy.CreditReportResponse.USDataAttributeType usDataAttributeType)
        {
            if (usDataAttributeType?.DataAttribute != null)
            {
                DataAttribute =
                    usDataAttributeType.DataAttribute.Select(p => new IdAndValueType(p)).ToList<IIdAndValueType>();
                ModelNumber = usDataAttributeType.modelNumber;
            }
        }

        public List<IIdAndValueType> DataAttribute { get; set; }

        public string ModelNumber { get; set; }
    }
}