﻿namespace LendFoundry.Syndication.Equifax.CreditReportResponse
{
    public class MilitaryLendingType : IMilitaryLendingType
    {
        public MilitaryLendingType(Proxy.CreditReportResponse.MilitaryLendingType militaryLendingType)
        {
            if (militaryLendingType != null)
            {
                if (militaryLendingType.RegulatedIdentifier != null)
                    RegulatedIdentifier = new CodeType(militaryLendingType.RegulatedIdentifier);
                Disclaimer = militaryLendingType.Disclaimer;
                CoveredBorrowerStatus = militaryLendingType.CoveredBorrowerStatus;
                InsufficientDataForMatch = militaryLendingType.InsufficientDataForMatch;
                ReferralContactNumber = militaryLendingType.ReferralContactNumber;
            }
        }
        public ICodeType RegulatedIdentifier { get; set; }
        public string Disclaimer { get; set; }
        public string CoveredBorrowerStatus { get; set; }
        public string InsufficientDataForMatch { get; set; }
        public string ReferralContactNumber { get; set; }
    }
}
