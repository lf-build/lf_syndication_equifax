﻿using System.Collections.Generic;
using System.Linq;

namespace LendFoundry.Syndication.Equifax.CreditReportResponse
{
    public class PropertyDataAndAnalyticLIENType : IPropertyDataAndAnalyticLIENType
    {
        public PropertyDataAndAnalyticLIENType(Proxy.CreditReportResponse.PropertyDataAndAnalyticLIENType propertyDataAndAnalyticLIENType)
        {
            if (propertyDataAndAnalyticLIENType != null)
            {
                if (propertyDataAndAnalyticLIENType.HitNoHitIndicator != null)
                    HitNoHitIndicator = new CodeType(propertyDataAndAnalyticLIENType.HitNoHitIndicator);
                if (propertyDataAndAnalyticLIENType.EstimatedLoantoValue != null)
                    EstimatedLoantoValue = new MoneyType(propertyDataAndAnalyticLIENType.EstimatedLoantoValue);
                if (propertyDataAndAnalyticLIENType.CombinedLoantoValue != null)
                    CombinedLoantoValue = new MoneyType(propertyDataAndAnalyticLIENType.CombinedLoantoValue);
                if (propertyDataAndAnalyticLIENType.EstimatedBalanceofFirstLien != null)
                    EstimatedBalanceofFirstLien = new MoneyType(propertyDataAndAnalyticLIENType.EstimatedBalanceofFirstLien);
                if (propertyDataAndAnalyticLIENType.EstimatedBalanceofSecondLien != null)
                    EstimatedBalanceofSecondLien = new MoneyType(propertyDataAndAnalyticLIENType.EstimatedBalanceofSecondLien);
                if (propertyDataAndAnalyticLIENType.LienDetail != null)
                    LienDetail = propertyDataAndAnalyticLIENType.LienDetail.Select(p => new PropertyDataAndAnalyticLIENDetailType(p)).ToList<IPropertyDataAndAnalyticLIENDetailType>();
            }
        }
        public ICodeType HitNoHitIndicator { get; set; }
        public IMoneyType EstimatedLoantoValue { get; set; }
        public IMoneyType CombinedLoantoValue { get; set; }
        public IMoneyType EstimatedBalanceofFirstLien { get; set; }
        public IMoneyType EstimatedBalanceofSecondLien { get; set; }
        public List<IPropertyDataAndAnalyticLIENDetailType> LienDetail { get; set; }
    }
}
