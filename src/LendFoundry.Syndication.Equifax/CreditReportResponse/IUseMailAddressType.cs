﻿namespace LendFoundry.Syndication.Equifax.CreditReportResponse
{
    public interface IUseMailAddressType
    {
        IDateType DateReported { get; set; }
        string InternetEMailAddress { get; set; }
    }
}