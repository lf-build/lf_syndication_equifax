namespace LendFoundry.Syndication.Equifax.CreditReportResponse
{
    public class UsEmploymentType : IUsEmploymentType
    {
        public UsEmploymentType(Proxy.CreditReportResponse.USEmploymentType usEmploymentType)
        {
            if (usEmploymentType != null)
            {
                Occupation = usEmploymentType.Occupation;
                Employer = usEmploymentType.Employer;
                if (usEmploymentType.City != null)
                    City = new CityType(usEmploymentType.City);
                State = usEmploymentType.State;
                if (usEmploymentType.DateEmployed != null)
                    DateEmployed = new DateType(usEmploymentType.DateEmployed);
                if (usEmploymentType.VerificationDate != null)
                    VerificationDate = new DateType(usEmploymentType.VerificationDate);

                //TODO: Changed from DateEmploymentEnded => DateLeft
                if (usEmploymentType.DateLeft != null)
                    DateEmploymentEnded = new DateType(usEmploymentType.DateLeft);

                Code = usEmploymentType.code;
                Description = usEmploymentType.description;
            }
        }

        public string Occupation { get; set; }

        public string Employer { get; set; }

        public ICityType City { get; set; }

        public string State { get; set; }

        public IDateType DateEmployed { get; set; }

        public IDateType VerificationDate { get; set; }

        public IDateType DateEmploymentEnded { get; set; }

        public string Code { get; set; }

        public string Description { get; set; }
    }
}