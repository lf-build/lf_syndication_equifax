using System.Collections.Generic;

namespace LendFoundry.Syndication.Equifax.CreditReportResponse
{
    public interface IConsumerCreditReport
    {
        List<IUsConsumerCreditReportType> UsConsumerCreditReportType { get; set; }
        IUsErrorMessages UsErrorMessages { get; set; }
        string UsPrintImage { get; set; }
        short RequestNumber { get; set; }
        string ReportId { get; set; }
    }
}