﻿using System.Collections.Generic;

namespace LendFoundry.Syndication.Equifax.CreditReportResponse
{
    public interface IIdentityScanType
    {
        List<ICodeType> AlertCodes { get; set; }
    }
}