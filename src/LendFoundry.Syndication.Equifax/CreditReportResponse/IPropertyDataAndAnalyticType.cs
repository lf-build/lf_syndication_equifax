﻿namespace LendFoundry.Syndication.Equifax.CreditReportResponse
{
    public interface IPropertyDataAndAnalyticType
    {
        ICodeType RegulatedIdentifier { get; set; }
        ICodeType HitNoHitIndicator { get; set; }
        IPropertyDataAndAnalyticALSType ActivListingData { get; set; }
        IPropertyDataAndAnalyticAVMType AutomatedValuationModelData { get; set; }
        IPropertyDataAndAnalyticLIENType LienData { get; set; }
        IPropertyDataAndAnalyticPROPType PropertyData { get; set; }
        IPropertyDataAndAnalyticMKTType MarketData { get; set; }
        IPropertyDataAndAnalyticHPIType HPIData { get; set; }
    }
}
