﻿using System.Collections.Generic;

namespace LendFoundry.Syndication.Equifax.CreditReportResponse
{
    public interface IUsBankruptcyType
    {
        IMoneyType AssetAmount { get; set; }
        string CaseNumber { get; set; }
        ICustomerIdType CourtId { get; set; }
        IDateType DateFiled { get; set; }
        IDateType DateReported { get; set; }
        ICodeDateType Disposition { get; set; }
        IMoneyType ExemptAmount { get; set; }
        ICodeType Filer { get; set; }
        IMoneyType LiabilityAmount { get; set; }
        List<ICodeType> Narratives { get; set; }
        ICodeType PriorIntentCode { get; set; }
        ICodeType Type { get; set; }
        IDateType VerificationDate { get; set; }
    }
}