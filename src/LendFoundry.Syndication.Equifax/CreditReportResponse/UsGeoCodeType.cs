using System.Collections.Generic;
using System.Linq;

namespace LendFoundry.Syndication.Equifax.CreditReportResponse
{
    public class UsGeoCodeType : IUsGeoCodeType
    {
        public UsGeoCodeType(Proxy.CreditReportResponse.USGeoCodeType usGeoCodeType)
        {
            if (usGeoCodeType != null)
            {
                GeoSmsa = usGeoCodeType.GeoSMSA;
                GeoStateCode = usGeoCodeType.GeoStateCode;

                GeoCountyCode = usGeoCodeType.GeoCountyCode;
                GeoCensusTract = usGeoCodeType.GeoCensusTract;
                GeoSuffix = usGeoCodeType.GeoSuffix;

                GeoBlockGroup = usGeoCodeType.GeoBlockGroup;

                if (usGeoCodeType.ParsedStreetAddress != null)
                {
                    StreetNumber = usGeoCodeType.ParsedStreetAddress.StreetNumber;

                    StreetName = usGeoCodeType.ParsedStreetAddress.StreetName;

                    StreetType = usGeoCodeType.ParsedStreetAddress.StreetType;
                }

                if (usGeoCodeType.City != null)
                    City = new CityType(usGeoCodeType.City);

                State = usGeoCodeType.State;

                PostalCode = usGeoCodeType.PostalCode;
                if (usGeoCodeType.Type != null)
                    Type = new CodeType(usGeoCodeType.Type);

                if (usGeoCodeType.GeoReturnCodes != null)
                    GeoReturnCodes = usGeoCodeType.GeoReturnCodes.ToList();

                MicroVisionCode = usGeoCodeType.MicroVisionCode;

                MicroVisionReturnCode = usGeoCodeType.MicroVisionReturnCode;
            }
        }

        public string GeoSmsa { get; set; }

        public string GeoStateCode { get; set; }

        public string GeoCountyCode { get; set; }

        public string GeoCensusTract { get; set; }

        public string GeoSuffix { get; set; }

        public string GeoBlockGroup { get; set; }

        public string StreetNumber { get; set; }

        public string StreetName { get; set; }

        public string StreetType { get; set; }

        public ICityType City { get; set; }

        public string State { get; set; }

        public string PostalCode { get; set; }

        public ICodeType Type { get; set; }

        public List<string> GeoReturnCodes { get; set; }

        public string MicroVisionCode { get; set; }

        public string MicroVisionReturnCode { get; set; }
    }
}