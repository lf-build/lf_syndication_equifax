﻿namespace LendFoundry.Syndication.Equifax.CreditReportResponse
{
    public class PropertyDataAndAnalyticAVMType : IPropertyDataAndAnalyticAVMType
    {
        public PropertyDataAndAnalyticAVMType(Proxy.CreditReportResponse.PropertyDataAndAnalyticAVMType propertyDataAndAnalyticAVMType)
        {
            if (propertyDataAndAnalyticAVMType != null)
            {
                if (propertyDataAndAnalyticAVMType.HitNoHitIndicator != null)
                    HitNoHitIndicator = new CodeType(propertyDataAndAnalyticAVMType.HitNoHitIndicator);
                if (propertyDataAndAnalyticAVMType.AutomatedValuationModelPropValueSpecificTime != null)
                    AutomatedValuationModelPropValueSpecificTime = new MoneyType(propertyDataAndAnalyticAVMType.AutomatedValuationModelPropValueSpecificTime);
                if (propertyDataAndAnalyticAVMType.LowRangeEstimatedValueofSubjectProperty != null)
                    LowRangeEstimatedValueofSubjectProperty = new MoneyType(propertyDataAndAnalyticAVMType.LowRangeEstimatedValueofSubjectProperty);
                if (propertyDataAndAnalyticAVMType.HighRangeEstimatedValueofSubjectsProperty != null)
                    HighRangeEstimatedValueofSubjectsProperty = new MoneyType(propertyDataAndAnalyticAVMType.HighRangeEstimatedValueofSubjectsProperty);
                ScoreIndicatorAccuracyofPropertyValuation = propertyDataAndAnalyticAVMType.ScoreIndicatorAccuracyofPropertyValuation;
            }
        }
        public ICodeType HitNoHitIndicator { get; set; }
        public IMoneyType AutomatedValuationModelPropValueSpecificTime { get; set; }
        public IMoneyType LowRangeEstimatedValueofSubjectProperty { get; set; }
        public IMoneyType HighRangeEstimatedValueofSubjectsProperty { get; set; }
        public string ScoreIndicatorAccuracyofPropertyValuation { get; set; }
    }
}
