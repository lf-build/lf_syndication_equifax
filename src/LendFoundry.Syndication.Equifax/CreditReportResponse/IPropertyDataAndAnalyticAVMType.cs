﻿namespace LendFoundry.Syndication.Equifax.CreditReportResponse
{
    public interface IPropertyDataAndAnalyticAVMType
    {
        ICodeType HitNoHitIndicator { get; set; }
        IMoneyType AutomatedValuationModelPropValueSpecificTime { get; set; }
        IMoneyType LowRangeEstimatedValueofSubjectProperty { get; set; }
        IMoneyType HighRangeEstimatedValueofSubjectsProperty { get; set; }
        string ScoreIndicatorAccuracyofPropertyValuation { get; set; }
    }
}
