﻿namespace LendFoundry.Syndication.Equifax.CreditReportResponse
{
    public interface IIdAndValueType
    {
        string Identifier { get; set; }
        string Value { get; set; }
    }
}