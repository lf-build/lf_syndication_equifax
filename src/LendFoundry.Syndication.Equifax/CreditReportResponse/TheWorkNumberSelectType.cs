﻿using System.Collections.Generic;
using System.Linq;

namespace LendFoundry.Syndication.Equifax.CreditReportResponse
{
    public class TheWorkNumberSelectType : ITheWorkNumberSelectType
    {
        public TheWorkNumberSelectType(Proxy.CreditReportResponse.TheWorkNumberSelectType theWorkNumberSelectType)
        {
            if (theWorkNumberSelectType != null)
            {
                if (theWorkNumberSelectType.RegulatedIdentifier != null)
                    RegulatedIdentifier = new CodeType(theWorkNumberSelectType.RegulatedIdentifier);
                if (theWorkNumberSelectType.HitNoHitIndicator != null)
                    HitNoHitIndicator = new CodeType(theWorkNumberSelectType.HitNoHitIndicator);
                TwnDisclaimer = theWorkNumberSelectType.TWNDisclaimer;
                if (theWorkNumberSelectType.EmployeeName != null)
                    EmployeeName = new SubjectNameFMLType(theWorkNumberSelectType.EmployeeName);
                EmployeeSSN = theWorkNumberSelectType.EmployeeSSN;
                EmployerName = theWorkNumberSelectType.EmployerName;
                if (theWorkNumberSelectType.DateEmployeeInformationisEffective != null)
                    DateEmployeeInformationisEffective = new DateType(theWorkNumberSelectType.DateEmployeeInformationisEffective);
                EmployeePositionNameorTitle = theWorkNumberSelectType.EmployeePositionNameorTitle;
                if (theWorkNumberSelectType.RateofpayperPayPeriod != null)
                    RateofpayperPayPeriod = new MoneyType(theWorkNumberSelectType.RateofpayperPayPeriod);
                FrequencyDescription = theWorkNumberSelectType.FrequencyDescription;
                if (theWorkNumberSelectType.CalculatedAnnualIncome != null)
                    CalculatedAnnualIncome = new MoneyType(theWorkNumberSelectType.CalculatedAnnualIncome);
                HoursWorkedPerPayPeriod = theWorkNumberSelectType.HoursWorkedPerPayPeriod;
                if (theWorkNumberSelectType.MostRecentStartDate != null)
                    MostRecentStartDate = new DateType(theWorkNumberSelectType.MostRecentStartDate);
                TotalLengthofServiceinMonths = theWorkNumberSelectType.TotalLengthofServiceinMonths;
                if (theWorkNumberSelectType.DateEmploymentTerminated != null)
                    DateEmploymentTerminated = new DateType(theWorkNumberSelectType.DateEmploymentTerminated);
                StatusMessage = theWorkNumberSelectType.StatusMessage;
                if (theWorkNumberSelectType.YearlyIncomes != null)
                    YearlyIncomes = theWorkNumberSelectType.YearlyIncomes.Select(p => new YearlyIncomeType(p))
                        .ToList<IYearlyIncomeType>();
                EmployerCode = theWorkNumberSelectType.EmployerCode;
                EmployerDisclaimer = theWorkNumberSelectType.EmployerDisclaimer;
                ReferenceNumberServerId = theWorkNumberSelectType.ReferenceNumberServerId;
                ReferralURL = theWorkNumberSelectType.ReferralURL;
                ReferralBureauName = theWorkNumberSelectType.ReferralBureauName;
                if (theWorkNumberSelectType.ReferralBureauAddress != null)
                    ReferralBureauAddress = new SimpleAddressType(theWorkNumberSelectType.ReferralBureauAddress);
            }
        }
        public ICodeType RegulatedIdentifier { get; set; }
        public ICodeType HitNoHitIndicator { get; set; }
        public string TwnDisclaimer { get; set; }
        public ISubjectNameFMLType EmployeeName { get; set; }
        public string EmployeeSSN { get; set; }
        public string EmployerName { get; set; }
        public IDateType DateEmployeeInformationisEffective { get; set; }
        public string EmployeePositionNameorTitle { get; set; }
        public IMoneyType RateofpayperPayPeriod { get; set; }
        public string FrequencyDescription { get; set; }
        public IMoneyType CalculatedAnnualIncome { get; set; }
        public string HoursWorkedPerPayPeriod { get; set; }
        public IDateType MostRecentStartDate { get; set; }
        public string TotalLengthofServiceinMonths { get; set; }
        public IDateType DateEmploymentTerminated { get; set; }
        public string StatusMessage { get; set; }
        public List<IYearlyIncomeType> YearlyIncomes { get; set; }
        public string EmployerCode { get; set; }
        public string EmployerDisclaimer { get; set; }
        public string ReferenceNumberServerId { get; set; }
        public string ReferralURL { get; set; }
        public string ReferralBureauName { get; set; }
        public ISimpleAddressType ReferralBureauAddress { get; set; }
    }
}
