﻿using System.Collections.Generic;
using System.Linq;

namespace LendFoundry.Syndication.Equifax.CreditReportResponse
{
    public class ElectronicIdCompareType : IElectronicIdCompareType
    {
        public ElectronicIdCompareType(Proxy.CreditReportResponse.ElectronicIDCompareType electronicIDCompareType)
        {
            if (electronicIDCompareType != null)
            {
                if (electronicIDCompareType.RegulatedIdentifier != null)
                    RegulatedIdentifier = new CodeType(electronicIDCompareType.RegulatedIdentifier);
                if (electronicIDCompareType.HitNoHitIndicator != null)
                    HitNoHitIndicator = new CodeType(electronicIDCompareType.HitNoHitIndicator);
                if (electronicIDCompareType.ReasonCodes != null)
                    ReasonCodes = electronicIDCompareType.ReasonCodes.Select(p => new ReasonCodeType(p)).ToList<IReasonCodeType>();
                if (electronicIDCompareType.CurrentAddress != null)
                    CurrentAddress = new SimpleAddressType(electronicIDCompareType.CurrentAddress);
                if (electronicIDCompareType.FormerAddress != null)
                    FormerAddress = new SimpleAddressType(electronicIDCompareType.FormerAddress);
                if (electronicIDCompareType.OtherAddress != null)
                    OtherAddress = new SimpleAddressType(electronicIDCompareType.OtherAddress);
                if (electronicIDCompareType.FraudIndicator != null)
                    FraudIndicator = new CodeType(electronicIDCompareType.FraudIndicator);
                Fraudlevel = electronicIDCompareType.Fraudlevel;
                FraudIndexScore = electronicIDCompareType.FraudIndexScore;
                FraudIndexScoreSpecified = electronicIDCompareType.FraudIndexScoreSpecified;
                if (electronicIDCompareType.MatchAssessment != null)
                    MatchAssessment = new CodeType(electronicIDCompareType.MatchAssessment);
                OverallScore = electronicIDCompareType.OverallScore;
                OverallScoreSpecified = electronicIDCompareType.OverallScoreSpecified;
                AssessmentPassed = electronicIDCompareType.AssessmentPassed;
                AddressStandardization = electronicIDCompareType.AddressStandardization;
                ConsumerID = electronicIDCompareType.ConsumerID;
                AggregatorClientID = electronicIDCompareType.AggregatorClientID;
                TransactionKey = electronicIDCompareType.TransactionKey;
                if (electronicIDCompareType.TransactionStatusField != null)
                    TransactionStatus = new CodeType(electronicIDCompareType.TransactionStatusField);
            }
        }
        public ICodeType RegulatedIdentifier { get; set; }
        public ICodeType HitNoHitIndicator { get; set; }
        public List<IReasonCodeType> ReasonCodes { get; set; }
        public ISimpleAddressType CurrentAddress { get; set; }
        public ISimpleAddressType FormerAddress { get; set; }
        public ISimpleAddressType OtherAddress { get; set; }
        public ICodeType FraudIndicator { get; set; }
        public string Fraudlevel { get; set; }
        public short FraudIndexScore { get; set; }
        public bool FraudIndexScoreSpecified { get; set; }
        public ICodeType MatchAssessment { get; set; }
        public short OverallScore { get; set; }
        public bool OverallScoreSpecified { get; set; }
        public string AssessmentPassed { get; set; }
        public string AddressStandardization { get; set; }
        public string ConsumerID { get; set; }
        public string AggregatorClientID { get; set; }
        public ICodeType TransactionStatus { get; set; }
        public string TransactionKey { get; set; }
    }
}
