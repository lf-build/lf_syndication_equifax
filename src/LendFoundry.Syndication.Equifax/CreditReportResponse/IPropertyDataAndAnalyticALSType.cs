﻿namespace LendFoundry.Syndication.Equifax.CreditReportResponse
{
    public interface IPropertyDataAndAnalyticALSType
    {
        CodeType HitNoHitIndicator { get; set; }
        string TrueFalseReturns { get; set; }
        string TransactionId { get; set; }
        string MedianDaysonMarket { get; set; }
        string EmailAddressofListingBroker { get; set; }
        string NameofListingBroker { get; set; }
        string PhoneNumberofListingBroker { get; set; }
        string DaysonMarket { get; set; }
        DateType OriginalListingDateonProperty { get; set; }
        MoneyType ListingPriceontheProperty { get; set; }
        string ListingIdAssociatedwithProperty { get; set; }
        string ListingStatus { get; set; }
        string UniqueIDfortheBoardPosted { get; set; }
        string AverageDaysonMarket { get; set; }
    }
}
