﻿namespace LendFoundry.Syndication.Equifax.CreditReportResponse
{
    public interface IUsConsumerSubject
    {
        int Age { get; set; }
        IDateType DateOfBirth { get; set; }
        IPersonNameType PersonName { get; set; }
        string SocialSecurityNumber { get; set; }
    }
}