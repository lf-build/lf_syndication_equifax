﻿namespace LendFoundry.Syndication.Equifax.CreditReportResponse
{
    public class Attribute2Type : IAttribute2Type
    {
        public Attribute2Type(Proxy.CreditReportResponse.Attribute2Type attribute2Type)
        {
            if (attribute2Type != null)
            {
                if (attribute2Type.Name != null)
                    Name = new CodeType(attribute2Type.Name);
                if (attribute2Type.Value != null)
                    Value = new CodeType(attribute2Type.Value);
            }
        }
        public ICodeType Name { get; set; }
        public ICodeType Value { get; set; }
    }
}
