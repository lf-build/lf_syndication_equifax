namespace LendFoundry.Syndication.Equifax.CreditReportResponse
{
    public class UsConsumerStatementType : IUsConsumerStatementType
    {
        public UsConsumerStatementType(Proxy.CreditReportResponse.USConsumerStatementType usConsumerStatementType)
        {
            if (usConsumerStatementType != null)
            {
                if (usConsumerStatementType.DateReported != null)
                    DateReported = new DateType(usConsumerStatementType.DateReported);
                if (usConsumerStatementType.DateToBePurged != null)
                    DateToBePurged = new DateType(usConsumerStatementType.DateToBePurged);
                Statement = usConsumerStatementType.Statement;
            }
        }

        public IDateType DateReported { get; set; }

        public IDateType DateToBePurged { get; set; }

        public string Statement { get; set; }
    }
}