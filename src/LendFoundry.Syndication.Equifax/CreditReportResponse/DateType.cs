namespace LendFoundry.Syndication.Equifax.CreditReportResponse
{
    public class DateType : IDateType
    {
        public DateType(Proxy.CreditReportResponse.DateType dateType)
        {
            if (dateType != null)
            {
                Format = (DateFormat)dateType.format;
                Value = dateType.Value;
            }
        }

        public DateFormat Format { get; set; }

        public string Value { get; set; }
    }
}