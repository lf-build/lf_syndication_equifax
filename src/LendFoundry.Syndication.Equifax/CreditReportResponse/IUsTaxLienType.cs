﻿using System.Collections.Generic;

namespace LendFoundry.Syndication.Equifax.CreditReportResponse
{
    public interface IUsTaxLienType
    {
        IMoneyType Amount { get; set; }
        string CaseNumber { get; set; }
        ICustomerIdType CourtId { get; set; }
        IDateType DateFiled { get; set; }
        IDateType DateReported { get; set; }
        ICodeType LienClass { get; set; }
        ICodeType LienStatus { get; set; }
        List<ICodeType> Narratives { get; set; }
        IDateType ReleaseDate { get; set; }
        IDateType VerificationDate { get; set; }
    }
}