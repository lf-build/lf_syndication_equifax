namespace LendFoundry.Syndication.Equifax.CreditReportResponse
{
    public interface IPersonNameType
    {
        string LastName { get; set; }
        string FirstName { get; set; }
        string MiddleName { get; set; }
        string NameSuffix { get; set; }
    }
}