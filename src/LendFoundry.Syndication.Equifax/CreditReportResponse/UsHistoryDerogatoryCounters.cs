namespace LendFoundry.Syndication.Equifax.CreditReportResponse
{
    public class UsHistoryDerogatoryCounters : IUsHistoryDerogatoryCounters
    {
        public UsHistoryDerogatoryCounters(Proxy.CreditReportResponse.USTradeTypeHistoryDerogatoryCounters usDerogatoryCounters)
        {
            if (usDerogatoryCounters != null)
            {
                Count30DayPastDue = usDerogatoryCounters.Count30DayPastDue;
                Count60DayPastDue = usDerogatoryCounters.Count60DayPastDue;
                Count90DayPastDue = usDerogatoryCounters.Count90DayPastDue;
            }
        }

        public short Count30DayPastDue { get; set; }

        public short Count60DayPastDue { get; set; }

        public short Count90DayPastDue { get; set; }
    }
}