﻿using System.Collections.Generic;
using System.Linq;

namespace LendFoundry.Syndication.Equifax.CreditReportResponse
{
    public class PropertyDataAndAnalyticPROPType : IPropertyDataAndAnalyticPROPType
    {
        public PropertyDataAndAnalyticPROPType(Proxy.CreditReportResponse.PropertyDataAndAnalyticPROPType propertyDataAndAnalyticPROPType)
        {
            if (propertyDataAndAnalyticPROPType != null)
            {
                if (propertyDataAndAnalyticPROPType.HitNoHitIndicator != null)
                    HitNoHitIndicator = new CodeType(propertyDataAndAnalyticPROPType.HitNoHitIndicator);
                AssessedLotSizeofPropertyinAcres = propertyDataAndAnalyticPROPType.AssessedLotSizeofPropertyinAcres;
                if (propertyDataAndAnalyticPROPType.AppraisedValueofPropertybyCtyAssessor != null)
                    AppraisedValueofPropertybyCtyAssessor = new MoneyType(propertyDataAndAnalyticPROPType.AppraisedValueofPropertybyCtyAssessor);
                if (propertyDataAndAnalyticPROPType.AssessedValueofPropertyValueAssigned != null)
                    AssessedValueofPropertyValueAssigned = new MoneyType(propertyDataAndAnalyticPROPType.AssessedValueofPropertyValueAssigned);
                YearthatPropertywasLastAssessed = propertyDataAndAnalyticPROPType.YearthatPropertywasLastAssessed;
                if (propertyDataAndAnalyticPROPType.ValueAssignedbyCountyTaxAssesImprovements != null)
                    ValueAssignedbyCountyTaxAssesImprovements = new MoneyType(propertyDataAndAnalyticPROPType.ValueAssignedbyCountyTaxAssesImprovements);
                if (propertyDataAndAnalyticPROPType.AssessedLandValueoftheProperty != null)
                    AssessedLandValueoftheProperty = new MoneyType(propertyDataAndAnalyticPROPType.AssessedLandValueoftheProperty);
                FinishedSquareFootageofPropertyBasement = propertyDataAndAnalyticPROPType.FinishedSquareFootageofPropertyBasement;
                TotalSquareFootageofpropertiesBasement = propertyDataAndAnalyticPROPType.TotalSquareFootageofpropertiesBasement;
                CountyWherePropertyisLocated = propertyDataAndAnalyticPROPType.CountyWherePropertyisLocated;
                ElementarySchoolDistrictforProperty = propertyDataAndAnalyticPROPType.ElementarySchoolDistrictforProperty;
                FederalInformationProcessingStandard = propertyDataAndAnalyticPROPType.FederalInformationProcessingStandard;
                TotalSquareFootageofPropertiesGarage = propertyDataAndAnalyticPROPType.TotalSquareFootageofPropertiesGarage;
                TypeofGarage = propertyDataAndAnalyticPROPType.TypeofGarage;
                SpecifiesNorthSouthPositionoftheProperty = propertyDataAndAnalyticPROPType.SpecifiesNorthSouthPositionoftheProperty;
                SpecifiesEastWestPositionoftheProperty = propertyDataAndAnalyticPROPType.SpecifiesEastWestPositionoftheProperty;
                MainSquareFootageofProperty = propertyDataAndAnalyticPROPType.MainSquareFootageofProperty;
                if (propertyDataAndAnalyticPROPType.LikelyPriceaPropertyisWorthinaFairSale != null)
                    LikelyPriceaPropertyisWorthinaFairSale = new MoneyType(propertyDataAndAnalyticPROPType.LikelyPriceaPropertyisWorthinaFairSale);
                MunicipalCode = propertyDataAndAnalyticPROPType.MunicipalCode;
                NumberofBathrooms = propertyDataAndAnalyticPROPType.NumberofBathrooms;
                NumberofBathroomsSpecified = propertyDataAndAnalyticPROPType.NumberofBathroomsSpecified;
                NumberofBedrooms = propertyDataAndAnalyticPROPType.NumberofBedrooms;
                NumberofBedroomsSpecified = propertyDataAndAnalyticPROPType.NumberofBedroomsSpecified;
                NumberofFireplacesontheProperty = propertyDataAndAnalyticPROPType.NumberofFireplacesontheProperty;
                NumberofFireplacesonthePropertySpecified = propertyDataAndAnalyticPROPType.NumberofFireplacesonthePropertySpecified;
                NumberofGarageSpacesontheProperty = propertyDataAndAnalyticPROPType.NumberofGarageSpacesontheProperty;
                NumberofGarageSpacesonthePropertySpecified = propertyDataAndAnalyticPROPType.NumberofGarageSpacesonthePropertySpecified;
                NumberofLevelsonProperty = propertyDataAndAnalyticPROPType.NumberofLevelsonProperty;
                NumberofLevelsonPropertySpecified = propertyDataAndAnalyticPROPType.NumberofLevelsonPropertySpecified;
                if (propertyDataAndAnalyticPROPType.PropertyOwnersNames != null)
                    PropertyOwnersNames = propertyDataAndAnalyticPROPType.PropertyOwnersNames.Select(p => new PropertyOwnersNamesType(p)).ToList<IPropertyOwnersNamesType>();
                if (propertyDataAndAnalyticPROPType.FlagtoIndicateifOwnerOccupiestheProperty != null)
                    FlagtoIndicateifOwnerOccupiestheProperty = new CodeType(propertyDataAndAnalyticPROPType.FlagtoIndicateifOwnerOccupiestheProperty);
                PropertyStyle = propertyDataAndAnalyticPROPType.PropertyStyle;
                TypeofProperty = propertyDataAndAnalyticPROPType.TypeofProperty;
                TypeofRoofonProperty = propertyDataAndAnalyticPROPType.TypeofRoofonProperty;
                SecondarySchoolDistrictforProperty = propertyDataAndAnalyticPROPType.SecondarySchoolDistrictforProperty;
                SideofStreetWherePropertyisLocated = propertyDataAndAnalyticPROPType.SideofStreetWherePropertyisLocated;
                StateFIPSinWhichthePropertyResides = propertyDataAndAnalyticPROPType.StateFIPSinWhichthePropertyResides;
                NameofSubdivisionWherePropertyisLocated = propertyDataAndAnalyticPROPType.NameofSubdivisionWherePropertyisLocated;
                WhetherorNotthePropertyisOwnedByanREOentity = propertyDataAndAnalyticPROPType.WhetherorNotthePropertyisOwnedByanREOentity;
                if (propertyDataAndAnalyticPROPType.TaxSaleDateforthePropertypertheCounty != null)
                    TaxSaleDateforthePropertypertheCounty = new DateType(propertyDataAndAnalyticPROPType.TaxSaleDateforthePropertypertheCounty);
                if (propertyDataAndAnalyticPROPType.TaxSalePriceforthePropertypertheCounty != null)
                    TaxSalePriceforthePropertypertheCounty = new MoneyType(propertyDataAndAnalyticPROPType.TaxSalePriceforthePropertypertheCounty);
                TotalNumberofRoomsonProperty = propertyDataAndAnalyticPROPType.TotalNumberofRoomsonProperty;
                TotalNumberofRoomsonPropertySpecified = propertyDataAndAnalyticPROPType.TotalNumberofRoomsonPropertySpecified;
                UseCodeforthePropertyPertheCounty = propertyDataAndAnalyticPROPType.UseCodeforthePropertyPertheCounty;
                UnifiedSchoolDistrictforthePropertypertheCounty = propertyDataAndAnalyticPROPType.UnifiedSchoolDistrictforthePropertypertheCounty;
                if (propertyDataAndAnalyticPROPType.YearthatthePropertywasBuilt != null)
                    YearthatthePropertywasBuilt = new DateType(propertyDataAndAnalyticPROPType.YearthatthePropertywasBuilt);
                LocalMunicipalLawSpecifiesWhatPurposeMaybeUsed = propertyDataAndAnalyticPROPType.LocalMunicipalLawSpecifiesWhatPurposeMaybeUsed;
                if (propertyDataAndAnalyticPROPType.TaxMailingAddressforthePropertysOwner != null)
                    TaxMailingAddressforthePropertysOwner = new SimpleAddressType(propertyDataAndAnalyticPROPType.TaxMailingAddressforthePropertysOwner);
                if (propertyDataAndAnalyticPROPType.LastSaleDateofthePropertyPertheCounty != null)
                    LastSaleDateofthePropertyPertheCounty = new DateType(propertyDataAndAnalyticPROPType.LastSaleDateofthePropertyPertheCounty);
                if (propertyDataAndAnalyticPROPType.LastSalePriceofthePropertyPertheCounty != null)
                    LastSalePriceofthePropertyPertheCounty = new MoneyType(propertyDataAndAnalyticPROPType.LastSalePriceofthePropertyPertheCounty);
            }
        }
        public ICodeType HitNoHitIndicator { get; set; }
        public string AssessedLotSizeofPropertyinAcres { get; set; }
        public IMoneyType AppraisedValueofPropertybyCtyAssessor { get; set; }
        public IMoneyType AssessedValueofPropertyValueAssigned { get; set; }
        public string YearthatPropertywasLastAssessed { get; set; }
        public IMoneyType ValueAssignedbyCountyTaxAssesImprovements { get; set; }
        public IMoneyType AssessedLandValueoftheProperty { get; set; }
        public string FinishedSquareFootageofPropertyBasement { get; set; }
        public string TotalSquareFootageofpropertiesBasement { get; set; }
        public string CountyWherePropertyisLocated { get; set; }
        public string ElementarySchoolDistrictforProperty { get; set; }
        public string FederalInformationProcessingStandard { get; set; }
        public string TotalSquareFootageofPropertiesGarage { get; set; }
        public string TypeofGarage { get; set; }
        public string SpecifiesNorthSouthPositionoftheProperty { get; set; }
        public string SpecifiesEastWestPositionoftheProperty { get; set; }
        public string MainSquareFootageofProperty { get; set; }
        public IMoneyType LikelyPriceaPropertyisWorthinaFairSale { get; set; }
        public string MunicipalCode { get; set; }
        public short NumberofBathrooms { get; set; }
        public bool NumberofBathroomsSpecified { get; set; }
        public short NumberofBedrooms { get; set; }
        public bool NumberofBedroomsSpecified { get; set; }
        public short NumberofFireplacesontheProperty { get; set; }
        public bool NumberofFireplacesonthePropertySpecified { get; set; }
        public short NumberofGarageSpacesontheProperty { get; set; }
        public bool NumberofGarageSpacesonthePropertySpecified { get; set; }
        public short NumberofLevelsonProperty { get; set; }
        public bool NumberofLevelsonPropertySpecified { get; set; }
        public List<IPropertyOwnersNamesType> PropertyOwnersNames { get; set; }
        public ICodeType FlagtoIndicateifOwnerOccupiestheProperty { get; set; }
        public string PropertyStyle { get; set; }
        public string TypeofProperty { get; set; }
        public string TypeofRoofonProperty { get; set; }
        public string SecondarySchoolDistrictforProperty { get; set; }
        public string SideofStreetWherePropertyisLocated { get; set; }
        public string StateFIPSinWhichthePropertyResides { get; set; }
        public string NameofSubdivisionWherePropertyisLocated { get; set; }
        public string WhetherorNotthePropertyisOwnedByanREOentity { get; set; }
        public IDateType TaxSaleDateforthePropertypertheCounty { get; set; }
        public IMoneyType TaxSalePriceforthePropertypertheCounty { get; set; }
        public short TotalNumberofRoomsonProperty { get; set; }
        public bool TotalNumberofRoomsonPropertySpecified { get; set; }
        public string UseCodeforthePropertyPertheCounty { get; set; }
        public string UnifiedSchoolDistrictforthePropertypertheCounty { get; set; }
        public IDateType YearthatthePropertywasBuilt { get; set; }
        public string LocalMunicipalLawSpecifiesWhatPurposeMaybeUsed { get; set; }
        public ISimpleAddressType TaxMailingAddressforthePropertysOwner { get; set; }
        public IDateType LastSaleDateofthePropertyPertheCounty { get; set; }
        public IMoneyType LastSalePriceofthePropertyPertheCounty { get; set; }
    }
}
