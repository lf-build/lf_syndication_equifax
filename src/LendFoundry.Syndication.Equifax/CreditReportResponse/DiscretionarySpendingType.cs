﻿namespace LendFoundry.Syndication.Equifax.CreditReportResponse
{
    public class DiscretionarySpendingType : IDiscretionarySpendingType
    {
        public DiscretionarySpendingType(Proxy.CreditReportResponse.DiscretionarySpendingType discretionarySpendingType)
        {
            if (discretionarySpendingType != null)
            {
                ProductId = discretionarySpendingType.ProductID;
                PostalCode = discretionarySpendingType.PostalCode;
                PostalCodePlus4 = discretionarySpendingType.PostalCodePlus4;
                Age = discretionarySpendingType.Age;
                AgeSpecified = discretionarySpendingType.AgeSpecified;
                AgeBand = discretionarySpendingType.AgeBand;
                if (discretionarySpendingType.IncomeofConsumer != null)
                    IncomeofConsumer = new MoneyType(discretionarySpendingType.IncomeofConsumer);
                if (discretionarySpendingType.Income != null)
                    Income = new MoneyType(discretionarySpendingType.Income);
                if (discretionarySpendingType.SpendingIndex != null)
                    SpendingIndex = new MoneyType(discretionarySpendingType.SpendingIndex);
                if (discretionarySpendingType.SpendingMassMarket != null)
                    SpendingMassMarket = new MoneyType(discretionarySpendingType.SpendingMassMarket);
                DispositionCode = discretionarySpendingType.DispositionCode;
            }
        }
        public string ProductId { get; set; }
        public string PostalCode { get; set; }
        public string PostalCodePlus4 { get; set; }
        public short Age { get; set; }
        public bool AgeSpecified { get; set; }
        public string AgeBand { get; set; }
        public IMoneyType IncomeofConsumer { get; set; }
        public IMoneyType Income { get; set; }
        public IMoneyType SpendingIndex { get; set; }
        public IMoneyType SpendingMassMarket { get; set; }
        public string DispositionCode { get; set; }
    }
}
