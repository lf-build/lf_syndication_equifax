using System.Collections.Generic;
using System.Linq;

namespace LendFoundry.Syndication.Equifax.CreditReportResponse
{
    public class UsAlertContactType : IUsAlertContactType
    {
        public UsAlertContactType(Proxy.CreditReportResponse.USAlertContactType usAlertContactType)
        {
            if (usAlertContactType != null)
            {
                if (usAlertContactType.AlertType != null)
                    AlertType = new CodeType(usAlertContactType.AlertType);

                if (usAlertContactType.DateReported != null)
                    DateReported = new DateType(usAlertContactType.DateReported);

                if (usAlertContactType.DateEffective != null)
                    DateEffective = new DateType(usAlertContactType.DateEffective);

                AddressLine1 = usAlertContactType.AddressLine1;
                AddressLine2 = usAlertContactType.AddressLine2;

                if (usAlertContactType.City != null)
                    City = new CityType(usAlertContactType.City);

                State = usAlertContactType.State;
                PostalCode = usAlertContactType.PostalCode;
                CountryCode = usAlertContactType.CountryCode;

                if (usAlertContactType.PhoneNumbers != null)
                    PhoneNumbers =
                        usAlertContactType.PhoneNumbers.Select(p => new UsAlertContactTypePhoneNumber(p))
                            .ToList<IUsAlertContactTypePhoneNumber>();
                FreeformInformation = usAlertContactType.FreeformInformation;
            }
        }

        public ICodeType AlertType { get; set; }

        public IDateType DateReported { get; set; }

        public IDateType DateEffective { get; set; }

        public string AddressLine1 { get; set; }

        public string AddressLine2 { get; set; }

        public ICityType City { get; set; }

        public string State { get; set; }

        public string PostalCode { get; set; }

        public string CountryCode { get; set; }

        public List<IUsAlertContactTypePhoneNumber> PhoneNumbers { get; set; }

        public string FreeformInformation { get; set; }
    }
}