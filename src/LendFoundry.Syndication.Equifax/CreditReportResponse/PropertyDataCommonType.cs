﻿using System.Collections.Generic;
using System.Linq;

namespace LendFoundry.Syndication.Equifax.CreditReportResponse
{
    public class PropertyDataCommonType : IPropertyDataCommonType
    {
        public PropertyDataCommonType(Proxy.CreditReportResponse.PropertyDataCommonType propertyDataCommonType)
        {
            if(propertyDataCommonType != null)
            {
                GeoAreaType = propertyDataCommonType.GeoAreaType;
                GeoValue = propertyDataCommonType.GeoValue;
                if (propertyDataCommonType.PricingData != null)
                    PricingData = propertyDataCommonType.PricingData.Select(p => new PropertyDataSubCommonType(p)).ToList<IPropertyDataSubCommonType>();
                Number = propertyDataCommonType.number;
            }
        }
        public string GeoAreaType { get; set; }
        public string GeoValue { get; set; }
        public List<IPropertyDataSubCommonType> PricingData { get; set; }
        public short Number { get; set; }
    }
}
