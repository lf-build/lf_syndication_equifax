using System.Collections.Generic;
using System.Linq;

namespace LendFoundry.Syndication.Equifax.CreditReportResponse
{
    public class UsConsumerCreditReportType : IUsConsumerCreditReportType
    {
        public UsConsumerCreditReportType(Proxy.CreditReportResponse.USConsumerCreditReportType usConsumerCreditReportType)
        {
            if (usConsumerCreditReportType != null)
            {
                if (usConsumerCreditReportType.USHeader != null)
                    UsHeader = new UsHeaderType(usConsumerCreditReportType.USHeader);
                if (usConsumerCreditReportType.USAddresses != null)
                    UsAddresses =
                        usConsumerCreditReportType.USAddresses.Select(p => new UsAddressType(p))
                            .ToList<IUsAddressType>();

                if (usConsumerCreditReportType.IdentityScan != null)
                    IdentityScan = new IdentityScanType(usConsumerCreditReportType.IdentityScan);

                if (usConsumerCreditReportType.USOtherNames != null)
                    UsOtherNames = usConsumerCreditReportType.USOtherNames.Select(p => new UsOtherNameType(
                        new Proxy.CreditReportResponse.SubjectNameType
                        {
                            NameSuffix = p.NameSuffix,
                            FirstName = p.FirstName,
                            MiddleName = p.MiddleName,
                            LastName = p.LastName,
                            //NamePrefix = p.NamePrefix
                        }, p
                        )).ToList<IUsOtherNameType>();

                if (usConsumerCreditReportType.USSubjectDeath != null)
                    UsSubjectDeath = new UsSubjectDeathType(usConsumerCreditReportType.USSubjectDeath);

                if (usConsumerCreditReportType.USEmployments != null)
                    UsEmployments =
                        usConsumerCreditReportType.USEmployments.Select(p => new UsEmploymentType(p))
                            .ToList<IUsEmploymentType>();

                if (usConsumerCreditReportType.USCollections != null)
                    UsCollections =
                        usConsumerCreditReportType.USCollections.Select(p => new UsCollectionType(p))
                            .ToList<IUsCollectionType>();

                if (usConsumerCreditReportType.USTrades != null)
                    UsTrades =
                        usConsumerCreditReportType.USTrades.Select(p => new UsTradeType((p))).ToList<IUsTradeType>();

                if (usConsumerCreditReportType.USInquiries != null)
                    UsInquiries =
                        usConsumerCreditReportType.USInquiries.Select(p => new UsInquiryType((p)))
                            .ToList<IUsInquiryType>();

                if (usConsumerCreditReportType.USOtherIdentifications != null)
                    UsOtherIdentifications =
                        usConsumerCreditReportType.USOtherIdentifications.Select(p => new UsOtherIdentificationType((p)))
                            .ToList<IUsOtherIdentificationType>();

                if (usConsumerCreditReportType.USBeacon != null)

                    UsBeacon = new UsBeaconType(usConsumerCreditReportType.USBeacon);

                if (usConsumerCreditReportType.USOnlineDirectories != null)
                    UsOnlineDirectories =
                        usConsumerCreditReportType.USOnlineDirectories.Select(p => new UsOnlineDirectoryType((p)))
                            .ToList<IUsOnlineDirectoryType>();

                if (usConsumerCreditReportType.USModelScores != null)
                    UsModelScores =
                        usConsumerCreditReportType.USModelScores.Select(p => new UsScoreType(p)).ToList<IUsScoreType>();

                if (usConsumerCreditReportType.USDataAttributes != null)
                    UsDataAttributes =
                        usConsumerCreditReportType.USDataAttributes.Select(p => new UsDataAttributeType(p))
                            .ToList<IUsDataAttributeType>();

                if (usConsumerCreditReportType.USIdentificationSSN != null)

                    UsIdentificationSsn = new UsIdentificationSsnType(usConsumerCreditReportType.USIdentificationSSN);
                //TODO: Changed from  USOFACAlerts -> USCDCOFACAlerts
                if (usConsumerCreditReportType.USCDCOFACAlerts != null)
                    UsofacAlerts =
                        usConsumerCreditReportType.USCDCOFACAlerts.Select(p => new UsofacAlertType(p))
                            .ToList<IUsofacAlertType>();

                if (usConsumerCreditReportType.USConsumerReferral != null)
                    UsConsumerReferral = new UsConsumerReferralType(usConsumerCreditReportType.USConsumerReferral);

                //TODO: Not Exist now in new schema
                //if (usConsumerCreditReportType.USEMailAddresses != null)
                //    UseMailAddresses =
                //        usConsumerCreditReportType.USEMailAddresses.Select(p => new UseMailAddressType(p))
                //            .ToList<IUseMailAddressType>();

                //TODO: Not Exist now in new schema
                //if (usConsumerCreditReportType.USOtherTelephoneNumbers != null)
                //    UsOtherTelephoneNumbers =
                //        usConsumerCreditReportType.USOtherTelephoneNumbers.Select(p => new UsOtherTelephoneNumberType(p))
                //            .ToList<IUsOtherTelephoneNumberType>();

                if (usConsumerCreditReportType.USBankruptcies != null)
                    UsBankruptcies =
                        usConsumerCreditReportType.USBankruptcies.Select(p => new UsBankruptcyType(p))
                            .ToList<IUsBankruptcyType>();
                if (usConsumerCreditReportType.USLegalItems != null)
                    UsLegalItems =
                        usConsumerCreditReportType.USLegalItems.Select(p => new UsLegalItemType(p))
                            .ToList<IUsLegalItemType>();

                //TODO: Not Exist now in new schema
                //if (usConsumerCreditReportType.USFileId != null)
                //    UsFileId = new UsFileIdType(usConsumerCreditReportType.USFileId);

                if (usConsumerCreditReportType.USAlertContacts != null)
                    UsAlertContacts =
                        usConsumerCreditReportType.USAlertContacts.Select(p => new UsAlertContactType(p))
                            .ToList<IUsAlertContactType>();

                if (usConsumerCreditReportType.USTaxLiens != null)
                    UsTaxLiens =
                        usConsumerCreditReportType.USTaxLiens.Select(p => new UsTaxLienType(p)).ToList<IUsTaxLienType>();

                if (usConsumerCreditReportType.USConsumerStatements != null)
                    UsConsumerStatements =
                        usConsumerCreditReportType.USConsumerStatements.Select(p => new UsConsumerStatementType(p))
                            .ToList<IUsConsumerStatementType>();

                if (usConsumerCreditReportType.USEDAS != null)
                    Usedas = new UsedasType(usConsumerCreditReportType.USEDAS);
                if (usConsumerCreditReportType.USFICO != null)
                    Usfico = new UsficoType(usConsumerCreditReportType.USFICO);

                if (usConsumerCreditReportType.USGeoCode != null)
                    UsGeoCode = new UsGeoCodeType(usConsumerCreditReportType.USGeoCode);

                if (usConsumerCreditReportType.USDecisionPowerSegments != null)
                    UsDecisionPowerSegments =
                        usConsumerCreditReportType.USDecisionPowerSegments.Select(p => new UsDecisionPowerDataType(p))
                            .ToList<IUsDecisionPowerDataType>();

                if (usConsumerCreditReportType.USFraudAdvisor != null)
                    UsFraudAdvisor = new UsFraudAdvisorType(usConsumerCreditReportType.USFraudAdvisor);

                if (usConsumerCreditReportType.USErrorMessages != null)
                    UsErrorMessages =
                        usConsumerCreditReportType.USErrorMessages.Select(p => new UsErrorMessageType(p))
                            .ToList<IUsErrorMessageType>();

                //TODO: Not Exist now in new schema
                //RelationshipIdentifier = (RelationshipType)usConsumerCreditReportType.relationshipIdentifier;
                MultipleNumber = usConsumerCreditReportType.multipleNumber;
                SubjectType = usConsumerCreditReportType.subjectType;
                if (usConsumerCreditReportType.USCDCOFACAlerts != null)
                    UsCdcOfacAlerts =
                        usConsumerCreditReportType.USCDCOFACAlerts.Select(p => new UsCdcOfacType(p))
                            .ToList<IUsCdcOfacType>();
                if (usConsumerCreditReportType.USAlternateDataSources != null)
                    UsAlternateDataSources = new UsConsumerCreditReportTypeUSAlternateDataSources(usConsumerCreditReportType.USAlternateDataSources);
            }
        }

        public IUsHeaderType UsHeader { get; set; }
        public List<IUsAddressType> UsAddresses { get; set; }
        public IIdentityScanType IdentityScan { get; set; }
        public List<IUsOtherNameType> UsOtherNames { get; set; }
        public IUsSubjectDeathType UsSubjectDeath { get; set; }

        public List<IUsEmploymentType> UsEmployments { get; set; }

        public List<IUsOtherIdentificationType> UsOtherIdentifications { get; set; }

        public List<IUseMailAddressType> UseMailAddresses { get; set; }

        public List<IUsOtherTelephoneNumberType> UsOtherTelephoneNumbers { get; set; }

        public List<IUsBankruptcyType> UsBankruptcies { get; set; }

        public List<IUsCollectionType> UsCollections { get; set; }

        public List<IUsLegalItemType> UsLegalItems { get; set; }

        public IUsFileIdType UsFileId { get; set; }
        public List<IUsAlertContactType> UsAlertContacts { get; set; }

        public List<IUsTaxLienType> UsTaxLiens { get; set; }

        public List<IUsTradeType> UsTrades { get; set; }

        public List<IUsInquiryType> UsInquiries { get; set; }

        public List<IUsConsumerStatementType> UsConsumerStatements { get; set; }

        public IUsedasType Usedas { get; set; }

        public IUsBeaconType UsBeacon { get; set; }

        public IUsficoType Usfico { get; set; }

        public List<IUsOnlineDirectoryType> UsOnlineDirectories { get; set; }

        public List<IUsScoreType> UsModelScores { get; set; }

        public List<IUsDataAttributeType> UsDataAttributes { get; set; }

        public IUsIdentificationSsnType UsIdentificationSsn { get; set; }

        public IUsGeoCodeType UsGeoCode { get; set; }

        public List<IUsDecisionPowerDataType> UsDecisionPowerSegments { get; set; }

        public IUsFraudAdvisorType UsFraudAdvisor { get; set; }

        public List<IUsofacAlertType> UsofacAlerts { get; set; }

        public IUsConsumerReferralType UsConsumerReferral { get; set; }

        public List<IUsErrorMessageType> UsErrorMessages { get; set; }

        public RelationshipType RelationshipIdentifier { get; set; }

        public short MultipleNumber { get; set; }
        public string SubjectType { get; set; }
        public List<IUsCdcOfacType> UsCdcOfacAlerts { get; set; }
        public IUsConsumerCreditReportTypeUSAlternateDataSources UsAlternateDataSources { get; set; }
    }
}