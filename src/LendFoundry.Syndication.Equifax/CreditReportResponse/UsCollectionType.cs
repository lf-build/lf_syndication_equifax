using System.Collections.Generic;
using System.Linq;

namespace LendFoundry.Syndication.Equifax.CreditReportResponse
{
    public class UsCollectionType : IUsCollectionType
    {
        public UsCollectionType(Proxy.CreditReportResponse.USCollectionType usCollectionType)
        {
            if (usCollectionType != null)
            {
                if (usCollectionType.DateReported != null)
                    DateReported = new DateType(usCollectionType.DateReported);

                if (usCollectionType.AssignedDate != null)
                    AssignedDate = new DateType(usCollectionType.AssignedDate);

                if (usCollectionType.CustomerId != null)
                    CustomerId = new CustomerIdType(usCollectionType.CustomerId);

                ClientNameNumber = usCollectionType.ClientNameNumber;
                AccountNumber = usCollectionType.AccountNumber;

                if (usCollectionType.OriginalAmount != null)
                    OriginalAmount = new MoneyType(usCollectionType.OriginalAmount);

                if (usCollectionType.BalanceAmount != null)
                    BalanceAmount = new MoneyType(usCollectionType.BalanceAmount);

                //TODO: Not Exist in new Schema 
                //if (usCollectionType.BalanceDate != null)
                //    BalanceDate = new DateType(usCollectionType.BalanceDate);

                if (usCollectionType.DateOfLastPayment != null)
                    DateOfLastPayment = new DateType(usCollectionType.DateOfLastPayment);

                if (usCollectionType.StatusDate != null)
                    StatusDate = new DateType(usCollectionType.StatusDate);

                if (usCollectionType.Status != null)
                    Status = new CodeType(usCollectionType.Status);

                if (usCollectionType.DateOfFirstDelinquency != null)
                    DateOfFirstDelinquency = new DateType(usCollectionType.DateOfFirstDelinquency);

                if (usCollectionType.AccountDesignator != null)
                    AccountDesignator = new CodeType(usCollectionType.AccountDesignator);

                //TODO: Changed from CreditorClassification => ExpandedCreditorClassification
                if (usCollectionType.ExpandedCreditorClassification != null)
                    CreditorClassification = new CodeType(usCollectionType.ExpandedCreditorClassification);

                if (usCollectionType.UpdateIndicator != null)
                    UpdateIndicator = new CodeType(usCollectionType.UpdateIndicator);

                if (usCollectionType.Narratives != null)
                    Narratives = usCollectionType.Narratives.Select((p => new CodeType(p))).ToList<ICodeType>();
            }
        }

        public IDateType DateReported { get; set; }

        public IDateType AssignedDate { get; set; }

        public ICustomerIdType CustomerId { get; set; }

        public string ClientNameNumber { get; set; }

        public string AccountNumber { get; set; }

        public IMoneyType OriginalAmount { get; set; }

        public IMoneyType BalanceAmount { get; set; }

        public IDateType BalanceDate { get; set; }

        public IDateType DateOfLastPayment { get; set; }

        public IDateType StatusDate { get; set; }

        public ICodeType Status { get; set; }

        public IDateType DateOfFirstDelinquency { get; set; }

        public ICodeType AccountDesignator { get; set; }

        public ICodeType CreditorClassification { get; set; }

        public ICodeType UpdateIndicator { get; set; }

        public List<ICodeType> Narratives { get; set; }
    }
}