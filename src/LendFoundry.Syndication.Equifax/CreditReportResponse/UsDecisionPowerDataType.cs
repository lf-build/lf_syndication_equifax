namespace LendFoundry.Syndication.Equifax.CreditReportResponse
{
    public class UsDecisionPowerDataType : IUsDecisionPowerDataType
    {
        public UsDecisionPowerDataType(Proxy.CreditReportResponse.USDecisionPowerDataType usDecisionPowerDataType)
        {
            if (usDecisionPowerDataType != null)
            {
                Decision = usDecisionPowerDataType.Decision;
            }
        }

        public string Decision { get; set; }
    }
}