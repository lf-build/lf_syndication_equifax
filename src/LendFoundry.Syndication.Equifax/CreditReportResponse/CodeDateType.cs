namespace LendFoundry.Syndication.Equifax.CreditReportResponse
{
    public class CodeDateType : CodeType, ICodeDateType
    {
        public CodeDateType(Proxy.CreditReportResponse.CodeType codeType, Proxy.CreditReportResponse.CodeDateType codeDateType) :
            base(codeType)
        {
            if (codeType != null)
            {
                if (codeDateType != null)

                    Date = new DateType(codeDateType.Date);
            }
        }

        public DateType Date { get; set; }
    }
}