﻿namespace LendFoundry.Syndication.Equifax.CreditReportResponse
{
    public interface IIDRevealDataTypeAttribute : IAttribute2Type
    {
        short Number { get; set; }
    }
}
