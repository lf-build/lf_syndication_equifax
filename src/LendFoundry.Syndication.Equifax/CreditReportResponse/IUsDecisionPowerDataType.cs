﻿namespace LendFoundry.Syndication.Equifax.CreditReportResponse
{
    public interface IUsDecisionPowerDataType
    {
        string Decision { get; set; }
    }
}