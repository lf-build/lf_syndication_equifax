﻿namespace LendFoundry.Syndication.Equifax.CreditReportResponse
{
    public class PropertyDataAndAnalyticALSType : IPropertyDataAndAnalyticALSType
    {
        public PropertyDataAndAnalyticALSType(Proxy.CreditReportResponse.PropertyDataAndAnalyticALSType propertyDataAndAnalyticALSType)
        {
            if (propertyDataAndAnalyticALSType != null)
            {
                if (propertyDataAndAnalyticALSType.HitNoHitIndicator != null)
                    HitNoHitIndicator = new CodeType(propertyDataAndAnalyticALSType.HitNoHitIndicator);
                TrueFalseReturns = propertyDataAndAnalyticALSType.TrueFalseReturns;
                TransactionId = propertyDataAndAnalyticALSType.TransactionID;
                MedianDaysonMarket = propertyDataAndAnalyticALSType.MedianDaysonMarket;
                EmailAddressofListingBroker = propertyDataAndAnalyticALSType.EmailAddressofListingBroker;
                NameofListingBroker = propertyDataAndAnalyticALSType.NameofListingBroker;
                PhoneNumberofListingBroker = propertyDataAndAnalyticALSType.PhoneNumberofListingBroker;
                DaysonMarket = propertyDataAndAnalyticALSType.DaysonMarket;
                if (propertyDataAndAnalyticALSType.OriginalListingDateonProperty != null)
                    OriginalListingDateonProperty = new DateType(propertyDataAndAnalyticALSType.OriginalListingDateonProperty);
                if (propertyDataAndAnalyticALSType.ListingPriceontheProperty != null)
                    ListingPriceontheProperty = new MoneyType(propertyDataAndAnalyticALSType.ListingPriceontheProperty);
                ListingIdAssociatedwithProperty = propertyDataAndAnalyticALSType.ListingIDAssociatedwithProperty;
                ListingStatus = propertyDataAndAnalyticALSType.ListingStatus;
                UniqueIDfortheBoardPosted = propertyDataAndAnalyticALSType.UniqueIDfortheBoardPosted;
                AverageDaysonMarket = propertyDataAndAnalyticALSType.AverageDaysonMarket;
            }
        }
        public CodeType HitNoHitIndicator { get; set; }
        public string TrueFalseReturns { get; set; }
        public string TransactionId { get; set; }
        public string MedianDaysonMarket { get; set; }
        public string EmailAddressofListingBroker { get; set; }
        public string NameofListingBroker { get; set; }
        public string PhoneNumberofListingBroker { get; set; }
        public string DaysonMarket { get; set; }
        public DateType OriginalListingDateonProperty { get; set; }
        public MoneyType ListingPriceontheProperty { get; set; }
        public string ListingIdAssociatedwithProperty { get; set; }
        public string ListingStatus { get; set; }
        public string UniqueIDfortheBoardPosted { get; set; }
        public string AverageDaysonMarket { get; set; }
    }
}
