﻿namespace LendFoundry.Syndication.Equifax.CreditReportResponse
{
    public interface IIncome360Type
    {
        string ProductId { get; set; }
        string PostalCode { get; set; }
        string PostalCodePlus4 { get; set; }
        short Age { get; set; }
        bool AgeSpecified { get; set; }
        string AgeBand { get; set; }
        IMoneyType IncomeofConsumer { get; set; }
        string DispositionCode { get; set; }
    }
}
