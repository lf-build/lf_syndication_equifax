﻿namespace LendFoundry.Syndication.Equifax.CreditReportResponse
{
    public interface IScoreReasonType
    {
        short Number { get; set; }
    }
}