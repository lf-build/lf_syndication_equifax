namespace LendFoundry.Syndication.Equifax.CreditReportResponse
{
    public class CustomerIdType : ICustomerIdType
    {
        public CustomerIdType(Proxy.CreditReportResponse.CustomerIdType customerIdType)
        {
            CustomerNumber = customerIdType.CustomerNumber;
            Name = customerIdType.Name;
            if (customerIdType.Industry != null)
                Industry = new CodeType(customerIdType.Industry);
        }

        public string CustomerNumber { get; set; }

        public string Name { get; set; }

        public ICodeType Industry { get; set; }
    }
}