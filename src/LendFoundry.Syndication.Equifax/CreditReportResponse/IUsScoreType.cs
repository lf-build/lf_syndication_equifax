﻿using System.Collections.Generic;

namespace LendFoundry.Syndication.Equifax.CreditReportResponse
{
    public interface IUsScoreType
    {
        object DoddFrank { get; set; }
        string FactActInquiriesAreKeyFactor { get; set; }
        string ModelId { get; set; }
        string ModelNumber { get; set; }
        ICodeType RejectCode { get; set; }
        object RiskBasedPricing { get; set; }
        string ScoreFormatType { get; set; }
        string ScoreNumber { get; set; }
        List<IScoreReasonType> ScoreReasons { get; set; }
        string ScoreResult { get; set; }
    }
}