﻿namespace LendFoundry.Syndication.Equifax.CreditReportResponse
{
    public interface IUsEmploymentType
    {
        ICityType City { get; set; }
        string Code { get; set; }
        IDateType DateEmployed { get; set; }
        IDateType DateEmploymentEnded { get; set; }
        string Description { get; set; }
        string Employer { get; set; }
        string Occupation { get; set; }
        string State { get; set; }
        IDateType VerificationDate { get; set; }
    }
}