﻿namespace LendFoundry.Syndication.Equifax.CreditReportResponse
{
    public interface IUnparsedStreetAddressType
    {
        string AddressLine1 { get; set; }
        string AddressLine2 { get; set; }
    }
}
