﻿namespace LendFoundry.Syndication.Equifax.CreditReportResponse
{
    public class YearlyIncomeType : IYearlyIncomeType
    {
        public YearlyIncomeType(Proxy.CreditReportResponse.YearlyIncomeType yearlyIncomeType)
        {
            if (yearlyIncomeType != null)
            {
                if (yearlyIncomeType.Year != null)
                    Year = new DateType(yearlyIncomeType.Year);
                if (yearlyIncomeType.Income != null)
                    Income = new MoneyType(yearlyIncomeType.Income);
                Number = yearlyIncomeType.number;
            }
        }
        public IDateType Year { get; set; }
        public IMoneyType Income { get; set; }
        public short Number { get; set; }
    }
}
