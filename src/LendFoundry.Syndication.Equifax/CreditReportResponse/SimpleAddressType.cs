﻿namespace LendFoundry.Syndication.Equifax.CreditReportResponse
{
    public class SimpleAddressType : ISimpleAddressType
    {
        public SimpleAddressType(Proxy.CreditReportResponse.SimpleAddressType simpleAddressType)
        {
            if(simpleAddressType != null)
            {
                if (simpleAddressType.UnparsedStreetAddress != null)
                    UnparsedStreetAddress = new UnparsedStreetAddressType(simpleAddressType.UnparsedStreetAddress);
                if (simpleAddressType.City != null)
                    City = new CityType(simpleAddressType.City);
                State = simpleAddressType.State;
                PostalCode = simpleAddressType.PostalCode;
                if (simpleAddressType.Telephone != null)
                    Telephone = new TelephoneType(simpleAddressType.Telephone);
                Code = simpleAddressType.code;
                Description = simpleAddressType.description;
            }
        }
        public IUnparsedStreetAddressType UnparsedStreetAddress { get; set; }
        public ICityType City { get; set; }
        public string State { get; set; }
        public string PostalCode { get; set; }
        public ITelephoneType Telephone { get; set; }
        public string Code { get; set; }
        public string Description { get; set; }
    }
}
