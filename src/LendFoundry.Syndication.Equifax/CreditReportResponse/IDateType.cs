﻿namespace LendFoundry.Syndication.Equifax.CreditReportResponse
{
    public interface IDateType
    {
        DateFormat Format { get; set; }
        string Value { get; set; }
    }
}