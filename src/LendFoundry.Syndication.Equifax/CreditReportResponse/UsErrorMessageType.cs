namespace LendFoundry.Syndication.Equifax.CreditReportResponse
{
    public class UsErrorMessageType : IUsErrorMessageType
    {
        public UsErrorMessageType(Proxy.CreditReportResponse.USErrorMessageType usErrorMessageType)
        {
            if (usErrorMessageType?.Error != null)
                Error = new CodeType(usErrorMessageType.Error);
        }

        public CodeType Error { get; set; }
    }
}