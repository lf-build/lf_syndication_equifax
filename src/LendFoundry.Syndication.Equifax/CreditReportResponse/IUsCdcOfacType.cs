﻿using System.Collections.Generic;

namespace LendFoundry.Syndication.Equifax.CreditReportResponse
{
    public interface IUsCdcOfacType
    {
        string RevisedLegalVerbiageIndicator { get; set; }

        string CdcMemberCode { get; set; }

        string CdcBranchAccountNumber { get; set; }

        IDateType TransactionDate { get; set; }

        string TransactionTime { get; set; }

        ICodeType ResponseType { get; set; }

        ICodeType ResponseCode { get; set; }

        ICodeType ProblemCode { get; set; }

        List<ICodeType> MatchCodes { get; set; }

        IDateType ProblemReportDate { get; set; }

        string IssueSource { get; set; }

        string IssueId { get; set; }

        string Comment { get; set; }

        IPersonNameType SubjectName { get; set; }

        string AddressLine1 { get; set; }

        ICityType City { get; set; }

        string State { get; set; }

        string PostalCode { get; set; }

        string CountryCode { get; set; }

        string LegalVerbiage { get; set; }

        BlankorNFlagType RegulatedNonRegulatedFlag { get; set; }

        bool RegulatedNonRegulatedFlagSpecified { get; set; }
    }
}
