﻿namespace LendFoundry.Syndication.Equifax.CreditReportResponse
{
    public class PropertyDataAndAnalyticType : IPropertyDataAndAnalyticType
    {
        public PropertyDataAndAnalyticType(Proxy.CreditReportResponse.PropertyDataAndAnalyticType propertyDataAndAnalyticType)
        {
            if (propertyDataAndAnalyticType != null)
            {
                if (propertyDataAndAnalyticType.RegulatedIdentifier != null)
                    RegulatedIdentifier = new CodeType(propertyDataAndAnalyticType.RegulatedIdentifier);
                if (propertyDataAndAnalyticType.HitNoHitIndicator != null)
                    HitNoHitIndicator = new CodeType(propertyDataAndAnalyticType.HitNoHitIndicator);
                if (propertyDataAndAnalyticType.ActivListingData != null)
                    ActivListingData = new PropertyDataAndAnalyticALSType(propertyDataAndAnalyticType.ActivListingData);
                if (propertyDataAndAnalyticType.AutomatedValuationModelData != null)
                    AutomatedValuationModelData = new PropertyDataAndAnalyticAVMType(propertyDataAndAnalyticType.AutomatedValuationModelData);
                if (propertyDataAndAnalyticType.LienData != null)
                    LienData = new PropertyDataAndAnalyticLIENType(propertyDataAndAnalyticType.LienData);
                if (propertyDataAndAnalyticType.PropertyData != null)
                    PropertyData = new PropertyDataAndAnalyticPROPType(propertyDataAndAnalyticType.PropertyData);
                if (propertyDataAndAnalyticType.MarketData != null)
                    MarketData = new PropertyDataAndAnalyticMKTType(propertyDataAndAnalyticType.MarketData);
                if (propertyDataAndAnalyticType.HPIData != null)
                    HPIData = new PropertyDataAndAnalyticHPIType(propertyDataAndAnalyticType.HPIData);
            }
        }
        public ICodeType RegulatedIdentifier { get; set; }
        public ICodeType HitNoHitIndicator { get; set; }
        public IPropertyDataAndAnalyticALSType ActivListingData { get; set; }
        public IPropertyDataAndAnalyticAVMType AutomatedValuationModelData { get; set; }
        public IPropertyDataAndAnalyticLIENType LienData { get; set; }
        public IPropertyDataAndAnalyticPROPType PropertyData { get; set; }
        public IPropertyDataAndAnalyticMKTType MarketData { get; set; }
        public IPropertyDataAndAnalyticHPIType HPIData { get; set; }
    }
}
