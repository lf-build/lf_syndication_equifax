﻿namespace LendFoundry.Syndication.Equifax.CreditReportResponse
{
    public class SubjectNameFMLType : ISubjectNameFMLType
    {
        public SubjectNameFMLType(Proxy.CreditReportResponse.SubjectNameFMLType subjectNameFMLType)
        {
            if (subjectNameFMLType != null)
            {
                FirstName = subjectNameFMLType.FirstName;
                MiddleName = subjectNameFMLType.MiddleName;
                LastName = subjectNameFMLType.LastName;
                NameSuffix = subjectNameFMLType.NameSuffix;
            }
        }
        public string FirstName { get; set; }
        public string MiddleName { get; set; }
        public string LastName { get; set; }
        public string NameSuffix { get; set; }
    }
}
