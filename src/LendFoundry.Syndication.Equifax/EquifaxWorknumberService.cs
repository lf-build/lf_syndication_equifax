﻿using System;
using System.Collections.Generic;
using System.Linq;
using LendFoundry.Syndication.Equifax.Proxy;
using LendFoundry.Syndication.Equifax.Proxy.WorkNumberRequest;
using LendFoundry.Syndication.Equifax.Response;

namespace LendFoundry.Syndication.Equifax
{
    public class EquifaxWorknumberService : IEquifaxWorknumberService
    {
        public EquifaxWorknumberService(IEquifaxWorkNumberProxy proxy, IEquifaxWorknumberConfiguration configuration)
        {
            if (proxy == null)
                throw new ArgumentNullException(nameof(proxy));

            Proxy = proxy;
            Configuration = configuration;

            if (string.IsNullOrWhiteSpace(Configuration.TransactionPurpose))
                throw new ArgumentNullException(nameof(Configuration.TransactionPurpose));
            if (string.IsNullOrWhiteSpace(Configuration.TemplateName))
                throw new ArgumentNullException(nameof(Configuration.TemplateName));
        }

        private IEquifaxWorknumberConfiguration Configuration { get; }
        private IEquifaxWorkNumberProxy Proxy { get; }
        public List<IEmploymentDetailResponse> GetEmploymentInformation(string ssn)
        {
            if (string.IsNullOrWhiteSpace(ssn))
                throw new ArgumentException("Value cannot be null or whitespace.", nameof(ssn));

            var request = new OFX
            {
                Items = new object[1],
                ItemsElementName = new ItemsChoiceType[1]
            };

            var tsver = new TSVERMSGSRQV1
            {
                TSVTWNSELECTTRNRQ = new[]
                {
                    new TSVTWNSELECTTRNRQ
                    {
                        TRNUID = new TRNUID { Text = new[] { Guid.NewGuid().ToString() } },
                        TRNPURPOSE = new TRNPURPOSE
                        {
                            CODE = new PPCODE(),
                            MESSAGE = new MESSAGE { Text = new[] { Configuration.TransactionPurpose } }
                        },
                        TSVTWNSELECTRQ = new TSVTWNSELECTRQ
                        {
                            TSVEMPLOYEEID = new TSVEMPLOYEEID
                            {
                                Text = new[] { ssn }
                            },
                            TEMPLATE_NAME = new TEMPLATE_NAME
                            {
                                Text = new[] { Configuration.TemplateName }
                            }
                        },
                    }
                }
            };

            request.Items[0] = tsver;
            request.ItemsElementName[0] = ItemsChoiceType.TSVERMSGSRQV1;

            var response = Proxy.ExecuteRequest(request);

            var employmentDetails = new List<IEmploymentDetailResponse>();
            for (var index = 0; index < response.Items.Length; index++)
            {
                var item = response.Items[index];
                var itemsElementName = response.ItemsElementName[index];
                if (itemsElementName == ItemsChoiceType.TSVERMSGSRSV1)
                {
                    var itemTsvermsgsrsv1 = item as TSVERMSGSRSV1;
                    if (itemTsvermsgsrsv1 != null && itemTsvermsgsrsv1.TSVTWNSELECTTRNRS != null)
                    {
                        foreach (var tsvtwnselecttrnrs in itemTsvermsgsrsv1.TSVTWNSELECTTRNRS)
                        {
                            var status = tsvtwnselecttrnrs.STATUS?.SEVERITY?.Text.FirstOrDefault();
                            if (status != null && status.Equals("ERROR", StringComparison.CurrentCultureIgnoreCase))
                            {
                                throw new EquifaxException(tsvtwnselecttrnrs.STATUS?.CODE?.Text.FirstOrDefault(), tsvtwnselecttrnrs.STATUS?.MESSAGE?.Text.FirstOrDefault() ??
                                                           "Unknown error occured");
                            }

                            employmentDetails.AddRange(
                                tsvtwnselecttrnrs.TSVTWNSELECTRS.Select(
                                    tsvresponseV100 => new EmploymentDetailResponse(tsvresponseV100)));
                        }
                    }
                }
            }

            return employmentDetails;
        }
    }
}