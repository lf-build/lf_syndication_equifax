﻿using System.Collections.Generic;
using LendFoundry.Foundation.Client;
namespace LendFoundry.Syndication.Equifax
{
    public interface IEquifaxCreditReportConfiguration : IDependencyConfiguration
    {
        string SiteId { get; set; }

        string SiteUrl { get; set; }

        string SiteUserName { get; set; }

        string SiteUserPassword { get; set; }

        string ServiceName { get; set; }

        string Version { get; set; }

        IEquifaxCustomerConfiguration CustomerConfiguration { get; set; }

        IEquifaxOptionalFeaturesConfiguration OptionalFeaturesConfiguration { get; set; }
        List<string> ModelNumbers { get; set; }
        IEquifaxEndUserConfiguration EndUserConfiguration { get; set; }
        int ExpirationInDays { get; set; }
        string ProxyUrl { get; set; }

        string ConnectionString { get; set; }

    }
}