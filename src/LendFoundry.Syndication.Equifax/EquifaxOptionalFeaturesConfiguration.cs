namespace LendFoundry.Syndication.Equifax
{
    public class EquifaxOptionalFeaturesConfiguration : IEquifaxOptionalFeaturesConfiguration
    {
        public string OutputFormat { get; set; } = "02";
        public string MultipleFileIndicator { get; set; } = "1";
        public CreditAgency CreditAgency { get; set; } = CreditAgency.Fico;
        public YesNo OnLineDirectoryFlag { get; set; } = YesNo.Y;
        public YesNo EDasFlag { get; set; } = YesNo.Y;
        public YesNo ConsumerReferralLocationFlag { get; set; } = YesNo.Y;
        public YesNo AlertContactFlag { get; set; } = YesNo.Y;
        public YesNo RbpBeaconOnlyFlag { get; set; } = YesNo.N;
        public YesNo RbpFicoOnlyFlag { get; set; } = YesNo.Y;
        public YesNo UseFicoVerbiageFlag { get; set; } = YesNo.Y;
        public YesNo RbpAllFlag { get; set; } = YesNo.Y;
        public YesNo DefaultModelFlag { get; set; } = YesNo.Y;
        public YesNo PaymentHistoryMetro2Flag { get; set; } = YesNo.None;
        public YesNo MlaOnly { get; set; }
    }

    public enum CreditAgency
    {
        Beacon,
        Fico
    }
}