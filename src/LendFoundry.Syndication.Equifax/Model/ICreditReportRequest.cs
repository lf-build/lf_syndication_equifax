﻿using System;

namespace LendFoundry.Syndication.Equifax
{
    public interface ICreditReportRequest : ICreditReportRequestKey
    {
        string EntityType { get; set; }

        string EntityId { get; set; }


    }
}