﻿using System;

namespace LendFoundry.Syndication.Equifax
{
    /// <summary>
    /// 
    /// </summary>
    public class CreditReportRequestKey : ICreditReportRequestKey
    {
        /// <summary>
        /// 
        /// </summary>
        public CreditReportRequestKey()
        {

        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="creditReportRequest"></param>
        public CreditReportRequestKey(ICreditReportRequest creditReportRequest)
        {
            FirstName = creditReportRequest.FirstName;
            LastName = creditReportRequest.LastName;
            Ssn = creditReportRequest.Ssn;
            City = creditReportRequest.City;
            State = creditReportRequest.State;
            StreetNumber = creditReportRequest.StreetNumber;
            StreetName = creditReportRequest.StreetName;
            ZipCode = creditReportRequest.ZipCode;
            DateOfBirth = creditReportRequest.DateOfBirth;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <value></value>
        public string FirstName { get; set; }
        /// <summary>
        /// 
        /// </summary>
        /// <value></value>
        public string LastName { get; set; }
        /// <summary>
        /// 
        /// </summary>
        /// <value></value>
        public string Ssn { get; set; }
        /// <summary>
        /// 
        /// </summary>
        /// <value></value>
        public string City { get; set; }
        /// <summary>
        /// 
        /// </summary>
        /// <value></value>
        public string State { get; set; }
        /// <summary>
        /// 
        /// </summary>
        /// <value></value>
        public string StreetNumber { get; set; }
        /// <summary>
        /// 
        /// </summary>
        /// <value></value>
        public string StreetName { get; set; }
        /// <summary>
        /// 
        /// </summary>
        /// <value></value>
        public string ZipCode { get; set; }
        /// <summary>
        /// 
        /// </summary>
        /// <value></value>
        public DateTime DateOfBirth { get; set; }
        
        /// <summary>
        /// 
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public bool isEquals(object obj)
        {
            if (obj == null)
            {
                return false;
            }
            ICreditReportRequestKey key = obj as ICreditReportRequestKey;
            if (key.FirstName != FirstName)
                return false;
            if (key.LastName != LastName)
                return false;
            if (key.Ssn != Ssn)
                return false;
            if (key.City != City)
                return false;
            if (key.State != State)
                return false;
            if (key.StreetNumber != StreetNumber)
                return false;
            if (key.StreetName != StreetName)
                return false;
            if (key.ZipCode != ZipCode)
                return false;
            if (key.DateOfBirth != DateOfBirth)
                return false;
            return true;
        }
    }
}
