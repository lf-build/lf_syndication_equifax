using System;

namespace LendFoundry.Syndication.Equifax
{
    public class CreditReportRequest  : CreditReportRequestKey, ICreditReportRequest
    {
        public string EntityType { get; set; }

        public string EntityId { get; set; }
    }

    
}