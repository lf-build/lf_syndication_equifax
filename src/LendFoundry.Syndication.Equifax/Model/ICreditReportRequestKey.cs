﻿using System;

namespace LendFoundry.Syndication.Equifax
{
    public interface ICreditReportRequestKey
    {
       string FirstName { get; set; }

       string LastName { get; set; }

       string Ssn { get; set; }

       string City { get; set; }

       string State { get; set; }

       string StreetNumber { get; set; }

       string StreetName { get; set; }

       string ZipCode { get; set; }

       DateTime DateOfBirth { get; set; }
       bool isEquals(object obj);
    }
}
