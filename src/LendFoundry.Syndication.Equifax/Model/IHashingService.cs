﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LendFoundry.Syndication.Equifax
{
    public interface IHashingService
    {
        string ComputeHash(string data);
    }
}
