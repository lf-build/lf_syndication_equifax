﻿using LendFoundry.Foundation.Logging;

namespace LendFoundry.Syndication.Equifax
{
    public interface IProviderFactory
    {
        IProvider Create(ILogger logger, CreditReportModeSettings configuration);
    }
}