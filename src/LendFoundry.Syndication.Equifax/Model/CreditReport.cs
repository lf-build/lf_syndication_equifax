﻿using System;
using LendFoundry.Foundation.Persistence;
using LendFoundry.Foundation.Client;
using Newtonsoft.Json;
using LendFoundry.Syndication.Equifax.Request;

namespace LendFoundry.Syndication.Equifax
{
    public class CreditReport : Aggregate, ICreditReport
    {

        [JsonConverter(typeof(InterfaceConverter<ICreditReportRequestKey, CreditReportRequestKey>))]
        public ICreditReportRequestKey Key { get; set; }
        public string HashedKey { get; set; }
        public string EntityType { get; set; }

        public string EntityId { get; set; }

        public DateTimeOffset ReportDate { get; set; }

        public DateTimeOffset ExpirationDate { get; set; }

        public object Report { get; set; }

        public ProviderType ProviderType { get; set; }
        public string Mode { get; set; }
        public string Title { get; set; }
    }
}