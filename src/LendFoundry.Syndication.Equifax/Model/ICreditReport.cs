﻿using System;
using LendFoundry.Foundation.Persistence;
using LendFoundry.Syndication.Equifax.Request;

namespace LendFoundry.Syndication.Equifax
{
    public interface ICreditReport : IAggregate 
    {
        string HashedKey { get; set; }
        ICreditReportRequestKey Key { get; set; }
        string EntityId { get; set; }
        string EntityType { get; set; }
        DateTimeOffset ReportDate { get; set; }

        DateTimeOffset ExpirationDate { get; set; }

        object Report { get; set; }

        ProviderType ProviderType { get; set; }

        string Mode { get; set; }

        string Title { get; set; }
    }
}