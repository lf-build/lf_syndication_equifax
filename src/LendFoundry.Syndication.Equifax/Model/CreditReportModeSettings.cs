﻿namespace LendFoundry.Syndication.Equifax
{
    public class CreditReportModeSettings
    {
        public ProviderType ProviderType { get; set; }
        public int ExpirationInDays { get; set; }
        public object ProviderSettings { get; set; }
        public string ProxyUrl { get; set; }
        public string Title {get;set;}
    }
}