﻿using LendFoundry.Foundation.Persistence;
using LendFoundry.Syndication.Equifax.Request;
using System.Collections.Generic;

namespace LendFoundry.Syndication.Equifax
{
    public interface ICreditReportRepository : IRepository<ICreditReport>
    {
        ICreditReport Get(ICreditReportRequestKey request, string mode);

        ICreditReport GetReport(ICreditReportRequestKey request, string reportId);

        List<ICreditReport> GetReports(ICreditReportRequestKey request, int? skip = default(int?), int? take = default(int?));
    }
}