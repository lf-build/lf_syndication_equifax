﻿namespace LendFoundry.Syndication.Equifax
{
    public interface IProvider
    {
        object Get(ICreditReportRequest request);
    }
}