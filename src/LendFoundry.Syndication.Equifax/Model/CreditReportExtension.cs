﻿using Newtonsoft.Json;

namespace LendFoundry.Syndication.Equifax
{
    public static class CreditReportExtension
    {
        public static string ToKey(this ICreditReportRequestKey request)
        {
            return JsonConvert.SerializeObject(request);
        }

    }
}
