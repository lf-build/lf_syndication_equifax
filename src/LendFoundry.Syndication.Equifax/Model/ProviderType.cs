﻿namespace LendFoundry.Syndication.Equifax
{
    public enum ProviderType
    {
        Undefined = 0,

        LexisNexis = 1,

        DunBradstreet = 2,

        Equifax = 3,
        TransUnion=4,
    }
}