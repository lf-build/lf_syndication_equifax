﻿using System.Collections.Generic;

namespace LendFoundry.Syndication.Equifax
{
    public sealed class CreditReportConfiguration
    {
        public Dictionary<string, CreditReportModeSettings> Modes {get;set;}
        public string DefaultMode { get; set; }
      
    }
}