﻿namespace LendFoundry.Syndication.Equifax.Events
{
    public class CreditReportAdded
    {
        public CreditReportAdded()
        {
        }

        public CreditReportAdded(ICreditReport creditReport)
        {
            CreditReport = creditReport;
        }

        public ICreditReport CreditReport { get; set; }
    }
}