﻿namespace LendFoundry.Syndication.Equifax
{
    public interface IEquifaxCustomerConfiguration
    {
        string CustomerNumber { get; set; }
        string SecurityCode { get; set; }
        string CustomerCode { get; set; }
        YesNo PrintImageCdataInclude { get; set; } 
    }
}