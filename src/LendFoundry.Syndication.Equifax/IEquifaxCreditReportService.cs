﻿using LendFoundry.Syndication.Equifax.Request;
using LendFoundry.Syndication.Equifax.Response;
using System.Collections.Generic;

namespace LendFoundry.Syndication.Equifax
{
    public interface IEquifaxCreditReportService
    {
        ICreditReport CreateReport(string entityType, string entityId, ICreditReportRequestKey creditReportRequest);
        ICreditReport GetReport(string entityType, string entityId, ICreditReportRequestKey creditReportRequest, string reportId);
        IEnumerable<ICreditReport> GetReports(string entityType, string entityId, ICreditReportRequestKey creditReportRequest, int? skip = default(int?), int? take = default(int?));
    }
}