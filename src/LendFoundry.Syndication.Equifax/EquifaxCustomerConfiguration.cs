namespace LendFoundry.Syndication.Equifax
{
    public class EquifaxCustomerConfiguration : IEquifaxCustomerConfiguration
    {
        public string CustomerNumber { get; set; }
        public string SecurityCode { get; set; }
        public string CustomerCode { get; set; }
        public YesNo PrintImageCdataInclude { get; set; } = YesNo.Y;
    }
}