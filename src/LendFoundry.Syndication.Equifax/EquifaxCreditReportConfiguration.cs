using LendFoundry.Foundation.Client;
using Newtonsoft.Json;
using System.Collections.Generic;

namespace LendFoundry.Syndication.Equifax
{
    public class EquifaxCreditReportConfiguration : IEquifaxCreditReportConfiguration ,IDependencyConfiguration
    {
        public string SiteId { get; set; }

        public string SiteUrl { get; set; }

        public string SiteUserName { get; set; }

        public string SiteUserPassword { get; set; }

        public string ServiceName { get; set; }

        public string Version { get; set; } = "7.0";

        [JsonConverter(typeof(InterfaceConverter<IEquifaxCustomerConfiguration, EquifaxCustomerConfiguration>))]
        public IEquifaxCustomerConfiguration CustomerConfiguration { get; set; } = new EquifaxCustomerConfiguration();

        [JsonConverter(typeof(InterfaceConverter<IEquifaxOptionalFeaturesConfiguration, EquifaxOptionalFeaturesConfiguration>))]
        public IEquifaxOptionalFeaturesConfiguration OptionalFeaturesConfiguration { get; set; } = new EquifaxOptionalFeaturesConfiguration();

        public List<string> ModelNumbers { get; set; } = new List<string>();

        [JsonConverter(typeof(InterfaceConverter<IEquifaxEndUserConfiguration, EquifaxEndUserConfiguration>))]
        public IEquifaxEndUserConfiguration EndUserConfiguration { get; set; }
        public int ExpirationInDays { get; set; }
        public string ProxyUrl { get; set; }
        public Dictionary<string, string> Dependencies { get; set; }
        public string Database { get; set; }
        public string ConnectionString { get; set; }
    }
}