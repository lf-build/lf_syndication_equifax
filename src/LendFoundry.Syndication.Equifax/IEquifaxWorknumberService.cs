﻿using System.Collections.Generic;
using LendFoundry.Syndication.Equifax.Response;

namespace LendFoundry.Syndication.Equifax
{
    public interface IEquifaxWorknumberService
    {
        List<IEmploymentDetailResponse> GetEmploymentInformation(string ssn);
    }
}