﻿namespace LendFoundry.Syndication.Equifax
{
    public class EquifaxEndUserConfiguration : IEquifaxEndUserConfiguration
    {
        public string UserName { get; set; }
        public string PermissiblePurposeCode { get; set; }
    }
}
