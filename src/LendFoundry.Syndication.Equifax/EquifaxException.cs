﻿using System;
using System.Runtime.Serialization;

namespace LendFoundry.Syndication.Equifax
{
    [Serializable]
    public class EquifaxException : Exception
    {
        public string ErrorCode { get; set; }

        public EquifaxException()
        {
        }
        public EquifaxException(string message) : this(null, message, null)
        {
        }

        public EquifaxException(string errorCode,string message) : this(errorCode, message, null)
        {
        }

        public EquifaxException(string errorCode, string message, Exception innerException) : base(message, innerException)
        {
            ErrorCode = errorCode;
        }
        public EquifaxException(string message, Exception innerException) : this(null, message, innerException)
        {
        }

        protected EquifaxException(SerializationInfo info, StreamingContext context) : base(info, context)
        {
        }
    }
}