namespace LendFoundry.Syndication.Equifax
{
    public enum YesNo
    {
        None = 0,
        Y = 1,
        N = 2
    }
}