﻿namespace LendFoundry.Syndication.Equifax
{
    public class EquifaxWorknumberConfiguration : IEquifaxWorknumberConfiguration
    {
        public string ApplicationId { get; set; } 
        public string ApplicationVersion { get; set; } 
        public string CertificatePassword { get; set; } 
        public string DtClientId { get; set; } 
        public string Language { get; set; } = "ENG";
        public string TemplateName { get; set; } = "AutoGold";
        public string TransactionPurpose { get; set; } = "PPCREDIT";
        public string UserId { get; set; } 
        public string UserPassword { get; set; } 
        public string WorkNumberUrl { get; set; } = "https://test.ofx4.talx.com/verifierinterimct2/xmlb2b_router.dll";
        public string Certificate { get; set; }
        public string ProxyUrl { get; set; }
    }
}