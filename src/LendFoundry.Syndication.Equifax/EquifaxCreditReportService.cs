﻿using LendFoundry.EventHub;
using LendFoundry.Foundation.Date;
using LendFoundry.Foundation.Logging;
using LendFoundry.Foundation.Lookup;
using LendFoundry.Foundation.Client;
using LendFoundry.Foundation.Services;
using LendFoundry.Syndication.Equifax.Events;
using LendFoundry.Syndication.Equifax.Proxy.CreditReportResponse;
using LendFoundry.Syndication.Equifax.Request;
using LendFoundry.Syndication.Equifax.Response;
using LendFoundry.SyndicationStore.Events;
using System;
using System.Collections.Generic;
using System.Linq;

namespace LendFoundry.Syndication.Equifax
{
    public class EquifaxCreditReportService : IEquifaxCreditReportService
    {
        public EquifaxCreditReportService(IEquifaxCreditReportConfiguration creditReportConfiguration,
            Proxy.IEquifaxCreditReportProxy creditReportProxy,
            ICreditReportRepository repository,
            ITenantTime tenantTime,
            IEventHubClient eventHubClient,
            ILogger logger,
            ILookupService lookup
            )
        {
            if (creditReportConfiguration == null)
                throw new ArgumentNullException(nameof(creditReportConfiguration));
            if (creditReportProxy == null)
                throw new ArgumentNullException(nameof(creditReportProxy));
            if (repository == null)
                throw new ArgumentException($"{nameof(repository)} is mandatory");
            if (creditReportConfiguration.CustomerConfiguration == null)
                throw new ArgumentNullException(nameof(creditReportConfiguration.CustomerConfiguration));

            if (string.IsNullOrWhiteSpace(creditReportConfiguration.SiteUrl))
                throw new ArgumentException("SiteUrl is required", nameof(creditReportConfiguration.SiteUrl));

            if (string.IsNullOrWhiteSpace(creditReportConfiguration.SiteId))
                throw new ArgumentException("SiteId is required", nameof(creditReportConfiguration.SiteId));

            if (string.IsNullOrWhiteSpace(creditReportConfiguration.ServiceName))
                throw new ArgumentException("ServiceName is required", nameof(creditReportConfiguration.ServiceName));

            if (string.IsNullOrWhiteSpace(creditReportConfiguration.SiteUserName))
                throw new ArgumentException("Username is required", nameof(creditReportConfiguration.SiteUserName));

            if (string.IsNullOrWhiteSpace(creditReportConfiguration.SiteUserPassword))
                throw new ArgumentException("User Password is required", nameof(creditReportConfiguration.SiteUserPassword));

            if (string.IsNullOrWhiteSpace(creditReportConfiguration.Version))
                throw new ArgumentException("Version is required", nameof(creditReportConfiguration.Version));

            if (string.IsNullOrWhiteSpace(creditReportConfiguration.CustomerConfiguration.CustomerNumber))
                throw new ArgumentException("CustomerNumber is required", nameof(creditReportConfiguration.CustomerConfiguration.CustomerNumber));

            if (string.IsNullOrWhiteSpace(creditReportConfiguration.CustomerConfiguration.SecurityCode))
                throw new ArgumentException("SecurityCode is required", nameof(creditReportConfiguration.CustomerConfiguration.SecurityCode));

            if (string.IsNullOrWhiteSpace(creditReportConfiguration.CustomerConfiguration.CustomerCode))
                throw new ArgumentException("CustomerCode is required", nameof(creditReportConfiguration.CustomerConfiguration.CustomerCode));

            if (string.IsNullOrWhiteSpace(creditReportConfiguration.OptionalFeaturesConfiguration.OutputFormat))
                throw new ArgumentException("OutputFormat is required", nameof(creditReportConfiguration.OptionalFeaturesConfiguration.OutputFormat));

            if (string.IsNullOrWhiteSpace(creditReportConfiguration.OptionalFeaturesConfiguration.MultipleFileIndicator))
                throw new ArgumentException("MultipleFileIndicator is required", nameof(creditReportConfiguration.OptionalFeaturesConfiguration.MultipleFileIndicator));

            CreditReportConfiguration = creditReportConfiguration;
            EquifaxCreditReportProxy = creditReportProxy;
            Repository = repository;
            TenantTime = tenantTime;
            EventHub = eventHubClient;
            Logger = logger;
            Lookup = lookup;
        }

        private IEquifaxCreditReportConfiguration CreditReportConfiguration { get; }
        private Proxy.IEquifaxCreditReportProxy EquifaxCreditReportProxy { get; }
        private ICreditReportRepository Repository { get; }

        private ITenantTime TenantTime { get; }
        private IEventHubClient EventHub { get; }
        private ILogger Logger { get; }
        private ILookupService Lookup { get; }
        private string ServiceName = "Equifax";
        public ICreditReport CreateReport(string entityType, string entityId, ICreditReportRequestKey creditReportRequest)
        {
            try
            {
                if (creditReportRequest == null)
                    throw new InvalidArgumentException($"The {nameof(creditReportRequest)} cannot be null",
                        nameof(creditReportRequest));

                entityType = ValidateEntity(entityType, entityId);

                var now = TenantTime.Now;
              
                if (CreditReportConfiguration.ExpirationInDays > 0)
                {
                    // First check if exists one Report
                    var creditReportFromCache = Repository.Get(creditReportRequest,ServiceName);

                    if (creditReportFromCache != null)
                    {
                        EventHub.Publish(new SyndicationCalledEvent
                        {
                            EntityId = entityId,
                            EntityType = entityType,
                            Name = ServiceName + "CreditReport",
                            Data = creditReportFromCache.Report
                        });

                        // to enable shared credit report among multiple applications, 
                        // and ensure event propagation.
                        creditReportFromCache.EntityId = entityId;
                        creditReportFromCache.EntityType = entityType;
                        EventHub.Publish(new CreditReportAdded(creditReportFromCache)).Wait();

                        Logger.Info($"Credit report retrieved from cache.");

                        return creditReportFromCache;
                    }

                }
                IGetCreditReportRequest request = new GetCreditReportRequest()
                {
                    City= creditReportRequest.City,
                    DateOfBirth= creditReportRequest.DateOfBirth,
                    FirstName= creditReportRequest.FirstName,
                    LastName= creditReportRequest.LastName,
                    Ssn= creditReportRequest.Ssn,
                    State= creditReportRequest.State,
                    StreetName= creditReportRequest.StreetName,
                    StreetNumber= creditReportRequest.StreetNumber,
                    ZipCode= creditReportRequest.ZipCode
                };
                var report = GetCreditReport(request);
                if (report == null)
                    throw new NotFoundException("Credit Report not found.");

                var creditReport = new CreditReport
                {
                    EntityId = entityId,
                    EntityType = entityType,
                    ExpirationDate = now.AddDays(CreditReportConfiguration.ExpirationInDays),
                    Report = report,
                    ReportDate = now,
                    Mode = ServiceName,
                    Key = creditReportRequest
                };
                if (CreditReportConfiguration.ExpirationInDays > 0)
                {
                    Repository.Add(creditReport);
                    Logger.Info(
                        $"Credit Report with ReportId:#{creditReport.Id}, ExpirationDate:#{creditReport.ExpirationDate}, added in database");
                }
                EventHub.Publish(new SyndicationCalledEvent
                {
                    EntityId = entityId,
                    EntityType = entityType,
                    Name = ServiceName+"CreditReport",
                    Data = report
                }).Wait();
                EventHub.Publish(new CreditReportAdded(creditReport)).Wait();
                Logger.Info(
                    $"Credit Report with event #{nameof(CreditReportAdded)} was processed.");

                return creditReport;
            }
            catch (Exception ex)
            {
                Logger.Error($"The method CreateReport raised an error:{ex.Message}, Full Error:", ex);
                  EventHub.Publish("CreditReportCalledFailed",new
                {
                    EntityId = entityId,
                    EntityType = entityType,
                    Name = ServiceName+"CreditReport",
                    Response = ex.Message,
                    Request = creditReportRequest,
                }).Wait();
                throw;
            }
        }

        public ICreditReport GetReport(string entityType, string entityId, ICreditReportRequestKey creditReportRequest, string reportId)
        {
            try
            {
                entityType = ValidateEntity(entityType, entityId);

                if (string.IsNullOrWhiteSpace(reportId))
                    throw new InvalidArgumentException($"#{nameof(reportId)} cannot be null", nameof(reportId));

                if (creditReportRequest == null)
                    throw new InvalidArgumentException($"#{nameof(creditReportRequest)} cannot be null", nameof(creditReportRequest));


                var creditReport = Repository.GetReport(creditReportRequest, reportId);
                if (creditReport == null)
                    throw new NotFoundException($"The Credit Report was not found ReportId:#{reportId}");

                return creditReport;
            }
            catch (Exception ex)
            {
                Logger.Error($"The method GetReport(, {reportId}) raised an error:{ex.Message}, Full Error:", ex);
                throw;
            }
        }

        public IEnumerable<ICreditReport> GetReports(string entityType, string entityId, ICreditReportRequestKey creditReportRequest, int? skip = default(int?), int? take = default(int?))
        {
            try
            {
                entityType = ValidateEntity(entityType, entityId);

                if (creditReportRequest == null)
                    throw new InvalidArgumentException($"#{nameof(creditReportRequest)} cannot be null", nameof(creditReportRequest));

                return Repository.GetReports(creditReportRequest, skip, take);

            }
            catch (Exception ex)
            {
                Logger.Error($"The method GetReports raised an error:{ex.Message}, Full Error:", ex);
                throw;
            }
        }

        private string ValidateEntity(string entityType, string entityId)
        {
            entityType = EnsureEntityType(entityType);

            if (string.IsNullOrWhiteSpace(entityId))
                throw new InvalidArgumentException($"The field #{nameof(entityId)} cannot be null", nameof(entityId));
            return entityType;
        }

        public string EnsureEntityType(string entityType)
        {
            if (string.IsNullOrWhiteSpace(entityType))
                throw new InvalidArgumentException("Invalid Entity Type");
            entityType = entityType.ToLower();
            var validEntityTypes = Lookup.GetLookupEntry("entityTypes", entityType);

            if (validEntityTypes == null || !validEntityTypes.Any())
                throw new InvalidArgumentException("Invalid Entity Type");

            return entityType;
        }

        private static string UppercaseFirst(string s)
        {
            if (string.IsNullOrEmpty(s)) return string.Empty;
            return $"{char.ToUpper(s[0])}{s.Substring(1)}";
        }
        private IGetCreditReportResponse GetCreditReport(IGetCreditReportRequest creditReportRequest)
        {
            if (creditReportRequest == null)
                throw new ArgumentNullException(nameof(creditReportRequest));
            
            var creditReportRequestProxy = new Proxy.CreditReportRequest.EfxReceive(CreditReportConfiguration, creditReportRequest);

            var creditReportResponseProxy = EquifaxCreditReportProxy.GetCreditReport(creditReportRequestProxy);
            if (creditReportResponseProxy == null)
                throw new ArgumentNullException(nameof(creditReportResponseProxy));

            var usConsumerCreditReports = creditReportResponseProxy.EfxReport.Select(p => p.USConsumerCreditReports).ToList();
            foreach (var efxTransmitEfxReportUsConsumerCreditReport in usConsumerCreditReports)
            {
                foreach (var usConsumerCreditReportItem in efxTransmitEfxReportUsConsumerCreditReport.Items)
                {
                    if (usConsumerCreditReportItem != null)
                    {
                        if (usConsumerCreditReportItem.GetType() == typeof(EfxTransmitEfxReportUSConsumerCreditReportsUSErrorMessages))
                        {
                            var usErrorMessageType = ((EfxTransmitEfxReportUSConsumerCreditReportsUSErrorMessages)usConsumerCreditReportItem).USErrorMessage;
                            var errorCodeAndDesc = usErrorMessageType.Error;
                            if (errorCodeAndDesc != null)
                                throw new EquifaxException(errorCodeAndDesc.code, $"Service call failed. Code {errorCodeAndDesc.code}. Description: {errorCodeAndDesc.description ?? ""}");

                            var errorType = usErrorMessageType.ErrorType;
                            if (errorType != null && errorType.description != null)
                                throw new EquifaxException(errorType.code, $"Service call failed. Code {errorType.code}. Description: {errorType.description ?? ""}");

                        }
                        if (usConsumerCreditReportItem.GetType() == typeof(USConsumerCreditReportType))
                        {
                            var consumerCreditReport =
                                (USConsumerCreditReportType)usConsumerCreditReportItem;
                            if (consumerCreditReport.USErrorMessages != null &&
                                consumerCreditReport.USErrorMessages.Any())
                            {
                                var errorCodeAndDesc = consumerCreditReport.USErrorMessages.First().Error;
                                if (errorCodeAndDesc != null)
                                    throw new EquifaxException(errorCodeAndDesc.code,
                                        $"Service call failed. Code {errorCodeAndDesc.code}. Description: {errorCodeAndDesc.description ?? ""}");
                            }
                        }
                    }
                }
                
            }


            return new GetCreditReportResponse(creditReportResponseProxy);
        }
    }
}