﻿namespace LendFoundry.Syndication.Equifax.Proxy
{
    public interface IEquifaxCreditReportProxy
    {
        CreditReportResponse.EfxTransmit GetCreditReport(CreditReportRequest.EfxReceive equifaxCreditReportRequest);
    }
}