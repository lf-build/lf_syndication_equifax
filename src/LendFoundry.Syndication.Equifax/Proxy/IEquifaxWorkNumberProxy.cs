﻿namespace LendFoundry.Syndication.Equifax.Proxy
{
    public interface IEquifaxWorkNumberProxy
    {
        WorkNumberRequest.OFX ExecuteRequest(WorkNumberRequest.OFX request);
    }
}