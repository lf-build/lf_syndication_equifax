namespace LendFoundry.Syndication.Equifax.Proxy.CreditReportResponse
{
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.6.1055.0")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute()]
    public partial class USTaxLienType {
    
        private CodeType expandedDataReturnedField;
    
        private DateType dateFiledField;
    
        private CustomerIdType courtIdField;
    
        private string caseNumberField;
    
        private MoneyType amountField;
    
        private CodeType creditorClassField;
    
        private DateType releaseDateField;
    
        private DateType verificationDateField;
    
        private CodeType[] narrativesField;
    
        private DateType expandedDateFiledField;
    
        private MoneyType expandedAmountField;
    
        private DateType expandedReleaseDateField;
    
        private DateType expandedVerificationDateField;
    
        /// <remarks/>
        public CodeType ExpandedDataReturned {
            get {
                return this.expandedDataReturnedField;
            }
            set {
                this.expandedDataReturnedField = value;
            }
        }
    
        /// <remarks/>
        public DateType DateFiled {
            get {
                return this.dateFiledField;
            }
            set {
                this.dateFiledField = value;
            }
        }
    
        /// <remarks/>
        public CustomerIdType CourtId {
            get {
                return this.courtIdField;
            }
            set {
                this.courtIdField = value;
            }
        }
    
        /// <remarks/>
        public string CaseNumber {
            get {
                return this.caseNumberField;
            }
            set {
                this.caseNumberField = value;
            }
        }
    
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(IsNullable=true)]
        public MoneyType Amount {
            get {
                return this.amountField;
            }
            set {
                this.amountField = value;
            }
        }
    
        /// <remarks/>
        public CodeType CreditorClass {
            get {
                return this.creditorClassField;
            }
            set {
                this.creditorClassField = value;
            }
        }
    
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute()]
        public DateType ReleaseDate {
            get {
                return this.releaseDateField;
            }
            set {
                this.releaseDateField = value;
            }
        }
    
        /// <remarks/>
        public DateType VerificationDate {
            get {
                return this.verificationDateField;
            }
            set {
                this.verificationDateField = value;
            }
        }
    
        /// <remarks/>
        [System.Xml.Serialization.XmlArrayItemAttribute("Narrative", IsNullable=false)]
        public CodeType[] Narratives {
            get {
                return this.narrativesField;
            }
            set {
                this.narrativesField = value;
            }
        }
    
        /// <remarks/>
        public DateType ExpandedDateFiled {
            get {
                return this.expandedDateFiledField;
            }
            set {
                this.expandedDateFiledField = value;
            }
        }
    
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(IsNullable=true)]
        public MoneyType ExpandedAmount {
            get {
                return this.expandedAmountField;
            }
            set {
                this.expandedAmountField = value;
            }
        }
    
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute()]
        public DateType ExpandedReleaseDate {
            get {
                return this.expandedReleaseDateField;
            }
            set {
                this.expandedReleaseDateField = value;
            }
        }
    
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute()]
        public DateType ExpandedVerificationDate {
            get {
                return this.expandedVerificationDateField;
            }
            set {
                this.expandedVerificationDateField = value;
            }
        }
    }
}