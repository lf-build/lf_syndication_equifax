namespace LendFoundry.Syndication.Equifax.Proxy.CreditReportResponse
{
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.6.1055.0")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute()]
    public partial class USIdentificationSSNType {
    
        private string mDBSubjectAgeField;
    
        private string mDBSubjectSSNField;
    
        private string mDBSubjectSSNConfirmedField;
    
        private string sSNMatchField;
    
        private string inquirySubjectSSNField;
    
        private string inquirySSNDateIssuedField;
    
        private DateType inquirySSNDateIssuedRangeField;
    
        private string inquirySSNStateIssuedField;
    
        private string inquirySSNDateOfDeathField;
    
        private string inquirySSNStateOfDeathField;
    
        private string inquirySSNByteToByteMatchField;
    
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute()]
        public string MDBSubjectAge {
            get {
                return this.mDBSubjectAgeField;
            }
            set {
                this.mDBSubjectAgeField = value;
            }
        }
    
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute()]
        public string MDBSubjectSSN {
            get {
                return this.mDBSubjectSSNField;
            }
            set {
                this.mDBSubjectSSNField = value;
            }
        }
    
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute()]
        public string MDBSubjectSSNConfirmed {
            get {
                return this.mDBSubjectSSNConfirmedField;
            }
            set {
                this.mDBSubjectSSNConfirmedField = value;
            }
        }
    
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute()]
        public string SSNMatch {
            get {
                return this.sSNMatchField;
            }
            set {
                this.sSNMatchField = value;
            }
        }
    
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute()]
        public string InquirySubjectSSN {
            get {
                return this.inquirySubjectSSNField;
            }
            set {
                this.inquirySubjectSSNField = value;
            }
        }
    
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute()]
        public string InquirySSNDateIssued {
            get {
                return this.inquirySSNDateIssuedField;
            }
            set {
                this.inquirySSNDateIssuedField = value;
            }
        }
    
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute()]
        public DateType InquirySSNDateIssuedRange {
            get {
                return this.inquirySSNDateIssuedRangeField;
            }
            set {
                this.inquirySSNDateIssuedRangeField = value;
            }
        }
    
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute()]
        public string InquirySSNStateIssued {
            get {
                return this.inquirySSNStateIssuedField;
            }
            set {
                this.inquirySSNStateIssuedField = value;
            }
        }
    
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute()]
        public string InquirySSNDateOfDeath {
            get {
                return this.inquirySSNDateOfDeathField;
            }
            set {
                this.inquirySSNDateOfDeathField = value;
            }
        }
    
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute()]
        public string InquirySSNStateOfDeath {
            get {
                return this.inquirySSNStateOfDeathField;
            }
            set {
                this.inquirySSNStateOfDeathField = value;
            }
        }
    
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute()]
        public string InquirySSNByteToByteMatch {
            get {
                return this.inquirySSNByteToByteMatchField;
            }
            set {
                this.inquirySSNByteToByteMatchField = value;
            }
        }
    }
}