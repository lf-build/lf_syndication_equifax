namespace LendFoundry.Syndication.Equifax.Proxy.CreditReportResponse
{
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.6.1055.0")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute()]
    public partial class USScoreType {
    
        private string scoreFormatTypeField;
    
        private string modelNumberField;
    
        private string modelIdField;
    
        private string scoreNumberField;
    
        private CodeType marketMaxIndField;
    
        private string scoreResultField;
    
        private ScoreReasonType[] scoreReasonsField;
    
        private CodeType rejectCodeField;
    
        private object riskBasedPricingField;
    
        private object doddFrankField;
    
        private string fACTActInquiriesAreKeyFactorField;
    
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute()]
        public string ScoreFormatType {
            get {
                return this.scoreFormatTypeField;
            }
            set {
                this.scoreFormatTypeField = value;
            }
        }
    
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute()]
        public string ModelNumber {
            get {
                return this.modelNumberField;
            }
            set {
                this.modelNumberField = value;
            }
        }
    
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute()]
        public string ModelId {
            get {
                return this.modelIdField;
            }
            set {
                this.modelIdField = value;
            }
        }
    
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute()]
        public string ScoreNumber {
            get {
                return this.scoreNumberField;
            }
            set {
                this.scoreNumberField = value;
            }
        }
    
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute()]
        public CodeType MarketMaxInd {
            get {
                return this.marketMaxIndField;
            }
            set {
                this.marketMaxIndField = value;
            }
        }
    
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute()]
        public string ScoreResult {
            get {
                return this.scoreResultField;
            }
            set {
                this.scoreResultField = value;
            }
        }
    
        /// <remarks/>
        [System.Xml.Serialization.XmlArrayAttribute()]
        [System.Xml.Serialization.XmlArrayItemAttribute("ScoreReason", IsNullable=false)]
        public ScoreReasonType[] ScoreReasons {
            get {
                return this.scoreReasonsField;
            }
            set {
                this.scoreReasonsField = value;
            }
        }
    
        /// <remarks/>
        public CodeType RejectCode {
            get {
                return this.rejectCodeField;
            }
            set {
                this.rejectCodeField = value;
            }
        }
    
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute()]
        public object RiskBasedPricing {
            get {
                return this.riskBasedPricingField;
            }
            set {
                this.riskBasedPricingField = value;
            }
        }
    
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute()]
        public object DoddFrank {
            get {
                return this.doddFrankField;
            }
            set {
                this.doddFrankField = value;
            }
        }
    
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute()]
        public string FACTActInquiriesAreKeyFactor {
            get {
                return this.fACTActInquiriesAreKeyFactorField;
            }
            set {
                this.fACTActInquiriesAreKeyFactorField = value;
            }
        }
    }
}