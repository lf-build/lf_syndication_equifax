namespace LendFoundry.Syndication.Equifax.Proxy.CreditReportResponse
{
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.6.1055.0")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute()]
    public partial class USBankruptcyType {
    
        private CodeType expandedDataReturnedField;
    
        private DateType dateFiledField;
    
        private CustomerIdType courtIdField;
    
        private CodeType typeField;
    
        private CodeType filerField;
    
        private CodeDateType dispositionField;
    
        private CodeType[] narrativesField;
    
        private string caseNumberField;
    
        private DateType expandedDateFiledField;
    
        private DateType expandedCurrDispositionDateField;
    
        private DateType expandedVerificationDateField;
    
        private CodeType expandedPriorDispositionField;
    
        private DateType expandedDateReportedField;
    
        /// <remarks/>
        public CodeType ExpandedDataReturned {
            get {
                return this.expandedDataReturnedField;
            }
            set {
                this.expandedDataReturnedField = value;
            }
        }
    
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute()]
        public DateType DateFiled {
            get {
                return this.dateFiledField;
            }
            set {
                this.dateFiledField = value;
            }
        }
    
        /// <remarks/>
        public CustomerIdType CourtId {
            get {
                return this.courtIdField;
            }
            set {
                this.courtIdField = value;
            }
        }
    
        /// <remarks/>
        public CodeType Type {
            get {
                return this.typeField;
            }
            set {
                this.typeField = value;
            }
        }
    
        /// <remarks/>
        public CodeType Filer {
            get {
                return this.filerField;
            }
            set {
                this.filerField = value;
            }
        }
    
        /// <remarks/>
        public CodeDateType Disposition {
            get {
                return this.dispositionField;
            }
            set {
                this.dispositionField = value;
            }
        }
    
        /// <remarks/>
        [System.Xml.Serialization.XmlArrayItemAttribute("Narrative", IsNullable=false)]
        public CodeType[] Narratives {
            get {
                return this.narrativesField;
            }
            set {
                this.narrativesField = value;
            }
        }
    
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute()]
        public string CaseNumber {
            get {
                return this.caseNumberField;
            }
            set {
                this.caseNumberField = value;
            }
        }
    
        /// <remarks/>
        public DateType ExpandedDateFiled {
            get {
                return this.expandedDateFiledField;
            }
            set {
                this.expandedDateFiledField = value;
            }
        }
    
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute()]
        public DateType ExpandedCurrDispositionDate {
            get {
                return this.expandedCurrDispositionDateField;
            }
            set {
                this.expandedCurrDispositionDateField = value;
            }
        }
    
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute()]
        public DateType ExpandedVerificationDate {
            get {
                return this.expandedVerificationDateField;
            }
            set {
                this.expandedVerificationDateField = value;
            }
        }
    
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute()]
        public CodeType ExpandedPriorDisposition {
            get {
                return this.expandedPriorDispositionField;
            }
            set {
                this.expandedPriorDispositionField = value;
            }
        }
    
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute()]
        public DateType ExpandedDateReported {
            get {
                return this.expandedDateReportedField;
            }
            set {
                this.expandedDateReportedField = value;
            }
        }
    }
}