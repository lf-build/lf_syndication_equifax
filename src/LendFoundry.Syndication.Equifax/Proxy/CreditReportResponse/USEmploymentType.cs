namespace LendFoundry.Syndication.Equifax.Proxy.CreditReportResponse
{
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.6.1055.0")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute()]
    public partial class USEmploymentType {
    
        private string occupationField;
    
        private string employerField;
    
        private CityType cityField;
    
        private string stateField;
    
        private DateType dateEmployedField;
    
        private DateType verificationDateField;
    
        private DateType dateLeftField;
    
        private string codeField;
    
        private string descriptionField;
    
        /// <remarks/>
        public string Occupation {
            get {
                return this.occupationField;
            }
            set {
                this.occupationField = value;
            }
        }
    
        /// <remarks/>
        public string Employer {
            get {
                return this.employerField;
            }
            set {
                this.employerField = value;
            }
        }
    
        /// <remarks/>
        public CityType City {
            get {
                return this.cityField;
            }
            set {
                this.cityField = value;
            }
        }
    
        /// <remarks/>
        public string State {
            get {
                return this.stateField;
            }
            set {
                this.stateField = value;
            }
        }
    
        /// <remarks/>
        public DateType DateEmployed {
            get {
                return this.dateEmployedField;
            }
            set {
                this.dateEmployedField = value;
            }
        }
    
        /// <remarks/>
        public DateType VerificationDate {
            get {
                return this.verificationDateField;
            }
            set {
                this.verificationDateField = value;
            }
        }
    
        /// <remarks/>
        public DateType DateLeft {
            get {
                return this.dateLeftField;
            }
            set {
                this.dateLeftField = value;
            }
        }
    
        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string code {
            get {
                return this.codeField;
            }
            set {
                this.codeField = value;
            }
        }
    
        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string description {
            get {
                return this.descriptionField;
            }
            set {
                this.descriptionField = value;
            }
        }
    }
}