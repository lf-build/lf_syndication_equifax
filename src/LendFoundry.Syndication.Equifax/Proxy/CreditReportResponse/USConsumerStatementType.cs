namespace LendFoundry.Syndication.Equifax.Proxy.CreditReportResponse
{
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.6.1055.0")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute()]
    public partial class USConsumerStatementType {
    
        private DateType dateReportedField;
    
        private DateType dateToBePurgedField;
    
        private string statementField;
    
        /// <remarks/>
        public DateType DateReported {
            get {
                return this.dateReportedField;
            }
            set {
                this.dateReportedField = value;
            }
        }
    
        /// <remarks/>
        public DateType DateToBePurged {
            get {
                return this.dateToBePurgedField;
            }
            set {
                this.dateToBePurgedField = value;
            }
        }
    
        /// <remarks/>
        public string Statement {
            get {
                return this.statementField;
            }
            set {
                this.statementField = value;
            }
        }
    }
}