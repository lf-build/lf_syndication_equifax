namespace LendFoundry.Syndication.Equifax.Proxy.CreditReportResponse
{
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.6.1055.0")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute()]
    public partial class DiscretionarySpendingType {
    
        private string productIDField;
    
        private string postalCodeField;
    
        private string postalCodePlus4Field;
    
        private short ageField;
    
        private bool ageFieldSpecified;
    
        private string ageBandField;
    
        private MoneyType incomeofConsumerField;
    
        private MoneyType incomeField;
    
        private MoneyType spendingIndexField;
    
        private MoneyType spendingMassMarketField;
    
        private string dispositionCodeField;
    
        /// <remarks/>
        public string ProductID {
            get {
                return this.productIDField;
            }
            set {
                this.productIDField = value;
            }
        }
    
        /// <remarks/>
        public string PostalCode {
            get {
                return this.postalCodeField;
            }
            set {
                this.postalCodeField = value;
            }
        }
    
        /// <remarks/>
        public string PostalCodePlus4 {
            get {
                return this.postalCodePlus4Field;
            }
            set {
                this.postalCodePlus4Field = value;
            }
        }
    
        /// <remarks/>
        public short Age {
            get {
                return this.ageField;
            }
            set {
                this.ageField = value;
            }
        }
    
        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool AgeSpecified {
            get {
                return this.ageFieldSpecified;
            }
            set {
                this.ageFieldSpecified = value;
            }
        }
    
        /// <remarks/>
        public string AgeBand {
            get {
                return this.ageBandField;
            }
            set {
                this.ageBandField = value;
            }
        }
    
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(IsNullable=true)]
        public MoneyType IncomeofConsumer {
            get {
                return this.incomeofConsumerField;
            }
            set {
                this.incomeofConsumerField = value;
            }
        }
    
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute()]
        public MoneyType Income {
            get {
                return this.incomeField;
            }
            set {
                this.incomeField = value;
            }
        }
    
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute()]
        public MoneyType SpendingIndex {
            get {
                return this.spendingIndexField;
            }
            set {
                this.spendingIndexField = value;
            }
        }
    
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute()]
        public MoneyType SpendingMassMarket {
            get {
                return this.spendingMassMarketField;
            }
            set {
                this.spendingMassMarketField = value;
            }
        }
    
        /// <remarks/>
        public string DispositionCode {
            get {
                return this.dispositionCodeField;
            }
            set {
                this.dispositionCodeField = value;
            }
        }
    }
}