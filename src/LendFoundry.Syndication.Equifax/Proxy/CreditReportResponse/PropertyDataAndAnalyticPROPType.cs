namespace LendFoundry.Syndication.Equifax.Proxy.CreditReportResponse
{
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.6.1055.0")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute()]
    public partial class PropertyDataAndAnalyticPROPType {
    
        private CodeType hitNoHitIndicatorField;
    
        private string assessedLotSizeofPropertyinAcresField;
    
        private MoneyType appraisedValueofPropertybyCtyAssessorField;
    
        private MoneyType assessedValueofPropertyValueAssignedField;
    
        private string yearthatPropertywasLastAssessedField;
    
        private MoneyType valueAssignedbyCountyTaxAssesImprovementsField;
    
        private MoneyType assessedLandValueofthePropertyField;
    
        private string finishedSquareFootageofPropertyBasementField;
    
        private string totalSquareFootageofpropertiesBasementField;
    
        private string countyWherePropertyisLocatedField;
    
        private string elementarySchoolDistrictforPropertyField;
    
        private string federalInformationProcessingStandardField;
    
        private string totalSquareFootageofPropertiesGarageField;
    
        private string typeofGarageField;
    
        private string specifiesNorthSouthPositionofthePropertyField;
    
        private string specifiesEastWestPositionofthePropertyField;
    
        private string mainSquareFootageofPropertyField;
    
        private MoneyType likelyPriceaPropertyisWorthinaFairSaleField;
    
        private string municipalCodeField;
    
        private short numberofBathroomsField;
    
        private bool numberofBathroomsFieldSpecified;
    
        private short numberofBedroomsField;
    
        private bool numberofBedroomsFieldSpecified;
    
        private short numberofFireplacesonthePropertyField;
    
        private bool numberofFireplacesonthePropertyFieldSpecified;
    
        private short numberofGarageSpacesonthePropertyField;
    
        private bool numberofGarageSpacesonthePropertyFieldSpecified;
    
        private short numberofLevelsonPropertyField;
    
        private bool numberofLevelsonPropertyFieldSpecified;
    
        private PropertyOwnersNamesType[] propertyOwnersNamesField;
    
        private CodeType flagtoIndicateifOwnerOccupiesthePropertyField;
    
        private string propertyStyleField;
    
        private string typeofPropertyField;
    
        private string typeofRoofonPropertyField;
    
        private string secondarySchoolDistrictforPropertyField;
    
        private string sideofStreetWherePropertyisLocatedField;
    
        private string stateFIPSinWhichthePropertyResidesField;
    
        private string nameofSubdivisionWherePropertyisLocatedField;
    
        private string whetherorNotthePropertyisOwnedByanREOentityField;
    
        private DateType taxSaleDateforthePropertypertheCountyField;
    
        private MoneyType taxSalePriceforthePropertypertheCountyField;
    
        private short totalNumberofRoomsonPropertyField;
    
        private bool totalNumberofRoomsonPropertyFieldSpecified;
    
        private string useCodeforthePropertyPertheCountyField;
    
        private string unifiedSchoolDistrictforthePropertypertheCountyField;
    
        private DateType yearthatthePropertywasBuiltField;
    
        private string localMunicipalLawSpecifiesWhatPurposeMaybeUsedField;
    
        private SimpleAddressType taxMailingAddressforthePropertysOwnerField;
    
        private DateType lastSaleDateofthePropertyPertheCountyField;
    
        private MoneyType lastSalePriceofthePropertyPertheCountyField;
    
        /// <remarks/>
        public CodeType HitNoHitIndicator {
            get {
                return this.hitNoHitIndicatorField;
            }
            set {
                this.hitNoHitIndicatorField = value;
            }
        }
    
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute()]
        public string AssessedLotSizeofPropertyinAcres {
            get {
                return this.assessedLotSizeofPropertyinAcresField;
            }
            set {
                this.assessedLotSizeofPropertyinAcresField = value;
            }
        }
    
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute()]
        public MoneyType AppraisedValueofPropertybyCtyAssessor {
            get {
                return this.appraisedValueofPropertybyCtyAssessorField;
            }
            set {
                this.appraisedValueofPropertybyCtyAssessorField = value;
            }
        }
    
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute()]
        public MoneyType AssessedValueofPropertyValueAssigned {
            get {
                return this.assessedValueofPropertyValueAssignedField;
            }
            set {
                this.assessedValueofPropertyValueAssignedField = value;
            }
        }
    
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute()]
        public string YearthatPropertywasLastAssessed {
            get {
                return this.yearthatPropertywasLastAssessedField;
            }
            set {
                this.yearthatPropertywasLastAssessedField = value;
            }
        }
    
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute()]
        public MoneyType ValueAssignedbyCountyTaxAssesImprovements {
            get {
                return this.valueAssignedbyCountyTaxAssesImprovementsField;
            }
            set {
                this.valueAssignedbyCountyTaxAssesImprovementsField = value;
            }
        }
    
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute()]
        public MoneyType AssessedLandValueoftheProperty {
            get {
                return this.assessedLandValueofthePropertyField;
            }
            set {
                this.assessedLandValueofthePropertyField = value;
            }
        }
    
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute()]
        public string FinishedSquareFootageofPropertyBasement {
            get {
                return this.finishedSquareFootageofPropertyBasementField;
            }
            set {
                this.finishedSquareFootageofPropertyBasementField = value;
            }
        }
    
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute()]
        public string TotalSquareFootageofpropertiesBasement {
            get {
                return this.totalSquareFootageofpropertiesBasementField;
            }
            set {
                this.totalSquareFootageofpropertiesBasementField = value;
            }
        }
    
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute()]
        public string CountyWherePropertyisLocated {
            get {
                return this.countyWherePropertyisLocatedField;
            }
            set {
                this.countyWherePropertyisLocatedField = value;
            }
        }
    
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute()]
        public string ElementarySchoolDistrictforProperty {
            get {
                return this.elementarySchoolDistrictforPropertyField;
            }
            set {
                this.elementarySchoolDistrictforPropertyField = value;
            }
        }
    
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute()]
        public string FederalInformationProcessingStandard {
            get {
                return this.federalInformationProcessingStandardField;
            }
            set {
                this.federalInformationProcessingStandardField = value;
            }
        }
    
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute()]
        public string TotalSquareFootageofPropertiesGarage {
            get {
                return this.totalSquareFootageofPropertiesGarageField;
            }
            set {
                this.totalSquareFootageofPropertiesGarageField = value;
            }
        }
    
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute()]
        public string TypeofGarage {
            get {
                return this.typeofGarageField;
            }
            set {
                this.typeofGarageField = value;
            }
        }
    
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute()]
        public string SpecifiesNorthSouthPositionoftheProperty {
            get {
                return this.specifiesNorthSouthPositionofthePropertyField;
            }
            set {
                this.specifiesNorthSouthPositionofthePropertyField = value;
            }
        }
    
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute()]
        public string SpecifiesEastWestPositionoftheProperty {
            get {
                return this.specifiesEastWestPositionofthePropertyField;
            }
            set {
                this.specifiesEastWestPositionofthePropertyField = value;
            }
        }
    
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute()]
        public string MainSquareFootageofProperty {
            get {
                return this.mainSquareFootageofPropertyField;
            }
            set {
                this.mainSquareFootageofPropertyField = value;
            }
        }
    
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute()]
        public MoneyType LikelyPriceaPropertyisWorthinaFairSale {
            get {
                return this.likelyPriceaPropertyisWorthinaFairSaleField;
            }
            set {
                this.likelyPriceaPropertyisWorthinaFairSaleField = value;
            }
        }
    
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute()]
        public string MunicipalCode {
            get {
                return this.municipalCodeField;
            }
            set {
                this.municipalCodeField = value;
            }
        }
    
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute()]
        public short NumberofBathrooms {
            get {
                return this.numberofBathroomsField;
            }
            set {
                this.numberofBathroomsField = value;
            }
        }
    
        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool NumberofBathroomsSpecified {
            get {
                return this.numberofBathroomsFieldSpecified;
            }
            set {
                this.numberofBathroomsFieldSpecified = value;
            }
        }
    
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute()]
        public short NumberofBedrooms {
            get {
                return this.numberofBedroomsField;
            }
            set {
                this.numberofBedroomsField = value;
            }
        }
    
        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool NumberofBedroomsSpecified {
            get {
                return this.numberofBedroomsFieldSpecified;
            }
            set {
                this.numberofBedroomsFieldSpecified = value;
            }
        }
    
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute()]
        public short NumberofFireplacesontheProperty {
            get {
                return this.numberofFireplacesonthePropertyField;
            }
            set {
                this.numberofFireplacesonthePropertyField = value;
            }
        }
    
        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool NumberofFireplacesonthePropertySpecified {
            get {
                return this.numberofFireplacesonthePropertyFieldSpecified;
            }
            set {
                this.numberofFireplacesonthePropertyFieldSpecified = value;
            }
        }
    
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute()]
        public short NumberofGarageSpacesontheProperty {
            get {
                return this.numberofGarageSpacesonthePropertyField;
            }
            set {
                this.numberofGarageSpacesonthePropertyField = value;
            }
        }
    
        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool NumberofGarageSpacesonthePropertySpecified {
            get {
                return this.numberofGarageSpacesonthePropertyFieldSpecified;
            }
            set {
                this.numberofGarageSpacesonthePropertyFieldSpecified = value;
            }
        }
    
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute()]
        public short NumberofLevelsonProperty {
            get {
                return this.numberofLevelsonPropertyField;
            }
            set {
                this.numberofLevelsonPropertyField = value;
            }
        }
    
        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool NumberofLevelsonPropertySpecified {
            get {
                return this.numberofLevelsonPropertyFieldSpecified;
            }
            set {
                this.numberofLevelsonPropertyFieldSpecified = value;
            }
        }
    
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute("PropertyOwnersNames")]
        public PropertyOwnersNamesType[] PropertyOwnersNames {
            get {
                return this.propertyOwnersNamesField;
            }
            set {
                this.propertyOwnersNamesField = value;
            }
        }
    
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute()]
        public CodeType FlagtoIndicateifOwnerOccupiestheProperty {
            get {
                return this.flagtoIndicateifOwnerOccupiesthePropertyField;
            }
            set {
                this.flagtoIndicateifOwnerOccupiesthePropertyField = value;
            }
        }
    
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute()]
        public string PropertyStyle {
            get {
                return this.propertyStyleField;
            }
            set {
                this.propertyStyleField = value;
            }
        }
    
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute()]
        public string TypeofProperty {
            get {
                return this.typeofPropertyField;
            }
            set {
                this.typeofPropertyField = value;
            }
        }
    
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute()]
        public string TypeofRoofonProperty {
            get {
                return this.typeofRoofonPropertyField;
            }
            set {
                this.typeofRoofonPropertyField = value;
            }
        }
    
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute()]
        public string SecondarySchoolDistrictforProperty {
            get {
                return this.secondarySchoolDistrictforPropertyField;
            }
            set {
                this.secondarySchoolDistrictforPropertyField = value;
            }
        }
    
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute()]
        public string SideofStreetWherePropertyisLocated {
            get {
                return this.sideofStreetWherePropertyisLocatedField;
            }
            set {
                this.sideofStreetWherePropertyisLocatedField = value;
            }
        }
    
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute()]
        public string StateFIPSinWhichthePropertyResides {
            get {
                return this.stateFIPSinWhichthePropertyResidesField;
            }
            set {
                this.stateFIPSinWhichthePropertyResidesField = value;
            }
        }
    
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute()]
        public string NameofSubdivisionWherePropertyisLocated {
            get {
                return this.nameofSubdivisionWherePropertyisLocatedField;
            }
            set {
                this.nameofSubdivisionWherePropertyisLocatedField = value;
            }
        }
    
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute()]
        public string WhetherorNotthePropertyisOwnedByanREOentity {
            get {
                return this.whetherorNotthePropertyisOwnedByanREOentityField;
            }
            set {
                this.whetherorNotthePropertyisOwnedByanREOentityField = value;
            }
        }
    
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute()]
        public DateType TaxSaleDateforthePropertypertheCounty {
            get {
                return this.taxSaleDateforthePropertypertheCountyField;
            }
            set {
                this.taxSaleDateforthePropertypertheCountyField = value;
            }
        }
    
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute()]
        public MoneyType TaxSalePriceforthePropertypertheCounty {
            get {
                return this.taxSalePriceforthePropertypertheCountyField;
            }
            set {
                this.taxSalePriceforthePropertypertheCountyField = value;
            }
        }
    
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute()]
        public short TotalNumberofRoomsonProperty {
            get {
                return this.totalNumberofRoomsonPropertyField;
            }
            set {
                this.totalNumberofRoomsonPropertyField = value;
            }
        }
    
        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool TotalNumberofRoomsonPropertySpecified {
            get {
                return this.totalNumberofRoomsonPropertyFieldSpecified;
            }
            set {
                this.totalNumberofRoomsonPropertyFieldSpecified = value;
            }
        }
    
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute()]
        public string UseCodeforthePropertyPertheCounty {
            get {
                return this.useCodeforthePropertyPertheCountyField;
            }
            set {
                this.useCodeforthePropertyPertheCountyField = value;
            }
        }
    
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute()]
        public string UnifiedSchoolDistrictforthePropertypertheCounty {
            get {
                return this.unifiedSchoolDistrictforthePropertypertheCountyField;
            }
            set {
                this.unifiedSchoolDistrictforthePropertypertheCountyField = value;
            }
        }
    
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute()]
        public DateType YearthatthePropertywasBuilt {
            get {
                return this.yearthatthePropertywasBuiltField;
            }
            set {
                this.yearthatthePropertywasBuiltField = value;
            }
        }
    
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute()]
        public string LocalMunicipalLawSpecifiesWhatPurposeMaybeUsed {
            get {
                return this.localMunicipalLawSpecifiesWhatPurposeMaybeUsedField;
            }
            set {
                this.localMunicipalLawSpecifiesWhatPurposeMaybeUsedField = value;
            }
        }
    
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute()]
        public SimpleAddressType TaxMailingAddressforthePropertysOwner {
            get {
                return this.taxMailingAddressforthePropertysOwnerField;
            }
            set {
                this.taxMailingAddressforthePropertysOwnerField = value;
            }
        }
    
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute()]
        public DateType LastSaleDateofthePropertyPertheCounty {
            get {
                return this.lastSaleDateofthePropertyPertheCountyField;
            }
            set {
                this.lastSaleDateofthePropertyPertheCountyField = value;
            }
        }
    
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute()]
        public MoneyType LastSalePriceofthePropertyPertheCounty {
            get {
                return this.lastSalePriceofthePropertyPertheCountyField;
            }
            set {
                this.lastSalePriceofthePropertyPertheCountyField = value;
            }
        }
    }
}