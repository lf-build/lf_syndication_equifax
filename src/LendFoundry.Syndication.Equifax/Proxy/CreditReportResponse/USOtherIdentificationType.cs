namespace LendFoundry.Syndication.Equifax.Proxy.CreditReportResponse
{
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.6.1055.0")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute()]
    public partial class USOtherIdentificationType {
    
        private DateType dateReportedField;
    
        private CodeType typeField;
    
        private string otherIdentificationNumberField;
    
        private CodeType reasonField;
    
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute()]
        public DateType DateReported {
            get {
                return this.dateReportedField;
            }
            set {
                this.dateReportedField = value;
            }
        }
    
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute()]
        public CodeType Type {
            get {
                return this.typeField;
            }
            set {
                this.typeField = value;
            }
        }
    
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute()]
        public string OtherIdentificationNumber {
            get {
                return this.otherIdentificationNumberField;
            }
            set {
                this.otherIdentificationNumberField = value;
            }
        }
    
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute()]
        public CodeType Reason {
            get {
                return this.reasonField;
            }
            set {
                this.reasonField = value;
            }
        }
    }
}