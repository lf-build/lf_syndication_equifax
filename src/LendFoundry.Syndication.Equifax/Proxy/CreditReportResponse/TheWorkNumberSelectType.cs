namespace LendFoundry.Syndication.Equifax.Proxy.CreditReportResponse
{
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.6.1055.0")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute()]
    public partial class TheWorkNumberSelectType {
    
        private CodeType regulatedIdentifierField;
    
        private CodeType hitNoHitIndicatorField;
    
        private string tWNDisclaimerField;
    
        private SubjectNameFMLType employeeNameField;
    
        private string employeeSSNField;
    
        private string employerNameField;
    
        private DateType dateEmployeeInformationisEffectiveField;
    
        private string employeePositionNameorTitleField;
    
        private MoneyType rateofpayperPayPeriodField;
    
        private string frequencyDescriptionField;
    
        private MoneyType calculatedAnnualIncomeField;
    
        private string hoursWorkedPerPayPeriodField;
    
        private DateType mostRecentStartDateField;
    
        private string totalLengthofServiceinMonthsField;
    
        private DateType dateEmploymentTerminatedField;
    
        private string statusMessageField;
    
        private YearlyIncomeType[] yearlyIncomesField;
    
        private string employerCodeField;
    
        private string employerDisclaimerField;
    
        private string referenceNumberServerIdField;
    
        private string referralURLField;
    
        private string referralBureauNameField;
    
        private SimpleAddressType referralBureauAddressField;
    
        /// <remarks/>
        public CodeType RegulatedIdentifier {
            get {
                return this.regulatedIdentifierField;
            }
            set {
                this.regulatedIdentifierField = value;
            }
        }
    
        /// <remarks/>
        public CodeType HitNoHitIndicator {
            get {
                return this.hitNoHitIndicatorField;
            }
            set {
                this.hitNoHitIndicatorField = value;
            }
        }
    
        /// <remarks/>
        public string TWNDisclaimer {
            get {
                return this.tWNDisclaimerField;
            }
            set {
                this.tWNDisclaimerField = value;
            }
        }
    
        /// <remarks/>
        public SubjectNameFMLType EmployeeName {
            get {
                return this.employeeNameField;
            }
            set {
                this.employeeNameField = value;
            }
        }
    
        /// <remarks/>
        public string EmployeeSSN {
            get {
                return this.employeeSSNField;
            }
            set {
                this.employeeSSNField = value;
            }
        }
    
        /// <remarks/>
        public string EmployerName {
            get {
                return this.employerNameField;
            }
            set {
                this.employerNameField = value;
            }
        }
    
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute()]
        public DateType DateEmployeeInformationisEffective {
            get {
                return this.dateEmployeeInformationisEffectiveField;
            }
            set {
                this.dateEmployeeInformationisEffectiveField = value;
            }
        }
    
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute()]
        public string EmployeePositionNameorTitle {
            get {
                return this.employeePositionNameorTitleField;
            }
            set {
                this.employeePositionNameorTitleField = value;
            }
        }
    
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute()]
        public MoneyType RateofpayperPayPeriod {
            get {
                return this.rateofpayperPayPeriodField;
            }
            set {
                this.rateofpayperPayPeriodField = value;
            }
        }
    
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute()]
        public string FrequencyDescription {
            get {
                return this.frequencyDescriptionField;
            }
            set {
                this.frequencyDescriptionField = value;
            }
        }
    
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute()]
        public MoneyType CalculatedAnnualIncome {
            get {
                return this.calculatedAnnualIncomeField;
            }
            set {
                this.calculatedAnnualIncomeField = value;
            }
        }
    
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute()]
        public string HoursWorkedPerPayPeriod {
            get {
                return this.hoursWorkedPerPayPeriodField;
            }
            set {
                this.hoursWorkedPerPayPeriodField = value;
            }
        }
    
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute()]
        public DateType MostRecentStartDate {
            get {
                return this.mostRecentStartDateField;
            }
            set {
                this.mostRecentStartDateField = value;
            }
        }
    
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute()]
        public string TotalLengthofServiceinMonths {
            get {
                return this.totalLengthofServiceinMonthsField;
            }
            set {
                this.totalLengthofServiceinMonthsField = value;
            }
        }
    
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute()]
        public DateType DateEmploymentTerminated {
            get {
                return this.dateEmploymentTerminatedField;
            }
            set {
                this.dateEmploymentTerminatedField = value;
            }
        }
    
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute()]
        public string StatusMessage {
            get {
                return this.statusMessageField;
            }
            set {
                this.statusMessageField = value;
            }
        }
    
        /// <remarks/>
        [System.Xml.Serialization.XmlArrayAttribute()]
        [System.Xml.Serialization.XmlArrayItemAttribute("YearlyIncome", IsNullable=false)]
        public YearlyIncomeType[] YearlyIncomes {
            get {
                return this.yearlyIncomesField;
            }
            set {
                this.yearlyIncomesField = value;
            }
        }
    
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute()]
        public string EmployerCode {
            get {
                return this.employerCodeField;
            }
            set {
                this.employerCodeField = value;
            }
        }
    
        /// <remarks/>
        public string EmployerDisclaimer {
            get {
                return this.employerDisclaimerField;
            }
            set {
                this.employerDisclaimerField = value;
            }
        }
    
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute()]
        public string ReferenceNumberServerId {
            get {
                return this.referenceNumberServerIdField;
            }
            set {
                this.referenceNumberServerIdField = value;
            }
        }
    
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute()]
        public string ReferralURL {
            get {
                return this.referralURLField;
            }
            set {
                this.referralURLField = value;
            }
        }
    
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute()]
        public string ReferralBureauName {
            get {
                return this.referralBureauNameField;
            }
            set {
                this.referralBureauNameField = value;
            }
        }
    
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute()]
        public SimpleAddressType ReferralBureauAddress {
            get {
                return this.referralBureauAddressField;
            }
            set {
                this.referralBureauAddressField = value;
            }
        }
    }
}