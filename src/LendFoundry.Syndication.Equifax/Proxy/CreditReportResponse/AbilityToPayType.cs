namespace LendFoundry.Syndication.Equifax.Proxy.CreditReportResponse
{
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.6.1055.0")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute()]
    public partial class AbilityToPayType {
    
        private string productIDField;
    
        private string postalCodeField;
    
        private string postalCodePlus4Field;
    
        private short ageField;
    
        private bool ageFieldSpecified;
    
        private string ageBandField;
    
        private MoneyType incomeofConsumerField;
    
        private string aTPScoreField;
    
        private string dispositionCodeField;
    
        /// <remarks/>
        public string ProductID {
            get {
                return this.productIDField;
            }
            set {
                this.productIDField = value;
            }
        }
    
        /// <remarks/>
        public string PostalCode {
            get {
                return this.postalCodeField;
            }
            set {
                this.postalCodeField = value;
            }
        }
    
        /// <remarks/>
        public string PostalCodePlus4 {
            get {
                return this.postalCodePlus4Field;
            }
            set {
                this.postalCodePlus4Field = value;
            }
        }
    
        /// <remarks/>
        public short Age {
            get {
                return this.ageField;
            }
            set {
                this.ageField = value;
            }
        }
    
        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool AgeSpecified {
            get {
                return this.ageFieldSpecified;
            }
            set {
                this.ageFieldSpecified = value;
            }
        }
    
        /// <remarks/>
        public string AgeBand {
            get {
                return this.ageBandField;
            }
            set {
                this.ageBandField = value;
            }
        }
    
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(IsNullable=true)]
        public MoneyType IncomeofConsumer {
            get {
                return this.incomeofConsumerField;
            }
            set {
                this.incomeofConsumerField = value;
            }
        }
    
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute()]
        public string ATPScore {
            get {
                return this.aTPScoreField;
            }
            set {
                this.aTPScoreField = value;
            }
        }
    
        /// <remarks/>
        public string DispositionCode {
            get {
                return this.dispositionCodeField;
            }
            set {
                this.dispositionCodeField = value;
            }
        }
    }
}