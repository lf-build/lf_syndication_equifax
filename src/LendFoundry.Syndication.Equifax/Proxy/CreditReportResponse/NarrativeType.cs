namespace LendFoundry.Syndication.Equifax.Proxy.CreditReportResponse
{
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.6.1055.0")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute()]
    [System.Xml.Serialization.XmlRootAttribute("DimensionsNarrativeCodes", IsNullable=false)]
    public partial class NarrativeType {
    
        private CodeType[] narrativeField;
    
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute("Narrative")]
        public CodeType[] Narrative {
            get {
                return this.narrativeField;
            }
            set {
                this.narrativeField = value;
            }
        }
    }
}