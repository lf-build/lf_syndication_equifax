namespace LendFoundry.Syndication.Equifax.Proxy.CreditReportResponse
{
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.6.1055.0")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute()]
    public partial class USDecisionPowerDataType {
    
        private string decisionField;
    
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute()]
        public string Decision {
            get {
                return this.decisionField;
            }
            set {
                this.decisionField = value;
            }
        }
    }
}