namespace LendFoundry.Syndication.Equifax.Proxy.CreditReportResponse
{
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.6.1055.0")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType=true)]
    public partial class USTradeTypeDimensionsDatasDimensionsData {
    
        private MoneyType dimensionsBalanceField;
    
        private MoneyType dimensionsSchedPaymentAmtField;
    
        private MoneyType dimensionsActualPaymentAmtField;
    
        private DateType dimensionsLastPmtDateField;
    
        private MoneyType dimensionsHighCreditField;
    
        private MoneyType dimensionsCreditLimitField;
    
        private MoneyType dimensionsPastDueAmtField;
    
        private CodeType[] dimensionsNarrativeCodesField;
    
        private CodeType dimensionsAccountTypField;
    
        private CodeType dimensionsAccountDesignatorField;
    
        private short numberField;
    
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute()]
        public MoneyType DimensionsBalance {
            get {
                return this.dimensionsBalanceField;
            }
            set {
                this.dimensionsBalanceField = value;
            }
        }
    
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute()]
        public MoneyType DimensionsSchedPaymentAmt {
            get {
                return this.dimensionsSchedPaymentAmtField;
            }
            set {
                this.dimensionsSchedPaymentAmtField = value;
            }
        }
    
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute()]
        public MoneyType DimensionsActualPaymentAmt {
            get {
                return this.dimensionsActualPaymentAmtField;
            }
            set {
                this.dimensionsActualPaymentAmtField = value;
            }
        }
    
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute()]
        public DateType DimensionsLastPmtDate {
            get {
                return this.dimensionsLastPmtDateField;
            }
            set {
                this.dimensionsLastPmtDateField = value;
            }
        }
    
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute()]
        public MoneyType DimensionsHighCredit {
            get {
                return this.dimensionsHighCreditField;
            }
            set {
                this.dimensionsHighCreditField = value;
            }
        }
    
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute()]
        public MoneyType DimensionsCreditLimit {
            get {
                return this.dimensionsCreditLimitField;
            }
            set {
                this.dimensionsCreditLimitField = value;
            }
        }
    
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute()]
        public MoneyType DimensionsPastDueAmt {
            get {
                return this.dimensionsPastDueAmtField;
            }
            set {
                this.dimensionsPastDueAmtField = value;
            }
        }
    
        /// <remarks/>
        [System.Xml.Serialization.XmlArrayItemAttribute("Narrative", IsNullable=false)]
        public CodeType[] DimensionsNarrativeCodes {
            get {
                return this.dimensionsNarrativeCodesField;
            }
            set {
                this.dimensionsNarrativeCodesField = value;
            }
        }
    
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute()]
        public CodeType DimensionsAccountTyp {
            get {
                return this.dimensionsAccountTypField;
            }
            set {
                this.dimensionsAccountTypField = value;
            }
        }
    
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute()]
        public CodeType DimensionsAccountDesignator {
            get {
                return this.dimensionsAccountDesignatorField;
            }
            set {
                this.dimensionsAccountDesignatorField = value;
            }
        }
    
        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public short number {
            get {
                return this.numberField;
            }
            set {
                this.numberField = value;
            }
        }
    }
}