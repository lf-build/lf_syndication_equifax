namespace LendFoundry.Syndication.Equifax.Proxy.CreditReportResponse
{
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.6.1055.0")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute()]
    public partial class IdentityScanType {
    
        private CodeType[] alertCodesField;
    
        private BlankorNFlagType regulatedNonRegulatedFlagField;
    
        private bool regulatedNonRegulatedFlagFieldSpecified;
    
        /// <remarks/>
        [System.Xml.Serialization.XmlArrayAttribute()]
        [System.Xml.Serialization.XmlArrayItemAttribute("AlertCode", IsNullable=false)]
        public CodeType[] AlertCodes {
            get {
                return this.alertCodesField;
            }
            set {
                this.alertCodesField = value;
            }
        }
    
        /// <remarks/>
        public BlankorNFlagType RegulatedNonRegulatedFlag {
            get {
                return this.regulatedNonRegulatedFlagField;
            }
            set {
                this.regulatedNonRegulatedFlagField = value;
            }
        }
    
        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool RegulatedNonRegulatedFlagSpecified {
            get {
                return this.regulatedNonRegulatedFlagFieldSpecified;
            }
            set {
                this.regulatedNonRegulatedFlagFieldSpecified = value;
            }
        }
    }
}