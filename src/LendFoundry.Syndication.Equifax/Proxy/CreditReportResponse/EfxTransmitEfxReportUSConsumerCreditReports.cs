namespace LendFoundry.Syndication.Equifax.Proxy.CreditReportResponse
{
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.6.1055.0")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
   [System.Xml.Serialization.XmlTypeAttribute(AnonymousType=true)]
    public partial class EfxTransmitEfxReportUSConsumerCreditReports {
    
        private object[] itemsField;
    
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute("USConsumerCreditReport", typeof(USConsumerCreditReportType))]
        [System.Xml.Serialization.XmlElementAttribute("USErrorMessages", typeof(EfxTransmitEfxReportUSConsumerCreditReportsUSErrorMessages))]
        public object[] Items {
            get {
                return this.itemsField;
            }
            set {
                this.itemsField = value;
            }
        }
    }
}