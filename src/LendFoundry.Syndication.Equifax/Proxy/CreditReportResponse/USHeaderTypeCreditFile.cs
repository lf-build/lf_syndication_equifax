namespace LendFoundry.Syndication.Equifax.Proxy.CreditReportResponse
{
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.6.1055.0")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType=true)]
    public partial class USHeaderTypeCreditFile {
    
        private CodeType hitCodeField;
    
        private DateType fileSinceDateField;
    
        private DateType dateOfLastActivityField;
    
        private DateType dateOfRequestField;
    
        private CodeType linkIndicatorField;
    
        private CodeType doNotCombineIndicatorField;
    
        private CodeType fraudVictimIndicatorField;
    
        private CodeType safescanField;
    
        private CodeType safescan2Field;
    
        private CodeType safescan3Field;
    
        private CodeType safescan4Field;
    
        private CodeType[] identityscansField;
    
        private BlankorNFlagType regulatedNonRegulatedFlagField;
    
        private bool regulatedNonRegulatedFlagFieldSpecified;
    
        private string nameMatchField;
    
        private string addressDiscrepancyIndicatorField;
    
        private string sSNAffirmIndicatorField;
    
        private string promotionBlockFlagField;
    
        private string createCodeField;
    
        private string fileStatus1Field;
    
        private string fileStatus2Field;
    
        private string fileStatus3Field;
    
        private string bureauCodeField;
    
        /// <remarks/>
        public CodeType HitCode {
            get {
                return this.hitCodeField;
            }
            set {
                this.hitCodeField = value;
            }
        }
    
        /// <remarks/>
        public DateType FileSinceDate {
            get {
                return this.fileSinceDateField;
            }
            set {
                this.fileSinceDateField = value;
            }
        }
    
        /// <remarks/>
        public DateType DateOfLastActivity {
            get {
                return this.dateOfLastActivityField;
            }
            set {
                this.dateOfLastActivityField = value;
            }
        }
    
        /// <remarks/>
        public DateType DateOfRequest {
            get {
                return this.dateOfRequestField;
            }
            set {
                this.dateOfRequestField = value;
            }
        }
    
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute()]
        public CodeType LinkIndicator {
            get {
                return this.linkIndicatorField;
            }
            set {
                this.linkIndicatorField = value;
            }
        }
    
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute()]
        public CodeType DoNotCombineIndicator {
            get {
                return this.doNotCombineIndicatorField;
            }
            set {
                this.doNotCombineIndicatorField = value;
            }
        }
    
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute()]
        public CodeType FraudVictimIndicator {
            get {
                return this.fraudVictimIndicatorField;
            }
            set {
                this.fraudVictimIndicatorField = value;
            }
        }
    
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute()]
        public CodeType Safescan {
            get {
                return this.safescanField;
            }
            set {
                this.safescanField = value;
            }
        }
    
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute()]
        public CodeType Safescan2 {
            get {
                return this.safescan2Field;
            }
            set {
                this.safescan2Field = value;
            }
        }
    
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute()]
        public CodeType Safescan3 {
            get {
                return this.safescan3Field;
            }
            set {
                this.safescan3Field = value;
            }
        }
    
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute()]
        public CodeType Safescan4 {
            get {
                return this.safescan4Field;
            }
            set {
                this.safescan4Field = value;
            }
        }
    
        /// <remarks/>
        [System.Xml.Serialization.XmlArrayAttribute()]
        [System.Xml.Serialization.XmlArrayItemAttribute("Identityscan", IsNullable=false)]
        public CodeType[] Identityscans {
            get {
                return this.identityscansField;
            }
            set {
                this.identityscansField = value;
            }
        }
    
        /// <remarks/>
        public BlankorNFlagType RegulatedNonRegulatedFlag {
            get {
                return this.regulatedNonRegulatedFlagField;
            }
            set {
                this.regulatedNonRegulatedFlagField = value;
            }
        }
    
        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool RegulatedNonRegulatedFlagSpecified {
            get {
                return this.regulatedNonRegulatedFlagFieldSpecified;
            }
            set {
                this.regulatedNonRegulatedFlagFieldSpecified = value;
            }
        }
    
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute()]
        public string NameMatch {
            get {
                return this.nameMatchField;
            }
            set {
                this.nameMatchField = value;
            }
        }
    
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute()]
        public string AddressDiscrepancyIndicator {
            get {
                return this.addressDiscrepancyIndicatorField;
            }
            set {
                this.addressDiscrepancyIndicatorField = value;
            }
        }
    
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute()]
        public string SSNAffirmIndicator {
            get {
                return this.sSNAffirmIndicatorField;
            }
            set {
                this.sSNAffirmIndicatorField = value;
            }
        }
    
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute()]
        public string PromotionBlockFlag {
            get {
                return this.promotionBlockFlagField;
            }
            set {
                this.promotionBlockFlagField = value;
            }
        }
    
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute()]
        public string CreateCode {
            get {
                return this.createCodeField;
            }
            set {
                this.createCodeField = value;
            }
        }
    
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute()]
        public string FileStatus1 {
            get {
                return this.fileStatus1Field;
            }
            set {
                this.fileStatus1Field = value;
            }
        }
    
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute()]
        public string FileStatus2 {
            get {
                return this.fileStatus2Field;
            }
            set {
                this.fileStatus2Field = value;
            }
        }
    
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute()]
        public string FileStatus3 {
            get {
                return this.fileStatus3Field;
            }
            set {
                this.fileStatus3Field = value;
            }
        }
    
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute()]
        public string BureauCode {
            get {
                return this.bureauCodeField;
            }
            set {
                this.bureauCodeField = value;
            }
        }
    }
}