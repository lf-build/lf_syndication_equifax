namespace LendFoundry.Syndication.Equifax.Proxy.CreditReportResponse
{
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.6.1055.0")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute()]
    public partial class USAddressType {
    
        private CodeType expandedDataReturnedField;
    
        private object itemField;
    
        private CityType cityField;
    
        private string stateField;
    
        private string postalCodeField;
    
        private DateType dateAddressFirstReportedField;
    
        private CodeType rentOwnBuyField;
    
        private CodeType addressSourceField;
    
        private DateType dateAddressLastReportedField;
    
        private TelephoneType telephoneField;
    
        private CodeType telephoneSourceField;
    
        private DateType dateTelephoneReportedField;
    
        private CodeType addressVarianceField;
    
        private DateType expandedDateFirstReportedField;
    
        private DateType expandedDateLastReportedField;
    
        private DateType expandedDateTelephoneReportedField;
    
        private string codeField;
    
        private string descriptionField;
    
        /// <remarks/>
        public CodeType ExpandedDataReturned {
            get {
                return this.expandedDataReturnedField;
            }
            set {
                this.expandedDataReturnedField = value;
            }
        }
    
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute("ParsedStreetAddress", typeof(ParsedStreetAddressType) )]
        [System.Xml.Serialization.XmlElementAttribute("UnparsedStreetAddress", typeof(UnparsedStreetAddressType) )]
        public object Item {
            get {
                return this.itemField;
            }
            set {
                this.itemField = value;
            }
        }
    
        /// <remarks/>
        public CityType City {
            get {
                return this.cityField;
            }
            set {
                this.cityField = value;
            }
        }
    
        /// <remarks/>
        public string State {
            get {
                return this.stateField;
            }
            set {
                this.stateField = value;
            }
        }
    
        /// <remarks/>
        public string PostalCode {
            get {
                return this.postalCodeField;
            }
            set {
                this.postalCodeField = value;
            }
        }
    
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute()]
        public DateType DateAddressFirstReported {
            get {
                return this.dateAddressFirstReportedField;
            }
            set {
                this.dateAddressFirstReportedField = value;
            }
        }
    
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute()]
        public CodeType RentOwnBuy {
            get {
                return this.rentOwnBuyField;
            }
            set {
                this.rentOwnBuyField = value;
            }
        }
    
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute()]
        public CodeType AddressSource {
            get {
                return this.addressSourceField;
            }
            set {
                this.addressSourceField = value;
            }
        }
    
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute()]
        public DateType DateAddressLastReported {
            get {
                return this.dateAddressLastReportedField;
            }
            set {
                this.dateAddressLastReportedField = value;
            }
        }
    
        /// <remarks/>
        public TelephoneType Telephone {
            get {
                return this.telephoneField;
            }
            set {
                this.telephoneField = value;
            }
        }
    
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute()]
        public CodeType TelephoneSource {
            get {
                return this.telephoneSourceField;
            }
            set {
                this.telephoneSourceField = value;
            }
        }
    
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute()]
        public DateType DateTelephoneReported {
            get {
                return this.dateTelephoneReportedField;
            }
            set {
                this.dateTelephoneReportedField = value;
            }
        }
    
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute()]
        public CodeType AddressVariance {
            get {
                return this.addressVarianceField;
            }
            set {
                this.addressVarianceField = value;
            }
        }
    
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute()]
        public DateType ExpandedDateFirstReported {
            get {
                return this.expandedDateFirstReportedField;
            }
            set {
                this.expandedDateFirstReportedField = value;
            }
        }
    
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute()]
        public DateType ExpandedDateLastReported {
            get {
                return this.expandedDateLastReportedField;
            }
            set {
                this.expandedDateLastReportedField = value;
            }
        }
    
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute()]
        public DateType ExpandedDateTelephoneReported {
            get {
                return this.expandedDateTelephoneReportedField;
            }
            set {
                this.expandedDateTelephoneReportedField = value;
            }
        }
    
        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string code {
            get {
                return this.codeField;
            }
            set {
                this.codeField = value;
            }
        }
    
        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string description {
            get {
                return this.descriptionField;
            }
            set {
                this.descriptionField = value;
            }
        }
    }
}