namespace LendFoundry.Syndication.Equifax.Proxy.CreditReportResponse
{
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.6.1055.0")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute()]
    public partial class USCustomCriteriaType {
    
        private string levelIdField;
    
        private string itemTypeField;
    
        private string itemResultField;
    
        private string itemNumberField;
    
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute()]
        public string LevelId {
            get {
                return this.levelIdField;
            }
            set {
                this.levelIdField = value;
            }
        }
    
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute()]
        public string ItemType {
            get {
                return this.itemTypeField;
            }
            set {
                this.itemTypeField = value;
            }
        }
    
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute()]
        public string ItemResult {
            get {
                return this.itemResultField;
            }
            set {
                this.itemResultField = value;
            }
        }
    
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute()]
        public string ItemNumber {
            get {
                return this.itemNumberField;
            }
            set {
                this.itemNumberField = value;
            }
        }
    }
}