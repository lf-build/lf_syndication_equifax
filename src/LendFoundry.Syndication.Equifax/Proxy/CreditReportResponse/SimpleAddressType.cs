namespace LendFoundry.Syndication.Equifax.Proxy.CreditReportResponse
{
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.6.1055.0")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute()]
    public partial class SimpleAddressType {
    
        private UnparsedStreetAddressType unparsedStreetAddressField;
    
        private CityType cityField;
    
        private string stateField;
    
        private string postalCodeField;
    
        private TelephoneType telephoneField;
    
        private string codeField;
    
        private string descriptionField;
    
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute()]
        public UnparsedStreetAddressType UnparsedStreetAddress {
            get {
                return this.unparsedStreetAddressField;
            }
            set {
                this.unparsedStreetAddressField = value;
            }
        }
    
        /// <remarks/>
        public CityType City {
            get {
                return this.cityField;
            }
            set {
                this.cityField = value;
            }
        }
    
        /// <remarks/>
        public string State {
            get {
                return this.stateField;
            }
            set {
                this.stateField = value;
            }
        }
    
        /// <remarks/>
        public string PostalCode {
            get {
                return this.postalCodeField;
            }
            set {
                this.postalCodeField = value;
            }
        }
    
        /// <remarks/>
        public TelephoneType Telephone {
            get {
                return this.telephoneField;
            }
            set {
                this.telephoneField = value;
            }
        }
    
        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string code {
            get {
                return this.codeField;
            }
            set {
                this.codeField = value;
            }
        }
    
        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string description {
            get {
                return this.descriptionField;
            }
            set {
                this.descriptionField = value;
            }
        }
    }
}