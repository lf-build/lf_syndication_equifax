namespace LendFoundry.Syndication.Equifax.Proxy.CreditReportResponse
{
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.6.1055.0")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute()]
    public partial class USTradeType {
    
        private CodeType expandedDataDimensionsReturnedField;
    
        private CustomerIdType creditorIdField;
    
        private DateType dateReportedField;
    
        private DateType dateOpenedField;
    
        private string accountNumberField;
    
        private MoneyType highCreditAmountField;
    
        private MoneyType balanceAmountField;
    
        private MoneyType pastDueAmountField;
    
        private CodeType portfolioTypeField;
    
        private CodeType statusField;
    
        private short monthsReviewedField;
    
        private bool monthsReviewedFieldSpecified;
    
        private CodeType accountDesignatorField;
    
        private DateType dateOfLastActivityField;
    
        private USTradeTypeHistoryDerogatoryCounters historyDerogatoryCountersField;
    
        private CodeDateType[] previousHighPaymentRatesField;
    
        private MoneyType scheduledPaymentAmountField;
    
        private string termsDurationField;
    
        private string paymentHistoryField;
    
        private CodeType updateIndicatorField;
    
        private CodeType[] narrativesField;
    
        private CustomerIdType expandedCreditorIdField;
    
        private DateType expandedDateReportedField;
    
        private DateType expandedDateOpenedField;
    
        private MoneyType expandedHighCreditAmountField;
    
        private MoneyType expandedCreditLimitField;
    
        private MoneyType expandedBalanceAmountField;
    
        private MoneyType expandedPastDueAmountField;
    
        private CodeType expandedPortfolioTypeField;
    
        private CodeType expandedStatusField;
    
        private USTradeTypeExpandedHistoryDerogatoryCounter expandedHistoryDerogatoryCounterField;
    
        private CodeDateType[] expandedPreviousHighPaymentRatesField;
    
        private DateType expandedDateOfLastActivityField;
    
        private CodeType[] expandedNarrativesField;
    
        private CodeType expandedAccountTypeField;
    
        private DateType expandedDateOfLastPaymentField;
    
        private DateType expandedDateClosedField;
    
        private DateType expandedDateMajorDelinquencyReportedField;
    
        private MoneyType expandedActualPaymentAmountField;
    
        private MoneyType expandedScheduledPaymentAmountField;
    
        private CodeType expandedTermsFrequencyField;
    
        private CodeType expandedTermsDurationField;
    
        private CodeType expandedPurchasedPortfolioIndicatorField;
    
        private string expandedPurchasedPortfolioNameField;
    
        private CodeType expandedCreditorClassificationField;
    
        private CodeType expandedActivityDesignatorField;
    
        private MoneyType expandedOriginalChargeOffAmountField;
    
        private DateType expandedDeferredPaymentStartDateField;
    
        private MoneyType expandedBalloonPaymentAmountField;
    
        private DateType expandedBalloonPaymentDueDateField;
    
        private string expandedMortgageIDNumberField;
    
        private string[] expandedPaymentHistoryField;
    
        private CodeType expandedPreviousHighRateOutsideHistoryField;
    
        private DateType expandedPreviousHighDateOutsideHistoryField;
    
        private USTradeTypeDimensionsDatas dimensionsDatasField;
    
        /// <remarks/>
        public CodeType ExpandedDataDimensionsReturned {
            get {
                return this.expandedDataDimensionsReturnedField;
            }
            set {
                this.expandedDataDimensionsReturnedField = value;
            }
        }
    
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute()]
        public CustomerIdType CreditorId {
            get {
                return this.creditorIdField;
            }
            set {
                this.creditorIdField = value;
            }
        }
    
        /// <remarks/>
        public DateType DateReported {
            get {
                return this.dateReportedField;
            }
            set {
                this.dateReportedField = value;
            }
        }
    
        /// <remarks/>
        public DateType DateOpened {
            get {
                return this.dateOpenedField;
            }
            set {
                this.dateOpenedField = value;
            }
        }
    
        /// <remarks/>
        public string AccountNumber {
            get {
                return this.accountNumberField;
            }
            set {
                this.accountNumberField = value;
            }
        }
    
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(IsNullable=true)]
        public MoneyType HighCreditAmount {
            get {
                return this.highCreditAmountField;
            }
            set {
                this.highCreditAmountField = value;
            }
        }
    
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(IsNullable=true)]
        public MoneyType BalanceAmount {
            get {
                return this.balanceAmountField;
            }
            set {
                this.balanceAmountField = value;
            }
        }
    
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(IsNullable=true)]
        public MoneyType PastDueAmount {
            get {
                return this.pastDueAmountField;
            }
            set {
                this.pastDueAmountField = value;
            }
        }
    
        /// <remarks/>
        public CodeType PortfolioType {
            get {
                return this.portfolioTypeField;
            }
            set {
                this.portfolioTypeField = value;
            }
        }
    
        /// <remarks/>
        public CodeType Status {
            get {
                return this.statusField;
            }
            set {
                this.statusField = value;
            }
        }
    
        /// <remarks/>
        public short MonthsReviewed {
            get {
                return this.monthsReviewedField;
            }
            set {
                this.monthsReviewedField = value;
            }
        }
    
        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool MonthsReviewedSpecified {
            get {
                return this.monthsReviewedFieldSpecified;
            }
            set {
                this.monthsReviewedFieldSpecified = value;
            }
        }
    
        /// <remarks/>
        public CodeType AccountDesignator {
            get {
                return this.accountDesignatorField;
            }
            set {
                this.accountDesignatorField = value;
            }
        }
    
        /// <remarks/>
        public DateType DateOfLastActivity {
            get {
                return this.dateOfLastActivityField;
            }
            set {
                this.dateOfLastActivityField = value;
            }
        }
    
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute()]
        public USTradeTypeHistoryDerogatoryCounters HistoryDerogatoryCounters {
            get {
                return this.historyDerogatoryCountersField;
            }
            set {
                this.historyDerogatoryCountersField = value;
            }
        }
    
        /// <remarks/>
        [System.Xml.Serialization.XmlArrayAttribute()]
        [System.Xml.Serialization.XmlArrayItemAttribute("PreviousHighPaymentRate", IsNullable=false)]
        public CodeDateType[] PreviousHighPaymentRates {
            get {
                return this.previousHighPaymentRatesField;
            }
            set {
                this.previousHighPaymentRatesField = value;
            }
        }
    
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute( IsNullable=true)]
        public MoneyType ScheduledPaymentAmount {
            get {
                return this.scheduledPaymentAmountField;
            }
            set {
                this.scheduledPaymentAmountField = value;
            }
        }
    
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute()]
        public string TermsDuration {
            get {
                return this.termsDurationField;
            }
            set {
                this.termsDurationField = value;
            }
        }
    
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute()]
        public string PaymentHistory {
            get {
                return this.paymentHistoryField;
            }
            set {
                this.paymentHistoryField = value;
            }
        }
    
        /// <remarks/>
        public CodeType UpdateIndicator {
            get {
                return this.updateIndicatorField;
            }
            set {
                this.updateIndicatorField = value;
            }
        }
    
        /// <remarks/>
        [System.Xml.Serialization.XmlArrayItemAttribute("Narrative", IsNullable=false)]
        public CodeType[] Narratives {
            get {
                return this.narrativesField;
            }
            set {
                this.narrativesField = value;
            }
        }
    
        /// <remarks/>
        public CustomerIdType ExpandedCreditorId {
            get {
                return this.expandedCreditorIdField;
            }
            set {
                this.expandedCreditorIdField = value;
            }
        }
    
        /// <remarks/>
        public DateType ExpandedDateReported {
            get {
                return this.expandedDateReportedField;
            }
            set {
                this.expandedDateReportedField = value;
            }
        }
    
        /// <remarks/>
        public DateType ExpandedDateOpened {
            get {
                return this.expandedDateOpenedField;
            }
            set {
                this.expandedDateOpenedField = value;
            }
        }
    
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(IsNullable=true)]
        public MoneyType ExpandedHighCreditAmount {
            get {
                return this.expandedHighCreditAmountField;
            }
            set {
                this.expandedHighCreditAmountField = value;
            }
        }
    
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(IsNullable=true)]
        public MoneyType ExpandedCreditLimit {
            get {
                return this.expandedCreditLimitField;
            }
            set {
                this.expandedCreditLimitField = value;
            }
        }
    
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(IsNullable=true)]
        public MoneyType ExpandedBalanceAmount {
            get {
                return this.expandedBalanceAmountField;
            }
            set {
                this.expandedBalanceAmountField = value;
            }
        }
    
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(IsNullable=true)]
        public MoneyType ExpandedPastDueAmount {
            get {
                return this.expandedPastDueAmountField;
            }
            set {
                this.expandedPastDueAmountField = value;
            }
        }
    
        /// <remarks/>
        public CodeType ExpandedPortfolioType {
            get {
                return this.expandedPortfolioTypeField;
            }
            set {
                this.expandedPortfolioTypeField = value;
            }
        }
    
        /// <remarks/>
        public CodeType ExpandedStatus {
            get {
                return this.expandedStatusField;
            }
            set {
                this.expandedStatusField = value;
            }
        }
    
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute()]
        public USTradeTypeExpandedHistoryDerogatoryCounter ExpandedHistoryDerogatoryCounter {
            get {
                return this.expandedHistoryDerogatoryCounterField;
            }
            set {
                this.expandedHistoryDerogatoryCounterField = value;
            }
        }
    
        /// <remarks/>
        [System.Xml.Serialization.XmlArrayAttribute()]
        [System.Xml.Serialization.XmlArrayItemAttribute("PreviousHighPaymentRate", IsNullable=false)]
        public CodeDateType[] ExpandedPreviousHighPaymentRates {
            get {
                return this.expandedPreviousHighPaymentRatesField;
            }
            set {
                this.expandedPreviousHighPaymentRatesField = value;
            }
        }
    
        /// <remarks/>
        public DateType ExpandedDateOfLastActivity {
            get {
                return this.expandedDateOfLastActivityField;
            }
            set {
                this.expandedDateOfLastActivityField = value;
            }
        }
    
        /// <remarks/>
        [System.Xml.Serialization.XmlArrayItemAttribute("Narrative", IsNullable=false)]
        public CodeType[] ExpandedNarratives {
            get {
                return this.expandedNarrativesField;
            }
            set {
                this.expandedNarrativesField = value;
            }
        }
    
        /// <remarks/>
        public CodeType ExpandedAccountType {
            get {
                return this.expandedAccountTypeField;
            }
            set {
                this.expandedAccountTypeField = value;
            }
        }
    
        /// <remarks/>
        public DateType ExpandedDateOfLastPayment {
            get {
                return this.expandedDateOfLastPaymentField;
            }
            set {
                this.expandedDateOfLastPaymentField = value;
            }
        }
    
        /// <remarks/>
        public DateType ExpandedDateClosed {
            get {
                return this.expandedDateClosedField;
            }
            set {
                this.expandedDateClosedField = value;
            }
        }
    
        /// <remarks/>
        public DateType ExpandedDateMajorDelinquencyReported {
            get {
                return this.expandedDateMajorDelinquencyReportedField;
            }
            set {
                this.expandedDateMajorDelinquencyReportedField = value;
            }
        }
    
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(IsNullable=true)]
        public MoneyType ExpandedActualPaymentAmount {
            get {
                return this.expandedActualPaymentAmountField;
            }
            set {
                this.expandedActualPaymentAmountField = value;
            }
        }
    
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(IsNullable=true)]
        public MoneyType ExpandedScheduledPaymentAmount {
            get {
                return this.expandedScheduledPaymentAmountField;
            }
            set {
                this.expandedScheduledPaymentAmountField = value;
            }
        }
    
        /// <remarks/>
        public CodeType ExpandedTermsFrequency {
            get {
                return this.expandedTermsFrequencyField;
            }
            set {
                this.expandedTermsFrequencyField = value;
            }
        }
    
        /// <remarks/>
        public CodeType ExpandedTermsDuration {
            get {
                return this.expandedTermsDurationField;
            }
            set {
                this.expandedTermsDurationField = value;
            }
        }
    
        /// <remarks/>
        public CodeType ExpandedPurchasedPortfolioIndicator {
            get {
                return this.expandedPurchasedPortfolioIndicatorField;
            }
            set {
                this.expandedPurchasedPortfolioIndicatorField = value;
            }
        }
    
        /// <remarks/>
        public string ExpandedPurchasedPortfolioName {
            get {
                return this.expandedPurchasedPortfolioNameField;
            }
            set {
                this.expandedPurchasedPortfolioNameField = value;
            }
        }
    
        /// <remarks/>
        public CodeType ExpandedCreditorClassification {
            get {
                return this.expandedCreditorClassificationField;
            }
            set {
                this.expandedCreditorClassificationField = value;
            }
        }
    
        /// <remarks/>
        public CodeType ExpandedActivityDesignator {
            get {
                return this.expandedActivityDesignatorField;
            }
            set {
                this.expandedActivityDesignatorField = value;
            }
        }
    
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(IsNullable=true)]
        public MoneyType ExpandedOriginalChargeOffAmount {
            get {
                return this.expandedOriginalChargeOffAmountField;
            }
            set {
                this.expandedOriginalChargeOffAmountField = value;
            }
        }
    
        /// <remarks/>
        public DateType ExpandedDeferredPaymentStartDate {
            get {
                return this.expandedDeferredPaymentStartDateField;
            }
            set {
                this.expandedDeferredPaymentStartDateField = value;
            }
        }
    
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(IsNullable=true)]
        public MoneyType ExpandedBalloonPaymentAmount {
            get {
                return this.expandedBalloonPaymentAmountField;
            }
            set {
                this.expandedBalloonPaymentAmountField = value;
            }
        }
    
        /// <remarks/>
        public DateType ExpandedBalloonPaymentDueDate {
            get {
                return this.expandedBalloonPaymentDueDateField;
            }
            set {
                this.expandedBalloonPaymentDueDateField = value;
            }
        }
    
        /// <remarks/>
        public string ExpandedMortgageIDNumber {
            get {
                return this.expandedMortgageIDNumberField;
            }
            set {
                this.expandedMortgageIDNumberField = value;
            }
        }
    
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute("ExpandedPaymentHistory")]
        public string[] ExpandedPaymentHistory {
            get {
                return this.expandedPaymentHistoryField;
            }
            set {
                this.expandedPaymentHistoryField = value;
            }
        }
    
        /// <remarks/>
        public CodeType ExpandedPreviousHighRateOutsideHistory {
            get {
                return this.expandedPreviousHighRateOutsideHistoryField;
            }
            set {
                this.expandedPreviousHighRateOutsideHistoryField = value;
            }
        }
    
        /// <remarks/>
        public DateType ExpandedPreviousHighDateOutsideHistory {
            get {
                return this.expandedPreviousHighDateOutsideHistoryField;
            }
            set {
                this.expandedPreviousHighDateOutsideHistoryField = value;
            }
        }
    
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute()]
        public USTradeTypeDimensionsDatas DimensionsDatas {
            get {
                return this.dimensionsDatasField;
            }
            set {
                this.dimensionsDatasField = value;
            }
        }
    }
}