namespace LendFoundry.Syndication.Equifax.Proxy.CreditReportResponse
{
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.6.1055.0")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute()]
    public partial class PropertyDataAndAnalyticAVMType {
    
        private CodeType hitNoHitIndicatorField;
    
        private MoneyType automatedValuationModelPropValueSpecificTimeField;
    
        private MoneyType lowRangeEstimatedValueofSubjectPropertyField;
    
        private MoneyType highRangeEstimatedValueofSubjectsPropertyField;
    
        private string scoreIndicatorAccuracyofPropertyValuationField;
    
        /// <remarks/>
        public CodeType HitNoHitIndicator {
            get {
                return this.hitNoHitIndicatorField;
            }
            set {
                this.hitNoHitIndicatorField = value;
            }
        }
    
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute()]
        public MoneyType AutomatedValuationModelPropValueSpecificTime {
            get {
                return this.automatedValuationModelPropValueSpecificTimeField;
            }
            set {
                this.automatedValuationModelPropValueSpecificTimeField = value;
            }
        }
    
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute()]
        public MoneyType LowRangeEstimatedValueofSubjectProperty {
            get {
                return this.lowRangeEstimatedValueofSubjectPropertyField;
            }
            set {
                this.lowRangeEstimatedValueofSubjectPropertyField = value;
            }
        }
    
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute()]
        public MoneyType HighRangeEstimatedValueofSubjectsProperty {
            get {
                return this.highRangeEstimatedValueofSubjectsPropertyField;
            }
            set {
                this.highRangeEstimatedValueofSubjectsPropertyField = value;
            }
        }
    
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute()]
        public string ScoreIndicatorAccuracyofPropertyValuation {
            get {
                return this.scoreIndicatorAccuracyofPropertyValuationField;
            }
            set {
                this.scoreIndicatorAccuracyofPropertyValuationField = value;
            }
        }
    }
}