namespace LendFoundry.Syndication.Equifax.Proxy.CreditReportResponse
{
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.6.1055.0")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType=true)]
    public partial class USTradeTypeHistoryDerogatoryCounters {
    
        private short count30DayPastDueField;
    
        private bool count30DayPastDueFieldSpecified;
    
        private short count60DayPastDueField;
    
        private bool count60DayPastDueFieldSpecified;
    
        private short count90DayPastDueField;
    
        private bool count90DayPastDueFieldSpecified;
    
        /// <remarks/>
        public short Count30DayPastDue {
            get {
                return this.count30DayPastDueField;
            }
            set {
                this.count30DayPastDueField = value;
            }
        }
    
        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool Count30DayPastDueSpecified {
            get {
                return this.count30DayPastDueFieldSpecified;
            }
            set {
                this.count30DayPastDueFieldSpecified = value;
            }
        }
    
        /// <remarks/>
        public short Count60DayPastDue {
            get {
                return this.count60DayPastDueField;
            }
            set {
                this.count60DayPastDueField = value;
            }
        }
    
        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool Count60DayPastDueSpecified {
            get {
                return this.count60DayPastDueFieldSpecified;
            }
            set {
                this.count60DayPastDueFieldSpecified = value;
            }
        }
    
        /// <remarks/>
        public short Count90DayPastDue {
            get {
                return this.count90DayPastDueField;
            }
            set {
                this.count90DayPastDueField = value;
            }
        }
    
        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool Count90DayPastDueSpecified {
            get {
                return this.count90DayPastDueFieldSpecified;
            }
            set {
                this.count90DayPastDueFieldSpecified = value;
            }
        }
    }
}