namespace LendFoundry.Syndication.Equifax.Proxy.CreditReportResponse
{
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.6.1055.0")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute()]
    public partial class PropertyDataAndAnalyticHPIType {
    
        private CodeType hitNoHitIndicatorField;
    
        private PropertyDataCommonType blendedMedianPriceChangeZipCodeField;
    
        private PropertyDataCommonType blendedMedianPriceChangeForSegorNeighborhoodField;
    
        private PropertyDataCommonType blendedMedianPriceChangeStateField;
    
        private PropertyDataCommonType blendedMedianPriceChangeCountyField;
    
        private PropertyDataCommonType retailMedianPriceChangeZipCodeField;
    
        private PropertyDataCommonType retailMedianPriceChangeSegmentField;
    
        private PropertyDataCommonType retailMedianPriceChangeStateField;
    
        private PropertyDataCommonType retailMedianPriceChangeCountyField;
    
        private PropertyDataCommonType rEOMedianPriceChangeZipCodeField;
    
        private PropertyDataCommonType rEOBlendedMedianPriceChangeSegmentField;
    
        private PropertyDataCommonType rEOMedianPriceChangeStateField;
    
        private PropertyDataCommonType rEOMedianPriceChangeCountyField;
    
        /// <remarks/>
        public CodeType HitNoHitIndicator {
            get {
                return this.hitNoHitIndicatorField;
            }
            set {
                this.hitNoHitIndicatorField = value;
            }
        }
    
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute()]
        public PropertyDataCommonType BlendedMedianPriceChangeZipCode {
            get {
                return this.blendedMedianPriceChangeZipCodeField;
            }
            set {
                this.blendedMedianPriceChangeZipCodeField = value;
            }
        }
    
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute()]
        public PropertyDataCommonType BlendedMedianPriceChangeForSegorNeighborhood {
            get {
                return this.blendedMedianPriceChangeForSegorNeighborhoodField;
            }
            set {
                this.blendedMedianPriceChangeForSegorNeighborhoodField = value;
            }
        }
    
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute()]
        public PropertyDataCommonType BlendedMedianPriceChangeState {
            get {
                return this.blendedMedianPriceChangeStateField;
            }
            set {
                this.blendedMedianPriceChangeStateField = value;
            }
        }
    
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute()]
        public PropertyDataCommonType BlendedMedianPriceChangeCounty {
            get {
                return this.blendedMedianPriceChangeCountyField;
            }
            set {
                this.blendedMedianPriceChangeCountyField = value;
            }
        }
    
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute()]
        public PropertyDataCommonType RetailMedianPriceChangeZipCode {
            get {
                return this.retailMedianPriceChangeZipCodeField;
            }
            set {
                this.retailMedianPriceChangeZipCodeField = value;
            }
        }
    
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute()]
        public PropertyDataCommonType RetailMedianPriceChangeSegment {
            get {
                return this.retailMedianPriceChangeSegmentField;
            }
            set {
                this.retailMedianPriceChangeSegmentField = value;
            }
        }
    
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute()]
        public PropertyDataCommonType RetailMedianPriceChangeState {
            get {
                return this.retailMedianPriceChangeStateField;
            }
            set {
                this.retailMedianPriceChangeStateField = value;
            }
        }
    
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute()]
        public PropertyDataCommonType RetailMedianPriceChangeCounty {
            get {
                return this.retailMedianPriceChangeCountyField;
            }
            set {
                this.retailMedianPriceChangeCountyField = value;
            }
        }
    
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute()]
        public PropertyDataCommonType REOMedianPriceChangeZipCode {
            get {
                return this.rEOMedianPriceChangeZipCodeField;
            }
            set {
                this.rEOMedianPriceChangeZipCodeField = value;
            }
        }
    
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute()]
        public PropertyDataCommonType REOBlendedMedianPriceChangeSegment {
            get {
                return this.rEOBlendedMedianPriceChangeSegmentField;
            }
            set {
                this.rEOBlendedMedianPriceChangeSegmentField = value;
            }
        }
    
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute()]
        public PropertyDataCommonType REOMedianPriceChangeState {
            get {
                return this.rEOMedianPriceChangeStateField;
            }
            set {
                this.rEOMedianPriceChangeStateField = value;
            }
        }
    
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute()]
        public PropertyDataCommonType REOMedianPriceChangeCounty {
            get {
                return this.rEOMedianPriceChangeCountyField;
            }
            set {
                this.rEOMedianPriceChangeCountyField = value;
            }
        }
    }
}