namespace LendFoundry.Syndication.Equifax.Proxy.CreditReportResponse
{
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.6.1055.0")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute()]
    public partial class ElectronicIDCompareType {
    
        private CodeType regulatedIdentifierField;
    
        private CodeType hitNoHitIndicatorField;
    
        private ReasonCodeType[] reasonCodesField;
    
        private SimpleAddressType currentAddressField;
    
        private SimpleAddressType formerAddressField;
    
        private SimpleAddressType otherAddressField;
    
        private CodeType fraudIndicatorField;
    
        private string fraudlevelField;
    
        private short fraudIndexScoreField;
    
        private bool fraudIndexScoreFieldSpecified;
    
        private CodeType matchAssessmentField;
    
        private short overallScoreField;
    
        private bool overallScoreFieldSpecified;
    
        private string assessmentPassedField;
    
        private string addressStandardizationField;
    
        private string consumerIDField;
    
        private string aggregatorClientIDField;
    
        private CodeType transactionStatusFieldField;
    
        private string transactionKeyField;
    
        /// <remarks/>
        public CodeType RegulatedIdentifier {
            get {
                return this.regulatedIdentifierField;
            }
            set {
                this.regulatedIdentifierField = value;
            }
        }
    
        /// <remarks/>
        public CodeType HitNoHitIndicator {
            get {
                return this.hitNoHitIndicatorField;
            }
            set {
                this.hitNoHitIndicatorField = value;
            }
        }
    
        /// <remarks/>
        [System.Xml.Serialization.XmlArrayAttribute()]
        [System.Xml.Serialization.XmlArrayItemAttribute("ReasonCode",IsNullable=false)]
        public ReasonCodeType[] ReasonCodes {
            get {
                return this.reasonCodesField;
            }
            set {
                this.reasonCodesField = value;
            }
        }
    
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute()]
        public SimpleAddressType CurrentAddress {
            get {
                return this.currentAddressField;
            }
            set {
                this.currentAddressField = value;
            }
        }
    
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute()]
        public SimpleAddressType FormerAddress {
            get {
                return this.formerAddressField;
            }
            set {
                this.formerAddressField = value;
            }
        }
    
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute()]
        public SimpleAddressType OtherAddress {
            get {
                return this.otherAddressField;
            }
            set {
                this.otherAddressField = value;
            }
        }
    
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute()]
        public CodeType FraudIndicator {
            get {
                return this.fraudIndicatorField;
            }
            set {
                this.fraudIndicatorField = value;
            }
        }
    
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute()]
        public string Fraudlevel {
            get {
                return this.fraudlevelField;
            }
            set {
                this.fraudlevelField = value;
            }
        }
    
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute()]
        public short FraudIndexScore {
            get {
                return this.fraudIndexScoreField;
            }
            set {
                this.fraudIndexScoreField = value;
            }
        }
    
        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool FraudIndexScoreSpecified {
            get {
                return this.fraudIndexScoreFieldSpecified;
            }
            set {
                this.fraudIndexScoreFieldSpecified = value;
            }
        }
    
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute()]
        public CodeType MatchAssessment {
            get {
                return this.matchAssessmentField;
            }
            set {
                this.matchAssessmentField = value;
            }
        }
    
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute()]
        public short OverallScore {
            get {
                return this.overallScoreField;
            }
            set {
                this.overallScoreField = value;
            }
        }
    
        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool OverallScoreSpecified {
            get {
                return this.overallScoreFieldSpecified;
            }
            set {
                this.overallScoreFieldSpecified = value;
            }
        }
    
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute()]
        public string AssessmentPassed {
            get {
                return this.assessmentPassedField;
            }
            set {
                this.assessmentPassedField = value;
            }
        }
    
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute()]
        public string AddressStandardization {
            get {
                return this.addressStandardizationField;
            }
            set {
                this.addressStandardizationField = value;
            }
        }
    
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute()]
        public string ConsumerID {
            get {
                return this.consumerIDField;
            }
            set {
                this.consumerIDField = value;
            }
        }
    
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute()]
        public string AggregatorClientID {
            get {
                return this.aggregatorClientIDField;
            }
            set {
                this.aggregatorClientIDField = value;
            }
        }
    
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute()]
        public CodeType TransactionStatusField {
            get {
                return this.transactionStatusFieldField;
            }
            set {
                this.transactionStatusFieldField = value;
            }
        }
    
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute()]
        public string TransactionKey {
            get {
                return this.transactionKeyField;
            }
            set {
                this.transactionKeyField = value;
            }
        }
    }
}