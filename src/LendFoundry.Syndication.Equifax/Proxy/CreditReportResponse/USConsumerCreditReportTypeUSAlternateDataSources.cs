namespace LendFoundry.Syndication.Equifax.Proxy.CreditReportResponse
{
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.6.1055.0")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType=true)]
    public partial class USConsumerCreditReportTypeUSAlternateDataSources {
    
        private AlternateDataSourceErrorType[] alternateDataSourceErrorsField;
    
        private TheWorkNumberSelectType[] theWorkNumberSelectsField;
    
        private ElectronicIDCompareType[] electronicIDComparesField;
    
        private PropertyDataAndAnalyticType[] propertyDataAndAnalyticsField;
    
        private MilitaryLendingType[] militaryLendingActDataSegmentsField;
    
        private InterConnectDecisioningType[] interConnectDecisioningsField;
    
        private IXIServicesType[] iXIServicesDatasField;
    
        private IDRevealDataType[] iDRevealDatasField;
    
        private FirstSearchDataType[] firstSearchDatasField;
    
        /// <remarks/>
        [System.Xml.Serialization.XmlArrayAttribute()]
        [System.Xml.Serialization.XmlArrayItemAttribute("AlternateDataSourceError", IsNullable=false)]
        public AlternateDataSourceErrorType[] AlternateDataSourceErrors {
            get {
                return this.alternateDataSourceErrorsField;
            }
            set {
                this.alternateDataSourceErrorsField = value;
            }
        }
    
        /// <remarks/>
        [System.Xml.Serialization.XmlArrayAttribute()]
        [System.Xml.Serialization.XmlArrayItemAttribute("TheWorkNumberSelect", IsNullable=false)]
        public TheWorkNumberSelectType[] TheWorkNumberSelects {
            get {
                return this.theWorkNumberSelectsField;
            }
            set {
                this.theWorkNumberSelectsField = value;
            }
        }
    
        /// <remarks/>
        [System.Xml.Serialization.XmlArrayAttribute()]
        [System.Xml.Serialization.XmlArrayItemAttribute("ElectronicIDCompare", IsNullable=false)]
        public ElectronicIDCompareType[] ElectronicIDCompares {
            get {
                return this.electronicIDComparesField;
            }
            set {
                this.electronicIDComparesField = value;
            }
        }
    
        /// <remarks/>
        [System.Xml.Serialization.XmlArrayAttribute()]
        [System.Xml.Serialization.XmlArrayItemAttribute("PropertyDataAndAnalytic", IsNullable=false)]
        public PropertyDataAndAnalyticType[] PropertyDataAndAnalytics {
            get {
                return this.propertyDataAndAnalyticsField;
            }
            set {
                this.propertyDataAndAnalyticsField = value;
            }
        }
    
        /// <remarks/>
        [System.Xml.Serialization.XmlArrayAttribute()]
        [System.Xml.Serialization.XmlArrayItemAttribute("MilitaryLendingActData", IsNullable=false)]
        public MilitaryLendingType[] MilitaryLendingActDataSegments {
            get {
                return this.militaryLendingActDataSegmentsField;
            }
            set {
                this.militaryLendingActDataSegmentsField = value;
            }
        }
    
        /// <remarks/>
        [System.Xml.Serialization.XmlArrayAttribute()]
        [System.Xml.Serialization.XmlArrayItemAttribute("InterConnectDecisioning",IsNullable=false)]
        public InterConnectDecisioningType[] InterConnectDecisionings {
            get {
                return this.interConnectDecisioningsField;
            }
            set {
                this.interConnectDecisioningsField = value;
            }
        }
    
        /// <remarks/>
        [System.Xml.Serialization.XmlArrayAttribute()]
        [System.Xml.Serialization.XmlArrayItemAttribute("IXIServicesData", IsNullable=false)]
        public IXIServicesType[] IXIServicesDatas {
            get {
                return this.iXIServicesDatasField;
            }
            set {
                this.iXIServicesDatasField = value;
            }
        }
    
        /// <remarks/>
        [System.Xml.Serialization.XmlArrayAttribute()]
        [System.Xml.Serialization.XmlArrayItemAttribute("IDRevealData", IsNullable=false)]
        public IDRevealDataType[] IDRevealDatas {
            get {
                return this.iDRevealDatasField;
            }
            set {
                this.iDRevealDatasField = value;
            }
        }
    
        /// <remarks/>
        [System.Xml.Serialization.XmlArrayAttribute()]
        [System.Xml.Serialization.XmlArrayItemAttribute("FirstSearchData", IsNullable=false)]
        public FirstSearchDataType[] FirstSearchDatas {
            get {
                return this.firstSearchDatasField;
            }
            set {
                this.firstSearchDatasField = value;
            }
        }
    }
}