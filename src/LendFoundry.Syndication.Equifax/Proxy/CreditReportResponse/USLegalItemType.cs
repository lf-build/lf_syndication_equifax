namespace LendFoundry.Syndication.Equifax.Proxy.CreditReportResponse
{
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.6.1055.0")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute()]
    public partial class USLegalItemType {
    
        private CodeType expandedDataReturnedField;
    
        private DateType dateFiledField;
    
        private CustomerIdType courtIdField;
    
        private string caseNumberField;
    
        private MoneyType amountField;
    
        private DateType dateSatisfiedField;
    
        private CodeType statusField;
    
        private DateType verificationDateField;
    
        private string defendantField;
    
        private string plaintiffField;
    
        private CodeType[] narrativesField;
    
        private DateType expandedDateFiledField;
    
        private MoneyType expandedAmountField;
    
        private DateType expandedDateSatisfiedField;
    
        private DateType expandedDateVerifiedField;
    
        private string codeField;
    
        private string descriptionField;
    
        /// <remarks/>
        public CodeType ExpandedDataReturned {
            get {
                return this.expandedDataReturnedField;
            }
            set {
                this.expandedDataReturnedField = value;
            }
        }
    
        /// <remarks/>
        public DateType DateFiled {
            get {
                return this.dateFiledField;
            }
            set {
                this.dateFiledField = value;
            }
        }
    
        /// <remarks/>
        public CustomerIdType CourtId {
            get {
                return this.courtIdField;
            }
            set {
                this.courtIdField = value;
            }
        }
    
        /// <remarks/>
        public string CaseNumber {
            get {
                return this.caseNumberField;
            }
            set {
                this.caseNumberField = value;
            }
        }
    
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(IsNullable=true)]
        public MoneyType Amount {
            get {
                return this.amountField;
            }
            set {
                this.amountField = value;
            }
        }
    
        /// <remarks/>
        public DateType DateSatisfied {
            get {
                return this.dateSatisfiedField;
            }
            set {
                this.dateSatisfiedField = value;
            }
        }
    
        /// <remarks/>
        public CodeType Status {
            get {
                return this.statusField;
            }
            set {
                this.statusField = value;
            }
        }
    
        /// <remarks/>
        public DateType VerificationDate {
            get {
                return this.verificationDateField;
            }
            set {
                this.verificationDateField = value;
            }
        }
    
        /// <remarks/>
        public string Defendant {
            get {
                return this.defendantField;
            }
            set {
                this.defendantField = value;
            }
        }
    
        /// <remarks/>
        public string Plaintiff {
            get {
                return this.plaintiffField;
            }
            set {
                this.plaintiffField = value;
            }
        }
    
        /// <remarks/>
        [System.Xml.Serialization.XmlArrayItemAttribute("Narrative", IsNullable=false)]
        public CodeType[] Narratives {
            get {
                return this.narrativesField;
            }
            set {
                this.narrativesField = value;
            }
        }
    
        /// <remarks/>
        public DateType ExpandedDateFiled {
            get {
                return this.expandedDateFiledField;
            }
            set {
                this.expandedDateFiledField = value;
            }
        }
    
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(IsNullable=true)]
        public MoneyType ExpandedAmount {
            get {
                return this.expandedAmountField;
            }
            set {
                this.expandedAmountField = value;
            }
        }
    
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute()]
        public DateType ExpandedDateSatisfied {
            get {
                return this.expandedDateSatisfiedField;
            }
            set {
                this.expandedDateSatisfiedField = value;
            }
        }
    
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute()]
        public DateType ExpandedDateVerified {
            get {
                return this.expandedDateVerifiedField;
            }
            set {
                this.expandedDateVerifiedField = value;
            }
        }
    
        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string code {
            get {
                return this.codeField;
            }
            set {
                this.codeField = value;
            }
        }
    
        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string description {
            get {
                return this.descriptionField;
            }
            set {
                this.descriptionField = value;
            }
        }
    }
}