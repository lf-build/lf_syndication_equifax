namespace LendFoundry.Syndication.Equifax.Proxy.CreditReportResponse
{
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.6.1055.0")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute()]
    public partial class USDecisionPowerExpressDataType {
    
        private string consumerDisclosureIndicatorField;
    
        private string reportTextMessageField;
    
        private string sSNVarianceIndicatorField;
    
        private USDecisionPowerExpressDataTypeDPExpressProduct[] dPExpressProductsField;
    
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute()]
        public string ConsumerDisclosureIndicator {
            get {
                return this.consumerDisclosureIndicatorField;
            }
            set {
                this.consumerDisclosureIndicatorField = value;
            }
        }
    
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute()]
        public string ReportTextMessage {
            get {
                return this.reportTextMessageField;
            }
            set {
                this.reportTextMessageField = value;
            }
        }
    
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute()]
        public string SSNVarianceIndicator {
            get {
                return this.sSNVarianceIndicatorField;
            }
            set {
                this.sSNVarianceIndicatorField = value;
            }
        }
    
        /// <remarks/>
        [System.Xml.Serialization.XmlArrayAttribute()]
        [System.Xml.Serialization.XmlArrayItemAttribute("DPExpressProduct", IsNullable=false)]
        public USDecisionPowerExpressDataTypeDPExpressProduct[] DPExpressProducts {
            get {
                return this.dPExpressProductsField;
            }
            set {
                this.dPExpressProductsField = value;
            }
        }
    }
}