namespace LendFoundry.Syndication.Equifax.Proxy.CreditReportResponse
{
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.6.1055.0")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute()]
    public partial class USMasterHeaderType {
    
        private string customerReferenceNumberField;
    
        private string customerNumberField;
    
        private ECOAInquiryType eCOAInquiryTypeField;
    
        private DateType dateOfRequestField;
    
        private string equifaxReferenceNumberField;
    
        /// <remarks/>
        public string CustomerReferenceNumber {
            get {
                return this.customerReferenceNumberField;
            }
            set {
                this.customerReferenceNumberField = value;
            }
        }
    
        /// <remarks/>
        public string CustomerNumber {
            get {
                return this.customerNumberField;
            }
            set {
                this.customerNumberField = value;
            }
        }
    
        /// <remarks/>
        public ECOAInquiryType ECOAInquiryType {
            get {
                return this.eCOAInquiryTypeField;
            }
            set {
                this.eCOAInquiryTypeField = value;
            }
        }
    
        /// <remarks/>
        public DateType DateOfRequest {
            get {
                return this.dateOfRequestField;
            }
            set {
                this.dateOfRequestField = value;
            }
        }
    
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute()]
        public string EquifaxReferenceNumber {
            get {
                return this.equifaxReferenceNumberField;
            }
            set {
                this.equifaxReferenceNumberField = value;
            }
        }
    }
}