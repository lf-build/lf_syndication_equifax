namespace LendFoundry.Syndication.Equifax.Proxy.CreditReportResponse
{
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.6.1055.0")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute()]
    public partial class WealthCompleteType {
    
        private string productIDField;
    
        private string postalCodeField;
    
        private string postalCodePlus4Field;
    
        private short ageField;
    
        private bool ageFieldSpecified;
    
        private string ageBandField;
    
        private MoneyType incomeofConsumerField;
    
        private MoneyType annuitiesField;
    
        private MoneyType stocksField;
    
        private MoneyType bondsField;
    
        private MoneyType depositsField;
    
        private MoneyType mutualFundsField;
    
        private MoneyType otherAssetsField;
    
        private MoneyType totalAssetsField;
    
        private MoneyType certificateofDepositField;
    
        private MoneyType interestCheckingField;
    
        private MoneyType moneyMarketDepositAcctField;
    
        private MoneyType nonInterestCheckingField;
    
        private MoneyType otherCheckingField;
    
        private MoneyType savingsField;
    
        private string modelVersionField;
    
        private string dataVintageField;
    
        private string dispositionCodeField;
    
        /// <remarks/>
        public string ProductID {
            get {
                return this.productIDField;
            }
            set {
                this.productIDField = value;
            }
        }
    
        /// <remarks/>
        public string PostalCode {
            get {
                return this.postalCodeField;
            }
            set {
                this.postalCodeField = value;
            }
        }
    
        /// <remarks/>
        public string PostalCodePlus4 {
            get {
                return this.postalCodePlus4Field;
            }
            set {
                this.postalCodePlus4Field = value;
            }
        }
    
        /// <remarks/>
        public short Age {
            get {
                return this.ageField;
            }
            set {
                this.ageField = value;
            }
        }
    
        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool AgeSpecified {
            get {
                return this.ageFieldSpecified;
            }
            set {
                this.ageFieldSpecified = value;
            }
        }
    
        /// <remarks/>
        public string AgeBand {
            get {
                return this.ageBandField;
            }
            set {
                this.ageBandField = value;
            }
        }
    
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(IsNullable=true)]
        public MoneyType IncomeofConsumer {
            get {
                return this.incomeofConsumerField;
            }
            set {
                this.incomeofConsumerField = value;
            }
        }
    
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute()]
        public MoneyType Annuities {
            get {
                return this.annuitiesField;
            }
            set {
                this.annuitiesField = value;
            }
        }
    
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute()]
        public MoneyType Stocks {
            get {
                return this.stocksField;
            }
            set {
                this.stocksField = value;
            }
        }
    
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute()]
        public MoneyType Bonds {
            get {
                return this.bondsField;
            }
            set {
                this.bondsField = value;
            }
        }
    
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute()]
        public MoneyType Deposits {
            get {
                return this.depositsField;
            }
            set {
                this.depositsField = value;
            }
        }
    
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute()]
        public MoneyType MutualFunds {
            get {
                return this.mutualFundsField;
            }
            set {
                this.mutualFundsField = value;
            }
        }
    
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute()]
        public MoneyType OtherAssets {
            get {
                return this.otherAssetsField;
            }
            set {
                this.otherAssetsField = value;
            }
        }
    
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute()]
        public MoneyType TotalAssets {
            get {
                return this.totalAssetsField;
            }
            set {
                this.totalAssetsField = value;
            }
        }
    
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute()]
        public MoneyType CertificateofDeposit {
            get {
                return this.certificateofDepositField;
            }
            set {
                this.certificateofDepositField = value;
            }
        }
    
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute()]
        public MoneyType InterestChecking {
            get {
                return this.interestCheckingField;
            }
            set {
                this.interestCheckingField = value;
            }
        }
    
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute()]
        public MoneyType MoneyMarketDepositAcct {
            get {
                return this.moneyMarketDepositAcctField;
            }
            set {
                this.moneyMarketDepositAcctField = value;
            }
        }
    
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute()]
        public MoneyType NonInterestChecking {
            get {
                return this.nonInterestCheckingField;
            }
            set {
                this.nonInterestCheckingField = value;
            }
        }
    
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute()]
        public MoneyType OtherChecking {
            get {
                return this.otherCheckingField;
            }
            set {
                this.otherCheckingField = value;
            }
        }
    
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute()]
        public MoneyType Savings {
            get {
                return this.savingsField;
            }
            set {
                this.savingsField = value;
            }
        }
    
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute()]
        public string ModelVersion {
            get {
                return this.modelVersionField;
            }
            set {
                this.modelVersionField = value;
            }
        }
    
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute()]
        public string DataVintage {
            get {
                return this.dataVintageField;
            }
            set {
                this.dataVintageField = value;
            }
        }
    
        /// <remarks/>
        public string DispositionCode {
            get {
                return this.dispositionCodeField;
            }
            set {
                this.dispositionCodeField = value;
            }
        }
    }
}