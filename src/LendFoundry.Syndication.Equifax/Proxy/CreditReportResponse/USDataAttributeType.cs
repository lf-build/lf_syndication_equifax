namespace LendFoundry.Syndication.Equifax.Proxy.CreditReportResponse
{
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.6.1055.0")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute()]
    public partial class USDataAttributeType {
    
        private IdAndValueType[] dataAttributeField;
    
        private string modelNumberField;
    
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute("DataAttribute")]
        public IdAndValueType[] DataAttribute {
            get {
                return this.dataAttributeField;
            }
            set {
                this.dataAttributeField = value;
            }
        }
    
        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string modelNumber {
            get {
                return this.modelNumberField;
            }
            set {
                this.modelNumberField = value;
            }
        }
    }
}