namespace LendFoundry.Syndication.Equifax.Proxy.CreditReportResponse
{
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.6.1055.0")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute()]
    public partial class USInquiryType {
    
        private DateType dateOfInquiryField;
    
        private CodeType inquiryAbbreviationField;
    
        private string endUserTextField;
    
        private CustomerIdType customerIdField;
    
        private CodeType inquiryIntentField;
    
        private CodeType expandedAccountTypeField;
    
        /// <remarks/>
        public DateType DateOfInquiry {
            get {
                return this.dateOfInquiryField;
            }
            set {
                this.dateOfInquiryField = value;
            }
        }
    
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute()]
        public CodeType InquiryAbbreviation {
            get {
                return this.inquiryAbbreviationField;
            }
            set {
                this.inquiryAbbreviationField = value;
            }
        }
    
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute()]
        public string EndUserText {
            get {
                return this.endUserTextField;
            }
            set {
                this.endUserTextField = value;
            }
        }
    
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute()]
        public CustomerIdType CustomerId {
            get {
                return this.customerIdField;
            }
            set {
                this.customerIdField = value;
            }
        }
    
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute()]
        public CodeType InquiryIntent {
            get {
                return this.inquiryIntentField;
            }
            set {
                this.inquiryIntentField = value;
            }
        }
    
        /// <remarks/>
        public CodeType ExpandedAccountType {
            get {
                return this.expandedAccountTypeField;
            }
            set {
                this.expandedAccountTypeField = value;
            }
        }
    }
}