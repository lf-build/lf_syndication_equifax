namespace LendFoundry.Syndication.Equifax.Proxy.CreditReportResponse
{
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.6.1055.0")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute()]
    [System.Xml.Serialization.XmlRootAttribute("DataTags", IsNullable=false)]
    public partial class DataTagsType {
    
        private DataTagAndDataFieldType[] dataTagField;
    
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute("DataTag")]
        public DataTagAndDataFieldType[] DataTag {
            get {
                return this.dataTagField;
            }
            set {
                this.dataTagField = value;
            }
        }
    }
}