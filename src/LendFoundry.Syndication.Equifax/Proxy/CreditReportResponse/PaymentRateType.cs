namespace LendFoundry.Syndication.Equifax.Proxy.CreditReportResponse
{
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.6.1055.0")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute()]
    public partial class PaymentRateType {
    
        private CodeType paymentRateField;
    
        private DateType paymentDateField;
    
        private string rateNumberField;
    
        /// <remarks/>
        public CodeType PaymentRate {
            get {
                return this.paymentRateField;
            }
            set {
                this.paymentRateField = value;
            }
        }
    
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute()]
        public DateType PaymentDate {
            get {
                return this.paymentDateField;
            }
            set {
                this.paymentDateField = value;
            }
        }
    
        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string rateNumber {
            get {
                return this.rateNumberField;
            }
            set {
                this.rateNumberField = value;
            }
        }
    }
}