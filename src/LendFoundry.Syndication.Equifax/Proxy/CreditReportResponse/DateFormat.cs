namespace LendFoundry.Syndication.Equifax.Proxy.CreditReportResponse
{
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.6.1055.0")]
    [System.SerializableAttribute()]
    [System.Xml.Serialization.XmlTypeAttribute()]
    public enum DateFormat {
    
        /// <remarks/>
        [System.Xml.Serialization.XmlEnumAttribute("MM/DD/CCYY")]
        MMDDCCYY,
    
        /// <remarks/>
        [System.Xml.Serialization.XmlEnumAttribute("MMDDCCYY")]
        MMDDCCYY1,
    
        /// <remarks/>
        MMCCYY,
    
        /// <remarks/>
        [System.Xml.Serialization.XmlEnumAttribute("MM/CCYY")]
        MMCCYY1,
    
        /// <remarks/>
        CCYYMMDD,
    
        /// <remarks/>
        [System.Xml.Serialization.XmlEnumAttribute("CCYY-MM")]
        CCYYMM,
    
        /// <remarks/>
        [System.Xml.Serialization.XmlEnumAttribute("CCYY-MM-DD")]
        CCYYMMDD1,
    
        /// <remarks/>
        CCYY,
    
        /// <remarks/>
        [System.Xml.Serialization.XmlEnumAttribute("MM-DD-CCYY")]
        MMDDCCYY2,
    }
}