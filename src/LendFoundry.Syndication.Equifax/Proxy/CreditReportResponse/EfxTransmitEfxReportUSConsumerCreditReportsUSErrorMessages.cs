namespace LendFoundry.Syndication.Equifax.Proxy.CreditReportResponse
{
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.6.1055.0")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType=true)]
    public partial class EfxTransmitEfxReportUSConsumerCreditReportsUSErrorMessages {
    
        private USErrorMessageType uSErrorMessageField;
    
        /// <remarks/>
        public USErrorMessageType USErrorMessage {
            get {
                return this.uSErrorMessageField;
            }
            set {
                this.uSErrorMessageField = value;
            }
        }
    }
}