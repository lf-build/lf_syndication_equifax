namespace LendFoundry.Syndication.Equifax.Proxy.CreditReportResponse
{
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.6.1055.0")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType=true)]
    public partial class USTradeTypeDimensionsDatas {
    
        private DateType dimensionsDataStartDateField;
    
        private string noMonthsDimensionsDataRequestedField;
    
        private USTradeTypeDimensionsDatasDimensionsData[] dimensionsDataField;
    
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute()]
        public DateType DimensionsDataStartDate {
            get {
                return this.dimensionsDataStartDateField;
            }
            set {
                this.dimensionsDataStartDateField = value;
            }
        }
    
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute()]
        public string NoMonthsDimensionsDataRequested {
            get {
                return this.noMonthsDimensionsDataRequestedField;
            }
            set {
                this.noMonthsDimensionsDataRequestedField = value;
            }
        }
    
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute("DimensionsData")]
        public USTradeTypeDimensionsDatasDimensionsData[] DimensionsData {
            get {
                return this.dimensionsDataField;
            }
            set {
                this.dimensionsDataField = value;
            }
        }
    }
}