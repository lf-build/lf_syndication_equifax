namespace LendFoundry.Syndication.Equifax.Proxy.CreditReportResponse
{
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.6.1055.0")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType=true)]
    [System.Xml.Serialization.XmlRootAttribute( IsNullable=false)]
    public partial class EfxTransmit {
    
        private EfxTransmitEfxReport[] efxReportField;
    
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute("EfxReport")]
        public EfxTransmitEfxReport[] EfxReport {
            get {
                return this.efxReportField;
            }
            set {
                this.efxReportField = value;
            }
        }
    }
}