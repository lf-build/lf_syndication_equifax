namespace LendFoundry.Syndication.Equifax.Proxy.CreditReportResponse
{
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.6.1055.0")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute()]
    public partial class InterConnectDecisioningType {
    
        private CodeType regulatedIdentifierField;
    
        private CodeType hitNoHitIndicatorField;
    
        private string decisionTypeField;
    
        private CodeType decisionDesignatorField;
    
        private string descriptionField;
    
        private string decisionCodeField;
    
        private ProductType[] productsField;
    
        /// <remarks/>
        public CodeType RegulatedIdentifier {
            get {
                return this.regulatedIdentifierField;
            }
            set {
                this.regulatedIdentifierField = value;
            }
        }
    
        /// <remarks/>
        public CodeType HitNoHitIndicator {
            get {
                return this.hitNoHitIndicatorField;
            }
            set {
                this.hitNoHitIndicatorField = value;
            }
        }
    
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute()]
        public string DecisionType {
            get {
                return this.decisionTypeField;
            }
            set {
                this.decisionTypeField = value;
            }
        }
    
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute()]
        public CodeType DecisionDesignator {
            get {
                return this.decisionDesignatorField;
            }
            set {
                this.decisionDesignatorField = value;
            }
        }
    
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute()]
        public string Description {
            get {
                return this.descriptionField;
            }
            set {
                this.descriptionField = value;
            }
        }
    
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute()]
        public string DecisionCode {
            get {
                return this.decisionCodeField;
            }
            set {
                this.decisionCodeField = value;
            }
        }
    
        /// <remarks/>
        [System.Xml.Serialization.XmlArrayAttribute()]
        [System.Xml.Serialization.XmlArrayItemAttribute("Product", IsNullable=false)]
        public ProductType[] Products {
            get {
                return this.productsField;
            }
            set {
                this.productsField = value;
            }
        }
    }
}