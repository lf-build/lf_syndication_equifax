namespace LendFoundry.Syndication.Equifax.Proxy.CreditReportResponse
{
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.6.1055.0")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute()]
    public partial class CodedErrorType : CodeType {
    
        private short errorNumberField;
    
        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public short errorNumber {
            get {
                return this.errorNumberField;
            }
            set {
                this.errorNumberField = value;
            }
        }
    }
}