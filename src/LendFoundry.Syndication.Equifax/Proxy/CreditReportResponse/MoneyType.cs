namespace LendFoundry.Syndication.Equifax.Proxy.CreditReportResponse
{
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.6.1055.0")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute()]
    [System.Xml.Serialization.XmlRootAttribute("Amount", IsNullable=true)]
    public partial class MoneyType {
    
        private CurrencyType currencyField;
    
        private long valueField;
    
        public MoneyType() {
            this.currencyField = CurrencyType.USD;
        }
    
        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        [System.ComponentModel.DefaultValueAttribute(CurrencyType.USD)]
        public CurrencyType currency {
            get {
                return this.currencyField;
            }
            set {
                this.currencyField = value;
            }
        }
    
        /// <remarks/>
        [System.Xml.Serialization.XmlTextAttribute()]
        public long Value {
            get {
                return this.valueField;
            }
            set {
                this.valueField = value;
            }
        }
    }
}