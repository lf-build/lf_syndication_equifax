namespace LendFoundry.Syndication.Equifax.Proxy.CreditReportResponse
{
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.6.1055.0")]
    [System.SerializableAttribute()]
    [System.Xml.Serialization.XmlTypeAttribute()]
    [System.Xml.Serialization.XmlRootAttribute("RegulatedNonRegulatedFlag", IsNullable=false)]
    public enum BlankorNFlagType {
    
        /// <remarks/>
        [System.Xml.Serialization.XmlEnumAttribute(" ")]
        Item,
    
        /// <remarks/>
        N,
    }
}