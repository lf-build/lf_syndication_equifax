namespace LendFoundry.Syndication.Equifax.Proxy.CreditReportResponse
{
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.6.1055.0")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType=true)]
    public partial class USHeaderTypeSubject {
    
        private SubjectNameType subjectNameField;
    
        private USHeaderTypeSubjectSubjectId subjectIdField;
    
        /// <remarks/>
        public SubjectNameType SubjectName {
            get {
                return this.subjectNameField;
            }
            set {
                this.subjectNameField = value;
            }
        }
    
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute()]
        public USHeaderTypeSubjectSubjectId SubjectId {
            get {
                return this.subjectIdField;
            }
            set {
                this.subjectIdField = value;
            }
        }
    }
}