namespace LendFoundry.Syndication.Equifax.Proxy.CreditReportResponse
{
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.6.1055.0")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute()]
    [System.Xml.Serialization.XmlRootAttribute("Disposition", IsNullable=false)]
    public partial class CodeDateType : CodeType {
    
        private DateType dateField;
    
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute()]
        public DateType Date {
            get {
                return this.dateField;
            }
            set {
                this.dateField = value;
            }
        }
    }
}