namespace LendFoundry.Syndication.Equifax.Proxy.CreditReportResponse
{
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.6.1055.0")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute()]
    public partial class USTelecheckType {
    
        private string customerNumberField;
    
        private string teleCheckSubscriberNumberField;
    
        private CodeType responseCodeField;
    
        private string approvalCodeField;
    
        private string consumerOrCompanyNameField;
    
        private string issueLocationAndYearField;
    
        private ScoreReasonType[] historyCodesField;
    
        private string confidenceScoreField;
    
        private ScoreReasonType[] reasonCodesField;
    
        private object traceIdField;
    
        /// <remarks/>
        public string CustomerNumber {
            get {
                return this.customerNumberField;
            }
            set {
                this.customerNumberField = value;
            }
        }
    
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute()]
        public string TeleCheckSubscriberNumber {
            get {
                return this.teleCheckSubscriberNumberField;
            }
            set {
                this.teleCheckSubscriberNumberField = value;
            }
        }
    
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute()]
        public CodeType ResponseCode {
            get {
                return this.responseCodeField;
            }
            set {
                this.responseCodeField = value;
            }
        }
    
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute()]
        public string ApprovalCode {
            get {
                return this.approvalCodeField;
            }
            set {
                this.approvalCodeField = value;
            }
        }
    
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute()]
        public string ConsumerOrCompanyName {
            get {
                return this.consumerOrCompanyNameField;
            }
            set {
                this.consumerOrCompanyNameField = value;
            }
        }
    
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute()]
        public string IssueLocationAndYear {
            get {
                return this.issueLocationAndYearField;
            }
            set {
                this.issueLocationAndYearField = value;
            }
        }
    
        /// <remarks/>
        [System.Xml.Serialization.XmlArrayAttribute()]
        [System.Xml.Serialization.XmlArrayItemAttribute("HistoryCode", IsNullable=false)]
        public ScoreReasonType[] HistoryCodes {
            get {
                return this.historyCodesField;
            }
            set {
                this.historyCodesField = value;
            }
        }
    
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute()]
        public string ConfidenceScore {
            get {
                return this.confidenceScoreField;
            }
            set {
                this.confidenceScoreField = value;
            }
        }
    
        /// <remarks/>
        [System.Xml.Serialization.XmlArrayAttribute()]
        [System.Xml.Serialization.XmlArrayItemAttribute("ReasonCode", IsNullable=false)]
        public ScoreReasonType[] ReasonCodes {
            get {
                return this.reasonCodesField;
            }
            set {
                this.reasonCodesField = value;
            }
        }
    
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute()]
        public object TraceId {
            get {
                return this.traceIdField;
            }
            set {
                this.traceIdField = value;
            }
        }
    }
}