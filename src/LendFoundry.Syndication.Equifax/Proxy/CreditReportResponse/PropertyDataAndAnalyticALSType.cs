namespace LendFoundry.Syndication.Equifax.Proxy.CreditReportResponse
{
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.6.1055.0")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute()]
    public partial class PropertyDataAndAnalyticALSType {
    
        private CodeType hitNoHitIndicatorField;
    
        private string trueFalseReturnsField;
    
        private string transactionIDField;
    
        private string medianDaysonMarketField;
    
        private string emailAddressofListingBrokerField;
    
        private string nameofListingBrokerField;
    
        private string phoneNumberofListingBrokerField;
    
        private string daysonMarketField;
    
        private DateType originalListingDateonPropertyField;
    
        private MoneyType listingPriceonthePropertyField;
    
        private string listingIDAssociatedwithPropertyField;
    
        private string listingStatusField;
    
        private string uniqueIDfortheBoardPostedField;
    
        private string averageDaysonMarketField;
    
        /// <remarks/>
        public CodeType HitNoHitIndicator {
            get {
                return this.hitNoHitIndicatorField;
            }
            set {
                this.hitNoHitIndicatorField = value;
            }
        }
    
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute()]
        public string TrueFalseReturns {
            get {
                return this.trueFalseReturnsField;
            }
            set {
                this.trueFalseReturnsField = value;
            }
        }
    
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute()]
        public string TransactionID {
            get {
                return this.transactionIDField;
            }
            set {
                this.transactionIDField = value;
            }
        }
    
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute()]
        public string MedianDaysonMarket {
            get {
                return this.medianDaysonMarketField;
            }
            set {
                this.medianDaysonMarketField = value;
            }
        }
    
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute()]
        public string EmailAddressofListingBroker {
            get {
                return this.emailAddressofListingBrokerField;
            }
            set {
                this.emailAddressofListingBrokerField = value;
            }
        }
    
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute()]
        public string NameofListingBroker {
            get {
                return this.nameofListingBrokerField;
            }
            set {
                this.nameofListingBrokerField = value;
            }
        }
    
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute()]
        public string PhoneNumberofListingBroker {
            get {
                return this.phoneNumberofListingBrokerField;
            }
            set {
                this.phoneNumberofListingBrokerField = value;
            }
        }
    
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute()]
        public string DaysonMarket {
            get {
                return this.daysonMarketField;
            }
            set {
                this.daysonMarketField = value;
            }
        }
    
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute()]
        public DateType OriginalListingDateonProperty {
            get {
                return this.originalListingDateonPropertyField;
            }
            set {
                this.originalListingDateonPropertyField = value;
            }
        }
    
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute()]
        public MoneyType ListingPriceontheProperty {
            get {
                return this.listingPriceonthePropertyField;
            }
            set {
                this.listingPriceonthePropertyField = value;
            }
        }
    
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute()]
        public string ListingIDAssociatedwithProperty {
            get {
                return this.listingIDAssociatedwithPropertyField;
            }
            set {
                this.listingIDAssociatedwithPropertyField = value;
            }
        }
    
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute()]
        public string ListingStatus {
            get {
                return this.listingStatusField;
            }
            set {
                this.listingStatusField = value;
            }
        }
    
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute()]
        public string UniqueIDfortheBoardPosted {
            get {
                return this.uniqueIDfortheBoardPostedField;
            }
            set {
                this.uniqueIDfortheBoardPostedField = value;
            }
        }
    
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute()]
        public string AverageDaysonMarket {
            get {
                return this.averageDaysonMarketField;
            }
            set {
                this.averageDaysonMarketField = value;
            }
        }
    }
}