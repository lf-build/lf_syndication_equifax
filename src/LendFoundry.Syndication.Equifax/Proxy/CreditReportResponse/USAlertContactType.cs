namespace LendFoundry.Syndication.Equifax.Proxy.CreditReportResponse
{
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.6.1055.0")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute()]
    public partial class USAlertContactType {
    
        private CodeType alertTypeField;
    
        private DateType dateReportedField;
    
        private DateType dateEffectiveField;
    
        private string addressLine1Field;
    
        private string addressLine2Field;
    
        private CityType cityField;
    
        private string stateField;
    
        private string postalCodeField;
    
        private string countryCodeField;
    
        private USAlertContactTypePhoneNumber[] phoneNumbersField;
    
        private string freeformInformationField;
    
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute()]
        public CodeType AlertType {
            get {
                return this.alertTypeField;
            }
            set {
                this.alertTypeField = value;
            }
        }
    
        /// <remarks/>
        public DateType DateReported {
            get {
                return this.dateReportedField;
            }
            set {
                this.dateReportedField = value;
            }
        }
    
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute()]
        public DateType DateEffective {
            get {
                return this.dateEffectiveField;
            }
            set {
                this.dateEffectiveField = value;
            }
        }
    
        /// <remarks/>
        public string AddressLine1 {
            get {
                return this.addressLine1Field;
            }
            set {
                this.addressLine1Field = value;
            }
        }
    
        /// <remarks/>
        public string AddressLine2 {
            get {
                return this.addressLine2Field;
            }
            set {
                this.addressLine2Field = value;
            }
        }
    
        /// <remarks/>
        public CityType City {
            get {
                return this.cityField;
            }
            set {
                this.cityField = value;
            }
        }
    
        /// <remarks/>
        public string State {
            get {
                return this.stateField;
            }
            set {
                this.stateField = value;
            }
        }
    
        /// <remarks/>
        public string PostalCode {
            get {
                return this.postalCodeField;
            }
            set {
                this.postalCodeField = value;
            }
        }
    
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute()]
        public string CountryCode {
            get {
                return this.countryCodeField;
            }
            set {
                this.countryCodeField = value;
            }
        }
    
        /// <remarks/>
        [System.Xml.Serialization.XmlArrayAttribute()]
        [System.Xml.Serialization.XmlArrayItemAttribute("PhoneNumber", IsNullable=false)]
        public USAlertContactTypePhoneNumber[] PhoneNumbers {
            get {
                return this.phoneNumbersField;
            }
            set {
                this.phoneNumbersField = value;
            }
        }
    
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute()]
        public string FreeformInformation {
            get {
                return this.freeformInformationField;
            }
            set {
                this.freeformInformationField = value;
            }
        }
    }
}