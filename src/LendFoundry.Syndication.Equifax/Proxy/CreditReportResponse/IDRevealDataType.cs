namespace LendFoundry.Syndication.Equifax.Proxy.CreditReportResponse
{
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.6.1055.0")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute]
    public partial class IDRevealDataType {
    
        private CodeType regulatedIdentifierField;
    
        private CodeType hitNoHitIndicatorField;
    
        private string modelScoreField;
    
        private ReasonCodeType[] reasonCodesField;
    
        private CodeType rejectCodeField;
    
        private IDRevealDataTypeAttribute[] attributesField;
    
        /// <remarks/>
        public CodeType RegulatedIdentifier {
            get {
                return this.regulatedIdentifierField;
            }
            set {
                this.regulatedIdentifierField = value;
            }
        }
    
        /// <remarks/>
        public CodeType HitNoHitIndicator {
            get {
                return this.hitNoHitIndicatorField;
            }
            set {
                this.hitNoHitIndicatorField = value;
            }
        }
    
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute()]
        public string ModelScore {
            get {
                return this.modelScoreField;
            }
            set {
                this.modelScoreField = value;
            }
        }
    
        /// <remarks/>
        [System.Xml.Serialization.XmlArrayAttribute()]
        [System.Xml.Serialization.XmlArrayItemAttribute("ReasonCode", IsNullable=false)]
        public ReasonCodeType[] ReasonCodes {
            get {
                return this.reasonCodesField;
            }
            set {
                this.reasonCodesField = value;
            }
        }
    
        /// <remarks/>
        public CodeType RejectCode {
            get {
                return this.rejectCodeField;
            }
            set {
                this.rejectCodeField = value;
            }
        }
    
        /// <remarks/>
        [System.Xml.Serialization.XmlArrayAttribute()]
        [System.Xml.Serialization.XmlArrayItemAttribute("Attribute", IsNullable=false)]
        public IDRevealDataTypeAttribute[] Attributes {
            get {
                return this.attributesField;
            }
            set {
                this.attributesField = value;
            }
        }
    }
}