namespace LendFoundry.Syndication.Equifax.Proxy.CreditReportResponse
{
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.6.1055.0")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType=true)]
    public partial class EfxTransmitEfxReport {
    
        private EfxTransmitEfxReportUSConsumerCreditReports uSConsumerCreditReportsField;
    
        private string uSPrintImageField;
    
        private short requestNumberField;
    
        private string reportIdField;
    
        /// <remarks/>
        public EfxTransmitEfxReportUSConsumerCreditReports USConsumerCreditReports {
            get {
                return this.uSConsumerCreditReportsField;
            }
            set {
                this.uSConsumerCreditReportsField = value;
            }
        }
    
        /// <remarks/>
        public string USPrintImage {
            get {
                return this.uSPrintImageField;
            }
            set {
                this.uSPrintImageField = value;
            }
        }
    
        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public short requestNumber {
            get {
                return this.requestNumberField;
            }
            set {
                this.requestNumberField = value;
            }
        }
    
        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string reportId {
            get {
                return this.reportIdField;
            }
            set {
                this.reportIdField = value;
            }
        }
    }
}