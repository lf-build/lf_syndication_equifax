namespace LendFoundry.Syndication.Equifax.Proxy.CreditReportResponse
{
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.6.1055.0")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType=true)]
    public partial class USDecisionPowerExpressDataTypeDPExpressProduct {
    
        private string descriptionField;
    
        private string approvalIndicatorField;
    
        private string limitField;
    
        private string miscellaneousField;
    
        private string numberField;
    
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute()]
        public string Description {
            get {
                return this.descriptionField;
            }
            set {
                this.descriptionField = value;
            }
        }
    
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute()]
        public string ApprovalIndicator {
            get {
                return this.approvalIndicatorField;
            }
            set {
                this.approvalIndicatorField = value;
            }
        }
    
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute()]
        public string Limit {
            get {
                return this.limitField;
            }
            set {
                this.limitField = value;
            }
        }
    
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute()]
        public string Miscellaneous {
            get {
                return this.miscellaneousField;
            }
            set {
                this.miscellaneousField = value;
            }
        }
    
        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string number {
            get {
                return this.numberField;
            }
            set {
                this.numberField = value;
            }
        }
    }
}