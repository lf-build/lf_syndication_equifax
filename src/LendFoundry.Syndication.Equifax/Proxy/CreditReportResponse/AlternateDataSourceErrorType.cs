namespace LendFoundry.Syndication.Equifax.Proxy.CreditReportResponse
{
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.6.1055.0")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute()]
    public partial class AlternateDataSourceErrorType {
    
        private string customerReferenceNumberField;
    
        private string customerNumberField;
    
        private CodeType dataSourceCodeField;
    
        private DerrType[] derrErrorsField;
    
        /// <remarks/>
        public string CustomerReferenceNumber {
            get {
                return this.customerReferenceNumberField;
            }
            set {
                this.customerReferenceNumberField = value;
            }
        }
    
        /// <remarks/>
        public string CustomerNumber {
            get {
                return this.customerNumberField;
            }
            set {
                this.customerNumberField = value;
            }
        }
    
        /// <remarks/>
        public CodeType DataSourceCode {
            get {
                return this.dataSourceCodeField;
            }
            set {
                this.dataSourceCodeField = value;
            }
        }
    
        /// <remarks/>
        [System.Xml.Serialization.XmlArrayAttribute()]
        [System.Xml.Serialization.XmlArrayItemAttribute("DerrError", IsNullable=false)]
        public DerrType[] DerrErrors {
            get {
                return this.derrErrorsField;
            }
            set {
                this.derrErrorsField = value;
            }
        }
    }
}