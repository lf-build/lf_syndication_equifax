namespace LendFoundry.Syndication.Equifax.Proxy.CreditReportResponse
{
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.6.1055.0")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute()]
    public partial class USConsumerCreditReportType {
    
        private USHeaderType uSHeaderField;
    
        private USAddressType[] uSAddressesField;
    
        private IdentityScanType identityScanField;
    
        private USOtherNameType[] uSOtherNamesField;
    
        private USSubjectDeathType uSSubjectDeathField;
    
        private USEmploymentType[] uSEmploymentsField;
    
        private USOtherIdentificationType[] uSOtherIdentificationsField;
    
        private USBankruptcyType[] uSBankruptciesField;
    
        private USCollectionType[] uSCollectionsField;
    
        private USLegalItemType[] uSLegalItemsField;
    
        private USAlertContactType[] uSAlertContactsField;
    
        private USTaxLienType[] uSTaxLiensField;
    
        private USTradeType[] uSTradesField;
    
        private USInquiryType[] uSInquiriesField;
    
        private USConsumerStatementType[] uSConsumerStatementsField;
    
        private USEDASType uSEDASField;
    
        private USBeaconType uSBeaconField;
    
        private USFICOType uSFICOField;
    
        private USOnlineDirectoryType[] uSOnlineDirectoriesField;
    
        private USScoreType[] uSModelScoresField;
    
        private USIdentificationSSNType uSIdentificationSSNField;
    
        private USGeoCodeType uSGeoCodeField;
    
        private USDecisionPowerDataType[] uSDecisionPowerSegmentsField;
    
        private USDataAttributeType[] uSDataAttributesField;
    
        private USFraudAdvisorType uSFraudAdvisorField;
    
        private USConsumerCreditReportTypeUSAlternateDataSources uSAlternateDataSourcesField;
    
        private USCDCOFACType[] uSCDCOFACAlertsField;
    
        private USConsumerReferralType uSConsumerReferralField;
    
        private USErrorMessageType[] uSErrorMessagesField;
    
        private string subjectTypeField;
    
        private short multipleNumberField;
    
        public USConsumerCreditReportType() {
            this.subjectTypeField = "Subject";
            this.multipleNumberField = ((short)(1));
        }
    
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute()]
        public USHeaderType USHeader {
            get {
                return this.uSHeaderField;
            }
            set {
                this.uSHeaderField = value;
            }
        }
    
        /// <remarks/>
        [System.Xml.Serialization.XmlArrayAttribute()]
        [System.Xml.Serialization.XmlArrayItemAttribute("USAddress", IsNullable=false)]
        public USAddressType[] USAddresses {
            get {
                return this.uSAddressesField;
            }
            set {
                this.uSAddressesField = value;
            }
        }
    
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute()]
        public IdentityScanType IdentityScan {
            get {
                return this.identityScanField;
            }
            set {
                this.identityScanField = value;
            }
        }
    
        /// <remarks/>
        [System.Xml.Serialization.XmlArrayAttribute()]
        [System.Xml.Serialization.XmlArrayItemAttribute("USOtherName", IsNullable=false)]
        public USOtherNameType[] USOtherNames {
            get {
                return this.uSOtherNamesField;
            }
            set {
                this.uSOtherNamesField = value;
            }
        }
    
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute()]
        public USSubjectDeathType USSubjectDeath {
            get {
                return this.uSSubjectDeathField;
            }
            set {
                this.uSSubjectDeathField = value;
            }
        }
    
        /// <remarks/>
        [System.Xml.Serialization.XmlArrayAttribute()]
        [System.Xml.Serialization.XmlArrayItemAttribute("USEmployment", IsNullable=false)]
        public USEmploymentType[] USEmployments {
            get {
                return this.uSEmploymentsField;
            }
            set {
                this.uSEmploymentsField = value;
            }
        }
    
        /// <remarks/>
        [System.Xml.Serialization.XmlArrayAttribute()]
        [System.Xml.Serialization.XmlArrayItemAttribute("USOtherIdentification", IsNullable=false)]
        public USOtherIdentificationType[] USOtherIdentifications {
            get {
                return this.uSOtherIdentificationsField;
            }
            set {
                this.uSOtherIdentificationsField = value;
            }
        }
    
        /// <remarks/>
        [System.Xml.Serialization.XmlArrayAttribute()]
        [System.Xml.Serialization.XmlArrayItemAttribute("USBankruptcy", IsNullable=false)]
        public USBankruptcyType[] USBankruptcies {
            get {
                return this.uSBankruptciesField;
            }
            set {
                this.uSBankruptciesField = value;
            }
        }
    
        /// <remarks/>
        [System.Xml.Serialization.XmlArrayAttribute()]
        [System.Xml.Serialization.XmlArrayItemAttribute("USCollection", IsNullable=false)]
        public USCollectionType[] USCollections {
            get {
                return this.uSCollectionsField;
            }
            set {
                this.uSCollectionsField = value;
            }
        }
    
        /// <remarks/>
        [System.Xml.Serialization.XmlArrayAttribute()]
        [System.Xml.Serialization.XmlArrayItemAttribute("USLegalItem", IsNullable=false)]
        public USLegalItemType[] USLegalItems {
            get {
                return this.uSLegalItemsField;
            }
            set {
                this.uSLegalItemsField = value;
            }
        }
    
        /// <remarks/>
        [System.Xml.Serialization.XmlArrayAttribute()]
        [System.Xml.Serialization.XmlArrayItemAttribute("USAlertContact", IsNullable=false)]
        public USAlertContactType[] USAlertContacts {
            get {
                return this.uSAlertContactsField;
            }
            set {
                this.uSAlertContactsField = value;
            }
        }
    
        /// <remarks/>
        [System.Xml.Serialization.XmlArrayAttribute()]
        [System.Xml.Serialization.XmlArrayItemAttribute("USTaxLien", IsNullable=false)]
        public USTaxLienType[] USTaxLiens {
            get {
                return this.uSTaxLiensField;
            }
            set {
                this.uSTaxLiensField = value;
            }
        }
    
        /// <remarks/>
        [System.Xml.Serialization.XmlArrayAttribute()]
        [System.Xml.Serialization.XmlArrayItemAttribute("USTrade", IsNullable=false)]
        public USTradeType[] USTrades {
            get {
                return this.uSTradesField;
            }
            set {
                this.uSTradesField = value;
            }
        }
    
        /// <remarks/>
        [System.Xml.Serialization.XmlArrayAttribute()]
        [System.Xml.Serialization.XmlArrayItemAttribute("USInquiry", IsNullable=false)]
        public USInquiryType[] USInquiries {
            get {
                return this.uSInquiriesField;
            }
            set {
                this.uSInquiriesField = value;
            }
        }
    
        /// <remarks/>
        [System.Xml.Serialization.XmlArrayAttribute()]
        [System.Xml.Serialization.XmlArrayItemAttribute("USConsumerStatement", IsNullable=false)]
        public USConsumerStatementType[] USConsumerStatements {
            get {
                return this.uSConsumerStatementsField;
            }
            set {
                this.uSConsumerStatementsField = value;
            }
        }
    
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute()]
        public USEDASType USEDAS {
            get {
                return this.uSEDASField;
            }
            set {
                this.uSEDASField = value;
            }
        }
    
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute()]
        public USBeaconType USBeacon {
            get {
                return this.uSBeaconField;
            }
            set {
                this.uSBeaconField = value;
            }
        }
    
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute()]
        public USFICOType USFICO {
            get {
                return this.uSFICOField;
            }
            set {
                this.uSFICOField = value;
            }
        }
    
        /// <remarks/>
        [System.Xml.Serialization.XmlArrayAttribute()]
        [System.Xml.Serialization.XmlArrayItemAttribute("USOnlineDirectory",IsNullable=false)]
        public USOnlineDirectoryType[] USOnlineDirectories {
            get {
                return this.uSOnlineDirectoriesField;
            }
            set {
                this.uSOnlineDirectoriesField = value;
            }
        }
    
        /// <remarks/>
        [System.Xml.Serialization.XmlArrayAttribute()]
        [System.Xml.Serialization.XmlArrayItemAttribute("USModelScore", IsNullable=false)]
        public USScoreType[] USModelScores {
            get {
                return this.uSModelScoresField;
            }
            set {
                this.uSModelScoresField = value;
            }
        }
    
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute()]
        public USIdentificationSSNType USIdentificationSSN {
            get {
                return this.uSIdentificationSSNField;
            }
            set {
                this.uSIdentificationSSNField = value;
            }
        }
    
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute()]
        public USGeoCodeType USGeoCode {
            get {
                return this.uSGeoCodeField;
            }
            set {
                this.uSGeoCodeField = value;
            }
        }
    
        /// <remarks/>
        [System.Xml.Serialization.XmlArrayAttribute()]
        [System.Xml.Serialization.XmlArrayItemAttribute("USDecisionPowerSegment", IsNullable=false)]
        public USDecisionPowerDataType[] USDecisionPowerSegments {
            get {
                return this.uSDecisionPowerSegmentsField;
            }
            set {
                this.uSDecisionPowerSegmentsField = value;
            }
        }
    
        /// <remarks/>
        [System.Xml.Serialization.XmlArrayAttribute()]
        [System.Xml.Serialization.XmlArrayItemAttribute("USDataAttribute", IsNullable=false)]
        public USDataAttributeType[] USDataAttributes {
            get {
                return this.uSDataAttributesField;
            }
            set {
                this.uSDataAttributesField = value;
            }
        }
    
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute()]
        public USFraudAdvisorType USFraudAdvisor {
            get {
                return this.uSFraudAdvisorField;
            }
            set {
                this.uSFraudAdvisorField = value;
            }
        }
    
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute()]
        public USConsumerCreditReportTypeUSAlternateDataSources USAlternateDataSources {
            get {
                return this.uSAlternateDataSourcesField;
            }
            set {
                this.uSAlternateDataSourcesField = value;
            }
        }
    
        /// <remarks/>
        [System.Xml.Serialization.XmlArrayAttribute()]
        [System.Xml.Serialization.XmlArrayItemAttribute("USCDCOFACAlert", IsNullable=false)]
        public USCDCOFACType[] USCDCOFACAlerts {
            get {
                return this.uSCDCOFACAlertsField;
            }
            set {
                this.uSCDCOFACAlertsField = value;
            }
        }
    
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute()]
        public USConsumerReferralType USConsumerReferral {
            get {
                return this.uSConsumerReferralField;
            }
            set {
                this.uSConsumerReferralField = value;
            }
        }
    
        /// <remarks/>
        [System.Xml.Serialization.XmlArrayAttribute()]
        [System.Xml.Serialization.XmlArrayItemAttribute("USErrorMessage", IsNullable=false)]
        public USErrorMessageType[] USErrorMessages {
            get {
                return this.uSErrorMessagesField;
            }
            set {
                this.uSErrorMessagesField = value;
            }
        }
    
        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        [System.ComponentModel.DefaultValueAttribute("Subject")]
        public string subjectType {
            get {
                return this.subjectTypeField;
            }
            set {
                this.subjectTypeField = value;
            }
        }
    
        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        [System.ComponentModel.DefaultValueAttribute(typeof(short), "1")]
        public short multipleNumber {
            get {
                return this.multipleNumberField;
            }
            set {
                this.multipleNumberField = value;
            }
        }
    }
}