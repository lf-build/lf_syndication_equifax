namespace LendFoundry.Syndication.Equifax.Proxy.CreditReportResponse
{
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.6.1055.0")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute()]
    public partial class IXIServicesType {
    
        private CodeType regulatedIdentifierField;
    
        private CodeType hitNoHitIndicatorField;
    
        private string disclaimerField;
    
        private WealthCompleteType wealthCompleteField;
    
        private Income360Type income360Field;
    
        private DiscretionarySpendingType discretionarySpendingField;
    
        private AbilityToPayType abilityToPayField;
    
        /// <remarks/>
        public CodeType RegulatedIdentifier {
            get {
                return this.regulatedIdentifierField;
            }
            set {
                this.regulatedIdentifierField = value;
            }
        }
    
        /// <remarks/>
        public CodeType HitNoHitIndicator {
            get {
                return this.hitNoHitIndicatorField;
            }
            set {
                this.hitNoHitIndicatorField = value;
            }
        }
    
        /// <remarks/>
        public string Disclaimer {
            get {
                return this.disclaimerField;
            }
            set {
                this.disclaimerField = value;
            }
        }
    
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute()]
        public WealthCompleteType WealthComplete {
            get {
                return this.wealthCompleteField;
            }
            set {
                this.wealthCompleteField = value;
            }
        }
    
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute()]
        public Income360Type Income360 {
            get {
                return this.income360Field;
            }
            set {
                this.income360Field = value;
            }
        }
    
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute()]
        public DiscretionarySpendingType DiscretionarySpending {
            get {
                return this.discretionarySpendingField;
            }
            set {
                this.discretionarySpendingField = value;
            }
        }
    
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute()]
        public AbilityToPayType AbilityToPay {
            get {
                return this.abilityToPayField;
            }
            set {
                this.abilityToPayField = value;
            }
        }
    }
}