namespace LendFoundry.Syndication.Equifax.Proxy.CreditReportResponse
{
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.6.1055.0")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute()]
    public partial class USEDASType {
    
        private string eDASScoreField;
    
        private ScoreReasonType[] scoreReasonsField;
    
        private CodeType rejectCodeField;
    
        private CodeType regionalIndicatorField;
    
        private CodeType enhancedDASIndicatorField;
    
        private string fACTActInquiriesAreKeyFactorField;
    
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute()]
        public string EDASScore {
            get {
                return this.eDASScoreField;
            }
            set {
                this.eDASScoreField = value;
            }
        }
    
        /// <remarks/>
        [System.Xml.Serialization.XmlArrayAttribute()]
        [System.Xml.Serialization.XmlArrayItemAttribute("ScoreReason", IsNullable=false)]
        public ScoreReasonType[] ScoreReasons {
            get {
                return this.scoreReasonsField;
            }
            set {
                this.scoreReasonsField = value;
            }
        }
    
        /// <remarks/>
        public CodeType RejectCode {
            get {
                return this.rejectCodeField;
            }
            set {
                this.rejectCodeField = value;
            }
        }
    
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute()]
        public CodeType RegionalIndicator {
            get {
                return this.regionalIndicatorField;
            }
            set {
                this.regionalIndicatorField = value;
            }
        }
    
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute()]
        public CodeType EnhancedDASIndicator {
            get {
                return this.enhancedDASIndicatorField;
            }
            set {
                this.enhancedDASIndicatorField = value;
            }
        }
    
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute()]
        public string FACTActInquiriesAreKeyFactor {
            get {
                return this.fACTActInquiriesAreKeyFactorField;
            }
            set {
                this.fACTActInquiriesAreKeyFactorField = value;
            }
        }
    }
}