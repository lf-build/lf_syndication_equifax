namespace LendFoundry.Syndication.Equifax.Proxy.CreditReportResponse
{
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.6.1055.0")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute()]
    public partial class USCollectionType {
    
        private CodeType expandedDataReturnedField;
    
        private DateType dateReportedField;
    
        private DateType assignedDateField;
    
        private CustomerIdType customerIdField;
    
        private string clientNameNumberField;
    
        private string accountNumberField;
    
        private MoneyType originalAmountField;
    
        private MoneyType balanceAmountField;
    
        private DateType dateOfLastPaymentField;
    
        private DateType statusDateField;
    
        private CodeType statusField;
    
        private DateType dateOfFirstDelinquencyField;
    
        private CodeType accountDesignatorField;
    
        private CodeType updateIndicatorField;
    
        private CodeType[] narrativesField;
    
        private DateType expandedDateReportedField;
    
        private DateType expandedDateAssignedField;
    
        private MoneyType expandedOriginalAmountField;
    
        private DateType expandedStatusDateField;
    
        private MoneyType expandedBalanceField;
    
        private DateType expandedDateOfLastPaymentField;
    
        private DateType expandedDateOfFirstDelinquencyField;
    
        private string expandedAccountNumberField;
    
        private CodeType expandedAccountDesignatorField;
    
        private CodeType expandedCreditorClassificationField;
    
        /// <remarks/>
        public CodeType ExpandedDataReturned {
            get {
                return this.expandedDataReturnedField;
            }
            set {
                this.expandedDataReturnedField = value;
            }
        }
    
        /// <remarks/>
        public DateType DateReported {
            get {
                return this.dateReportedField;
            }
            set {
                this.dateReportedField = value;
            }
        }
    
        /// <remarks/>
        public DateType AssignedDate {
            get {
                return this.assignedDateField;
            }
            set {
                this.assignedDateField = value;
            }
        }
    
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute()]
        public CustomerIdType CustomerId {
            get {
                return this.customerIdField;
            }
            set {
                this.customerIdField = value;
            }
        }
    
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute()]
        public string ClientNameNumber {
            get {
                return this.clientNameNumberField;
            }
            set {
                this.clientNameNumberField = value;
            }
        }
    
        /// <remarks/>
        public string AccountNumber {
            get {
                return this.accountNumberField;
            }
            set {
                this.accountNumberField = value;
            }
        }
    
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(IsNullable=true)]
        public MoneyType OriginalAmount {
            get {
                return this.originalAmountField;
            }
            set {
                this.originalAmountField = value;
            }
        }
    
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(IsNullable=true)]
        public MoneyType BalanceAmount {
            get {
                return this.balanceAmountField;
            }
            set {
                this.balanceAmountField = value;
            }
        }
    
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute()]
        public DateType DateOfLastPayment {
            get {
                return this.dateOfLastPaymentField;
            }
            set {
                this.dateOfLastPaymentField = value;
            }
        }
    
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute()]
        public DateType StatusDate {
            get {
                return this.statusDateField;
            }
            set {
                this.statusDateField = value;
            }
        }
    
        /// <remarks/>
        public CodeType Status {
            get {
                return this.statusField;
            }
            set {
                this.statusField = value;
            }
        }
    
        /// <remarks/>
        public DateType DateOfFirstDelinquency {
            get {
                return this.dateOfFirstDelinquencyField;
            }
            set {
                this.dateOfFirstDelinquencyField = value;
            }
        }
    
        /// <remarks/>
        public CodeType AccountDesignator {
            get {
                return this.accountDesignatorField;
            }
            set {
                this.accountDesignatorField = value;
            }
        }
    
        /// <remarks/>
        public CodeType UpdateIndicator {
            get {
                return this.updateIndicatorField;
            }
            set {
                this.updateIndicatorField = value;
            }
        }
    
        /// <remarks/>
        [System.Xml.Serialization.XmlArrayItemAttribute("Narrative", IsNullable=false)]
        public CodeType[] Narratives {
            get {
                return this.narrativesField;
            }
            set {
                this.narrativesField = value;
            }
        }
    
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute()]
        public DateType ExpandedDateReported {
            get {
                return this.expandedDateReportedField;
            }
            set {
                this.expandedDateReportedField = value;
            }
        }
    
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute()]
        public DateType ExpandedDateAssigned {
            get {
                return this.expandedDateAssignedField;
            }
            set {
                this.expandedDateAssignedField = value;
            }
        }
    
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute( IsNullable=true)]
        public MoneyType ExpandedOriginalAmount {
            get {
                return this.expandedOriginalAmountField;
            }
            set {
                this.expandedOriginalAmountField = value;
            }
        }
    
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute()]
        public DateType ExpandedStatusDate {
            get {
                return this.expandedStatusDateField;
            }
            set {
                this.expandedStatusDateField = value;
            }
        }
    
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(IsNullable=true)]
        public MoneyType ExpandedBalance {
            get {
                return this.expandedBalanceField;
            }
            set {
                this.expandedBalanceField = value;
            }
        }
    
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute()]
        public DateType ExpandedDateOfLastPayment {
            get {
                return this.expandedDateOfLastPaymentField;
            }
            set {
                this.expandedDateOfLastPaymentField = value;
            }
        }
    
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute()]
        public DateType ExpandedDateOfFirstDelinquency {
            get {
                return this.expandedDateOfFirstDelinquencyField;
            }
            set {
                this.expandedDateOfFirstDelinquencyField = value;
            }
        }
    
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute()]
        public string ExpandedAccountNumber {
            get {
                return this.expandedAccountNumberField;
            }
            set {
                this.expandedAccountNumberField = value;
            }
        }
    
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute()]
        public CodeType ExpandedAccountDesignator {
            get {
                return this.expandedAccountDesignatorField;
            }
            set {
                this.expandedAccountDesignatorField = value;
            }
        }
    
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute()]
        public CodeType ExpandedCreditorClassification {
            get {
                return this.expandedCreditorClassificationField;
            }
            set {
                this.expandedCreditorClassificationField = value;
            }
        }
    }
}