namespace LendFoundry.Syndication.Equifax.Proxy.CreditReportResponse
{
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.6.1055.0")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType=true)]
    public partial class USAlertContactTypePhoneNumber {
    
        private CodeType telephoneTypeField;
    
        private string internationalCallingCodeField;
    
        private string telephoneNumberField;
    
        private string telephoneExtensionField;
    
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute()]
        public CodeType TelephoneType {
            get {
                return this.telephoneTypeField;
            }
            set {
                this.telephoneTypeField = value;
            }
        }
    
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute()]
        public string InternationalCallingCode {
            get {
                return this.internationalCallingCodeField;
            }
            set {
                this.internationalCallingCodeField = value;
            }
        }
    
        /// <remarks/>
        public string TelephoneNumber {
            get {
                return this.telephoneNumberField;
            }
            set {
                this.telephoneNumberField = value;
            }
        }
    
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute()]
        public string TelephoneExtension {
            get {
                return this.telephoneExtensionField;
            }
            set {
                this.telephoneExtensionField = value;
            }
        }
    }
}