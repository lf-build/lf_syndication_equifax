namespace LendFoundry.Syndication.Equifax.Proxy.CreditReportResponse
{
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.6.1055.0")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute()]
    public partial class PropertyDataAndAnalyticType {
    
        private CodeType regulatedIdentifierField;
    
        private CodeType hitNoHitIndicatorField;
    
        private PropertyDataAndAnalyticALSType activListingDataField;
    
        private PropertyDataAndAnalyticAVMType automatedValuationModelDataField;
    
        private PropertyDataAndAnalyticLIENType lienDataField;
    
        private PropertyDataAndAnalyticPROPType propertyDataField;
    
        private PropertyDataAndAnalyticMKTType marketDataField;
    
        private PropertyDataAndAnalyticHPIType hPIDataField;
    
        /// <remarks/>
        public CodeType RegulatedIdentifier {
            get {
                return this.regulatedIdentifierField;
            }
            set {
                this.regulatedIdentifierField = value;
            }
        }
    
        /// <remarks/>
        public CodeType HitNoHitIndicator {
            get {
                return this.hitNoHitIndicatorField;
            }
            set {
                this.hitNoHitIndicatorField = value;
            }
        }
    
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute()]
        public PropertyDataAndAnalyticALSType ActivListingData {
            get {
                return this.activListingDataField;
            }
            set {
                this.activListingDataField = value;
            }
        }
    
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute()]
        public PropertyDataAndAnalyticAVMType AutomatedValuationModelData {
            get {
                return this.automatedValuationModelDataField;
            }
            set {
                this.automatedValuationModelDataField = value;
            }
        }
    
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute()]
        public PropertyDataAndAnalyticLIENType LienData {
            get {
                return this.lienDataField;
            }
            set {
                this.lienDataField = value;
            }
        }
    
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute()]
        public PropertyDataAndAnalyticPROPType PropertyData {
            get {
                return this.propertyDataField;
            }
            set {
                this.propertyDataField = value;
            }
        }
    
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute()]
        public PropertyDataAndAnalyticMKTType MarketData {
            get {
                return this.marketDataField;
            }
            set {
                this.marketDataField = value;
            }
        }
    
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute()]
        public PropertyDataAndAnalyticHPIType HPIData {
            get {
                return this.hPIDataField;
            }
            set {
                this.hPIDataField = value;
            }
        }
    }
}