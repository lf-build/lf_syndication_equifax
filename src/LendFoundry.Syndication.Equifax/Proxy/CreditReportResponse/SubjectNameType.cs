namespace LendFoundry.Syndication.Equifax.Proxy.CreditReportResponse
{
    /// <remarks/>
    [System.Xml.Serialization.XmlIncludeAttribute(typeof(USOtherNameType))]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.6.1055.0")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute()]
    [System.Xml.Serialization.XmlRootAttribute("SubjectName", IsNullable=false)]
    public partial class SubjectNameType {
    
        private string lastNameField;
    
        private string firstNameField;
    
        private string middleNameField;
    
        private string nameSuffixField;
    
        /// <remarks/>
        public string LastName {
            get {
                return this.lastNameField;
            }
            set {
                this.lastNameField = value;
            }
        }
    
        /// <remarks/>
        public string FirstName {
            get {
                return this.firstNameField;
            }
            set {
                this.firstNameField = value;
            }
        }
    
        /// <remarks/>
        public string MiddleName {
            get {
                return this.middleNameField;
            }
            set {
                this.middleNameField = value;
            }
        }
    
        /// <remarks/>
        public string NameSuffix {
            get {
                return this.nameSuffixField;
            }
            set {
                this.nameSuffixField = value;
            }
        }
    }
}