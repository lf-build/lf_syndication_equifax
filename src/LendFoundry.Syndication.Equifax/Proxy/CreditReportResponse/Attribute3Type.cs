namespace LendFoundry.Syndication.Equifax.Proxy.CreditReportResponse
{
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.6.1055.0")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute()]
    [System.Xml.Serialization.XmlRootAttribute("ProductAttribute", IsNullable=false)]
    public partial class Attribute3Type {
    
        private string attributeCodeField;
    
        private string attributeValueField;
    
        private short numberField;
    
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute()]
        public string AttributeCode {
            get {
                return this.attributeCodeField;
            }
            set {
                this.attributeCodeField = value;
            }
        }
    
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute()]
        public string AttributeValue {
            get {
                return this.attributeValueField;
            }
            set {
                this.attributeValueField = value;
            }
        }
    
        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public short number {
            get {
                return this.numberField;
            }
            set {
                this.numberField = value;
            }
        }
    }
}