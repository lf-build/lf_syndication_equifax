namespace LendFoundry.Syndication.Equifax.Proxy.CreditReportResponse
{
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.6.1055.0")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute()]
    public partial class USPPSDataType {
    
        private CodeType responseCodeField;
    
        private CodeType resultCodeField;
    
        private string resultMessageField;
    
        private string pPSTransactionIDField;
    
        private DateType transactionDateField;
    
        private string transactionTimeField;
    
        private string referenceNumberField;
    
        private DateType taxIDNumberStartDateField;
    
        private DateType taxIDNumberEndDateField;
    
        private DateType identificationStartDateField;
    
        private DateType identificationEndDateField;
    
        private string verifyParametersField;
    
        private string verifyAlertsField;
    
        private string resendParametersField;
    
        private string resendTriesField;
    
        private bool maxTriesExceededField;
    
        private ScoreReasonType[] warningFlagsField;
    
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute()]
        public CodeType ResponseCode {
            get {
                return this.responseCodeField;
            }
            set {
                this.responseCodeField = value;
            }
        }
    
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute()]
        public CodeType ResultCode {
            get {
                return this.resultCodeField;
            }
            set {
                this.resultCodeField = value;
            }
        }
    
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute()]
        public string ResultMessage {
            get {
                return this.resultMessageField;
            }
            set {
                this.resultMessageField = value;
            }
        }
    
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute()]
        public string PPSTransactionID {
            get {
                return this.pPSTransactionIDField;
            }
            set {
                this.pPSTransactionIDField = value;
            }
        }
    
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute()]
        public DateType TransactionDate {
            get {
                return this.transactionDateField;
            }
            set {
                this.transactionDateField = value;
            }
        }
    
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute()]
        public string TransactionTime {
            get {
                return this.transactionTimeField;
            }
            set {
                this.transactionTimeField = value;
            }
        }
    
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute()]
        public string ReferenceNumber {
            get {
                return this.referenceNumberField;
            }
            set {
                this.referenceNumberField = value;
            }
        }
    
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute()]
        public DateType TaxIDNumberStartDate {
            get {
                return this.taxIDNumberStartDateField;
            }
            set {
                this.taxIDNumberStartDateField = value;
            }
        }
    
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute()]
        public DateType TaxIDNumberEndDate {
            get {
                return this.taxIDNumberEndDateField;
            }
            set {
                this.taxIDNumberEndDateField = value;
            }
        }
    
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute()]
        public DateType IdentificationStartDate {
            get {
                return this.identificationStartDateField;
            }
            set {
                this.identificationStartDateField = value;
            }
        }
    
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute()]
        public DateType IdentificationEndDate {
            get {
                return this.identificationEndDateField;
            }
            set {
                this.identificationEndDateField = value;
            }
        }
    
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute()]
        public string VerifyParameters {
            get {
                return this.verifyParametersField;
            }
            set {
                this.verifyParametersField = value;
            }
        }
    
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute()]
        public string VerifyAlerts {
            get {
                return this.verifyAlertsField;
            }
            set {
                this.verifyAlertsField = value;
            }
        }
    
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute()]
        public string ResendParameters {
            get {
                return this.resendParametersField;
            }
            set {
                this.resendParametersField = value;
            }
        }
    
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute()]
        public string ResendTries {
            get {
                return this.resendTriesField;
            }
            set {
                this.resendTriesField = value;
            }
        }
    
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute()]
        public bool MaxTriesExceeded {
            get {
                return this.maxTriesExceededField;
            }
            set {
                this.maxTriesExceededField = value;
            }
        }
    
        /// <remarks/>
        [System.Xml.Serialization.XmlArrayAttribute()]
        [System.Xml.Serialization.XmlArrayItemAttribute("WarningFlag", IsNullable=false)]
        public ScoreReasonType[] WarningFlags {
            get {
                return this.warningFlagsField;
            }
            set {
                this.warningFlagsField = value;
            }
        }
    }
}