namespace LendFoundry.Syndication.Equifax.Proxy.CreditReportResponse
{
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.6.1055.0")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute()]
    public partial class PropertyDataAndAnalyticLIENDetailType {
    
        private string typeofLienField;
    
        private MoneyType amountofLienField;
    
        private DateType originationDateofLienField;
    
        private string interestRateofLienField;
    
        private string interestRateTypeofLienField;
    
        private string nameofLenderField;
    
        private string modeledMortgageTypeField;
    
        private short numberField;
    
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute()]
        public string TypeofLien {
            get {
                return this.typeofLienField;
            }
            set {
                this.typeofLienField = value;
            }
        }
    
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute()]
        public MoneyType AmountofLien {
            get {
                return this.amountofLienField;
            }
            set {
                this.amountofLienField = value;
            }
        }
    
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute()]
        public DateType OriginationDateofLien {
            get {
                return this.originationDateofLienField;
            }
            set {
                this.originationDateofLienField = value;
            }
        }
    
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute()]
        public string InterestRateofLien {
            get {
                return this.interestRateofLienField;
            }
            set {
                this.interestRateofLienField = value;
            }
        }
    
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute()]
        public string InterestRateTypeofLien {
            get {
                return this.interestRateTypeofLienField;
            }
            set {
                this.interestRateTypeofLienField = value;
            }
        }
    
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute()]
        public string NameofLender {
            get {
                return this.nameofLenderField;
            }
            set {
                this.nameofLenderField = value;
            }
        }
    
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute()]
        public string ModeledMortgageType {
            get {
                return this.modeledMortgageTypeField;
            }
            set {
                this.modeledMortgageTypeField = value;
            }
        }
    
        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public short number {
            get {
                return this.numberField;
            }
            set {
                this.numberField = value;
            }
        }
    }
}