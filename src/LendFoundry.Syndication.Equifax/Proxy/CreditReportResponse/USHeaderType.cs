namespace LendFoundry.Syndication.Equifax.Proxy.CreditReportResponse
{
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.6.1055.0")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute()]
    public partial class USHeaderType {
    
        private USHeaderTypeRequest requestField;
    
        private USHeaderTypeCreditFile creditFileField;
    
        private USHeaderTypeSubject subjectField;
    
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute()]
        public USHeaderTypeRequest Request {
            get {
                return this.requestField;
            }
            set {
                this.requestField = value;
            }
        }
    
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute()]
        public USHeaderTypeCreditFile CreditFile {
            get {
                return this.creditFileField;
            }
            set {
                this.creditFileField = value;
            }
        }
    
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute()]
        public USHeaderTypeSubject Subject {
            get {
                return this.subjectField;
            }
            set {
                this.subjectField = value;
            }
        }
    }
}