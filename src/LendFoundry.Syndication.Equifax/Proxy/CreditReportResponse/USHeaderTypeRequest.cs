namespace LendFoundry.Syndication.Equifax.Proxy.CreditReportResponse
{
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.6.1055.0")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType=true)]
    public partial class USHeaderTypeRequest {
    
        private string customerReferenceNumberField;
    
        private string customerNumberField;
    
        private string consumerReferralCodeField;
    
        private ECOAInquiryType eCOAInquiryTypeField;
    
        private string numberOfMonthsToCountInquiriesField;
    
        private string numberOfMonthsToCountDelinquenciesField;
    
        /// <remarks/>
        public string CustomerReferenceNumber {
            get {
                return this.customerReferenceNumberField;
            }
            set {
                this.customerReferenceNumberField = value;
            }
        }
    
        /// <remarks/>
        public string CustomerNumber {
            get {
                return this.customerNumberField;
            }
            set {
                this.customerNumberField = value;
            }
        }
    
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute()]
        public string ConsumerReferralCode {
            get {
                return this.consumerReferralCodeField;
            }
            set {
                this.consumerReferralCodeField = value;
            }
        }
    
        /// <remarks/>
        public ECOAInquiryType ECOAInquiryType {
            get {
                return this.eCOAInquiryTypeField;
            }
            set {
                this.eCOAInquiryTypeField = value;
            }
        }
    
        /// <remarks/>
        public string NumberOfMonthsToCountInquiries {
            get {
                return this.numberOfMonthsToCountInquiriesField;
            }
            set {
                this.numberOfMonthsToCountInquiriesField = value;
            }
        }
    
        /// <remarks/>
        public string NumberOfMonthsToCountDelinquencies {
            get {
                return this.numberOfMonthsToCountDelinquenciesField;
            }
            set {
                this.numberOfMonthsToCountDelinquenciesField = value;
            }
        }
    }
}