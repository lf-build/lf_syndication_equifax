namespace LendFoundry.Syndication.Equifax.Proxy.CreditReportResponse
{
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.6.1055.0")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute()]
    public partial class PropertyDataAndAnalyticLIENType {
    
        private CodeType hitNoHitIndicatorField;
    
        private MoneyType estimatedLoantoValueField;
    
        private MoneyType combinedLoantoValueField;
    
        private MoneyType estimatedBalanceofFirstLienField;
    
        private MoneyType estimatedBalanceofSecondLienField;
    
        private PropertyDataAndAnalyticLIENDetailType[] lienDetailField;
    
        /// <remarks/>
        public CodeType HitNoHitIndicator {
            get {
                return this.hitNoHitIndicatorField;
            }
            set {
                this.hitNoHitIndicatorField = value;
            }
        }
    
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute()]
        public MoneyType EstimatedLoantoValue {
            get {
                return this.estimatedLoantoValueField;
            }
            set {
                this.estimatedLoantoValueField = value;
            }
        }
    
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute()]
        public MoneyType CombinedLoantoValue {
            get {
                return this.combinedLoantoValueField;
            }
            set {
                this.combinedLoantoValueField = value;
            }
        }
    
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute()]
        public MoneyType EstimatedBalanceofFirstLien {
            get {
                return this.estimatedBalanceofFirstLienField;
            }
            set {
                this.estimatedBalanceofFirstLienField = value;
            }
        }
    
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute()]
        public MoneyType EstimatedBalanceofSecondLien {
            get {
                return this.estimatedBalanceofSecondLienField;
            }
            set {
                this.estimatedBalanceofSecondLienField = value;
            }
        }
    
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute("LienDetail")]
        public PropertyDataAndAnalyticLIENDetailType[] LienDetail {
            get {
                return this.lienDetailField;
            }
            set {
                this.lienDetailField = value;
            }
        }
    }
}