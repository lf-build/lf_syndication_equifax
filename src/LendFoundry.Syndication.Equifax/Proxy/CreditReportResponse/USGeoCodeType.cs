namespace LendFoundry.Syndication.Equifax.Proxy.CreditReportResponse
{
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.6.1055.0")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute()]
    public partial class USGeoCodeType {
    
        private string geoSMSAField;
    
        private string geoStateCodeField;
    
        private string geoCountyCodeField;
    
        private string geoCensusTractField;
    
        private string geoSuffixField;
    
        private string geoBlockGroupField;
    
        private ParsedStreetAddressType parsedStreetAddressField;
    
        private string geo5DigitSMSAField;
    
        private CityType cityField;
    
        private string stateField;
    
        private string postalCodeField;
    
        private CodeType typeField;
    
        private string[] geoReturnCodesField;
    
        private string microVisionCodeField;
    
        private string microVisionReturnCodeField;
    
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute()]
        public string GeoSMSA {
            get {
                return this.geoSMSAField;
            }
            set {
                this.geoSMSAField = value;
            }
        }
    
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute()]
        public string GeoStateCode {
            get {
                return this.geoStateCodeField;
            }
            set {
                this.geoStateCodeField = value;
            }
        }
    
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute()]
        public string GeoCountyCode {
            get {
                return this.geoCountyCodeField;
            }
            set {
                this.geoCountyCodeField = value;
            }
        }
    
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute()]
        public string GeoCensusTract {
            get {
                return this.geoCensusTractField;
            }
            set {
                this.geoCensusTractField = value;
            }
        }
    
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute()]
        public string GeoSuffix {
            get {
                return this.geoSuffixField;
            }
            set {
                this.geoSuffixField = value;
            }
        }
    
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute()]
        public string GeoBlockGroup {
            get {
                return this.geoBlockGroupField;
            }
            set {
                this.geoBlockGroupField = value;
            }
        }
    
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute()]
        public ParsedStreetAddressType ParsedStreetAddress {
            get {
                return this.parsedStreetAddressField;
            }
            set {
                this.parsedStreetAddressField = value;
            }
        }
    
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute()]
        public string Geo5DigitSMSA {
            get {
                return this.geo5DigitSMSAField;
            }
            set {
                this.geo5DigitSMSAField = value;
            }
        }
    
        /// <remarks/>
        public CityType City {
            get {
                return this.cityField;
            }
            set {
                this.cityField = value;
            }
        }
    
        /// <remarks/>
        public string State {
            get {
                return this.stateField;
            }
            set {
                this.stateField = value;
            }
        }
    
        /// <remarks/>
        public string PostalCode {
            get {
                return this.postalCodeField;
            }
            set {
                this.postalCodeField = value;
            }
        }
    
        /// <remarks/>
        public CodeType Type {
            get {
                return this.typeField;
            }
            set {
                this.typeField = value;
            }
        }
    
        /// <remarks/>
        [System.Xml.Serialization.XmlArrayAttribute()]
        [System.Xml.Serialization.XmlArrayItemAttribute("GeoReturnCode", IsNullable=false)]
        public string[] GeoReturnCodes {
            get {
                return this.geoReturnCodesField;
            }
            set {
                this.geoReturnCodesField = value;
            }
        }
    
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute()]
        public string MicroVisionCode {
            get {
                return this.microVisionCodeField;
            }
            set {
                this.microVisionCodeField = value;
            }
        }
    
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute()]
        public string MicroVisionReturnCode {
            get {
                return this.microVisionReturnCodeField;
            }
            set {
                this.microVisionReturnCodeField = value;
            }
        }
    }
}