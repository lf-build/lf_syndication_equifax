namespace LendFoundry.Syndication.Equifax.Proxy.CreditReportResponse
{
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.6.1055.0")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute()]
    public partial class USBeaconType {
    
        private string beaconScoreField;
    
        private ScoreReasonType[] scoreReasonsField;
    
        private CodeType rejectCodeField;
    
        private object riskBasedPricingField;
    
        private object doddFrankField;
    
        private CodeType scoreIndicatorField;
    
        private string fACTActInquiriesAreKeyFactorField;
    
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute()]
        public string BeaconScore {
            get {
                return this.beaconScoreField;
            }
            set {
                this.beaconScoreField = value;
            }
        }
    
        /// <remarks/>
        [System.Xml.Serialization.XmlArrayAttribute()]
        [System.Xml.Serialization.XmlArrayItemAttribute("ScoreReason", IsNullable=false)]
        public ScoreReasonType[] ScoreReasons {
            get {
                return this.scoreReasonsField;
            }
            set {
                this.scoreReasonsField = value;
            }
        }
    
        /// <remarks/>
        public CodeType RejectCode {
            get {
                return this.rejectCodeField;
            }
            set {
                this.rejectCodeField = value;
            }
        }
    
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute()]
        public object RiskBasedPricing {
            get {
                return this.riskBasedPricingField;
            }
            set {
                this.riskBasedPricingField = value;
            }
        }
    
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute()]
        public object DoddFrank {
            get {
                return this.doddFrankField;
            }
            set {
                this.doddFrankField = value;
            }
        }
    
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute()]
        public CodeType ScoreIndicator {
            get {
                return this.scoreIndicatorField;
            }
            set {
                this.scoreIndicatorField = value;
            }
        }
    
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute()]
        public string FACTActInquiriesAreKeyFactor {
            get {
                return this.fACTActInquiriesAreKeyFactorField;
            }
            set {
                this.fACTActInquiriesAreKeyFactorField = value;
            }
        }
    }
}