namespace LendFoundry.Syndication.Equifax.Proxy.CreditReportResponse
{
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.6.1055.0")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute()]
    public partial class USCDCOFACType {
    
        private string revisedLegalVerbiageIndicatorField;
    
        private string cDCMemberCodeField;
    
        private string cDCBranchAccountNumberField;
    
        private DateType transactionDateField;
    
        private string transactionTimeField;
    
        private CodeType responseTypeField;
    
        private CodeType responseCodeField;
    
        private CodeType problemCodeField;
    
        private CodeType[] matchCodesField;
    
        private DateType problemReportDateField;
    
        private string issueSourceField;
    
        private string issueIdField;
    
        private string commentField;
    
        private SubjectNameType subjectNameField;
    
        private string addressLine1Field;
    
        private CityType cityField;
    
        private string stateField;
    
        private string postalCodeField;
    
        private string countryCodeField;
    
        private string legalVerbiageField;
    
        private BlankorNFlagType regulatedNonRegulatedFlagField;
    
        private bool regulatedNonRegulatedFlagFieldSpecified;
    
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute()]
        public string RevisedLegalVerbiageIndicator {
            get {
                return this.revisedLegalVerbiageIndicatorField;
            }
            set {
                this.revisedLegalVerbiageIndicatorField = value;
            }
        }
    
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute()]
        public string CDCMemberCode {
            get {
                return this.cDCMemberCodeField;
            }
            set {
                this.cDCMemberCodeField = value;
            }
        }
    
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute()]
        public string CDCBranchAccountNumber {
            get {
                return this.cDCBranchAccountNumberField;
            }
            set {
                this.cDCBranchAccountNumberField = value;
            }
        }
    
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute()]
        public DateType TransactionDate {
            get {
                return this.transactionDateField;
            }
            set {
                this.transactionDateField = value;
            }
        }
    
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute()]
        public string TransactionTime {
            get {
                return this.transactionTimeField;
            }
            set {
                this.transactionTimeField = value;
            }
        }
    
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute()]
        public CodeType ResponseType {
            get {
                return this.responseTypeField;
            }
            set {
                this.responseTypeField = value;
            }
        }
    
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute()]
        public CodeType ResponseCode {
            get {
                return this.responseCodeField;
            }
            set {
                this.responseCodeField = value;
            }
        }
    
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute()]
        public CodeType ProblemCode {
            get {
                return this.problemCodeField;
            }
            set {
                this.problemCodeField = value;
            }
        }
    
        /// <remarks/>
        [System.Xml.Serialization.XmlArrayAttribute()]
        [System.Xml.Serialization.XmlArrayItemAttribute("MatchCode", IsNullable=false)]
        public CodeType[] MatchCodes {
            get {
                return this.matchCodesField;
            }
            set {
                this.matchCodesField = value;
            }
        }
    
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute()]
        public DateType ProblemReportDate {
            get {
                return this.problemReportDateField;
            }
            set {
                this.problemReportDateField = value;
            }
        }
    
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute()]
        public string IssueSource {
            get {
                return this.issueSourceField;
            }
            set {
                this.issueSourceField = value;
            }
        }
    
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute()]
        public string IssueId {
            get {
                return this.issueIdField;
            }
            set {
                this.issueIdField = value;
            }
        }
    
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute()]
        public string Comment {
            get {
                return this.commentField;
            }
            set {
                this.commentField = value;
            }
        }
    
        /// <remarks/>
        public SubjectNameType SubjectName {
            get {
                return this.subjectNameField;
            }
            set {
                this.subjectNameField = value;
            }
        }
    
        /// <remarks/>
        public string AddressLine1 {
            get {
                return this.addressLine1Field;
            }
            set {
                this.addressLine1Field = value;
            }
        }
    
        /// <remarks/>
        public CityType City {
            get {
                return this.cityField;
            }
            set {
                this.cityField = value;
            }
        }
    
        /// <remarks/>
        public string State {
            get {
                return this.stateField;
            }
            set {
                this.stateField = value;
            }
        }
    
        /// <remarks/>
        public string PostalCode {
            get {
                return this.postalCodeField;
            }
            set {
                this.postalCodeField = value;
            }
        }
    
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute()]
        public string CountryCode {
            get {
                return this.countryCodeField;
            }
            set {
                this.countryCodeField = value;
            }
        }
    
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute()]
        public string LegalVerbiage {
            get {
                return this.legalVerbiageField;
            }
            set {
                this.legalVerbiageField = value;
            }
        }
    
        /// <remarks/>
        public BlankorNFlagType RegulatedNonRegulatedFlag {
            get {
                return this.regulatedNonRegulatedFlagField;
            }
            set {
                this.regulatedNonRegulatedFlagField = value;
            }
        }
    
        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool RegulatedNonRegulatedFlagSpecified {
            get {
                return this.regulatedNonRegulatedFlagFieldSpecified;
            }
            set {
                this.regulatedNonRegulatedFlagFieldSpecified = value;
            }
        }
    }
}