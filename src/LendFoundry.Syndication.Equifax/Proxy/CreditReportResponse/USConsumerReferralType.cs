namespace LendFoundry.Syndication.Equifax.Proxy.CreditReportResponse
{
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.6.1055.0")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute()]
    public partial class USConsumerReferralType {
    
        private string bureauIdField;
    
        private string nameField;
    
        private string addressLine1Field;
    
        private string addressLine2Field;
    
        private CityType cityField;
    
        private string stateField;
    
        private string postalCodeField;
    
        private TelephoneType telephoneField;
    
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute()]
        public string BureauId {
            get {
                return this.bureauIdField;
            }
            set {
                this.bureauIdField = value;
            }
        }
    
        /// <remarks/>
        public string Name {
            get {
                return this.nameField;
            }
            set {
                this.nameField = value;
            }
        }
    
        /// <remarks/>
        public string AddressLine1 {
            get {
                return this.addressLine1Field;
            }
            set {
                this.addressLine1Field = value;
            }
        }
    
        /// <remarks/>
        public string AddressLine2 {
            get {
                return this.addressLine2Field;
            }
            set {
                this.addressLine2Field = value;
            }
        }
    
        /// <remarks/>
        public CityType City {
            get {
                return this.cityField;
            }
            set {
                this.cityField = value;
            }
        }
    
        /// <remarks/>
        public string State {
            get {
                return this.stateField;
            }
            set {
                this.stateField = value;
            }
        }
    
        /// <remarks/>
        public string PostalCode {
            get {
                return this.postalCodeField;
            }
            set {
                this.postalCodeField = value;
            }
        }
    
        /// <remarks/>
        public TelephoneType Telephone {
            get {
                return this.telephoneField;
            }
            set {
                this.telephoneField = value;
            }
        }
    }
}