namespace LendFoundry.Syndication.Equifax.Proxy.CreditReportResponse
{
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.6.1055.0")]
    [System.SerializableAttribute()]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType=true)]
    [System.Xml.Serialization.XmlRootAttribute( IsNullable=false)]
    public enum ECOAInquiryType {
    
        /// <remarks/>
        I,
    
        /// <remarks/>
        J,
    
        /// <remarks/>
        P,
    
        /// <remarks/>
        S,
    }
}