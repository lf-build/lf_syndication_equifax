namespace LendFoundry.Syndication.Equifax.Proxy.CreditReportResponse
{
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.6.1055.0")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute()]
    public partial class USSubjectDeathType {
    
        private DateType dateOfDeathField;
    
        /// <remarks/>
        public DateType DateOfDeath {
            get {
                return this.dateOfDeathField;
            }
            set {
                this.dateOfDeathField = value;
            }
        }
    }
}