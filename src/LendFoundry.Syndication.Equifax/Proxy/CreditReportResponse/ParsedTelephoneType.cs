namespace LendFoundry.Syndication.Equifax.Proxy.CreditReportResponse
{
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.6.1055.0")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute()]
    public partial class ParsedTelephoneType {
    
        private short areaCodeField;
    
        private bool areaCodeFieldSpecified;
    
        private string numberField;
    
        private short extensionField;
    
        private bool extensionFieldSpecified;
    
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute()]
        public short AreaCode {
            get {
                return this.areaCodeField;
            }
            set {
                this.areaCodeField = value;
            }
        }
    
        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool AreaCodeSpecified {
            get {
                return this.areaCodeFieldSpecified;
            }
            set {
                this.areaCodeFieldSpecified = value;
            }
        }
    
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute()]
        public string Number {
            get {
                return this.numberField;
            }
            set {
                this.numberField = value;
            }
        }
    
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute()]
        public short Extension {
            get {
                return this.extensionField;
            }
            set {
                this.extensionField = value;
            }
        }
    
        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool ExtensionSpecified {
            get {
                return this.extensionFieldSpecified;
            }
            set {
                this.extensionFieldSpecified = value;
            }
        }
    }
}