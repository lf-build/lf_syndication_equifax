namespace LendFoundry.Syndication.Equifax.Proxy.CreditReportResponse
{
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.6.1055.0")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute()]
    public partial class PropertyDataAndAnalyticMKTType {
    
        private CodeType hitNoHitIndicatorField;
    
        private string percentofNonOwnerOccupiedSegmentField;
    
        private string percentofNonOwnerOccupiedZipField;
    
        private string percentSpreadMedianListSaleSegField;
    
        private string percentSpreadMedianListSaleZipField;
    
        private string avgDaysonMarketSegmentField;
    
        private string avgDaysonMarketZipField;
    
        private short avgLotSizeField;
    
        private bool avgLotSizeFieldSpecified;
    
        private short avgMainSquareFeetField;
    
        private bool avgMainSquareFeetFieldSpecified;
    
        private short avgNumberofBathroomsField;
    
        private bool avgNumberofBathroomsFieldSpecified;
    
        private short avgNumberofBedroomsField;
    
        private bool avgNumberofBedroomsFieldSpecified;
    
        private short avgNumberofMonthlySalesField;
    
        private bool avgNumberofMonthlySalesFieldSpecified;
    
        private short avgNumberofMonthlySalesZipField;
    
        private bool avgNumberofMonthlySalesZipFieldSpecified;
    
        private short avgPricePerSquareFootField;
    
        private bool avgPricePerSquareFootFieldSpecified;
    
        private short avgYearsofConstructionField;
    
        private bool avgYearsofConstructionFieldSpecified;
    
        private string estMonthsofCurrentInvestmentField;
    
        private string estMonthsofCurrentInvestmentZipField;
    
        private string highListRangeSegField;
    
        private string highListRangeZipField;
    
        private string highSaleRangeSegField;
    
        private string highSaleRangeZipField;
    
        private string lowListRangeSegField;
    
        private string lowListRangeZipField;
    
        private string lowSaleRangeSegField;
    
        private string lowSaleRangeZipField;
    
        private string medianSaleField;
    
        private string medianSaleZipField;
    
        private string noticeofDefaultRateSegField;
    
        private string noticeofDefaultRateZipField;
    
        private string noticeofTrusteeSaleRateSegField;
    
        private string noticeofTrusteeSaleRateZipField;
    
        private string reoRate1stQtrField;
    
        private string reoRate1stQtrZipField;
    
        private string reoRate1YearSegmentField;
    
        private string reoRate1yearZipField;
    
        private string reoRate2YearSegmentField;
    
        private string reoRate2yearZipField;
    
        private string reoSaleRate1stQtrField;
    
        private string reoSaleRate1stQtrZipField;
    
        private string reoSaleRate1YearSegmentField;
    
        private string reoSaleRate1YearZipField;
    
        private string reoSaleRate2YearSegmentField;
    
        private string reoSaleRate2YearZipField;
    
        private string totalLoanValueSegField;
    
        private string totalLoanValueZipField;
    
        /// <remarks/>
        public CodeType HitNoHitIndicator {
            get {
                return this.hitNoHitIndicatorField;
            }
            set {
                this.hitNoHitIndicatorField = value;
            }
        }
    
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute()]
        public string PercentofNonOwnerOccupiedSegment {
            get {
                return this.percentofNonOwnerOccupiedSegmentField;
            }
            set {
                this.percentofNonOwnerOccupiedSegmentField = value;
            }
        }
    
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute()]
        public string PercentofNonOwnerOccupiedZip {
            get {
                return this.percentofNonOwnerOccupiedZipField;
            }
            set {
                this.percentofNonOwnerOccupiedZipField = value;
            }
        }
    
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute()]
        public string PercentSpreadMedianListSaleSeg {
            get {
                return this.percentSpreadMedianListSaleSegField;
            }
            set {
                this.percentSpreadMedianListSaleSegField = value;
            }
        }
    
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute()]
        public string PercentSpreadMedianListSaleZip {
            get {
                return this.percentSpreadMedianListSaleZipField;
            }
            set {
                this.percentSpreadMedianListSaleZipField = value;
            }
        }
    
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute()]
        public string AvgDaysonMarketSegment {
            get {
                return this.avgDaysonMarketSegmentField;
            }
            set {
                this.avgDaysonMarketSegmentField = value;
            }
        }
    
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute()]
        public string AvgDaysonMarketZip {
            get {
                return this.avgDaysonMarketZipField;
            }
            set {
                this.avgDaysonMarketZipField = value;
            }
        }
    
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute()]
        public short AvgLotSize {
            get {
                return this.avgLotSizeField;
            }
            set {
                this.avgLotSizeField = value;
            }
        }
    
        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool AvgLotSizeSpecified {
            get {
                return this.avgLotSizeFieldSpecified;
            }
            set {
                this.avgLotSizeFieldSpecified = value;
            }
        }
    
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute()]
        public short AvgMainSquareFeet {
            get {
                return this.avgMainSquareFeetField;
            }
            set {
                this.avgMainSquareFeetField = value;
            }
        }
    
        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool AvgMainSquareFeetSpecified {
            get {
                return this.avgMainSquareFeetFieldSpecified;
            }
            set {
                this.avgMainSquareFeetFieldSpecified = value;
            }
        }
    
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute()]
        public short AvgNumberofBathrooms {
            get {
                return this.avgNumberofBathroomsField;
            }
            set {
                this.avgNumberofBathroomsField = value;
            }
        }
    
        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool AvgNumberofBathroomsSpecified {
            get {
                return this.avgNumberofBathroomsFieldSpecified;
            }
            set {
                this.avgNumberofBathroomsFieldSpecified = value;
            }
        }
    
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute()]
        public short AvgNumberofBedrooms {
            get {
                return this.avgNumberofBedroomsField;
            }
            set {
                this.avgNumberofBedroomsField = value;
            }
        }
    
        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool AvgNumberofBedroomsSpecified {
            get {
                return this.avgNumberofBedroomsFieldSpecified;
            }
            set {
                this.avgNumberofBedroomsFieldSpecified = value;
            }
        }
    
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute()]
        public short AvgNumberofMonthlySales {
            get {
                return this.avgNumberofMonthlySalesField;
            }
            set {
                this.avgNumberofMonthlySalesField = value;
            }
        }
    
        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool AvgNumberofMonthlySalesSpecified {
            get {
                return this.avgNumberofMonthlySalesFieldSpecified;
            }
            set {
                this.avgNumberofMonthlySalesFieldSpecified = value;
            }
        }
    
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute()]
        public short AvgNumberofMonthlySalesZip {
            get {
                return this.avgNumberofMonthlySalesZipField;
            }
            set {
                this.avgNumberofMonthlySalesZipField = value;
            }
        }
    
        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool AvgNumberofMonthlySalesZipSpecified {
            get {
                return this.avgNumberofMonthlySalesZipFieldSpecified;
            }
            set {
                this.avgNumberofMonthlySalesZipFieldSpecified = value;
            }
        }
    
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute()]
        public short AvgPricePerSquareFoot {
            get {
                return this.avgPricePerSquareFootField;
            }
            set {
                this.avgPricePerSquareFootField = value;
            }
        }
    
        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool AvgPricePerSquareFootSpecified {
            get {
                return this.avgPricePerSquareFootFieldSpecified;
            }
            set {
                this.avgPricePerSquareFootFieldSpecified = value;
            }
        }
    
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute()]
        public short AvgYearsofConstruction {
            get {
                return this.avgYearsofConstructionField;
            }
            set {
                this.avgYearsofConstructionField = value;
            }
        }
    
        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool AvgYearsofConstructionSpecified {
            get {
                return this.avgYearsofConstructionFieldSpecified;
            }
            set {
                this.avgYearsofConstructionFieldSpecified = value;
            }
        }
    
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute()]
        public string EstMonthsofCurrentInvestment {
            get {
                return this.estMonthsofCurrentInvestmentField;
            }
            set {
                this.estMonthsofCurrentInvestmentField = value;
            }
        }
    
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute()]
        public string EstMonthsofCurrentInvestmentZip {
            get {
                return this.estMonthsofCurrentInvestmentZipField;
            }
            set {
                this.estMonthsofCurrentInvestmentZipField = value;
            }
        }
    
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute()]
        public string HighListRangeSeg {
            get {
                return this.highListRangeSegField;
            }
            set {
                this.highListRangeSegField = value;
            }
        }
    
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute()]
        public string HighListRangeZip {
            get {
                return this.highListRangeZipField;
            }
            set {
                this.highListRangeZipField = value;
            }
        }
    
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute()]
        public string HighSaleRangeSeg {
            get {
                return this.highSaleRangeSegField;
            }
            set {
                this.highSaleRangeSegField = value;
            }
        }
    
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute()]
        public string HighSaleRangeZip {
            get {
                return this.highSaleRangeZipField;
            }
            set {
                this.highSaleRangeZipField = value;
            }
        }
    
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute()]
        public string LowListRangeSeg {
            get {
                return this.lowListRangeSegField;
            }
            set {
                this.lowListRangeSegField = value;
            }
        }
    
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute()]
        public string LowListRangeZip {
            get {
                return this.lowListRangeZipField;
            }
            set {
                this.lowListRangeZipField = value;
            }
        }
    
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute()]
        public string LowSaleRangeSeg {
            get {
                return this.lowSaleRangeSegField;
            }
            set {
                this.lowSaleRangeSegField = value;
            }
        }
    
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute()]
        public string LowSaleRangeZip {
            get {
                return this.lowSaleRangeZipField;
            }
            set {
                this.lowSaleRangeZipField = value;
            }
        }
    
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute()]
        public string MedianSale {
            get {
                return this.medianSaleField;
            }
            set {
                this.medianSaleField = value;
            }
        }
    
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute()]
        public string MedianSaleZip {
            get {
                return this.medianSaleZipField;
            }
            set {
                this.medianSaleZipField = value;
            }
        }
    
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute()]
        public string NoticeofDefaultRateSeg {
            get {
                return this.noticeofDefaultRateSegField;
            }
            set {
                this.noticeofDefaultRateSegField = value;
            }
        }
    
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute()]
        public string NoticeofDefaultRateZip {
            get {
                return this.noticeofDefaultRateZipField;
            }
            set {
                this.noticeofDefaultRateZipField = value;
            }
        }
    
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute()]
        public string NoticeofTrusteeSaleRateSeg {
            get {
                return this.noticeofTrusteeSaleRateSegField;
            }
            set {
                this.noticeofTrusteeSaleRateSegField = value;
            }
        }
    
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute()]
        public string NoticeofTrusteeSaleRateZip {
            get {
                return this.noticeofTrusteeSaleRateZipField;
            }
            set {
                this.noticeofTrusteeSaleRateZipField = value;
            }
        }
    
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute()]
        public string ReoRate1stQtr {
            get {
                return this.reoRate1stQtrField;
            }
            set {
                this.reoRate1stQtrField = value;
            }
        }
    
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute()]
        public string ReoRate1stQtrZip {
            get {
                return this.reoRate1stQtrZipField;
            }
            set {
                this.reoRate1stQtrZipField = value;
            }
        }
    
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute()]
        public string ReoRate1YearSegment {
            get {
                return this.reoRate1YearSegmentField;
            }
            set {
                this.reoRate1YearSegmentField = value;
            }
        }
    
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute()]
        public string ReoRate1yearZip {
            get {
                return this.reoRate1yearZipField;
            }
            set {
                this.reoRate1yearZipField = value;
            }
        }
    
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute()]
        public string ReoRate2YearSegment {
            get {
                return this.reoRate2YearSegmentField;
            }
            set {
                this.reoRate2YearSegmentField = value;
            }
        }
    
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute()]
        public string ReoRate2yearZip {
            get {
                return this.reoRate2yearZipField;
            }
            set {
                this.reoRate2yearZipField = value;
            }
        }
    
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute()]
        public string ReoSaleRate1stQtr {
            get {
                return this.reoSaleRate1stQtrField;
            }
            set {
                this.reoSaleRate1stQtrField = value;
            }
        }
    
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute()]
        public string ReoSaleRate1stQtrZip {
            get {
                return this.reoSaleRate1stQtrZipField;
            }
            set {
                this.reoSaleRate1stQtrZipField = value;
            }
        }
    
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute()]
        public string ReoSaleRate1YearSegment {
            get {
                return this.reoSaleRate1YearSegmentField;
            }
            set {
                this.reoSaleRate1YearSegmentField = value;
            }
        }
    
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute()]
        public string ReoSaleRate1YearZip {
            get {
                return this.reoSaleRate1YearZipField;
            }
            set {
                this.reoSaleRate1YearZipField = value;
            }
        }
    
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute()]
        public string ReoSaleRate2YearSegment {
            get {
                return this.reoSaleRate2YearSegmentField;
            }
            set {
                this.reoSaleRate2YearSegmentField = value;
            }
        }
    
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute()]
        public string ReoSaleRate2YearZip {
            get {
                return this.reoSaleRate2YearZipField;
            }
            set {
                this.reoSaleRate2YearZipField = value;
            }
        }
    
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute()]
        public string TotalLoanValueSeg {
            get {
                return this.totalLoanValueSegField;
            }
            set {
                this.totalLoanValueSegField = value;
            }
        }
    
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute()]
        public string TotalLoanValueZip {
            get {
                return this.totalLoanValueZipField;
            }
            set {
                this.totalLoanValueZipField = value;
            }
        }
    }
}