namespace LendFoundry.Syndication.Equifax.Proxy.CreditReportResponse
{
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.6.1055.0")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType=true)]
    public partial class USHeaderTypeSubjectSubjectId {
    
        private string subjectSSNField;
    
        private DateType dateOfBirthField;
    
        private short ageField;
    
        private bool ageFieldSpecified;
    
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute()]
        public string SubjectSSN {
            get {
                return this.subjectSSNField;
            }
            set {
                this.subjectSSNField = value;
            }
        }
    
        /// <remarks/>
        public DateType DateOfBirth {
            get {
                return this.dateOfBirthField;
            }
            set {
                this.dateOfBirthField = value;
            }
        }
    
        /// <remarks/>
        public short Age {
            get {
                return this.ageField;
            }
            set {
                this.ageField = value;
            }
        }
    
        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool AgeSpecified {
            get {
                return this.ageFieldSpecified;
            }
            set {
                this.ageFieldSpecified = value;
            }
        }
    }
}