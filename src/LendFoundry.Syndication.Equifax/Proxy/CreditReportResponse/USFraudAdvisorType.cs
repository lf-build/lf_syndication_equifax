namespace LendFoundry.Syndication.Equifax.Proxy.CreditReportResponse
{
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.6.1055.0")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute()]
    public partial class USFraudAdvisorType {
    
        private string fraudAdvisorVersionField;
    
        private string fraudAdvisorIndexField;
    
        private ScoreReasonType[] warningsField;
    
        private CodeType rejectCodeField;
    
        private string firstNameCounterField;
    
        private string lastNameCounterField;
    
        private string businessNameCounterField;
    
        private string streetAddressCounterField;
    
        private string sSNCounterField;
    
        private string homePhoneCounterField;
    
        private string workPhoneCounterField;
    
        private string dateOfBirthConfirmedField;
    
        private string driversLicenseConfirmedField;
    
        private string eMailAddressConfirmedField;
    
        private string sSNVerifiedLevelField;
    
        private string numberVerifiedFieldsGTZeroField;
    
        private string sumOfVerifiedCountersField;
    
        private string iDAdvisorIndexField;
    
        private ScoreReasonType[] iDAdvisorWarningsField;
    
        private string firstNameField;
    
        private string lastNameField;
    
        private string addressLine1Field;
    
        private string addressLine2Field;
    
        private CityType cityField;
    
        private string stateField;
    
        private string postalCodeField;
    
        private string subjectSSNField;
    
        private DateType dateOfBirthField;
    
        private TelephoneType telephoneField;
    
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute()]
        public string FraudAdvisorVersion {
            get {
                return this.fraudAdvisorVersionField;
            }
            set {
                this.fraudAdvisorVersionField = value;
            }
        }
    
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute()]
        public string FraudAdvisorIndex {
            get {
                return this.fraudAdvisorIndexField;
            }
            set {
                this.fraudAdvisorIndexField = value;
            }
        }
    
        /// <remarks/>
        [System.Xml.Serialization.XmlArrayAttribute()]
        [System.Xml.Serialization.XmlArrayItemAttribute("Warning", IsNullable=false)]
        public ScoreReasonType[] Warnings {
            get {
                return this.warningsField;
            }
            set {
                this.warningsField = value;
            }
        }
    
        /// <remarks/>
        public CodeType RejectCode {
            get {
                return this.rejectCodeField;
            }
            set {
                this.rejectCodeField = value;
            }
        }
    
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute()]
        public string FirstNameCounter {
            get {
                return this.firstNameCounterField;
            }
            set {
                this.firstNameCounterField = value;
            }
        }
    
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute()]
        public string LastNameCounter {
            get {
                return this.lastNameCounterField;
            }
            set {
                this.lastNameCounterField = value;
            }
        }
    
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute()]
        public string BusinessNameCounter {
            get {
                return this.businessNameCounterField;
            }
            set {
                this.businessNameCounterField = value;
            }
        }
    
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute()]
        public string StreetAddressCounter {
            get {
                return this.streetAddressCounterField;
            }
            set {
                this.streetAddressCounterField = value;
            }
        }
    
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute()]
        public string SSNCounter {
            get {
                return this.sSNCounterField;
            }
            set {
                this.sSNCounterField = value;
            }
        }
    
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute()]
        public string HomePhoneCounter {
            get {
                return this.homePhoneCounterField;
            }
            set {
                this.homePhoneCounterField = value;
            }
        }
    
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute()]
        public string WorkPhoneCounter {
            get {
                return this.workPhoneCounterField;
            }
            set {
                this.workPhoneCounterField = value;
            }
        }
    
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute()]
        public string DateOfBirthConfirmed {
            get {
                return this.dateOfBirthConfirmedField;
            }
            set {
                this.dateOfBirthConfirmedField = value;
            }
        }
    
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute()]
        public string DriversLicenseConfirmed {
            get {
                return this.driversLicenseConfirmedField;
            }
            set {
                this.driversLicenseConfirmedField = value;
            }
        }
    
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute()]
        public string EMailAddressConfirmed {
            get {
                return this.eMailAddressConfirmedField;
            }
            set {
                this.eMailAddressConfirmedField = value;
            }
        }
    
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute()]
        public string SSNVerifiedLevel {
            get {
                return this.sSNVerifiedLevelField;
            }
            set {
                this.sSNVerifiedLevelField = value;
            }
        }
    
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute()]
        public string NumberVerifiedFieldsGTZero {
            get {
                return this.numberVerifiedFieldsGTZeroField;
            }
            set {
                this.numberVerifiedFieldsGTZeroField = value;
            }
        }
    
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute()]
        public string SumOfVerifiedCounters {
            get {
                return this.sumOfVerifiedCountersField;
            }
            set {
                this.sumOfVerifiedCountersField = value;
            }
        }
    
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute()]
        public string IDAdvisorIndex {
            get {
                return this.iDAdvisorIndexField;
            }
            set {
                this.iDAdvisorIndexField = value;
            }
        }
    
        /// <remarks/>
        [System.Xml.Serialization.XmlArrayAttribute()]
        [System.Xml.Serialization.XmlArrayItemAttribute("IDAdvisorWarning", IsNullable=false)]
        public ScoreReasonType[] IDAdvisorWarnings {
            get {
                return this.iDAdvisorWarningsField;
            }
            set {
                this.iDAdvisorWarningsField = value;
            }
        }
    
        /// <remarks/>
        public string FirstName {
            get {
                return this.firstNameField;
            }
            set {
                this.firstNameField = value;
            }
        }
    
        /// <remarks/>
        public string LastName {
            get {
                return this.lastNameField;
            }
            set {
                this.lastNameField = value;
            }
        }
    
        /// <remarks/>
        public string AddressLine1 {
            get {
                return this.addressLine1Field;
            }
            set {
                this.addressLine1Field = value;
            }
        }
    
        /// <remarks/>
        public string AddressLine2 {
            get {
                return this.addressLine2Field;
            }
            set {
                this.addressLine2Field = value;
            }
        }
    
        /// <remarks/>
        public CityType City {
            get {
                return this.cityField;
            }
            set {
                this.cityField = value;
            }
        }
    
        /// <remarks/>
        public string State {
            get {
                return this.stateField;
            }
            set {
                this.stateField = value;
            }
        }
    
        /// <remarks/>
        public string PostalCode {
            get {
                return this.postalCodeField;
            }
            set {
                this.postalCodeField = value;
            }
        }
    
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute()]
        public string SubjectSSN {
            get {
                return this.subjectSSNField;
            }
            set {
                this.subjectSSNField = value;
            }
        }
    
        /// <remarks/>
        public DateType DateOfBirth {
            get {
                return this.dateOfBirthField;
            }
            set {
                this.dateOfBirthField = value;
            }
        }
    
        /// <remarks/>
        public TelephoneType Telephone {
            get {
                return this.telephoneField;
            }
            set {
                this.telephoneField = value;
            }
        }
    }
}