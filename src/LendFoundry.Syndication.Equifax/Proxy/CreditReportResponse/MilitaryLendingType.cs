namespace LendFoundry.Syndication.Equifax.Proxy.CreditReportResponse
{
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.6.1055.0")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute()]
    public partial class MilitaryLendingType {
    
        private CodeType regulatedIdentifierField;
    
        private string disclaimerField;
    
        private string coveredBorrowerStatusField;
    
        private string insufficientDataForMatchField;
    
        private string referralContactNumberField;
    
        /// <remarks/>
        public CodeType RegulatedIdentifier {
            get {
                return this.regulatedIdentifierField;
            }
            set {
                this.regulatedIdentifierField = value;
            }
        }
    
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute()]
        public string Disclaimer {
            get {
                return this.disclaimerField;
            }
            set {
                this.disclaimerField = value;
            }
        }
    
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute()]
        public string CoveredBorrowerStatus {
            get {
                return this.coveredBorrowerStatusField;
            }
            set {
                this.coveredBorrowerStatusField = value;
            }
        }
    
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute()]
        public string InsufficientDataForMatch {
            get {
                return this.insufficientDataForMatchField;
            }
            set {
                this.insufficientDataForMatchField = value;
            }
        }
    
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute()]
        public string ReferralContactNumber {
            get {
                return this.referralContactNumberField;
            }
            set {
                this.referralContactNumberField = value;
            }
        }
    }
}