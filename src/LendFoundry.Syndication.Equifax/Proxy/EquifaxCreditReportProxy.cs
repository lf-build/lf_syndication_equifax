using RestSharp;
using System;
using System.Net;

namespace LendFoundry.Syndication.Equifax.Proxy
{
    public class EquifaxCreditReportProxy : IEquifaxCreditReportProxy
    {
        private IEquifaxCreditReportConfiguration EquifaxCreditReportConfiguration { get; }

        public EquifaxCreditReportProxy(IEquifaxCreditReportConfiguration creditReportConfiguration)
        {
            if (creditReportConfiguration == null)
                throw new ArgumentNullException(nameof(creditReportConfiguration));

            if (string.IsNullOrWhiteSpace(creditReportConfiguration.SiteUrl))
                throw new ArgumentException("SiteUrl is required", nameof(creditReportConfiguration.SiteUrl));

            if (creditReportConfiguration.CustomerConfiguration == null)
                throw new ArgumentNullException(nameof(creditReportConfiguration.CustomerConfiguration));

            if (string.IsNullOrWhiteSpace(creditReportConfiguration.SiteId))
                throw new ArgumentException("SiteId is required", nameof(creditReportConfiguration.SiteId));

            if (string.IsNullOrWhiteSpace(creditReportConfiguration.ServiceName))
                throw new ArgumentException("ServiceName is required", nameof(creditReportConfiguration.ServiceName));

            if (string.IsNullOrWhiteSpace(creditReportConfiguration.SiteUserName))
                throw new ArgumentException("Username is required", nameof(creditReportConfiguration.SiteUserName));

            if (string.IsNullOrWhiteSpace(creditReportConfiguration.SiteUserPassword))
                throw new ArgumentException("User Password is required", nameof(creditReportConfiguration.SiteUserPassword));

            if (string.IsNullOrWhiteSpace(creditReportConfiguration.Version))
                throw new ArgumentException("Version is required", nameof(creditReportConfiguration.Version));

            if (string.IsNullOrWhiteSpace(creditReportConfiguration.CustomerConfiguration.CustomerNumber))
                throw new ArgumentException("CustomerNumber is required", nameof(creditReportConfiguration.CustomerConfiguration.CustomerNumber));

            if (string.IsNullOrWhiteSpace(creditReportConfiguration.CustomerConfiguration.SecurityCode))
                throw new ArgumentException("SecurityCode is required", nameof(creditReportConfiguration.CustomerConfiguration.SecurityCode));

            if (string.IsNullOrWhiteSpace(creditReportConfiguration.CustomerConfiguration.CustomerCode))
                throw new ArgumentException("CustomerCode is required", nameof(creditReportConfiguration.CustomerConfiguration.CustomerCode));

            if (string.IsNullOrWhiteSpace(creditReportConfiguration.OptionalFeaturesConfiguration.OutputFormat))
                throw new ArgumentException("OutputFormat is required", nameof(creditReportConfiguration.OptionalFeaturesConfiguration.OutputFormat));

            if (string.IsNullOrWhiteSpace(creditReportConfiguration.OptionalFeaturesConfiguration.MultipleFileIndicator))
                throw new ArgumentException("MultipleFileIndicator is required", nameof(creditReportConfiguration.OptionalFeaturesConfiguration.MultipleFileIndicator));

            EquifaxCreditReportConfiguration = creditReportConfiguration;
        }

        public CreditReportResponse.EfxTransmit GetCreditReport(CreditReportRequest.EfxReceive creditReportRequest)
        {
            if (creditReportRequest == null)
                throw new ArgumentNullException(nameof(creditReportRequest));

            var client = new RestClient(EquifaxCreditReportConfiguration.SiteUrl);

            var request = new RestRequest(Method.POST)
            {
                Credentials = new NetworkCredential()
                {
                    UserName = EquifaxCreditReportConfiguration.SiteUserName,
                    Password = EquifaxCreditReportConfiguration.SiteUserPassword
                }               
            };
            
            var efxRequest = XmlSerialization.SerializeObject(creditReportRequest);

            if (efxRequest == null)
                throw new ArgumentNullException(nameof(efxRequest));

            request.AddParameter("site_id", EquifaxCreditReportConfiguration.SiteId);
            request.AddParameter("service_name", EquifaxCreditReportConfiguration.ServiceName);
            request.AddParameter("efx_request", efxRequest);
            request.AddHeader("content-type", "application/x-www-form-urlencoded");
            request.AddHeader("Accept", "application/xml");

            var objectResponse = ExecuteRequest<CreditReportResponse.EfxTransmit>(client, request);

            return objectResponse;
        }

        private static T ExecuteRequest<T>(IRestClient client, IRestRequest request) where T : class
        {
            var response = client.Execute(request);
            if (response == null)
                throw new ArgumentNullException(nameof(response));

            if (response.ErrorException != null)
                throw new EquifaxException("Service call failed", response.ErrorException);

            if (response.ResponseStatus != ResponseStatus.Completed)
                throw new EquifaxException(
                    $"Service call failed. Status {response.ResponseStatus}. Response: {response.Content ?? ""}");

            if (response.ResponseStatus == ResponseStatus.Completed)
            {
                if (response.StatusCode.ToString().ToLower() == "methodnotallowed")
                    throw new EquifaxException(
                        $"Service call failed. Status {response.ResponseStatus}. Response: {response.StatusDescription ?? ""}");
            }

            if (response.Content == "???MUST BE MX, NEW OR IQ???")
                throw new EquifaxException($"Service call failed. " + response.Content);

            return XmlSerialization.DeserializeObject<T>(response.Content);
        }
    }
}