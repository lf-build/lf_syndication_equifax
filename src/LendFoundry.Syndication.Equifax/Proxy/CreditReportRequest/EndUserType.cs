namespace LendFoundry.Syndication.Equifax.Proxy.CreditReportRequest
{
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.6.1055.0")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute()]
    public partial class EndUserType {
    
        private string endUserNameField;
    
        private string permissiblePurposeCodeField;
    
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute()]
        public string EndUserName {
            get {
                return this.endUserNameField;
            }
            set {
                this.endUserNameField = value;
            }
        }
    
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute()]
        public string PermissiblePurposeCode {
            get {
                return this.permissiblePurposeCodeField;
            }
            set {
                this.permissiblePurposeCodeField = value;
            }
        }
    }
}