namespace LendFoundry.Syndication.Equifax.Proxy.CreditReportRequest
{
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.6.1055.0")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute()]
    public partial class FirstSearchType {
    
        private string searchTypeField;
    
        private string maxNumberResponsesField;
    
        private string customerTrackingNumberField;
    
        private string telephoneNumbersField;
    
        private YNFlagType creditReportIndicatorField;
    
        public FirstSearchType() {
            this.creditReportIndicatorField = YNFlagType.N;
        }
    
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute()]
        public string SearchType {
            get {
                return this.searchTypeField;
            }
            set {
                this.searchTypeField = value;
            }
        }
    
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute()]
        public string MaxNumberResponses {
            get {
                return this.maxNumberResponsesField;
            }
            set {
                this.maxNumberResponsesField = value;
            }
        }
    
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute()]
        public string CustomerTrackingNumber {
            get {
                return this.customerTrackingNumberField;
            }
            set {
                this.customerTrackingNumberField = value;
            }
        }
    
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute]
        public string TelephoneNumbers {
            get {
                return this.telephoneNumbersField;
            }
            set {
                this.telephoneNumbersField = value;
            }
        }
    
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute()]
        public YNFlagType CreditReportIndicator {
            get {
                return this.creditReportIndicatorField;
            }
            set {
                this.creditReportIndicatorField = value;
            }
        }
    }
}