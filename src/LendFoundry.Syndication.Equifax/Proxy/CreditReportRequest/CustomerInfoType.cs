namespace LendFoundry.Syndication.Equifax.Proxy.CreditReportRequest
{
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.6.1055.0")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute()]
    public partial class CustomerInfoType {
    
        private string customerNumberField;
    
        private string securityCodeField;
    
        private string customerCodeField;
    
        private string outputFormatField;
    
        private CustomerInfoTypeMultipleFileIndicator multipleFileIndicatorField;
    
        private string numberOfMonthsToCountInquiriesField;
    
        private string numberOfMonthsToCountMOPField;
    
        private string postForeignBureauInquiryCodeField;
    
        private string nALOrgCustomerNumberField;
    
        private string nALPlainLanguageField;
    
        private YNFlagType rawFileAppendIndicatorField;
    
        private YNFlagType printImageCDATAIncludeField;
    
        public CustomerInfoType() {
            this.multipleFileIndicatorField = CustomerInfoTypeMultipleFileIndicator.F;
            this.rawFileAppendIndicatorField = YNFlagType.N;
            this.printImageCDATAIncludeField = YNFlagType.N;
        }
    
        /// <remarks/>
        public string CustomerNumber {
            get {
                return this.customerNumberField;
            }
            set {
                this.customerNumberField = value;
            }
        }
    
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute()]
        public string SecurityCode {
            get {
                return this.securityCodeField;
            }
            set {
                this.securityCodeField = value;
            }
        }
    
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute()]
        public string CustomerCode {
            get {
                return this.customerCodeField;
            }
            set {
                this.customerCodeField = value;
            }
        }
    
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute()]
        public string OutputFormat {
            get {
                return this.outputFormatField;
            }
            set {
                this.outputFormatField = value;
            }
        }
    
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute()]
        public CustomerInfoTypeMultipleFileIndicator MultipleFileIndicator {
            get {
                return this.multipleFileIndicatorField;
            }
            set {
                this.multipleFileIndicatorField = value;
            }
        }
    
        /// <remarks/>
        public string NumberOfMonthsToCountInquiries {
            get {
                return this.numberOfMonthsToCountInquiriesField;
            }
            set {
                this.numberOfMonthsToCountInquiriesField = value;
            }
        }
    
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute()]
        public string NumberOfMonthsToCountMOP {
            get {
                return this.numberOfMonthsToCountMOPField;
            }
            set {
                this.numberOfMonthsToCountMOPField = value;
            }
        }
    
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute()]
        public string PostForeignBureauInquiryCode {
            get {
                return this.postForeignBureauInquiryCodeField;
            }
            set {
                this.postForeignBureauInquiryCodeField = value;
            }
        }
    
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute()]
        public string NALOrgCustomerNumber {
            get {
                return this.nALOrgCustomerNumberField;
            }
            set {
                this.nALOrgCustomerNumberField = value;
            }
        }
    
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute()]
        public string NALPlainLanguage {
            get {
                return this.nALPlainLanguageField;
            }
            set {
                this.nALPlainLanguageField = value;
            }
        }
    
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute()]
        [System.ComponentModel.DefaultValueAttribute(YNFlagType.Y)]
        public YNFlagType RawFileAppendIndicator {
            get {
                return this.rawFileAppendIndicatorField;
            }
            set {
                this.rawFileAppendIndicatorField = value;
            }
        }
    
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute()]
        [System.ComponentModel.DefaultValueAttribute(YNFlagType.N)]
        public YNFlagType PrintImageCDATAInclude {
            get {
                return this.printImageCDATAIncludeField;
            }
            set {
                this.printImageCDATAIncludeField = value;
            }
        }
    }
}