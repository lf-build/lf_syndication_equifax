namespace LendFoundry.Syndication.Equifax.Proxy.CreditReportRequest
{
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.6.1055.0")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType=true)]
    [System.Xml.Serialization.XmlRootAttribute( IsNullable=false)]
    public partial class EfxReceive {
    
        private CustomerInfoType customerInfoField;
    
        private EfxRequestType[] efxRequestField;
    
        private string versionField;
    
        private string tranIDField;
    
        private string customerNumberField;
    
        private string securityCodeField;
    
        /// <remarks/>
        public CustomerInfoType CustomerInfo {
            get {
                return this.customerInfoField;
            }
            set {
                this.customerInfoField = value;
            }
        }
    
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute("EfxRequest")]
        public EfxRequestType[] EfxRequest {
            get {
                return this.efxRequestField;
            }
            set {
                this.efxRequestField = value;
            }
        }
    
        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string version {
            get {
                return this.versionField;
            }
            set {
                this.versionField = value;
            }
        }
    
        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string tranID {
            get {
                return this.tranIDField;
            }
            set {
                this.tranIDField = value;
            }
        }
    
        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string customerNumber {
            get {
                return this.customerNumberField;
            }
            set {
                this.customerNumberField = value;
            }
        }
    
        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string securityCode {
            get {
                return this.securityCodeField;
            }
            set {
                this.securityCodeField = value;
            }
        }
    }
}