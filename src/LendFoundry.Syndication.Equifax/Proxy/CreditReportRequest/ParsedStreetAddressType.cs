namespace LendFoundry.Syndication.Equifax.Proxy.CreditReportRequest
{
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.6.1055.0")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute()]
    public partial class ParsedStreetAddressType {
    
        private string streetPreDirectionField;
    
        private string streetNumberField;
    
        private string streetNameField;
    
        private string streetTypeField;
    
        private string streetPostDirectionField;
    
        private string unitDesignatorField;
    
        private string unitNumberField;
    
        private string urbanizationCondoField;
    
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute()]
        public string StreetPreDirection {
            get {
                return this.streetPreDirectionField;
            }
            set {
                this.streetPreDirectionField = value;
            }
        }
    
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute()]
        public string StreetNumber {
            get {
                return this.streetNumberField;
            }
            set {
                this.streetNumberField = value;
            }
        }
    
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute()]
        public string StreetName {
            get {
                return this.streetNameField;
            }
            set {
                this.streetNameField = value;
            }
        }
    
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute()]
        public string StreetType {
            get {
                return this.streetTypeField;
            }
            set {
                this.streetTypeField = value;
            }
        }
    
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute()]
        public string StreetPostDirection {
            get {
                return this.streetPostDirectionField;
            }
            set {
                this.streetPostDirectionField = value;
            }
        }
    
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute()]
        public string UnitDesignator {
            get {
                return this.unitDesignatorField;
            }
            set {
                this.unitDesignatorField = value;
            }
        }
    
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute()]
        public string UnitNumber {
            get {
                return this.unitNumberField;
            }
            set {
                this.unitNumberField = value;
            }
        }
    
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute()]
        public string UrbanizationCondo {
            get {
                return this.urbanizationCondoField;
            }
            set {
                this.urbanizationCondoField = value;
            }
        }
    }
}