namespace LendFoundry.Syndication.Equifax.Proxy.CreditReportRequest
{
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.6.1055.0")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute()]
    public partial class ProductInfoType {
    
        private string driversLicenseStateField;
    
        private string driversLicenseNumberField;
    
        private string coApplicantDriversLicenseStateField;
    
        private string coApplicantDriversLicenseNumberField;
    
        private CurrencyType subjectMonthlyIncomeField;
    
        private bool subjectMonthlyIncomeFieldSpecified;
    
        private CurrencyType coApplicantMonthlyIncomeField;
    
        private bool coApplicantMonthlyIncomeFieldSpecified;
    
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute()]
        public string DriversLicenseState {
            get {
                return this.driversLicenseStateField;
            }
            set {
                this.driversLicenseStateField = value;
            }
        }
    
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute()]
        public string DriversLicenseNumber {
            get {
                return this.driversLicenseNumberField;
            }
            set {
                this.driversLicenseNumberField = value;
            }
        }
    
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute()]
        public string CoApplicantDriversLicenseState {
            get {
                return this.coApplicantDriversLicenseStateField;
            }
            set {
                this.coApplicantDriversLicenseStateField = value;
            }
        }
    
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute()]
        public string CoApplicantDriversLicenseNumber {
            get {
                return this.coApplicantDriversLicenseNumberField;
            }
            set {
                this.coApplicantDriversLicenseNumberField = value;
            }
        }
    
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute()]
        public CurrencyType SubjectMonthlyIncome {
            get {
                return this.subjectMonthlyIncomeField;
            }
            set {
                this.subjectMonthlyIncomeField = value;
            }
        }
    
        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool SubjectMonthlyIncomeSpecified {
            get {
                return this.subjectMonthlyIncomeFieldSpecified;
            }
            set {
                this.subjectMonthlyIncomeFieldSpecified = value;
            }
        }
    
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute()]
        public CurrencyType CoApplicantMonthlyIncome {
            get {
                return this.coApplicantMonthlyIncomeField;
            }
            set {
                this.coApplicantMonthlyIncomeField = value;
            }
        }
    
        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool CoApplicantMonthlyIncomeSpecified {
            get {
                return this.coApplicantMonthlyIncomeFieldSpecified;
            }
            set {
                this.coApplicantMonthlyIncomeFieldSpecified = value;
            }
        }
    }
}