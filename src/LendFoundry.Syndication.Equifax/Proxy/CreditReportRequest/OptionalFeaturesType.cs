namespace LendFoundry.Syndication.Equifax.Proxy.CreditReportRequest
{
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.6.1055.0")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute()]
    public partial class OptionalFeaturesType {
    
        private YNFlagType alertContactFlagField;
    
        private YNFlagType beaconFlagField;
    
        private YNFlagType consumerReferralLocationFlagField;
    
        private YNFlagType defaultModelFlagField;
    
        private YNFlagType dimensionsDataFlagField;
    
        private YNFlagType eDASFlagField;
    
        private YNFlagType expandedAddressField;
    
        private YNFlagType expandedCollectionsField;
    
        private YNFlagType expandedInquiryField;
    
        private YNFlagType expandedTradeAll60Field;
    
        private YNFlagType expandedTradeDimensionsField;
    
        private YNFlagType expandedTradeHighValueField;
    
        private YNFlagType expandedTradeTermsField;
    
        private YNFlagType fICOFlagField;
    
        private YNFlagType mLAOnlyFlagField;
    
        private YNFlagType onLineDirectoryFlagField;
    
        private YNFlagType paymentHistory24MonthFlagField;
    
        private YNFlagType rBPAllFlagField;
    
        private YNFlagType rBPBeaconOnlyFlagField;
    
        private YNFlagType rBPFICOOnlyFlagField;
    
        private YNFlagType useFICOVerbiageFlagField;
    
        public OptionalFeaturesType() {
            this.alertContactFlagField = YNFlagType.N;
            this.beaconFlagField = YNFlagType.N;
            this.consumerReferralLocationFlagField = YNFlagType.N;
            this.defaultModelFlagField = YNFlagType.N;
            this.dimensionsDataFlagField = YNFlagType.N;
            this.eDASFlagField = YNFlagType.N;
            this.expandedAddressField = YNFlagType.N;
            this.expandedCollectionsField = YNFlagType.N;
            this.expandedInquiryField = YNFlagType.N;
            this.expandedTradeAll60Field = YNFlagType.N;
            this.expandedTradeDimensionsField = YNFlagType.N;
            this.expandedTradeHighValueField = YNFlagType.N;
            this.expandedTradeTermsField = YNFlagType.N;
            this.fICOFlagField = YNFlagType.N;
            this.mLAOnlyFlagField = YNFlagType.N;
            this.onLineDirectoryFlagField = YNFlagType.N;
            this.paymentHistory24MonthFlagField = YNFlagType.N;
            this.rBPAllFlagField = YNFlagType.N;
            this.rBPBeaconOnlyFlagField = YNFlagType.N;
            this.rBPFICOOnlyFlagField = YNFlagType.N;
            this.useFICOVerbiageFlagField = YNFlagType.N;
        }
    
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute()]
        [System.ComponentModel.DefaultValueAttribute(YNFlagType.N)]
        public YNFlagType AlertContactFlag {
            get {
                return this.alertContactFlagField;
            }
            set {
                this.alertContactFlagField = value;
            }
        }
    
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute()]
        [System.ComponentModel.DefaultValueAttribute(YNFlagType.N)]
        public YNFlagType BeaconFlag {
            get {
                return this.beaconFlagField;
            }
            set {
                this.beaconFlagField = value;
            }
        }
    
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute()]
        [System.ComponentModel.DefaultValueAttribute(YNFlagType.N)]
        public YNFlagType ConsumerReferralLocationFlag {
            get {
                return this.consumerReferralLocationFlagField;
            }
            set {
                this.consumerReferralLocationFlagField = value;
            }
        }
    
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute()]
        [System.ComponentModel.DefaultValueAttribute(YNFlagType.N)]
        public YNFlagType DefaultModelFlag {
            get {
                return this.defaultModelFlagField;
            }
            set {
                this.defaultModelFlagField = value;
            }
        }
    
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute()]
        [System.ComponentModel.DefaultValueAttribute(YNFlagType.N)]
        public YNFlagType DimensionsDataFlag {
            get {
                return this.dimensionsDataFlagField;
            }
            set {
                this.dimensionsDataFlagField = value;
            }
        }
    
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute()]
        [System.ComponentModel.DefaultValueAttribute(YNFlagType.N)]
        public YNFlagType EDASFlag {
            get {
                return this.eDASFlagField;
            }
            set {
                this.eDASFlagField = value;
            }
        }
    
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute()]
        [System.ComponentModel.DefaultValueAttribute(YNFlagType.N)]
        public YNFlagType ExpandedAddress {
            get {
                return this.expandedAddressField;
            }
            set {
                this.expandedAddressField = value;
            }
        }
    
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute()]
        [System.ComponentModel.DefaultValueAttribute(YNFlagType.N)]
        public YNFlagType ExpandedCollections {
            get {
                return this.expandedCollectionsField;
            }
            set {
                this.expandedCollectionsField = value;
            }
        }
    
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute()]
        [System.ComponentModel.DefaultValueAttribute(YNFlagType.N)]
        public YNFlagType ExpandedInquiry {
            get {
                return this.expandedInquiryField;
            }
            set {
                this.expandedInquiryField = value;
            }
        }
    
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute()]
        [System.ComponentModel.DefaultValueAttribute(YNFlagType.N)]
        public YNFlagType ExpandedTradeAll60 {
            get {
                return this.expandedTradeAll60Field;
            }
            set {
                this.expandedTradeAll60Field = value;
            }
        }
    
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute()]
        [System.ComponentModel.DefaultValueAttribute(YNFlagType.N)]
        public YNFlagType ExpandedTradeDimensions {
            get {
                return this.expandedTradeDimensionsField;
            }
            set {
                this.expandedTradeDimensionsField = value;
            }
        }
    
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute()]
        [System.ComponentModel.DefaultValueAttribute(YNFlagType.N)]
        public YNFlagType ExpandedTradeHighValue {
            get {
                return this.expandedTradeHighValueField;
            }
            set {
                this.expandedTradeHighValueField = value;
            }
        }
    
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute()]
        [System.ComponentModel.DefaultValueAttribute(YNFlagType.N)]
        public YNFlagType ExpandedTradeTerms {
            get {
                return this.expandedTradeTermsField;
            }
            set {
                this.expandedTradeTermsField = value;
            }
        }
    
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute()]
        [System.ComponentModel.DefaultValueAttribute(YNFlagType.N)]
        public YNFlagType FICOFlag {
            get {
                return this.fICOFlagField;
            }
            set {
                this.fICOFlagField = value;
            }
        }
    
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute()]
        [System.ComponentModel.DefaultValueAttribute(YNFlagType.N)]
        public YNFlagType MLAOnlyFlag {
            get {
                return this.mLAOnlyFlagField;
            }
            set {
                this.mLAOnlyFlagField = value;
            }
        }
    
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute()]
        [System.ComponentModel.DefaultValueAttribute(YNFlagType.N)]
        public YNFlagType OnLineDirectoryFlag {
            get {
                return this.onLineDirectoryFlagField;
            }
            set {
                this.onLineDirectoryFlagField = value;
            }
        }
    
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute()]
        [System.ComponentModel.DefaultValueAttribute(YNFlagType.N)]
        public YNFlagType PaymentHistory24MonthFlag {
            get {
                return this.paymentHistory24MonthFlagField;
            }
            set {
                this.paymentHistory24MonthFlagField = value;
            }
        }
    
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute()]
        [System.ComponentModel.DefaultValueAttribute(YNFlagType.N)]
        public YNFlagType RBPAllFlag {
            get {
                return this.rBPAllFlagField;
            }
            set {
                this.rBPAllFlagField = value;
            }
        }
    
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute()]
        [System.ComponentModel.DefaultValueAttribute(YNFlagType.N)]
        public YNFlagType RBPBeaconOnlyFlag {
            get {
                return this.rBPBeaconOnlyFlagField;
            }
            set {
                this.rBPBeaconOnlyFlagField = value;
            }
        }
    
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute()]
        [System.ComponentModel.DefaultValueAttribute(YNFlagType.N)]
        public YNFlagType RBPFICOOnlyFlag {
            get {
                return this.rBPFICOOnlyFlagField;
            }
            set {
                this.rBPFICOOnlyFlagField = value;
            }
        }
    
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute()]
        [System.ComponentModel.DefaultValueAttribute(YNFlagType.N)]
        public YNFlagType UseFICOVerbiageFlag {
            get {
                return this.useFICOVerbiageFlagField;
            }
            set {
                this.useFICOVerbiageFlagField = value;
            }
        }
    }
}