namespace LendFoundry.Syndication.Equifax.Proxy.CreditReportRequest
{
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.6.1055.0")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute()]
    public partial class ModelDataType {
    
        private ModelDataTypeModelInfo modelInfoField;
    
        private ModelDataTypeModelField[] modelFieldField;
    
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute()]
        public ModelDataTypeModelInfo ModelInfo {
            get {
                return this.modelInfoField;
            }
            set {
                this.modelInfoField = value;
            }
        }
    
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute("ModelField")]
        public ModelDataTypeModelField[] ModelField {
            get {
                return this.modelFieldField;
            }
            set {
                this.modelFieldField = value;
            }
        }
    }
}