namespace LendFoundry.Syndication.Equifax.Proxy.CreditReportRequest
{
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.6.1055.0")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute()]
    public partial class CouplesNamesType {
    
        private string fullNameField;
    
        private string firstNameField;
    
        private string lastNameField;
    
        private string middleNameField;
    
        private string spouseFirstNameField;
    
        private string spouseMiddleNameField;
    
        /// <remarks/>
        public string FullName {
            get {
                return this.fullNameField;
            }
            set {
                this.fullNameField = value;
            }
        }
    
        /// <remarks/>
        public string FirstName {
            get {
                return this.firstNameField;
            }
            set {
                this.firstNameField = value;
            }
        }
    
        /// <remarks/>
        public string LastName {
            get {
                return this.lastNameField;
            }
            set {
                this.lastNameField = value;
            }
        }
    
        /// <remarks/>
        public string MiddleName {
            get {
                return this.middleNameField;
            }
            set {
                this.middleNameField = value;
            }
        }
    
        /// <remarks/>
        public string SpouseFirstName {
            get {
                return this.spouseFirstNameField;
            }
            set {
                this.spouseFirstNameField = value;
            }
        }
    
        /// <remarks/>
        public string SpouseMiddleName {
            get {
                return this.spouseMiddleNameField;
            }
            set {
                this.spouseMiddleNameField = value;
            }
        }
    }
}