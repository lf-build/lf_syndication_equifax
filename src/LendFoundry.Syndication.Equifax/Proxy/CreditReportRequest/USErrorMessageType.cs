namespace LendFoundry.Syndication.Equifax.Proxy.CreditReportRequest
{
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.6.1055.0")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute()]
    public partial class USErrorMessageType {
    
        private CodeType errorTypeField;
    
        private CodeType errorField;
    
        private string messageTextField;
    
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute()]
        public CodeType ErrorType {
            get {
                return this.errorTypeField;
            }
            set {
                this.errorTypeField = value;
            }
        }
    
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute()]
        public CodeType Error {
            get {
                return this.errorField;
            }
            set {
                this.errorField = value;
            }
        }
    
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute()]
        public string MessageText {
            get {
                return this.messageTextField;
            }
            set {
                this.messageTextField = value;
            }
        }
    }
}