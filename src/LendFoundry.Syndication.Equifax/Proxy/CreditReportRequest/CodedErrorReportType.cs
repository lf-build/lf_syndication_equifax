namespace LendFoundry.Syndication.Equifax.Proxy.CreditReportRequest
{
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.6.1055.0")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute()]
    public partial class CodedErrorReportType {
    
        private string customerReferenceNumberField;
    
        private string customerNumberField;
    
        private ECOAInquiryType eCOAInquiryTypeField;
    
        private bool eCOAInquiryTypeFieldSpecified;
    
        private string numberOfMonthsToCountInquiriesField;
    
        private string numberOfMonthsToCountDelinquenciesField;
    
        private CodedErrorType[] formatErrorField;
    
        private CodedErrorType[] validityErrorField;
    
        private CodedErrorType[] processingErrorField;
    
        private string messageTextField;
    
        private string subjectTypeField;
    
        private short multipleNumberField;
    
        public CodedErrorReportType() {
            this.subjectTypeField = "Subject";
            this.multipleNumberField = ((short)(1));
        }
    
        /// <remarks/>
        public string CustomerReferenceNumber {
            get {
                return this.customerReferenceNumberField;
            }
            set {
                this.customerReferenceNumberField = value;
            }
        }
    
        /// <remarks/>
        public string CustomerNumber {
            get {
                return this.customerNumberField;
            }
            set {
                this.customerNumberField = value;
            }
        }
    
        /// <remarks/>
        public ECOAInquiryType ECOAInquiryType {
            get {
                return this.eCOAInquiryTypeField;
            }
            set {
                this.eCOAInquiryTypeField = value;
            }
        }
    
        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool ECOAInquiryTypeSpecified {
            get {
                return this.eCOAInquiryTypeFieldSpecified;
            }
            set {
                this.eCOAInquiryTypeFieldSpecified = value;
            }
        }
    
        /// <remarks/>
        public string NumberOfMonthsToCountInquiries {
            get {
                return this.numberOfMonthsToCountInquiriesField;
            }
            set {
                this.numberOfMonthsToCountInquiriesField = value;
            }
        }
    
        /// <remarks/>
        public string NumberOfMonthsToCountDelinquencies {
            get {
                return this.numberOfMonthsToCountDelinquenciesField;
            }
            set {
                this.numberOfMonthsToCountDelinquenciesField = value;
            }
        }
    
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute("FormatError")]
        public CodedErrorType[] FormatError {
            get {
                return this.formatErrorField;
            }
            set {
                this.formatErrorField = value;
            }
        }
    
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute("ValidityError")]
        public CodedErrorType[] ValidityError {
            get {
                return this.validityErrorField;
            }
            set {
                this.validityErrorField = value;
            }
        }
    
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute("ProcessingError")]
        public CodedErrorType[] ProcessingError {
            get {
                return this.processingErrorField;
            }
            set {
                this.processingErrorField = value;
            }
        }
    
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute()]
        public string MessageText {
            get {
                return this.messageTextField;
            }
            set {
                this.messageTextField = value;
            }
        }
    
        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        [System.ComponentModel.DefaultValueAttribute("Subject")]
        public string subjectType {
            get {
                return this.subjectTypeField;
            }
            set {
                this.subjectTypeField = value;
            }
        }
    
        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        [System.ComponentModel.DefaultValueAttribute(typeof(short), "1")]
        public short multipleNumber {
            get {
                return this.multipleNumberField;
            }
            set {
                this.multipleNumberField = value;
            }
        }
    }
}