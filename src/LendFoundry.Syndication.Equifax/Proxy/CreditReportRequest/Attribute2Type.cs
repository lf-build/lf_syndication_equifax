namespace LendFoundry.Syndication.Equifax.Proxy.CreditReportRequest
{
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.6.1055.0")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute()]
    public partial class Attribute2Type {
    
        private CodeType nameField;
    
        private CodeType valueField;
    
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute()]
        public CodeType Name {
            get {
                return this.nameField;
            }
            set {
                this.nameField = value;
            }
        }
    
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute()]
        public CodeType Value {
            get {
                return this.valueField;
            }
            set {
                this.valueField = value;
            }
        }
    }
}