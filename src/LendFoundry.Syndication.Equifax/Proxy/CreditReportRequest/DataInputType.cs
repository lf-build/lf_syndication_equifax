namespace LendFoundry.Syndication.Equifax.Proxy.CreditReportRequest
{
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.6.1055.0")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute()]
    public partial class DataInputType {
    
        private string memberCodeField;
    
        private string cDCBranchAcctNumField;
    
        private string transactionTypeField;
    
        private string taxIdField;
    
        private string cDCAccountTypeField;
    
        private string foreignCountryField;
    
        private string commentField;
    
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute()]
        public string MemberCode {
            get {
                return this.memberCodeField;
            }
            set {
                this.memberCodeField = value;
            }
        }
    
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute()]
        public string CDCBranchAcctNum {
            get {
                return this.cDCBranchAcctNumField;
            }
            set {
                this.cDCBranchAcctNumField = value;
            }
        }
    
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute()]
        public string TransactionType {
            get {
                return this.transactionTypeField;
            }
            set {
                this.transactionTypeField = value;
            }
        }
    
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute()]
        public string TaxId {
            get {
                return this.taxIdField;
            }
            set {
                this.taxIdField = value;
            }
        }
    
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute()]
        public string CDCAccountType {
            get {
                return this.cDCAccountTypeField;
            }
            set {
                this.cDCAccountTypeField = value;
            }
        }
    
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute()]
        public string ForeignCountry {
            get {
                return this.foreignCountryField;
            }
            set {
                this.foreignCountryField = value;
            }
        }
    
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute()]
        public string Comment {
            get {
                return this.commentField;
            }
            set {
                this.commentField = value;
            }
        }
    }
}