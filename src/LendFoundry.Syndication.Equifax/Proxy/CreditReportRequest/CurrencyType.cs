namespace LendFoundry.Syndication.Equifax.Proxy.CreditReportRequest
{
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.6.1055.0")]
    [System.SerializableAttribute()]
    [System.Xml.Serialization.XmlTypeAttribute()]
    public enum CurrencyType {
    
        /// <remarks/>
        USD,
    
        /// <remarks/>
        CAD,
    
        /// <remarks/>
        EUR,
    
        /// <remarks/>
        GBP,
    }
}