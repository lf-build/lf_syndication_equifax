namespace LendFoundry.Syndication.Equifax.Proxy.CreditReportRequest
{
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.6.1055.0")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute()]
    public partial class EmploymentType {
    
        private string subjectOccupationField;
    
        private string subjectEmployerField;
    
        private DateType dateEmployedField;
    
        private DateType verificationDateField;
    
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute()]
        public string SubjectOccupation {
            get {
                return this.subjectOccupationField;
            }
            set {
                this.subjectOccupationField = value;
            }
        }
    
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute()]
        public string SubjectEmployer {
            get {
                return this.subjectEmployerField;
            }
            set {
                this.subjectEmployerField = value;
            }
        }
    
        /// <remarks/>
        public DateType DateEmployed {
            get {
                return this.dateEmployedField;
            }
            set {
                this.dateEmployedField = value;
            }
        }
    
        /// <remarks/>
        public DateType VerificationDate {
            get {
                return this.verificationDateField;
            }
            set {
                this.verificationDateField = value;
            }
        }
    }
}