﻿using LendFoundry.Syndication.Equifax.Request;
using System;
using System.Collections.Generic;
using System.Linq;

namespace LendFoundry.Syndication.Equifax.Proxy.CreditReportRequest
{
    public partial class EfxReceive
    {
        public EfxReceive()
        {
        }

        public EfxReceive(IEquifaxCreditReportConfiguration creditReportConfiguration, IGetCreditReportRequest creditReportRequest)
        {
            if (creditReportConfiguration == null)
                throw new ArgumentNullException(nameof(creditReportConfiguration));

            if (creditReportRequest == null)
                throw new ArgumentNullException(nameof(creditReportRequest));

            if (string.IsNullOrWhiteSpace(creditReportConfiguration.SiteUrl))
                throw new ArgumentException("SiteUrl is required", nameof(creditReportConfiguration.SiteUrl));

            if (string.IsNullOrWhiteSpace(creditReportConfiguration.SiteId))
                throw new ArgumentException("SiteId is required", nameof(creditReportConfiguration.SiteId));

            if (string.IsNullOrWhiteSpace(creditReportConfiguration.ServiceName))
                throw new ArgumentException("ServiceName is required", nameof(creditReportConfiguration.ServiceName));

            if (string.IsNullOrWhiteSpace(creditReportConfiguration.SiteUserName))
                throw new ArgumentException("Username is required", nameof(creditReportConfiguration.SiteUserName));

            if (string.IsNullOrWhiteSpace(creditReportConfiguration.SiteUserPassword))
                throw new ArgumentException("User Password is required", nameof(creditReportConfiguration.SiteUserPassword));

            if (string.IsNullOrWhiteSpace(creditReportConfiguration.Version))
                throw new ArgumentException("Version is required",
                    nameof(creditReportConfiguration.Version));

            if (creditReportConfiguration.CustomerConfiguration == null)
                throw new ArgumentNullException(nameof(creditReportConfiguration.CustomerConfiguration));

            if (string.IsNullOrWhiteSpace(creditReportConfiguration.CustomerConfiguration.CustomerNumber))
                throw new ArgumentException("CustomerNumber is required", nameof(creditReportConfiguration.CustomerConfiguration.CustomerNumber));

            if (string.IsNullOrWhiteSpace(creditReportConfiguration.CustomerConfiguration.SecurityCode))
                throw new ArgumentException("SecurityCode is required", nameof(creditReportConfiguration.CustomerConfiguration.SecurityCode));

            if (string.IsNullOrWhiteSpace(creditReportConfiguration.CustomerConfiguration.CustomerCode))
                throw new ArgumentException("CustomerCode is required", nameof(creditReportConfiguration.CustomerConfiguration.CustomerCode));

            if (string.IsNullOrWhiteSpace(creditReportConfiguration.OptionalFeaturesConfiguration.OutputFormat))
                throw new ArgumentException("OutputFormat is required", nameof(creditReportConfiguration.OptionalFeaturesConfiguration.OutputFormat));

            if (string.IsNullOrWhiteSpace(creditReportConfiguration.OptionalFeaturesConfiguration.MultipleFileIndicator))
                throw new ArgumentException("MultipleFileIndicator is required", nameof(creditReportConfiguration.OptionalFeaturesConfiguration.MultipleFileIndicator));

            /* Removed Validation related to Sprint5 - TrelloCard-31
            if (string.IsNullOrWhiteSpace(creditReportRequest.FirstName))
                throw new ArgumentException("FirstName is required", nameof(creditReportRequest.FirstName));

            if (string.IsNullOrWhiteSpace(creditReportRequest.LastName))
                throw new ArgumentException("LastName is required", nameof(creditReportRequest.LastName));

            if (string.IsNullOrWhiteSpace(creditReportRequest.City))
                throw new ArgumentException("City is required", nameof(creditReportRequest.City));
                
            if (string.IsNullOrWhiteSpace(creditReportRequest.State))
                throw new ArgumentException("State is required", nameof(creditReportRequest.State));

            if (string.IsNullOrWhiteSpace(creditReportRequest.Ssn))
                throw new ArgumentException("Ssn is required", nameof(creditReportRequest.Ssn));
            */

            if (!string.IsNullOrWhiteSpace(creditReportConfiguration.Version))
                version = creditReportConfiguration.Version;

            if (!string.IsNullOrWhiteSpace(creditReportConfiguration.CustomerConfiguration.CustomerNumber))
                customerNumber = creditReportConfiguration.CustomerConfiguration.CustomerNumber;

            if (!string.IsNullOrWhiteSpace(creditReportConfiguration.CustomerConfiguration.SecurityCode))
                securityCode = creditReportConfiguration.CustomerConfiguration.SecurityCode;

            CustomerInfo = new CustomerInfoType(creditReportConfiguration, creditReportRequest);

            List<EfxRequestType> efxRequestData = new List<EfxRequestType>
            {
                new EfxRequestType(creditReportConfiguration, creditReportRequest)
            };

            if (efxRequestData.Count > 0)
                EfxRequest = efxRequestData.ToArray();

        }
    }

    public partial class CustomerInfoType
    {
        public CustomerInfoType(IEquifaxCreditReportConfiguration creditReportConfiguration,
            IGetCreditReportRequest creditReportRequest) : this()
        {
            if (creditReportConfiguration == null)
                throw new ArgumentNullException(nameof(creditReportConfiguration));

            if (creditReportRequest == null)
                throw new ArgumentNullException(nameof(creditReportRequest));

            if (creditReportConfiguration.CustomerConfiguration == null)
                throw new ArgumentNullException(nameof(creditReportConfiguration.CustomerConfiguration));

            if (!string.IsNullOrWhiteSpace(creditReportConfiguration.CustomerConfiguration.CustomerNumber))

                CustomerNumber = creditReportConfiguration.CustomerConfiguration.CustomerNumber;
            if (!string.IsNullOrWhiteSpace(creditReportConfiguration.CustomerConfiguration.SecurityCode))
                SecurityCode = creditReportConfiguration.CustomerConfiguration.SecurityCode;

            if (!string.IsNullOrWhiteSpace(creditReportConfiguration.CustomerConfiguration.CustomerCode))
                CustomerCode = creditReportConfiguration.CustomerConfiguration.CustomerCode;

            if (!string.IsNullOrWhiteSpace(creditReportConfiguration.OptionalFeaturesConfiguration.OutputFormat))
                OutputFormat = creditReportConfiguration.OptionalFeaturesConfiguration.OutputFormat;

            if (!string.IsNullOrWhiteSpace(creditReportConfiguration.OptionalFeaturesConfiguration.MultipleFileIndicator))
                MultipleFileIndicator =
                    (CustomerInfoTypeMultipleFileIndicator)
                        Enum.Parse(typeof(CustomerInfoTypeMultipleFileIndicator),
                            creditReportConfiguration.OptionalFeaturesConfiguration.MultipleFileIndicator);

            switch (creditReportConfiguration.CustomerConfiguration.PrintImageCdataInclude)
            {
                case YesNo.Y:
                    PrintImageCDATAInclude = YNFlagType.Y;
                    break;
                case YesNo.N:
                    PrintImageCDATAInclude = YNFlagType.N;
                    break;
                default:
                    throw new ArgumentOutOfRangeException();
            }
        }
    }

    public partial class EfxRequestType
    {
        public EfxRequestType()
        {
        }

        public EfxRequestType(IEquifaxCreditReportConfiguration creditReportConfiguration, IGetCreditReportRequest creditReportRequest)
        {
            if (creditReportConfiguration == null)
                throw new ArgumentNullException(nameof(creditReportConfiguration));

            if (creditReportRequest == null)
                throw new ArgumentNullException(nameof(creditReportRequest));

            //RequestInfo = new RequestInfoType(creditReportConfiguration, creditReportRequest);

            //var consumerSubjectTypeList = new List<ConsumerSubjectType>();
            //List<ConsumerSubjectType> consumerSubjectTypeData = consumerSubjectTypeList;

            // consumerSubjectTypeData.Add(new SubjectType(creditReportConfiguration, creditReportRequest));
            //if (consumerSubjectTypeData.Count > 0)
            Subject = new SubjectType(creditReportConfiguration, creditReportRequest);

            OptionalFeatures = new OptionalFeaturesType(creditReportConfiguration, creditReportRequest);

            if (creditReportConfiguration.EndUserConfiguration != null)
                EndUser = new EndUserType(creditReportConfiguration.EndUserConfiguration);

            var usAddressList = new List<AddressType>();
            var usAddressData = usAddressList;
            usAddressData.Add(new AddressType(creditReportConfiguration, creditReportRequest));
            if (usAddressData.Count > 0)
                Address = usAddressData.ToArray();

            if (creditReportConfiguration.ModelNumbers.Any())
            {
                ModelData = creditReportConfiguration.ModelNumbers.Select(modelNumber => new ModelDataType()
                {
                    ModelInfo = new ModelDataTypeModelInfo() { modelNumber = modelNumber }
                }).ToArray();
            }
        }
    }

    //public partial class RequestInfoType
    //{
    //    public RequestInfoType()
    //    {
    //    }

    //    public RequestInfoType(IEquifaxCreditReportConfiguration creditReportConfiguration, IGetCreditReportRequest creditReportRequest)
    //    {
    //        if (creditReportConfiguration == null)
    //            throw new ArgumentNullException(nameof(creditReportConfiguration));

    //        if (creditReportRequest == null)
    //            throw new ArgumentNullException(nameof(creditReportRequest));
    //    }
    //}

    public partial class SubjectType
    {
        public SubjectType()
        {
        }

        public SubjectType(IEquifaxCreditReportConfiguration creditReportConfiguration, IGetCreditReportRequest creditReportRequest)
        {
            if (creditReportConfiguration == null)
                throw new ArgumentNullException(nameof(creditReportConfiguration));

            if (creditReportRequest == null)
                throw new ArgumentNullException(nameof(creditReportRequest));

            SubjectName = new SubjectNameType(creditReportConfiguration, creditReportRequest);
            if (creditReportRequest.DateOfBirth.Date != DateTime.MinValue)
                Item = creditReportRequest.DateOfBirth;
            else if (creditReportRequest.Age > 0)
                Item = creditReportRequest.Age;

            if (!string.IsNullOrWhiteSpace(creditReportRequest.Ssn))
                SubjectSSN = creditReportRequest.Ssn;
        }
    }

    public partial class SubjectNameType
    {
        public SubjectNameType()
        {
        }

        public SubjectNameType(IEquifaxCreditReportConfiguration creditReportConfiguration, IGetCreditReportRequest creditReportRequest)
        {
            if (creditReportConfiguration == null)
                throw new ArgumentNullException(nameof(creditReportConfiguration));

            if (creditReportRequest == null)
                throw new ArgumentNullException(nameof(creditReportRequest));

            //TODO: Not exist
            //if (!string.IsNullOrWhiteSpace(creditReportRequest.NamePrefix))
            //    NamePrefix = (NamePrefix) Enum.Parse(typeof(NamePrefix), creditReportRequest.NamePrefix);

            if (!string.IsNullOrWhiteSpace(creditReportRequest.LastName))
                LastName = creditReportRequest.LastName;

            if (!string.IsNullOrWhiteSpace(creditReportRequest.FirstName))
                FirstName = creditReportRequest.FirstName;

            if (!string.IsNullOrWhiteSpace(creditReportRequest.MiddleName))
                MiddleName = creditReportRequest.MiddleName;

            if (!string.IsNullOrWhiteSpace(creditReportRequest.NameSuffix))
                NameSuffix = creditReportRequest.NameSuffix;
        }
    }

    public partial class DateType
    {
        public DateType()
        {
        }

        public DateType(string value)
        {
            format = DateFormat.MMDDCCYY;
            Value = value;
        }
    }

    public partial class OptionalFeaturesType
    {
        public OptionalFeaturesType(IEquifaxCreditReportConfiguration creditReportConfiguration, IGetCreditReportRequest creditReportRequest) : this()
        {
            if (creditReportConfiguration == null)
                throw new ArgumentNullException(nameof(creditReportConfiguration));

            if (creditReportConfiguration.OptionalFeaturesConfiguration == null)
                throw new ArgumentNullException(nameof(creditReportConfiguration.OptionalFeaturesConfiguration));

            if (creditReportRequest == null)
                throw new ArgumentNullException(nameof(creditReportRequest));

            if (creditReportConfiguration.OptionalFeaturesConfiguration.CreditAgency == CreditAgency.Fico)
                FICOFlag = YNFlagType.Y;

            if (creditReportConfiguration.OptionalFeaturesConfiguration.CreditAgency == CreditAgency.Beacon)
                BeaconFlag = YNFlagType.Y;

            if (creditReportConfiguration.OptionalFeaturesConfiguration.EDasFlag != YesNo.None)
                EDASFlag = (YNFlagType)Enum.Parse(typeof(YNFlagType), creditReportConfiguration.OptionalFeaturesConfiguration.EDasFlag.ToString());
            if (creditReportConfiguration.OptionalFeaturesConfiguration.OnLineDirectoryFlag != YesNo.None)
                OnLineDirectoryFlag = (YNFlagType)Enum.Parse(typeof(YNFlagType), creditReportConfiguration.OptionalFeaturesConfiguration.OnLineDirectoryFlag.ToString());
            if (creditReportConfiguration.OptionalFeaturesConfiguration.ConsumerReferralLocationFlag != YesNo.None)
                ConsumerReferralLocationFlag = (YNFlagType)Enum.Parse(typeof(YNFlagType), creditReportConfiguration.OptionalFeaturesConfiguration.ConsumerReferralLocationFlag.ToString());
            if (creditReportConfiguration.OptionalFeaturesConfiguration.AlertContactFlag != YesNo.None)
                AlertContactFlag = (YNFlagType)Enum.Parse(typeof(YNFlagType), creditReportConfiguration.OptionalFeaturesConfiguration.AlertContactFlag.ToString());
            if (creditReportConfiguration.OptionalFeaturesConfiguration.RbpBeaconOnlyFlag != YesNo.None)
                RBPBeaconOnlyFlag = (YNFlagType)Enum.Parse(typeof(YNFlagType), creditReportConfiguration.OptionalFeaturesConfiguration.RbpBeaconOnlyFlag.ToString());
            if (creditReportConfiguration.OptionalFeaturesConfiguration.RbpFicoOnlyFlag != YesNo.None)
                RBPFICOOnlyFlag = (YNFlagType)Enum.Parse(typeof(YNFlagType), creditReportConfiguration.OptionalFeaturesConfiguration.RbpFicoOnlyFlag.ToString());
            if (creditReportConfiguration.OptionalFeaturesConfiguration.UseFicoVerbiageFlag != YesNo.None)
                UseFICOVerbiageFlag = (YNFlagType)Enum.Parse(typeof(YNFlagType), creditReportConfiguration.OptionalFeaturesConfiguration.UseFicoVerbiageFlag.ToString());
            if (creditReportConfiguration.OptionalFeaturesConfiguration.RbpAllFlag != YesNo.None)
                RBPAllFlag = (YNFlagType)Enum.Parse(typeof(YNFlagType), creditReportConfiguration.OptionalFeaturesConfiguration.RbpAllFlag.ToString());
            if (creditReportConfiguration.OptionalFeaturesConfiguration.DefaultModelFlag != YesNo.None)
                DefaultModelFlag = (YNFlagType)Enum.Parse(typeof(YNFlagType), creditReportConfiguration.OptionalFeaturesConfiguration.DefaultModelFlag.ToString());
            if (creditReportConfiguration.OptionalFeaturesConfiguration.PaymentHistoryMetro2Flag != YesNo.None)
                paymentHistory24MonthFlagField = (YNFlagType)Enum.Parse(typeof(YNFlagType), creditReportConfiguration.OptionalFeaturesConfiguration.PaymentHistoryMetro2Flag.ToString());
            if (creditReportConfiguration.OptionalFeaturesConfiguration.MlaOnly != YesNo.None)
                mLAOnlyFlagField = (YNFlagType)Enum.Parse(typeof(YNFlagType), creditReportConfiguration.OptionalFeaturesConfiguration.MlaOnly.ToString());
        }
    }

    public partial class AddressType
    {
        public AddressType()
        {
        }

        public AddressType(IEquifaxCreditReportConfiguration creditReportConfiguration, IGetCreditReportRequest creditReportRequest)
        {
            if (creditReportConfiguration == null)
                throw new ArgumentNullException(nameof(creditReportConfiguration));
            if (creditReportRequest == null)
                throw new ArgumentNullException(nameof(creditReportRequest));

            StreetAddress = new ParsedStreetAddressType();
            if (!string.IsNullOrWhiteSpace(creditReportRequest.StreetNumber))
                StreetAddress.StreetNumber = creditReportRequest.StreetNumber;
            if (!string.IsNullOrWhiteSpace(creditReportRequest.StreetName))
                StreetAddress.StreetName = creditReportRequest.StreetName;
            if (!string.IsNullOrWhiteSpace(creditReportRequest.StreetType))
                StreetAddress.StreetType = creditReportRequest.StreetType;

            if (!string.IsNullOrWhiteSpace(creditReportRequest.City))
                City = new CityType(creditReportRequest.City);
            if (!string.IsNullOrWhiteSpace(creditReportRequest.State))
                State = creditReportRequest.State;

            if (!string.IsNullOrWhiteSpace(creditReportRequest.ZipCode))
                PostalCode = creditReportRequest.ZipCode;
        }
    }

    public partial class CityType
    {
        public CityType()
        {
        }

        public CityType(string valueData)
        {
            Value = valueData;
        }
    }
    public partial class EndUserType
    {
        public EndUserType()
        {

        }
        public EndUserType(IEquifaxEndUserConfiguration equifaxEndUserConfiguration)
        {
            EndUserName = equifaxEndUserConfiguration.UserName;
            PermissiblePurposeCode = equifaxEndUserConfiguration.PermissiblePurposeCode;
        }
    }
}