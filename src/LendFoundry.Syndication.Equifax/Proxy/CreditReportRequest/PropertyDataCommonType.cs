namespace LendFoundry.Syndication.Equifax.Proxy.CreditReportRequest
{
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.6.1055.0")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute()]
    public partial class PropertyDataCommonType {
    
        private string geoAreaTypeField;
    
        private string geoValueField;
    
        private PropertyDataSubCommonType[] pricingDataField;
    
        private short numberField;
    
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute()]
        public string GeoAreaType {
            get {
                return this.geoAreaTypeField;
            }
            set {
                this.geoAreaTypeField = value;
            }
        }
    
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute()]
        public string GeoValue {
            get {
                return this.geoValueField;
            }
            set {
                this.geoValueField = value;
            }
        }
    
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute("PricingData")]
        public PropertyDataSubCommonType[] PricingData {
            get {
                return this.pricingDataField;
            }
            set {
                this.pricingDataField = value;
            }
        }
    
        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public short number {
            get {
                return this.numberField;
            }
            set {
                this.numberField = value;
            }
        }
    }
}