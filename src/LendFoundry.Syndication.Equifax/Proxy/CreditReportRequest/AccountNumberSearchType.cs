namespace LendFoundry.Syndication.Equifax.Proxy.CreditReportRequest
{
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.6.1055.0")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute()]
    public partial class AccountNumberSearchType {
    
        private string accountNumberField;
    
        private AccountNumberSearchTypeCreditCardID creditCardIDField;
    
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute()]
        public string AccountNumber {
            get {
                return this.accountNumberField;
            }
            set {
                this.accountNumberField = value;
            }
        }
    
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute()]
        public AccountNumberSearchTypeCreditCardID CreditCardID {
            get {
                return this.creditCardIDField;
            }
            set {
                this.creditCardIDField = value;
            }
        }
    }
}