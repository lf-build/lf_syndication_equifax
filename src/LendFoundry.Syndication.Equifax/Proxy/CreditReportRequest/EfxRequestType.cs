namespace LendFoundry.Syndication.Equifax.Proxy.CreditReportRequest
{
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.6.1055.0")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute()]
    public partial class EfxRequestType {
    
        private SubjectType subjectField;
    
        private OptionalFeaturesType optionalFeaturesField;
    
        private AddressType[] addressField;
    
        private AddressType jointAddressField;
    
        private EmploymentType employmentField;
    
        private AccountNumberSearchType accountNumberSearchField;
    
        private EndUserType endUserField;
    
        private ProductCodeInputType productCodeField;
    
        private ProductInfoType productInfoField;
    
        private DataInputType dataInputField;
    
        private PropertyAddressType propertyAddressField;
    
        private FirstSearchType firstSearchField;
    
        private DataSourceSegmentType dataSourceSegmentField;
    
        private ModelDataType[] modelDataField;
    
        private short requestNumberField;
    
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute()]
        public SubjectType Subject {
            get {
                return this.subjectField;
            }
            set {
                this.subjectField = value;
            }
        }
    
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute()]
        public OptionalFeaturesType OptionalFeatures {
            get {
                return this.optionalFeaturesField;
            }
            set {
                this.optionalFeaturesField = value;
            }
        }
    
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute("Address")]
        public AddressType[] Address {
            get {
                return this.addressField;
            }
            set {
                this.addressField = value;
            }
        }
    
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute()]
        public AddressType JointAddress {
            get {
                return this.jointAddressField;
            }
            set {
                this.jointAddressField = value;
            }
        }
    
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute()]
        public EmploymentType Employment {
            get {
                return this.employmentField;
            }
            set {
                this.employmentField = value;
            }
        }
    
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute()]
        public AccountNumberSearchType AccountNumberSearch {
            get {
                return this.accountNumberSearchField;
            }
            set {
                this.accountNumberSearchField = value;
            }
        }
    
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute()]
        public EndUserType EndUser {
            get {
                return this.endUserField;
            }
            set {
                this.endUserField = value;
            }
        }
    
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute()]
        public ProductCodeInputType ProductCode {
            get {
                return this.productCodeField;
            }
            set {
                this.productCodeField = value;
            }
        }
    
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute()]
        public ProductInfoType ProductInfo {
            get {
                return this.productInfoField;
            }
            set {
                this.productInfoField = value;
            }
        }
    
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute()]
        public DataInputType DataInput {
            get {
                return this.dataInputField;
            }
            set {
                this.dataInputField = value;
            }
        }
    
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute()]
        public PropertyAddressType PropertyAddress {
            get {
                return this.propertyAddressField;
            }
            set {
                this.propertyAddressField = value;
            }
        }
    
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute()]
        public FirstSearchType FirstSearch {
            get {
                return this.firstSearchField;
            }
            set {
                this.firstSearchField = value;
            }
        }
    
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute()]
        public DataSourceSegmentType DataSourceSegment {
            get {
                return this.dataSourceSegmentField;
            }
            set {
                this.dataSourceSegmentField = value;
            }
        }
    
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute("ModelData")]
        public ModelDataType[] ModelData {
            get {
                return this.modelDataField;
            }
            set {
                this.modelDataField = value;
            }
        }
    
        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public short requestNumber {
            get {
                return this.requestNumberField;
            }
            set {
                this.requestNumberField = value;
            }
        }
    }
}