namespace LendFoundry.Syndication.Equifax.Proxy.CreditReportRequest
{
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.6.1055.0")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute()]
    public partial class SubjectType {
    
        private string customerReferenceNumberField;
    
        private ECOAInquiryType eCOAInquiryTypeField;
    
        private string vendorIdentificationCodeField;
    
        private string subjectSSNField;
    
        private string subjectPinField;
    
        private SubjectNameType subjectNameField;
    
        private object itemField;
    
        private string coApplicantSSNField;
    
        private object coApplicantPinField;
    
        private SubjectNameType coApplicantNameField;
    
        private object item1Field;
    
        /// <remarks/>
        public string CustomerReferenceNumber {
            get {
                return this.customerReferenceNumberField;
            }
            set {
                this.customerReferenceNumberField = value;
            }
        }
    
        /// <remarks/>
        public ECOAInquiryType ECOAInquiryType {
            get {
                return this.eCOAInquiryTypeField;
            }
            set {
                this.eCOAInquiryTypeField = value;
            }
        }
    
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute()]
        public string VendorIdentificationCode {
            get {
                return this.vendorIdentificationCodeField;
            }
            set {
                this.vendorIdentificationCodeField = value;
            }
        }
    
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute()]
        public string SubjectSSN {
            get {
                return this.subjectSSNField;
            }
            set {
                this.subjectSSNField = value;
            }
        }
    
        /// <remarks/>
        public string SubjectPin {
            get {
                return this.subjectPinField;
            }
            set {
                this.subjectPinField = value;
            }
        }
    
        /// <remarks/>
        public SubjectNameType SubjectName {
            get {
                return this.subjectNameField;
            }
            set {
                this.subjectNameField = value;
            }
        }
    
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute("SubjectAge", typeof(short) )]
        [System.Xml.Serialization.XmlElementAttribute("SubjectBirthDate", typeof(System.DateTime), DataType="date")]
        public object Item {
            get {
                return this.itemField;
            }
            set {
                this.itemField = value;
            }
        }
    
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute()]
        public string CoApplicantSSN {
            get {
                return this.coApplicantSSNField;
            }
            set {
                this.coApplicantSSNField = value;
            }
        }
    
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute()]
        public object CoApplicantPin {
            get {
                return this.coApplicantPinField;
            }
            set {
                this.coApplicantPinField = value;
            }
        }
    
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute()]
        public SubjectNameType CoApplicantName {
            get {
                return this.coApplicantNameField;
            }
            set {
                this.coApplicantNameField = value;
            }
        }
    
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute("CoApplicantAge", typeof(short))]
        [System.Xml.Serialization.XmlElementAttribute("CoApplicantBirthDate", typeof(System.DateTime),  DataType="date")]
        public object Item1 {
            get {
                return this.item1Field;
            }
            set {
                this.item1Field = value;
            }
        }
    }
}