namespace LendFoundry.Syndication.Equifax.Proxy.CreditReportRequest
{
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.6.1055.0")]
    [System.SerializableAttribute()]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType=true)]
    public enum AccountNumberSearchTypeCreditCardID {
    
        /// <remarks/>
        MC,
    
        /// <remarks/>
        VS,
    
        /// <remarks/>
        AE,
    
        /// <remarks/>
        DS,
    }
}