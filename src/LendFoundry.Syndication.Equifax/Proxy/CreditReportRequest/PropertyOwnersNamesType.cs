namespace LendFoundry.Syndication.Equifax.Proxy.CreditReportRequest
{
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.6.1055.0")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute()]
    public partial class PropertyOwnersNamesType {
    
        private CouplesNamesType propertyOwnersField;
    
        private short numberField;
    
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute()]
        public CouplesNamesType PropertyOwners {
            get {
                return this.propertyOwnersField;
            }
            set {
                this.propertyOwnersField = value;
            }
        }
    
        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public short number {
            get {
                return this.numberField;
            }
            set {
                this.numberField = value;
            }
        }
    }
}