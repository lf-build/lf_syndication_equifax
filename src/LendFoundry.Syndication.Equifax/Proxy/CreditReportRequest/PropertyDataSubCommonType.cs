namespace LendFoundry.Syndication.Equifax.Proxy.CreditReportRequest
{
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.6.1055.0")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute()]
    public partial class PropertyDataSubCommonType {
    
        private string quarterField;
    
        private PropertyDataYearType yearField;
    
        private MoneyType smoothedMedianPriceField;
    
        private string smoothedIndexField;
    
        private string smoothedPercentChangeField;
    
        private short numberField;
    
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute()]
        public string Quarter {
            get {
                return this.quarterField;
            }
            set {
                this.quarterField = value;
            }
        }
    
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute()]
        public PropertyDataYearType Year {
            get {
                return this.yearField;
            }
            set {
                this.yearField = value;
            }
        }
    
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute()]
        public MoneyType SmoothedMedianPrice {
            get {
                return this.smoothedMedianPriceField;
            }
            set {
                this.smoothedMedianPriceField = value;
            }
        }
    
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute()]
        public string SmoothedIndex {
            get {
                return this.smoothedIndexField;
            }
            set {
                this.smoothedIndexField = value;
            }
        }
    
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute()]
        public string SmoothedPercentChange {
            get {
                return this.smoothedPercentChangeField;
            }
            set {
                this.smoothedPercentChangeField = value;
            }
        }
    
        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public short number {
            get {
                return this.numberField;
            }
            set {
                this.numberField = value;
            }
        }
    }
}