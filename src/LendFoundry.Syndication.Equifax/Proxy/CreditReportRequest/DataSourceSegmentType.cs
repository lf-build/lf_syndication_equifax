namespace LendFoundry.Syndication.Equifax.Proxy.CreditReportRequest
{
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.6.1055.0")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute()]
    public partial class DataSourceSegmentType {
    
        private YNFlagType creditReportIndField;
    
        private string orchestrationCodeField;
    
        private string organizationCodeField;
    
        private DataTagAndDataFieldType[] dataTagsField;
    
        public DataSourceSegmentType() {
            this.creditReportIndField = YNFlagType.N;
        }
    
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute()]
        public YNFlagType CreditReportInd {
            get {
                return this.creditReportIndField;
            }
            set {
                this.creditReportIndField = value;
            }
        }
    
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute()]
        public string OrchestrationCode {
            get {
                return this.orchestrationCodeField;
            }
            set {
                this.orchestrationCodeField = value;
            }
        }
    
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute()]
        public string OrganizationCode {
            get {
                return this.organizationCodeField;
            }
            set {
                this.organizationCodeField = value;
            }
        }
    
        /// <remarks/>
        [System.Xml.Serialization.XmlArrayItemAttribute("DataTag", IsNullable=false)]
        public DataTagAndDataFieldType[] DataTags {
            get {
                return this.dataTagsField;
            }
            set {
                this.dataTagsField = value;
            }
        }
    }
}