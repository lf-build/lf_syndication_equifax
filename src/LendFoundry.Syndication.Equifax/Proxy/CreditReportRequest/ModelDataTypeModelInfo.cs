namespace LendFoundry.Syndication.Equifax.Proxy.CreditReportRequest
{
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.6.1055.0")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType=true)]
    public partial class ModelDataTypeModelInfo {
    
        private string modelNumberField;
    
        private string numberOfModelFieldsField;
    
        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string modelNumber {
            get {
                return this.modelNumberField;
            }
            set {
                this.modelNumberField = value;
            }
        }
    
        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string numberOfModelFields {
            get {
                return this.numberOfModelFieldsField;
            }
            set {
                this.numberOfModelFieldsField = value;
            }
        }
    }
}