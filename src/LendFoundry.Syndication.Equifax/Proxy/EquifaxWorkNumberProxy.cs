﻿using System;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using LendFoundry.Syndication.Equifax.Proxy.WorkNumberRequest;
using RestSharp;

namespace LendFoundry.Syndication.Equifax.Proxy
{
    public class EquifaxWorkNumberProxy : IEquifaxWorkNumberProxy
    {
        public EquifaxWorkNumberProxy(IEquifaxWorknumberConfiguration configuration)
        {
            if (configuration == null)
                throw new ArgumentNullException(nameof(configuration));

            if(string.IsNullOrWhiteSpace(configuration.ApplicationId))
                throw new ArgumentNullException(nameof(configuration.ApplicationId));

            if (string.IsNullOrWhiteSpace(configuration.ApplicationVersion))
                throw new ArgumentNullException(nameof(configuration.ApplicationVersion));

            if (string.IsNullOrWhiteSpace(configuration.Certificate))
                throw new ArgumentNullException(nameof(configuration.Certificate));

            if (string.IsNullOrWhiteSpace(configuration.CertificatePassword))
                throw new ArgumentNullException(nameof(configuration.CertificatePassword));

            if (string.IsNullOrWhiteSpace(configuration.DtClientId))
                throw new ArgumentNullException(nameof(configuration.DtClientId));

            if (string.IsNullOrWhiteSpace(configuration.Language))
                throw new ArgumentNullException(nameof(configuration.Language));

            if (string.IsNullOrWhiteSpace(configuration.TemplateName))
                throw new ArgumentNullException(nameof(configuration.TemplateName));

            if (string.IsNullOrWhiteSpace(configuration.TransactionPurpose))
                throw new ArgumentNullException(nameof(configuration.TransactionPurpose));

            if (string.IsNullOrWhiteSpace(configuration.UserId))
                throw new ArgumentNullException(nameof(configuration.UserId));

            if (string.IsNullOrWhiteSpace(configuration.UserPassword))
                throw new ArgumentNullException(nameof(configuration.UserPassword));

            if (string.IsNullOrWhiteSpace(configuration.WorkNumberUrl))
                throw new ArgumentNullException(nameof(configuration.WorkNumberUrl));

            Configuration = configuration;
        }

        private IEquifaxWorknumberConfiguration Configuration { get; }

        public OFX ExecuteRequest(OFX request)
        {
            if (request == null)
                throw new ArgumentNullException(nameof(request));

            var baseUri = new Uri(Configuration.WorkNumberUrl);
            RestClient client;
            var restRequest = new RestRequest(Method.POST) { RequestFormat = DataFormat.Xml };

            if (!string.IsNullOrWhiteSpace(Configuration.ProxyUrl))
            {
                var uri = new Uri($"{Configuration.ProxyUrl}{baseUri.PathAndQuery}");
                client = new RestClient(uri);
                restRequest.AddHeader("Host", baseUri.Host);
                restRequest.AddHeader("X-Proxy-Certificate", Configuration.Certificate);
                restRequest.AddHeader("X-Proxy-passphrase",
                    Convert.ToBase64String(Encoding.UTF8.GetBytes(Configuration.CertificatePassword)));
            }
            else
            {
                client = new RestClient(baseUri);

                if (!string.IsNullOrWhiteSpace(Configuration.Certificate))
                {
                    var certificateRowData = Convert.FromBase64String(Configuration.Certificate);
                    var certificates = new X509Certificate2();
                    certificates.Import(certificateRowData,
                        Configuration.CertificatePassword, X509KeyStorageFlags.DefaultKeySet);

                    client.ClientCertificates = new X509Certificate2Collection
                    {
                        new X509Certificate2(certificateRowData,
                            Configuration.CertificatePassword)
                    };
                }
            }

            var items = request.Items.ToList();
            var itemsElementNames = request.ItemsElementName.ToList();

            itemsElementNames.Insert(0, ItemsChoiceType.SIGNONMSGSRQV1);
            items.Insert(0,new SIGNONMSGSRQV1
            {
                SONRQ = new SONRQ
                {
                    APPID = new APPID { Text = new[] { Configuration.ApplicationId } },
                    DTCLIENT = new DTCLIENT
                    {
                        Text = new[] { Configuration.DtClientId }
                    },
                    Items = new object[]
                    {
                        new USERID {Text = new[] { Configuration.UserId}},
                        new USERPASS {Text = new[] { Configuration.UserPassword}}
                    },
                    LANGUAGE = new LANGUAGE { Text = new[] { Configuration.Language } },
                    APPVER = new APPVER
                    {
                        Text = new[] { Configuration.ApplicationVersion }
                    }
                }
            });

            request.Items = items.ToArray();
            request.ItemsElementName = itemsElementNames.ToArray();

            restRequest.AddParameter("text/xml", XmlSerialization.SerializeObject(request), ParameterType.RequestBody);

            return ExecuteRequest<OFX>(client,restRequest);
        }

        private static T ExecuteRequest<T>(IRestClient client, IRestRequest request) where T : class
        {
            var response = client.Execute(request);
            if (response == null)
                throw new ArgumentNullException(nameof(response));

            if (response.ErrorException != null)
                throw new EquifaxException("Service call failed", response.ErrorException);

            if (response.ResponseStatus != ResponseStatus.Completed)
                throw new EquifaxException(
                    $"Service call failed. Status {response.ResponseStatus}. Response: {response.Content ?? ""}");

            if (response.ResponseStatus == ResponseStatus.Completed)
            {
                if (response.StatusCode.ToString().ToLower() == "methodnotallowed")
                    throw new EquifaxException(
                        $"Service call failed. Status {response.ResponseStatus}. Response: {response.StatusDescription ?? ""}");
            }

            try
            {
                return XmlSerialization.DeserializeObject<T>(response.Content);
            }
            catch (Exception exception)
            {
                throw new Exception("Unable to deserialize:" + response.Content, exception);
            }
        }
    }
}