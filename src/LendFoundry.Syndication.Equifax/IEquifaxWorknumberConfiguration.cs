﻿namespace LendFoundry.Syndication.Equifax
{
    public interface IEquifaxWorknumberConfiguration
    {
        string WorkNumberUrl { get; set; }
        string DtClientId { get; set; }
        string UserId { get; set; }
        string UserPassword { get; set; }
        string Language { get; set; }
        string ApplicationId { get; set; }
        string ApplicationVersion { get; set; }
        string TemplateName { get; set; }
        string TransactionPurpose { get; set; }
        string Certificate { get; set; }
        string CertificatePassword { get; set; }
        string ProxyUrl { get; set; }
    }
}