namespace LendFoundry.Syndication.Equifax
{
    public interface IEquifaxOptionalFeaturesConfiguration
    {
        string OutputFormat { get; set; }
        string MultipleFileIndicator { get; set; }

        // ToDo : Confirm with manoj
        CreditAgency CreditAgency { get; set; }

        YesNo OnLineDirectoryFlag { get; set; }
        YesNo EDasFlag { get; set; }
        YesNo ConsumerReferralLocationFlag { get; set; }
        YesNo AlertContactFlag { get; set; }
        YesNo RbpBeaconOnlyFlag { get; set; }
        YesNo RbpFicoOnlyFlag { get; set; }
        YesNo UseFicoVerbiageFlag { get; set; }
        YesNo RbpAllFlag { get; set; }
        YesNo DefaultModelFlag { get; set; }
        YesNo PaymentHistoryMetro2Flag { get; set; }
        YesNo MlaOnly { get; set; }
    }
}