﻿namespace LendFoundry.Syndication.Equifax.Request
{
    public class GetWorknumberSelectWithPdfRequest : IGetWorknumberSelectWithPdfRequest
    {
        public string EmployeeSocialSecurityNumber { get; set; }
        public string TransactionPurpose { get; set; }
        public string TransactionUserId { get; set; }
    }
}