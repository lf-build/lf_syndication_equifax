﻿namespace LendFoundry.Syndication.Equifax.Request
{
    public interface IGetWorknumberSelectWithPdfRequest
    {
        string TransactionUserId { get; set; }
        string TransactionPurpose { get; set; }
        string EmployeeSocialSecurityNumber { get; set; }
    }
}