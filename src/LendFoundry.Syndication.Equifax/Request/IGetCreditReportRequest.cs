﻿using System;

namespace LendFoundry.Syndication.Equifax.Request
{
    public interface IGetCreditReportRequest
    {
        string NameSuffix { get; set; }
        //TODO: Not Exist in new schema
        //string NamePrefix { get; set; }
        string FirstName { get; set; }
        string MiddleName { get; set; }
        string LastName { get; set; }
        DateTime DateOfBirth { get; set; }
        int Age { get; set; }
        string Ssn { get; set; }
        string StreetNumber { get; set; }
        string StreetName { get; set; }
        string StreetType { get; set; }
        string City { get; set; }
        string State { get; set; }
        string ZipCode { get; set; }
    }
}