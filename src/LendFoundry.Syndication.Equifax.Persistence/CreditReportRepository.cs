﻿using LendFoundry.Foundation.Date;
using LendFoundry.Foundation.Logging;
using LendFoundry.Foundation.Persistence.Mongo;
using LendFoundry.Foundation.Services;
using LendFoundry.Security.Encryption;
using LendFoundry.Tenant.Client;
using MongoDB.Bson;
using MongoDB.Bson.Serialization;
using MongoDB.Bson.Serialization.Serializers;
using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;

namespace LendFoundry.Syndication.Equifax.Persistence
{
    public class CreditReportRepository : MongoRepository<ICreditReport, CreditReport>, ICreditReportRepository
    {
        public CreditReportRepository(IMongoConfiguration configuration, ITenantService tenantService, ITenantTime tenantTime, IEncryptionService encrypterService, ILogger logger) 
            : base(tenantService, configuration, "creditreport")
        {
            if (!BsonClassMap.IsClassMapRegistered(typeof(CreditReport)))
            {
                BsonClassMap.RegisterClassMap<CreditReport>(map =>
                {
                    map.AutoMap();
                    map.MapMember(m => m.ReportDate).SetSerializer(new DateTimeOffsetSerializer(BsonType.Document));
                    map.MapMember(m => m.ExpirationDate).SetSerializer(new DateTimeOffsetSerializer(BsonType.Document));
                    map.MapMember(m => m.Report).SetSerializer(new BsonEncryptor<object,object>(encrypterService));
                    map.MapMember(m => m.Key).SetSerializer(new BsonEncryptor<ICreditReportRequestKey, CreditReportRequestKey>(encrypterService));

                    var type = typeof(CreditReport);
                    map.SetDiscriminator($"{type.FullName}, {type.Assembly.GetName().Name}");
                    map.SetIsRootClass(true);
                });
            }

            CreateIndexIfNotExists("equifax-credit-report", Builders<ICreditReport>.IndexKeys.Ascending(i => i.TenantId).Ascending(i => i.Mode).Ascending(i => i.Key).Ascending(i => i.ExpirationDate));
            CreateIndexIfNotExists("entity", Builders<ICreditReport>.IndexKeys.Ascending(i => i.TenantId).Ascending(i => i.EntityType).Ascending(i => i.EntityId));
            CreateIndexIfNotExists("key", Builders<ICreditReport>.IndexKeys.Ascending(i => i.TenantId).Ascending(i => i.Key));
            CreateIndexIfNotExists("key-and-expiration", Builders<ICreditReport>.IndexKeys.Ascending(i => i.TenantId).Ascending(i => i.Key).Ascending(i => i.ExpirationDate));

            TenantTime = tenantTime;
            Logger = logger;
        }

        private ITenantTime TenantTime { get; }
        private ILogger Logger { get; }

        public ICreditReport Get(ICreditReportRequestKey key, string mode)
        {
            if (key==null)
                throw new ArgumentException($"#{nameof(key)} cannot be null");

            var now = TenantTime.Now;

            var creditReports = Query
                .Where(r => r.Mode == mode)
                .Where(r => r.HashedKey == key.ToKey())
                .Where(r => r.ExpirationDate >= now)
                .OrderByDescending(b => b.ReportDate)
                .ToList();

            foreach (var creditReport in creditReports)
            {
                if (creditReport.Key.isEquals(key))
                {
                    return creditReport;
                }
            }

            return null;
        }

        public ICreditReport GetReport(ICreditReportRequestKey key, string reportId)
        {
            if (key == null)
                throw new ArgumentException($"#{nameof(key)} cannot be null");

            if (string.IsNullOrWhiteSpace(reportId))
                throw new ArgumentException($"#{nameof(reportId)} cannot be null");

            var creditReports = Query
                .Where(b => b.HashedKey == key.ToKey() && b.Id == reportId)
                .ToList();

            foreach (var creditReport in creditReports)
            {
                if (creditReport.Key.isEquals(key))
                {
                    return creditReport;
                }
            }

            return null;
        }

        public List<ICreditReport> GetReports(ICreditReportRequestKey key, int? skip = default(int?), int? take = default(int?))
        {
            if (key == null)
                throw new ArgumentException($"#{nameof(key)} cannot be null");

            var now = TenantTime.Now;
            var keyValue = key.ToKey();


            Expression<Func<ICreditReport, bool>> query = q => q.HashedKey == keyValue && q.ExpirationDate >= TenantTime.Now;

            var totalRecords = Count(query);
            var allCreditReports = All(query, skip, take).Result.ToList();
            var creditReports = new List<ICreditReport>();
            foreach (var creditReport in allCreditReports)
            {
                if (creditReport.Key.isEquals(key))
                {
                    creditReports.Add(creditReport);
                }
            }

            if ((totalRecords > creditReports.Count) && (skip.HasValue && skip.Value >= totalRecords))
                throw new NotFoundException($"Credit Report: has only {totalRecords} credit reports");

            if (!creditReports.Any())
                throw new NotFoundException($"Credit Report: has no credit reports");

            Logger.Info($"CreditReport list searched the requested. #{creditReports.Count} results");

            return creditReports;

        }
    }
}