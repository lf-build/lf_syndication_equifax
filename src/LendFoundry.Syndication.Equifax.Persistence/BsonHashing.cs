﻿using MongoDB.Bson.Serialization;
using System;

namespace LendFoundry.Syndication.Equifax.Persistence
{
    public class BsonHashing : IBsonSerializer
    {
        private IHashingService HashingService { get; set; }

        public BsonHashing(IHashingService hashingService)
        {
            HashingService = hashingService;
        }

        public string Deserialize(BsonDeserializationContext context, BsonDeserializationArgs args)
        {
            if (context.Reader.CurrentBsonType == MongoDB.Bson.BsonType.Null)
            {
                context.Reader.ReadNull();
                return null;
            }
            return context.Reader.ReadString();
        }

        public void Serialize(BsonSerializationContext context, BsonSerializationArgs args, string value)
        {
            if (value == null)
                context.Writer.WriteNull();
            else
            {
                var valueAsJson = HashingService.ComputeHash(value);
                context.Writer.WriteString(valueAsJson);
            }
        }

        public Type ValueType => typeof(string);

        public void Serialize(BsonSerializationContext context, BsonSerializationArgs args, object value)
        {
            if (value == null)
                context.Writer.WriteNull();
            else
            {
                if (value.GetType() != typeof(string))
                    throw new ArgumentException($"{nameof(value)} must be a string");
                var valueAsHash = HashingService.ComputeHash(value as string);
                context.Writer.WriteString(valueAsHash);
            }

        }

        object IBsonSerializer.Deserialize(BsonDeserializationContext context, BsonDeserializationArgs args)
        {
            if (context.Reader.CurrentBsonType == MongoDB.Bson.BsonType.Null)
            {
                context.Reader.ReadNull();
                return null;
            }
            return context.Reader.ReadString();
        }
    }

}
