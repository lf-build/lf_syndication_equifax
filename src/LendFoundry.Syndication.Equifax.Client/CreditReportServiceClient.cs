﻿using System;
using System.Collections.Generic;
using LendFoundry.Foundation.Client;
using RestSharp;

namespace LendFoundry.Syndication.Equifax.Client
{
    internal class CreditReportServiceClient : IEquifaxCreditReportService
    {
        public CreditReportServiceClient(IServiceClient client)
        {
            Client = client;
        }

        private IServiceClient Client { get; }


        public ICreditReport CreateReport(string entityType, string entityId, ICreditReportRequestKey creditReportRequest)
        {
            var request = new RestRequest("/{entityType}/{entityId}/creditreport", Method.POST);
            request.AddUrlSegment("entityType", entityType);
            request.AddUrlSegment("entityId", entityId);
            request.AddJsonBody(creditReportRequest);

            return Client.Execute<CreditReport>(request);
        }


        public ICreditReport CreateReport(string settingName, ICreditReportRequest creditReportRequest)
        {
            var request = new RestRequest("/{settingName}", Method.POST);
            request.AddUrlSegment(nameof(settingName), settingName);
            request.AddJsonBody(creditReportRequest);
            return Client.Execute<CreditReport>(request);
        }

        public ICreditReport GetReport(string entityType, string entityId, ICreditReportRequestKey creditReportRequest, string reportId)
        {
            var request = new RestRequest("/{entityType}/{entityId}/{reportId}", Method.POST);
            request.AddUrlSegment(nameof(entityType), entityType);
            request.AddUrlSegment(nameof(entityId), entityId);
            request.AddJsonBody(creditReportRequest);
            return Client.Execute<CreditReport>(request);
        }

        public IEnumerable<ICreditReport> GetReports(string entityType, string entityId, ICreditReportRequestKey creditReportRequest, int? skip = default(int?), int? take = default(int?))
        {
            var request = new RestRequest("/{entityType}/{entityId}/{skip?}/{take?}", Method.POST);
            request.AddUrlSegment(nameof(entityType), entityType);
            request.AddUrlSegment(nameof(entityId), entityId);
            if (skip != null) request.AddUrlSegment(nameof(skip), skip.Value.ToString());
            if (take != null) request.AddUrlSegment(nameof(take), take.Value.ToString());
            request.AddJsonBody(creditReportRequest);
            return Client.Execute<List<CreditReport>>(request);
        }
    }
}