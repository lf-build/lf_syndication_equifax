﻿using LendFoundry.Foundation.Client;
using LendFoundry.Security.Tokens;
using System;
using Microsoft.Extensions.DependencyInjection;

namespace LendFoundry.Syndication.Equifax.Client
{
    public static class CreditReportClientExtensions
    {
        [Obsolete("Need to use the overloaded with Uri")]
        public static IServiceCollection AddCreditReportService(this IServiceCollection services, string endpoint, int port)
        {
            services.AddTransient<ICreditReportClientFactory>(p => new CreditReportClientFactory(p, endpoint, port));
            services.AddTransient(p => p.GetService<ICreditReportClientFactory>().Create(p.GetService<ITokenReader>()));

            return services;
        }
        public static IServiceCollection AddCreditReportService(this IServiceCollection services, Uri uri)
        {
            services.AddSingleton<ICreditReportClientFactory>(p => new CreditReportClientFactory(p, uri));
            services.AddSingleton(p => p.GetService<ICreditReportClientFactory>().Create(p.GetService<ITokenReader>()));
            return services;
        }

        public static IServiceCollection AddCreditReportService(this IServiceCollection services)
        {
            services.AddSingleton<ICreditReportClientFactory>(p => new CreditReportClientFactory(p));
            services.AddSingleton(p => p.GetService<ICreditReportClientFactory>().Create(p.GetService<ITokenReader>()));
            return services;
        }
    }
}
