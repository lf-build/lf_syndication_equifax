﻿using LendFoundry.Foundation.Logging;
using LendFoundry.Foundation.Client;
using LendFoundry.Security.Tokens;
using System;
using Microsoft.Extensions.DependencyInjection;

namespace LendFoundry.Syndication.Equifax.Client
{
    internal class CreditReportClientFactory : ICreditReportClientFactory
    {
        [Obsolete("Need to use the overloaded with Uri")]
        public CreditReportClientFactory(IServiceProvider provider, string endpoint, int port)
        {
            Provider = provider;
            Endpoint = endpoint;
            Port = port;
            Uri = new UriBuilder("http", endpoint, port).Uri;
        }
        public CreditReportClientFactory(IServiceProvider provider, Uri uri = null)
        {
            Provider = provider;
            Uri = uri;
        }

        private IServiceProvider Provider { get; }
        private Uri Uri { get; }

        private string Endpoint { get; }

        private int Port { get; }

        public IEquifaxCreditReportService Create(ITokenReader reader)
        {
            var uri = Uri;
            if (uri == null)
            {
                var logger = Provider.GetService<ILoggerFactory>().Create(NullLogContext.Instance);
                uri = Provider.GetRequiredService<IDependencyServiceUriResolverFactory>().Create(reader, logger).Get("equifax");
            }
            var client = Provider.GetServiceClient(reader, uri);
            return new CreditReportServiceClient(client);
        }
    }
}
