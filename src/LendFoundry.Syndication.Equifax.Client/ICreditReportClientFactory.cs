﻿using LendFoundry.Security.Tokens;

namespace LendFoundry.Syndication.Equifax.Client
{
    public interface ICreditReportClientFactory
    {
        IEquifaxCreditReportService Create(ITokenReader reader);
    }
}